<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


require_once $GLOBALS['babInstallPath'] . 'utilit/controller.class.php';



class crm_UnknownActionException extends Exception
{
    public function __construct($action, $code = 0)
    {
        $message = 'Unknown method "' . $action->getController() . '::' . $action->getMethod() . '"';
        parent::__construct($message, $code);
    }
}

class crm_MissingActionParameterException extends Exception
{
    public function __construct($action, $parameterName, $code = 0)
    {
        $message = 'Mandatory parameter "' . $parameterName . '" missing in ' . $action->getController() . '::' . $action->getMethod();
        parent::__construct($message, $code);
    }
}

/**
 * Interface for controller used in the online shop back-office
 * the interface will be used to apply a different skin for back-office pages
 */
interface crm_ShopAdminCtrl
{

}




class crm_Controller extends bab_Controller implements crm_Object_Interface
{
    /**
     * @var Func_Crm
     */
    protected $crm = null;



    /**
     * @var string[]
     */
    protected $reloadSelectors = array();



    /**
     * @param string $reloadSelector
     */
    public function addReloadSelector($reloadSelector)
    {
        $this->reloadSelectors[$reloadSelector] = $reloadSelector;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getReloadSelectors()
    {
        return $this->reloadSelectors;
    }


    /**
     * @var array
     */
    protected $acceptedClipboardObjects = array();


    /**
     * Can be used in a controller to check if the current request was made by ajax.
     * @var bool
     */
    protected $isAjaxRequest = null;


    /**
     * Name of method to use to create action
     * @var string
     */
    public $createActionMethod = 'createActionForTg';



    /**
     * @param Func_Crm $crm
     */
    public function __construct(Func_Crm $crm = null)
    {
        $this->setCrm($crm);
    }


    /**
     * {@inheritDoc}
     * @see crm_Object::setCrm()
     */
    public function setCrm(Func_Crm $crm = null)
    {
        $this->crm = $crm;
        return $this;
    }

    /**
     * {@inheritDoc}
     * @see crm_Object::Crm()
     */
    public function Crm()
    {
        return $this->crm;
    }


    /**
     * @return string
     */
    protected function getControllerTg()
    {
        return $this->Crm()->controllerTg;
    }


    /**
     * Can be used in a controller to check if the current request was made by ajax.
     * @return bool
     */
    public function isAjaxRequest()
    {
        if (!isset($this->isAjaxRequest)) {
            $this->isAjaxRequest = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
        }
        return $this->isAjaxRequest;
    }


    /**
     * Ensures that the user is logged in.
     *
     * Tries to use the appropriate authentication type depending on the situation.
     */
    public function requireCredential($message = null)
    {
        if ($this->isAjaxRequest()) {
            $authType = 'Basic';
        } else {
            $authType = '';
        }

        if (!isset($message)) {
            $message = $this->Crm()->translate('You must be logged in to access this page.');
        }

        bab_requireCredential($message, $authType);
    }


    /**
     *
     * @param string 	    $objectClass
     * @param Widget_Action $objectAction
     * @param string		$actionLabel
     */
    protected function addAcceptedClipboardObject($objectClass, $objectAction, $actionLabel, $actionIcon)
    {
        if (!isset($this->acceptedClipboardObjects[$objectClass])) {
            $this->acceptedClipboardObjects[$objectClass] = array();
        }
        $this->acceptedClipboardObjects[$objectClass][] = array(
            'action' => $objectAction->url(),
            'label' => $actionLabel,
            'icon' => $actionIcon
        );
    }

    /**
     * @return array
     */
    protected function getAcceptedClipboardObjects()
    {
        return $this->acceptedClipboardObjects;
    }





    /**
     * Returns an instance of the specific controller class.
     * If $proxy is true, a proxy to this controller is returned instead.
     *
     * @deprecated use $Crm->ControllerProxy() instead
     *
     * @param string	$classname
     * @param bool		$proxy
     * @return crm_Controller
     */
    public static function getInstance($classname, $proxy = true)
    {


        // If the crm object was not specified (through the setCrm() method)
        // we try to select one according to the classname prefix.
        list($prefix) = explode('_', __CLASS__);
        $functionalityName = ucwords($prefix);
        $Crm = @bab_functionality::get('Crm/' . $functionalityName);

        if (!$Crm)
        {
            throw new crm_Exception('Faild to autodetect functionality Crm/' . $functionalityName.', the getInstance method is deprecated, use $Crm->ControllerProxy() instead');
        }

        $controller = $Crm->ControllerProxy($classname, $proxy);

        return $controller;
    }


    /**
     * Dynamically creates a proxy class for this controller with all public, non-final and non-static functions
     * overriden so that they return an action (Widget_Action) corresponding to each of them.
     *
     * @param string $classname
     * @return crm_Controller
     */
    static function getProxyInstance(Func_Crm $Crm, $classname)
    {
        $class = new ReflectionClass($classname);
        $proxyClassname = $class->name . self::PROXY_CLASS_SUFFIX;
        if (!class_exists($proxyClassname)) {
            $classStr = 'class ' . $proxyClassname . ' extends ' . $class->name . ' {' . "\n";
            $methods = $class->getMethods();

            $classStr .= '	public function __construct(Func_Crm $Crm) {
                $this->setCrm($Crm);
            }' . "\n";

            foreach ($methods as $method) {
                if ($method->name === '__construct' || !$method->isPublic() || $method->isStatic() || $method->isFinal() || $method->name === 'setCrm' || $method->name === 'Crm') {
                    continue;
                }


                $classStr .= '	public function ' . $method->name . '(';
                $parameters = $method->getParameters();
                $parametersStr = array();
                foreach ($parameters as $parameter) {

                    if ($parameter->isDefaultValueAvailable()) {
                        $parametersStr[] = '$' . $parameter->name . ' = ' . var_export($parameter->getDefaultValue(), true);
                    } else {
                        $parametersStr[] = '$' . $parameter->name;
                    }
                }
                $classStr .= implode(', ', $parametersStr);
                $classStr .= ') {' . "\n";
                $classStr .= '		$args = func_get_args();' . "\n";
                $classStr .= '		return $this->getMethodAction(__FUNCTION__, $args);' . "\n";
                $classStr .= '	}' . "\n";
            }
            $classStr .= '}' . "\n";

            // We define the proxy class
            eval($classStr);
        }

        $proxy = new $proxyClassname($Crm);

        //$proxy = bab_getInstance($proxyClassname);
        return $proxy;
    }


    /**
     * @return self
     */
    public function proxy()
    {
        return self::getProxyInstance($this->Crm(), get_class($this));
    }

    /**
     * Get proxy class with action from the new addon controller
     * @return self
     */
    protected function quickProxy()
    {
        $proxy = $this->proxy();
        $proxy->createActionMethod = 'createActionForAddon';
        return $proxy;
    }




    /**
     * Method to override default skin
     *
     */
    protected function applySkin()
    {
        if ($this->Crm()->onlineShop)
        {
            if ($this instanceof crm_ShopAdminCtrl)
            {
                // back-office of online shop

                $this->Crm()->applyShopAdminTheme();
            } else {

                // front-office of online shop

                global $babBody;
                if (isset($babBody))
                {
                    $addon = bab_getAddonInfosInstance('LibCrm');
                    $babBody->addStyleSheet($addon->getStylePath().'shop.css');
                }
            }
        }
    }



    /**
     * Tries to execute the method corresponding to $action
     * on the current object.
     *
     * Called by crm_Controller::execute()
     *
     * @param Widget_Action 	$action
     * @param string			$previous 		UID of previous page
     * @return mixed
     */
    final protected function execAction(Widget_Action $action, $previous = null)
    {
        // add page to breadCrumb

        if (!$this->isAjaxRequest()) {
            // We do not store the current request in the breadcrumb if it is an ajax request.
            crm_BreadCrumbs::addPage($action, $previous);
        }
        crm_BreadCrumbs::setCurrentController($this);

        $this->applySkin();

        $methodStr = $action->getMethod();

        if (strstr($methodStr, '.') === false) {
            list(, $methodName) = explode(':', $methodStr);
        } else {
            list(, $methodName) = explode('.', $methodStr);
        }
        if (!method_exists($this, $methodName)) {
            header('HTTP/1.0 400 Bad Request');
            throw new crm_UnknownActionException($action);
        }
        $method = new ReflectionMethod($this, $methodName);

        // Check restrictions embedded in doc comments
        $docComment = $method->getDocComment();
        if (strpos($docComment, '@requireSaveMethod') !== false) {
            $this->requireSaveMethod();
        }
        if (strpos($docComment, '@requireDeleteMethod') !== false) {
            $this->requireDeleteMethod();
        }


        $parameters = $method->getParameters();
        $args = array();
        foreach ($parameters as $parameter) {
            $parameterName = $parameter->getName();
            if ($action->parameterExists($parameter->getName())) {
                $args[$parameterName] = $action->getParameter($parameterName);
            } elseif ($parameter->isDefaultValueAvailable()) {
                $args[$parameterName] = $parameter->getDefaultValue();
            } else {
                throw new crm_MissingActionParameterException($action, $parameterName);
            }
        }

        return $method->invokeArgs($this, $args);
    }





    /**
     * Returns the action object corresponding to the current object method $methodName
     * with the parameters $args.
     *
     * @param string $methodName
     * @param array $args
     * @return Widget_Action	Or null on error.
     */
    protected function getMethodAction($methodName, $args)
    {
        $classname = substr(get_class($this), 0, -strlen(self::PROXY_CLASS_SUFFIX));
        if (!method_exists($classname, $methodName)) {
            throw new bab_InvalidActionException($classname . '::' . $methodName);
        }
        $cls = new ReflectionClass($classname);
        $filename = $cls->getFileName();

        $method = new ReflectionMethod($classname, $methodName);

        $objectName = basename($filename, '.ctrl.php');
        $parameters = $method->getParameters();
        $actionParams = array();
        $argNumber = 0;
        foreach ($parameters as $parameter) {
            $parameterName = $parameter->getName();
            if (isset($args[$argNumber])) {
                $actionParams[$parameterName] = $args[$argNumber];
            } elseif ($parameter->isDefaultValueAvailable()) {
                $actionParams[$parameterName] = $parameter->getDefaultValue();
            } else {
                $actionParams[$parameterName] = null;
            }
            $argNumber++;
        }

        /* @var $action Widget_Action */
        $action = $this->{$this->createActionMethod}($objectName. '.' . $methodName, $actionParams);

        $docComment = $method->getDocComment();

        if (strpos($docComment, '@ajax') !== false) {
            $action->setAjax(true);
        }
        if (strpos($docComment, '@requireSaveMethod') !== false && method_exists($action, 'setRequireSaveMethod')) {
            $action->setRequireSaveMethod(true);
        }
        if (strpos($docComment, '@requireDeleteMethod') !== false && method_exists($action, 'setRequireDeleteMethod')) {
            $action->setRequireDeleteMethod(true);
        }

        return $action;
    }


    /**
     * @return Widget_Action
     */
    protected function createActionForTg($idx, $actionParams)
    {
        $action = new Widget_Action();

        $action->setMethod($this->Crm()->controllerTg, $idx, $actionParams);

        return $action;
    }


    /**
     * @return Widget_Action
     */
    protected function createActionForAddon($idx, $actionParams)
    {
        $Crm = $this->Crm();
        $action = new Widget_Action();

        $action->setParameters($actionParams);


        list(,,$file) = explode('/', $Crm->controllerTg);

        $action->setParameter('addon', $Crm->getAddonName().'.'.$file);
        $action->setParameter('idx', $idx);

        return $action;
    }



    /**
     * Tries to dispatch the action to the correct sub-controller.
     *
     * @param Widget_Action 	$action
     * @return mixed
     */
    final public function execute(Widget_Action $action)
    {
        $method = $action->getMethod();

        if (!isset($method) || '' === $method) {
            return false;
        }

        if (strstr($method, '.') === false) {
            list($objectName, ) = explode(':', $method);
        } else {
            list($objectName, ) = explode('.', $method);
        }
        if (!method_exists($this, $objectName)) {
            header('HTTP/1.0 400 Bad Request');
            throw new crm_UnknownActionException($action);
        }

        $objectController = $this->{$objectName}(false);
        if ( ! ($objectController instanceof crm_Controller)) {
            return false;
        }

        $previous 		= null;
        $successAction 	= null;
        $failedAction 	= null;

        if (isset($_REQUEST['_ctrl_previous']))
        {
            $previous = $_REQUEST['_ctrl_previous'];
        }

        if (isset($_REQUEST['_ctrl_success'][$method]))
        {
            $successAction = Widget_Action::fromUrl($_REQUEST['_ctrl_success'][$method]);
        }

        if (isset($_REQUEST['_ctrl_failed'][$method]))
        {
            $failedAction = Widget_Action::fromUrl($_REQUEST['_ctrl_failed'][$method]);
        }

        $returnedValue = null;
        try {
            $returnedValue = $objectController->execAction($action, $previous);
        } catch (crm_AccessException $e) {

            $this->Crm()->ShopLog($e->getMessage());

            if (!bab_isUserLogged() && $e->requireCredential)
            {
                bab_requireCredential($e->getMessage());
            } else {

                if ($this->isAjaxRequest()) {

                    header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden', true, 403);

                    die(
                        crm_json_encode(
                            array(
                                'exception' => 'crm_AccessException',
                                'message' => bab_convertStringFromDatabase($e->getMessage(), 'UTF-8'),
                                'errorMessage' => bab_convertStringFromDatabase($e->getMessage(), 'UTF-8'),
                            )
                        )
                    );
                }
                crm_addPageInfo($e->getMessage());

                $returnedValue = $this->Crm()->Ui()->Page();
                // $returnedValue->addError($e->getMessage());
            }

        } catch (crm_SaveException $e) {

            $this->Crm()->ShopLog($e->getMessage());

            if ($this->isAjaxRequest()) {

                header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden', true, 403);
                header('Cache-Control: no-cache, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Content-type: application/json');

                die(
                    crm_json_encode(

                        array(
                            'exception' => 'crm_SaveException',
                            'message' => bab_convertStringFromDatabase($e->getMessage(), 'UTF-8'),
                            'errorMessage' => bab_convertStringFromDatabase($e->getMessage(), 'UTF-8'),
                        )
                    )
                );
            }

            // display the edit form

            if (isset($failedAction))
            {
                $lastPosition = $failedAction;
                $reason = 'because the failedAction is not valid';
            } else {
                $lastPosition = crm_BreadCrumbs::getPosition($e->breadcrumbs_position);
                $reason = sprintf('because the last position could not be found in breadCrumb at position %s', $e->breadcrumbs_position);
            }

            if (null === $lastPosition)
            {
                throw new ErrorException(sprintf('An error produced a crm_SaveException but there is no valid action to execute %s, the error message witch cannot be displayed because of the missing action is "%s"', $reason, $e->getMessage()));
            }

            if ($e->redirect)
            {
                crm_redirect($lastPosition, $e->getMessage());
            } else {
                crm_addPageInfo($e->getMessage());
                if (0 == count($lastPosition->getParameters()))
                {
                    throw new ErrorException('Error, incorrect action');
                }
                $returnedValue = $objectController->execAction($lastPosition, $previous);
            }

        } catch (crm_DeletedRecordException $e) {
            $this->deletedItemPage($action, $e);

        } catch (crm_NotFoundException $e) {
            $this->notFoundPage($action, $e);

        } catch (ORM_Exception $e) {
            $this->errorPage($e);
        }

        $W = bab_Widgets();

        $this->acceptedClipboardObjects = $objectController->getAcceptedClipboardObjects();

        if ($returnedValue instanceof Widget_Displayable_Interface) {

            if ($returnedValue instanceof Widget_BabPage && !$this->isAjaxRequest()) {

                if ($this->Crm()->hasClipboardManagement()) {
                    $returnedValue->addItem(
                        $W->Link(
                            '',
                            $this->Crm()->Controller()->Clipboard()->displayContent($this->getAcceptedClipboardObjects())
                        )->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                        ->addClass('widget-dockable')
                    );
                }

                // If the action returned a page, we display it.
                $returnedValue->displayHtml();

            } else {

                $htmlCanvas = $W->HtmlCanvas();
                if (self::$acceptJson) {
                    $itemId = $returnedValue->getId();
                    $returnArray = array($itemId => $returnedValue->display($htmlCanvas));
                    header('Cache-Control: no-cache, must-revalidate');
                    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                    header('Content-type: application/json');
                    die(crm_json_encode($returnArray));
                } else {
                    header('Cache-Control: no-cache, must-revalidate');
                    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                    header('Content-type: text/html');
                    if ($returnedValue instanceof Widget_Page && method_exists($returnedValue, 'getPageTitle')) {
                        $pageTitle = $returnedValue->getPageTitle();
                        if (method_exists($htmlCanvas, 'sendPageTitle')) {
                            $htmlCanvas->sendPageTitle($pageTitle);
                        } else {
                            header('X-Cto-PageTitle: ' . $pageTitle);
                        }
                    }
                    $html = $returnedValue->display($htmlCanvas);
                    die($html);
                }
            }

        } else if (is_array($returnedValue)) {

            $htmlCanvas = $W->HtmlCanvas();
            $returnedArray = array();
            foreach ($returnedValue as $key => &$item) {
                if ($item instanceof Widget_Displayable_Interface) {
                    $returnedArray[$item->getId()] = $item->display($htmlCanvas);
                } else {
                    $returnedArray[$key] = $item;
                }

            }
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');
            die(bab_convertStringFromDatabase(crm_json_encode($returnedArray), bab_charset::UTF_8));

        } else if (true === $returnedValue || is_string($returnedValue)) {


            $message = (true === $returnedValue) ? null : $returnedValue;

            if (isset($successAction)) {

                // a save action return true, goto defaut location defined in the button
                crm_redirect($successAction, $message);

            } else {

                if ($this->isAjaxRequest()) {
                    $body = bab_getBody();
                    $json = array();
                    $json['messages'] = array();
                    foreach ($body->messages as $message) {
                        $json['messages'][] = array(
                            'level' => 'info',
                            'content' => $message
                        );
                    }
                    foreach ($body->errors as $message) {
                        $json['messages'][] = array(
                            'level' => 'danger',
                            'content' => $message
                        );
                    }
                    if ($objectController->getReloadSelectors()) {
                        $json['reloadSelector'] = implode(',', $objectController->getReloadSelectors());
                    }
                    echo bab_json_encode($json);
                    die;
                }

                if (crm_BreadCrumbs::last() === $action->url()) {
                    throw new ErrorException('Controller failed to redirect to the same page as current page : '.crm_BreadCrumbs::last());
                }

                crm_redirect(crm_BreadCrumbs::last(), $message);
            }
        }

        return $returnedValue;
    }

    /**
     * Get back button
     * @return Widget_Link
     */
    protected function getBackButton()
    {
        $W = bab_Widgets();
        $lastAction = crm_BreadCrumbs::lastGet();

        if (!isset($lastAction)) {
            return null;
        }

        $label  = crm_translate('Previous page');
        if ($title = $lastAction->getTitle()) {
            $label = sprintf(crm_translate('Get back to page: %s'), bab_abbr($title, BAB_ABBR_FULL_WORDS, 20));
        }

        return $W->Link($label, $lastAction)
            ->addClass('icon')
            ->addClass(Func_Icons::ACTIONS_DIALOG_OK);
    }



    private function deletedItemPage(Widget_Action $action, crm_DeletedRecordException $e)
    {
        if ($this->isAjaxRequest()) {
            header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden', true, 403);
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');

            die(
                crm_json_encode(

                    array(
                        'exception' => 'crm_SaveException',
                        'message' => bab_convertStringFromDatabase($e->getMessage(), 'UTF-8'),
                        'errorMessage' => bab_convertStringFromDatabase($e->getMessage(), 'UTF-8'),
                    )
                )
            );

        }
        header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found', true, 404);

        $Crm = $this->Crm();
        $W = bab_Widgets();

        crm_BreadCrumbs::setCurrentPosition($action, crm_translate('Deleted item'));

        $page = $Crm->Ui()->Page();

        $dialog = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(2, 'em'))
            ->addClass(Func_Icons::ICON_LEFT_16);

        $page->addItem($dialog);

        //TRANSLATORS: %s can be task, contact, organization ...
        $dialog->addItem($W->Title(sprintf(crm_translate('This %s has been deleted'), $e->getObjectTitle()), 1));

        if ($e instanceof crm_DeletedRecordException) {
            $dialog->addItem(
                $W->VBoxItems(
                    $e->getDeletedBy(),
                    $e->getDeletedOn()
                )
            );
        }

        $dialog->addItem($this->getBackButton());

        $page->displayHtml();

    }




    private function notFoundPage(Widget_Action $action, crm_NotFoundException $e)
    {
        if ($this->isAjaxRequest()) {
            $message = sprintf(crm_translate('This %s does not exists'), $e->getObjectTitle() . ' (' . $e->getId() . ')');
            $json = array(
                'messages' => array(
                    array(
                        'level' => 'danger',
                        'content' => $message
                    )
                )
            );
            echo bab_json_encode($json);
            die;
        }
        header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found', true, 404);
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

        $Crm = $this->Crm();
        $W = bab_Widgets();

        crm_BreadCrumbs::setCurrentPosition($action, crm_translate('Not found'));

        $page = $Crm->Ui()->Page();
        $page->addClass(Func_Icons::ICON_LEFT_16);

        //TRANSLATORS: %s can be task, contact, organization ...
        $page->addItem($W->Title(sprintf(crm_translate('This %s does not exists'), $e->getObjectTitle()), 1));
        $page->addItem($this->getBackButton());
        $page->displayHtml();

    }



    private function errorPage(Exception $e)
    {
        header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal server error', true, 500);

        require_once $GLOBALS['babInstallPath'] . 'utilit/uiutil.php';
        $popup = new babBodyPopup();
        $popup->babEcho(
            '<h1>' . $this->Crm()->translate('There was a problem when trying to perform this operation') . '</h1>'
            . '<h2 class="error">' . $this->Crm()->translate('Additional debugging information:') . '</h2>'
            . '<p class="error">' . $e->getMessage() . ' ' .  $e->getFile() . ' ' . $e->getLine()  . ' ' . $e->getTraceAsString() . '</p>'
            );


        die($popup->printout());
    }



    /**
     * Method to call before saving
     * @since 1.0.22
     * @return self
     */
    public function requireSaveMethod()
    {
        if ('GET' === $_SERVER['REQUEST_METHOD']) {
            throw new crm_SaveException('Method not allowed');
        }

        return $this;
    }


    /**
     * Method to call before deleting
     * @since 1.0.22
     * @return self
     */
    public function requireDeleteMethod()
    {
        if ('GET' === $_SERVER['REQUEST_METHOD']) {
            throw new crm_SaveException('Method not allowed');
        }

        return $this;
    }



    /**
     * @return crm_CtrlSearch
     */
    public function Search($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'search.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlSearch', $proxy);
    }

    /**
     * @return crm_CtrlContact
     */
    public function Contact($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'contact.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlContact', $proxy);
    }

    /**
     * @return crm_CtrlMyContact
     */
    public function MyContact($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'mycontact.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlMyContact', $proxy);
    }

    /**
     * @return crm_CtrlOrganization
     */
    public function Organization($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'organization.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlOrganization', $proxy);
    }


    /**
     * @return crm_CtrlDeal
     */
    public function Deal($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'deal.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlDeal', $proxy);
    }


    /**
     * @return crm_CtrlTeam
     */
    public function Team($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'team.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlTeam', $proxy);
    }


    /**
     * @return crm_CtrlMail
     */
    public function Email($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'email.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlMail', $proxy);
    }

    /**
     * @return crm_CtrlEmailSignature
     */
    public function EmailSignature($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'emailsignature.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlEmailSignature', $proxy);
    }

    /**
     * @return crm_CtrlNote
     */
    public function Note($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'note.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlNote', $proxy);
    }

    /**
     * @return crm_CtrlComment
     */
    public function Comment($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'comment.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlComment', $proxy);
    }

    /**
     * @return crm_CtrlTask
     */
    public function Task($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'task.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlTask', $proxy);
    }


    /**
     * @return crm_CtrlTag
     */
    public function Tag($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'tag.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlTag', $proxy);
    }

    /**
     * @return crm_CtrlCalendar
     */
    public function Calendar($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'calendar.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlCalendar', $proxy);
    }

    /**
     * @return crm_CtrlCampaign
     */
    public function Campaign($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'campaign.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlCampaign', $proxy);
    }


    /**
     * @return crm_CtrlDataManagement
     */
    public function DataManagement($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'datamanagement.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlDataManagement', $proxy);
    }


    /**
     * @return crm_CtrlArticle
     */
    public function Article($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'article.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlArticle', $proxy);
    }


    /**
     * @return crm_CtrlCatalog
     */
    public function Catalog($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'catalog.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlCatalog', $proxy);
    }


    /**
     * @return crm_CtrlCatalogItem
     */
    public function CatalogItem($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'catalogitem.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlCatalogItem', $proxy);
    }


    /**
     * @return crm_CtrlShoppingCart
     */
    public function ShoppingCart($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'shoppingcart.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlShoppingCart', $proxy);
    }

    /**
     * Shopping cart administration in back office
     * @return crm_CtrlShoppingCartAdm
     */
    public function ShoppingCartAdm($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'shoppingcartadm.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlShoppingCartAdm', $proxy);
    }


    /**
     * Shipping cost scale
     * @return crm_CtrlShippingScale
     */
    public function ShippingScale($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'shippingscale.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlShippingScale', $proxy);
    }


    /**
     * Packaging units for articles
     * @return crm_CtrlPackaging
     */
    public function Packaging($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'packaging.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlPackaging', $proxy);
    }


    /**
     * Article types for articles
     * @return crm_CtrlArticleType
     */
    public function ArticleType($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'articletype.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlArticleType', $proxy);
    }


    /**
     * Article availability
     * @return crm_CtrlArticleType
     */
    public function ArticleAvailability($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'articleavailability.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlArticleAvailability', $proxy);
    }


    /**
     * Discounts
     * @return crm_CtrlDiscount
     */
    public function Discount($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'discount.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlDiscount', $proxy);
    }


    /**
     * Coupons
     * @return crm_CtrlCoupon
     */
    public function Coupon($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'coupon.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlCoupon', $proxy);
    }


    /**
     * Custom fields
     * @return crm_CtrlCustomField
     */
    public function CustomField($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'customfield.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlCustomField', $proxy);
    }


    /**
     * Custom sections
     * @return crm_CtrlCustomSection
     */
    public function CustomSection($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'customsection.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlCustomSection', $proxy);
    }


    /**
     * Status
     * @return crm_CtrlStatus
     */
    public function Status($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'status.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlStatus', $proxy);
    }


    /**
     * @return crm_CtrlOrder
     */
    public function Order($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'order.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlOrder', $proxy);
    }


    /**
     * @return crm_CtrlOrderItem
     */
    public function OrderItem($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'orderitem.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlOrderItem', $proxy);
    }


    /**
     * @return crm_CtrlImport
     */
    public function Import($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'import.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlImport', $proxy);
    }


    /**
     * @return crm_CtrlFile
     */
    public function File($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'file.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlFile', $proxy);
    }


    /**
     * @return crm_CtrlHome
     */
    public function Home($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'home.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlHome', $proxy);
    }


    /**
     * @return crm_CtrlAdmin
     */
    public function Admin($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'admin.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlAdmin', $proxy);
    }


    /**
     * @return crm_CtrlShopAdmin
     */
    public function ShopAdmin($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'shopadmin.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlShopAdmin', $proxy);
    }


    /**
     * @return crm_CtrlShopAdmin
     */
    public function PrivateSell($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'privatesell.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlPrivateSell', $proxy);
    }



    /**
     * @return crm_CtrlClipboard
     */
    public function Clipboard($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'clipboard.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlClipboard', $proxy);
    }


    /**
     * @return crm_CtrlLostPassword
     */
    public function LostPassword($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'lostpassword.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlLostPassword', $proxy);
    }


    /**
     * @return crm_CtrlGiftCard
     */
    public function GiftCard($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'giftcard.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlGiftCard', $proxy);
    }


    /**
     * @return crm_CtrlVat
     */
    public function Vat($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'vat.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlVat', $proxy);
    }


    /**
     * @return crm_CtrlMailBox
     */
    public function MailBox($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'mailbox.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlMailBox', $proxy);
    }

    /**
     *
     * @return crm_CtrlSearchCatalog
     */
    public function SearchCatalog($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'searchcatalog.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlSearchCatalog', $proxy);
    }

    /**
     * @return crm_CtrlShopTrace
     */
    public function ShopTrace($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'shoptrace.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlShopTrace', $proxy);
    }


    /**
     * @return crm_CtrlCollectStore
     */
    public function CollectStore($proxy = true)
    {
        require_once FUNC_CRM_CTRL_PATH . 'collectstore.ctrl.php';
        return $this->Crm()->ControllerProxy('crm_CtrlCollectStore', $proxy);
    }
}

