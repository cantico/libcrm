<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


/**
 * Export UTF-8 or ISO-8859-15 csv file
 * 
 */ 
class crm_csvExport
{
	const DELIMITER = ',';
	const ENCLOSURE = '"';
	
	
	/**
	 * Headers of csv file
	 * @param	string	$name		Name of item, used in filename
	 */ 
	private static function headers($name)
	{
		$name = bab_convertStringFromDatabase($name, 'ISO-8859-15');
		$name = bab_removeDiacritics($name);
		$name = str_replace(' ', '_', $name);
		
		header('content-type:text/csv');
		header('Content-Disposition: attachment; filename="'.$name.'.csv"');
		
		//header('content-type:text/plain');
	}
	
	
	private static function headersDebug()
	{
		header('content-type:text/plain');
	} 
	
	/**
	 * 
	 * @param	string	$encoding	format of the encoding of the export (utf-8 or iso-8859-15)
	 */
	private static function quote($str, $encoding='utf-8')
	{	
		$str = bab_convertStringFromDatabase($str, mb_strtoupper($encoding));
		
		return self::ENCLOSURE.str_replace(self::ENCLOSURE, self::ENCLOSURE.self::ENCLOSURE, $str).self::ENCLOSURE;
	}
	
	/**
	 * encode an array to CSV row
	 * @param	Array	$row
	 * @param	string	$encoding	format of the encoding of the export (utf-8 or iso-8859-15)
	 * @return 	string
	 */ 
	public static function row(Array $arr, $encoding='utf-8')
	{
		foreach($arr as &$val) {
			$val = self::quote($val, $encoding);
		}
		
		return implode(self::DELIMITER, $arr)."\n";
	}
	
	
	/**
	 * Export an ORM set
	 * 
	 * @param	ORM_RecordSet	$set		
	 * @param	Array			$export		columns to export
	 * @param	string			$title		title for filename
	 * @param 	string 			$encoding 	format of the encoding of the export (utf-8 or iso-8859-15)
	 */ 
	public static function exportSet(ORM_RecordSet $set, Array $export, $title, $encoding='utf-8')
	{
		
		self::headers($title);
		//self::headersDebug();
		print self::row($export, $encoding);
		
		$template = array_flip(array_keys($export));
		
		foreach($set->select() as $record) {

			$arr = $template;
			
			foreach($set->getFields() as $field) {
				self::getValuesFromField($record, $field, $export, $arr, '');
			}
			
			print self::row($arr, $encoding);
		}
		
		exit;
	}
	
	
	/**
	 * Return the number of values set in array $arr
	 * @return int
	 */
	private static function getValuesFromField(ORM_Record $record, ORM_Field $field, Array $export, &$arr, $prefix)
	{
		$fieldName = $field->getName();
		$value = $record->$fieldName;
		$return = 0;
		
		
		if ($value instanceOf ORM_Record) {
			
			
			$beforesubfield = $return;
			foreach($value->getParentSet()->getFields() as $subField) {
				$return += self::getValuesFromField($value, $subField, $export, $arr, $prefix.$field->getName().'/');
			}
			
			if ($beforesubfield === $return) {
				throw new Exception('the export columns try to extract the value form one of the fields under %s but there is no sub-field with this name');
			}
			
		} elseif (isset($export[$prefix.$fieldName])) {
			
			
			$arr[$prefix.$fieldName] = '';
			$return++;
			
			if ($field instanceOf ORM_DateField) {
				if (-1 !== $ts = bab_mktime($value)) {
					$arr[$prefix.$fieldName] = date('d-m-Y', $ts);
				}
				
			} elseif ($field instanceOf ORM_DateTimeField) {
				if (-1 !== $ts = bab_mktime($value)) {
					$arr[$prefix.$fieldName] = date('d-m-Y H:i', $ts);
				}
				
			} else {
				$arr[$prefix.$fieldName] = $field->output($value);
			}
		}
		
		return $return;
	}
}
