<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


$W = bab_Widgets();

$W->includePhpClass('Widget_Frame');





/**
 * Display the tags associated to a crm_Record.
 *
 * @extends Widget_FlowLayout
 */
class crm_TagsDisplay extends crm_UiObject
{
    /**
     * @param Func_Crm			$crm
     * @param array|Iterator	$tags		Array or Iterator of crm_Tag
     * @param string			$id
     */
    public function __construct($crm, $tags, crm_Record $associatedObject, Widget_Action $tagAction = null, $id = null)
    {
        parent::__construct($crm);
        $W = bab_Widgets();

        // We simulate inheritance from Widget_FlowLayout.
        // Now $this can be used as a FlowLayout
        $this->setInheritedItem($W->FlowLayout($id));

        $this->setSpacing(2, 'px');

        $this->addClass(Func_Icons::ICON_LEFT_SYMBOLIC);

        foreach ($tags as $tag) {
            /* @var $tag crm_Tag */
            if ($tag instanceof crm_Link) {
                $tag = $tag->targetId;
            }

            $tagLabel = $W->Label($tag->label)
                ->setTitle($tag->description);
            if (isset($tagAction)) {
                $tagLabel = $W->Link($tagLabel, $tagAction->setParameter('tag', $tag->label));
            }

            $tagDeleteLink = $W->Link(
                '',
                $crm->Controller()->Tag()->unlink($tag->id, $associatedObject->getRef())
            )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE);

            $tagLayout = $W->FlowItems(
                $tagLabel,
                $tagDeleteLink
            )->setVerticalAlign('middle')
            ->addClass('crm-tag');

            if (!$tag->checked) {
                $tagLayout->addClass('crm-not-checked');
            }
            $this->addItem(
                $tagLayout
            );
        }
    }
}





/**
 * Display the tags associated to a crm_Record.
 *
 * @extends Widget_FlowLayout
 */
function crm_displayTags(crm_Record $record, Widget_Action $tagAction = null, $id = null)
{
	$W = bab_Widgets();

	$Crm = $record->Crm();

	$tagSet = $Crm->TagSet();
	$tags = $tagSet->selectFor($record);

	$layout = $W->FlowLayout($id);
	$layout->setSpacing(2, 'px');

	foreach ($tags as $tag) {

		/* @var $tag crm_Tag */
		if ($tag instanceof crm_Link) {
			$tag = $tag->targetId;
		}

		if (isset($tagAction)) {
			$tagAction->setParameter('tag', $tag->label);
			$tagLabel = $W->Link($tag->label, $tagAction);
		} else {
			$tagLabel = $W->Label($tag->label);
		}
		$tagLabel->setTitle($tag->description);

		$tagDeleteLink = $W->Link(
			$W->Icon('', Func_Icons::ACTIONS_EDIT_DELETE),
			$Crm->Controller()->Tag()->unlink($tag->id, $record->getRef())
		)->setAjaxAction($Crm->Controller()->Tag()->unlink($tag->id, $record->getRef()), $tagLabel);

		$tagLayout = $W->FlowItems(
			$tagLabel,
			$tagDeleteLink
		)
		->setVerticalAlign('middle')
		->addClass('crm-tag')->addClass('icon-left-16 icon-16x16 icon-left-16');

		if (!$tag->checked) {
			$tagLayout->addClass('crm-not-checked');
		}
		$layout->addItem(
			$tagLayout
		);
	}

	return $layout;
}





/**
 * @return crm_Editor
 */
class crm_TagEditor extends crm_Editor
{
    /**
     * {@inheritDoc}
     * @see crm_Editor::prependFields()
     */
    protected function prependFields()
    {
        $W = $this->widgets;
        $Crm = $this->Crm();

        $this->isAjax = bab_isAjaxRequest();

        $this->addItem($W->Hidden()->setName('id'));

        $labelFormItem = $this->labelledField(
            $Crm->translate('Label'),
            $Crm->Ui()->SuggestTag()->setSize(60)->setMaxSize(255)
                ->addClass('widget-100pc')
                ->setMandatory(true, $Crm->translate('The tag label must not be empty.')),
            'label'
        );

        $descriptionFormItem = $this->labelledField(
            $Crm->translate('Description'),
            $W->LineEdit()
            ->addClass('widget-100pc'),
            'description'
        );

        $checkedFormItem = $this->labelledField(
            $Crm->translate('Checked'),
            $W->CheckBox(),
            'checked'
        );

        $this->addItem($labelFormItem);
        $this->addItem($descriptionFormItem);
        $this->addItem($checkedFormItem);
    }


    /**
     *
     * @param array $tag
     * @param array $namePathBase
     * @return self
     */
    public function setValues($tag, $namePathBase = array())
    {
        if (! ($tag instanceof crm_Record)) {
            return parent::setValues($tag, $namePathBase);
        }

        $values = $tag->getValues();
        parent::setValues($values, $namePathBase);

//         if (! empty($tag->id)) {
//             $this->setHiddenValue('data[id]', $tag->id);
//         }
        return $this;
    }
}



/**
 *
 * @method Func_Crm Crm()
 */
class crm_TagTableView extends crm_TableModelView
{

    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $tagSet = null)
    {
        if (!isset($tagSet)) {
            $tagSet = $this->getRecordSet();
        }
        $this->addColumn(
            crm_TableModelViewColumn($tagSet->label)
        );
        $this->addColumn(
            crm_TableModelViewColumn($tagSet->description)
        );
        $this->addColumn(
            crm_TableModelViewColumn($tagSet->modifiedOn)
        );
        $this->addColumn(
            crm_TableModelViewColumn($tagSet->modifiedBy)
        );
        $this->addColumn(
            crm_TableModelViewColumn($tagSet->checked)
        );
        $this->addColumn(
            widget_TableModelViewColumn('_actions_', '')
        );
    }


    /**
     * {@inheritDoc}
     * @see crm_TableModelView::computeCellContent()
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $Crm = $record->Crm();
        $W = bab_Widgets();

        if ($fieldPath == '_actions_') {
            $cellContent = $W->Menu()
                ->setLayout($W->VBoxLayout())
                ->addClass(Func_Icons::ICON_LEFT_SYMBOLIC);
            if ($record->isUpdatable()) {
                if (!$record->checked) {
                    $cellContent->addItem(
                        $W->Link(
                            $Crm->translate('Validate'),
                            $Crm->Controller()->Tag()->validate($record->id)
                        )->addClass('icon', Func_Icons::ACTIONS_DIALOG_OK)
                        ->setAjaxAction()
                    );
                }
                $cellContent->addItem(
                    $W->Link(
                        $Crm->translate('Edit'),
                        $Crm->Controller()->Tag()->edit($record->id)
                    )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                    ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                );
            }
            if ($record->isDeletable()) {
                $cellContent->addItem(
                    $W->Link(
                        $Crm->translate('Delete'),
                        $Crm->Controller()->Tag()->confirmDelete($record->id)
                    )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                    ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                );
            }

        } else {
            $cellContent = parent::computeCellContent($record, $fieldPath);
        }

        if ($record->checked) {
            $cellContent->addClass('checked');
        } else {
            $cellContent->addClass('not-checked');
        }

        return $cellContent;
    }
}

