<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



$W = bab_Widgets();

$W->includePhpClass('Widget_Frame');





/**
 * @return crm_Editor
 */
class crm_NoteEditor extends crm_Editor
{
    private $filepicker = null;

    protected $note = null;


    /**
     * @param Func_Crm $crm
     * @param string $id
     * @param Widget_Layout $layout
     */
    public function __construct(Func_Crm $crm, $id = null, Widget_Layout $layout = null)
    {
        parent::__construct($crm, $id, $layout);

        $this->setHiddenValue('tg', $crm->controllerTg);
        $this->setSaveAction(
            $this->Crm()->Controller()->Note()->save(),
            $this->Crm()->translate('Add this note')
        );
    }


    /**
     * Add fields into form
     * @return crm_NoteEditor
     */
    public function prependFields($withOptions = true, $withAttachements = true)
    {
        $W = $this->widgets;

        $this->addItem($this->Summary());

        if ($withOptions) {
            $this->addItem(
                $W->FlowItems(
                    $this->PrivateItem(),
                    $this->PinnedItem()
                )
                ->setHorizontalSpacing(2, 'em')
                ->setVerticalSpacing(1, 'em')
            );
        }
        if ($withAttachements) {
            if ($this->Crm()->Access()->editNoteAttachments($this->note)) {
                $this->addItem($this->getFilePicker());
            }
        }

        return $this;
    }



    protected function Summary()
    {
        $summaryFormItem = $this->widgets->TextEdit()
            ->setLines(12)
            ->setName('summary')
            ->setColumns(70)
            ->addClass('widget-fullwidth');
        $summaryFormItem->setMandatory(true, $this->Crm()->translate('The note must not be empty.'));

        return $summaryFormItem;
    }


    protected function PrivateItem()
    {
        $W = $this->widgets;
        $Crm = $this->Crm();

        return $this->labelledField(
                $Crm->translate('Private note'),
                $W->CheckBox(),
                'private',
                $Crm->translate('Private notes are only visible by their author.')
            );
    }


    protected function PinnedItem()
    {
        $W = $this->widgets;
        $Crm = $this->Crm();

        return $this->labelledField(
                $Crm->translate('Pinned note'),
                $W->CheckBox(),
                'pinned',
                $Crm->translate('Pinned notes stay on top of history.')
            );
    }


    public function setNote(crm_Note $note = null)
    {
        if (isset($note)) {
            $this->note = $note;
            $noteValues = $note->getValues();
            $this->setValues(array('note' => $noteValues));

            if (!empty($note->id)) {
                $this->setHiddenValue('data[id]', $note->id);
            }
        }


        if (isset($note)) {
            $this->setSaveAction($this->Crm()->Controller()->Note()->save(), $this->Crm()->translate('Save this note'));
        } else {

        }
    }

    /**
     * set editor in create note mode
     */
    public function addNote()
    {
        $this->setNote();
    }



    /**
     * Get file picker widget
     * @see crm_NoteEditor::prependFields()
     *
     * @return Widget_FilePicker
     */
    public function getFilePicker()
    {
        if (null === $this->filepicker) {

            $W = $this->widgets;

            $this->filepicker = $W->FilePicker()
                ->setName('attachments')
            //	->hideFiles()
            ;

            if (isset($this->note))
            {
                $this->filepicker->setFolder($this->note->uploadPath());
            }
        }

        return $this->filepicker;
    }


}



/**
 *
 * @param string 	$pathname
 * @param int		$width
 * @param int		$height
 * @return Widget_Image
 */
function crm_fileAttachementThumbnail($pathname, $width, $height)
{
	$W = bab_Widgets();
    $T = @bab_functionality::get('Thumbnailer');

    $image = $W->Image();

    if ($T) {
         $T->setSourceFile($pathname);
        $T->setBorder(1, '#cccccc', 2, '#ffffff');
        $imageUrl = $T->getThumbnail($width, $height);
        $image->setUrl($imageUrl);
    }

    return $image;
}




/**
 * Display a note as seen in history frames.
 *
 *
 */
class crm_HistoryNote extends crm_HistoryElement
{


    public function __construct(crm_Note $note, $mode = null, $id = null)
    {
        parent::__construct($note->Crm(), $mode, $id);

        $this->setNote($note);
         $this->addClass('crm-note');
    }

    /**
     * Set Note image
     * @param	Widget_Image	$image
     * @return	crm_historyNote
     */
    public function setImage(Widget_Image $image)
    {
        $this->addImage($image);
    }


    /**
     * Set Note content
     * @return crm_historyNote
     */
    public function setNote(crm_Note $note, $label = null, Widget_Action $link = null)
    {
		$W = bab_Widgets();
        require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
        $this->setElement($note);

        $Crm = $this->Crm();

        $Crm->Ui()->includeDeal();
        $Crm->Ui()->includeOrganization();
        $Crm->Ui()->includeContact();

        $access = $Crm->Access();





        if ($note->createdBy) {

            if (isset($Crm->Contact))
            {
                $contactSet = $Crm->ContactSet();

                $creator = $contactSet->get($contactSet->user->is($note->createdBy));
                if ($creator && $access->viewContact($creator)) {
                    $creatorName = $creator->getFullName();
                    $this->addIcon(
                        $W->Link(
                            $Crm->Ui()->ContactPhoto($creator, 32, 32, 0),
                            $Crm->Controller()->Contact()->display($creator->id)
                        )
                    );
                } else {
                    $creatorName = bab_getUserName($note->createdBy, true);
                }

            } else {
                $creatorName = bab_getUserName($note->createdBy, true);
            }
        } else {
            $creatorName = '';
        }


        $subtitleLayout = $W->FlowLayout()
            ->setHorizontalSpacing(1, 'ex')
            ->addClass(Func_Icons::ICON_LEFT_24)
            ->addClass('crm-small');


        $noteIsPrivate =  $note->private;
        $noteIsPinned =  $note->pinned;


        if ($noteIsPrivate) {
            $this->addClass('crm-note-private');
        }
        if ($noteIsPinned) {
            $this->addClass('crm-note-pinned');
        }
        $labelPlus = '';


        if ($note->private) {
            $subTitle = sprintf($Crm->translate('Private note by %s'),  $creatorName);
            $this->addIcon(Func_Icons::APPS_PREFERENCES_AUTHENTICATION);
        } else {
            if ($note->pinned) {
                $subTitle = sprintf($Crm->translate('Pinned note by %s'), $creatorName);
            } else {
                $subTitle = sprintf($Crm->translate('Note by %s'),  $creatorName);
            }
        }


        $this->addDefaultLinkedElements();


		/*$this->addTitle(
			$W->Title(
				$W->Link(bab_abbr(strip_tags($note->summary), BAB_ABBR_FULL_WORDS, 50),
				$Crm->Controller()->Note()->display($note->id)),
				4
			)
		);*/

		$noteIcon = $W->Icon($subTitle . "\n" . BAB_DateTimeUtil::relativePastDate($note->createdOn, true), Func_Icons::ACTIONS_VIEW_PIM_NOTES);
		$subtitleLayout->addItem($noteIcon);

		$this->addTitle($subtitleLayout);



		switch ($this->getMode()) {

			case self::MODE_COMPACT :
				break;

			case self::MODE_SHORT :
				$this->addContent($W->Html(bab_toHtml(bab_abbr(strip_tags($note->summary), BAB_ABBR_FULL_WORDS, 200))));
				break;

			case self::MODE_FULL :
			default:
				$this->addContent($W->RichText($note->summary));
				if ($attachments = $note->attachments()) {
					$this->setAttachments($attachments);
				}
				break;
		}

		return $this;
	}



    /**
     * Add a contextual menu with several actions applicable
     * to the note.
     *
     * @return crm_historyNote
     */
    public function addActions()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $this->addAction(
            $W->Link(
                $Crm->translate('View this note'),
                $Crm->Controller()->Note()->Display($this->element->id)
            )->addClass('icon', Func_Icons::ACTIONS_VIEW_PIM_NOTES)
        );

        if ($Crm->Access()->updateNote($this->element)) {
            $this->addAction(
                $W->Link(
                    $Crm->translate('Edit this note'),
                    $Crm->Controller()->Note()->edit($this->element->id)
                )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            );
        }

        if ($Crm->Access()->deleteNote($this->element)) {
            $this->addAction(
                $W->Link(
                    $Crm->translate('Delete this note'),
                    $Crm->Controller()->Note()->confirmDelete($this->element->id)
                )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
                ->setOpenMode(widget_Link::OPEN_DIALOG_AND_RELOAD)
            );
        }

        return $this;
    }




    /**
     * Returns a link widget to the specified attached file
     *
     * @param	crm_Note				$note
     * @param	Widget_FilePickerItem 	$file
     *
     * @return Widget_Link
     */
    protected function getAttachmentWidget(Widget_FilePickerItem $file)
    {
        $W = bab_Widgets();

        return $W->Link(
            crm_fileAttachementIcon($file, 28, 28),
            $this->Crm()->Controller()->Note()->downloadAttachment($this->element->id, $file->getFileName(), 0)
        );
    }
}




/**
 *
 * @param crm_Note $note
 *
 * @return widget_Frame
 */
function crm_noteLinkedObjects(crm_Note $note)
{
	$W = bab_Widgets();
    $Crm = $note->Crm();

    $Crm->includeOrganizationSet();
    $Crm->Ui()->includeTask();

    $objectsFrame = $W->VBoxLayout()->setVerticalSpacing(0.5, 'em');

    $orgs = array();


	$notes = $note->getLinkedNotes();
	if (count($notes) > 0) {
	    foreach ($notes as $parentNote) {
	        $noteFrame = crm_noteLinkedObjects($parentNote);
	        $objectsFrame->addItem($noteFrame);
        }
	}

	$tasks = $note->getLinkedTasks();
	if (count($tasks) > 0) {
	    $objectsFrame->addItem(
	        $W->Title($Crm->translate('As comment on the task'), 4)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
	    );
	    foreach ($tasks as $task) {
	        $taskFrame = $Crm->Ui()->HistoryElement($task, crm_HistoryElement::MODE_SHORT);
	        $objectsFrame->addItem($taskFrame);
	        $taskFrame = crm_taskLinkedObjects($task);
	        $objectsFrame->addItem($taskFrame);
        }
	}

	$contacts = $note->getLinkedContacts();
	if (count($contacts) > 0) {

        $Crm->Ui()->includeContact();

        $objectsFrame->addItem(
            $W->Title($Crm->translate('Contacts linked to this note'), 4)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
        );

        foreach ($contacts as $contact) {
            $contactFrame = $W->VBoxLayout()->setVerticalSpacing(1, 'em')->addClass('crm-card');
            $objectsFrame->addItem($contactFrame);
            /* @var $contact crm_Contact */
            $card = $Crm->Ui()->contactCardFrame($contact);
            $contactFrame->addItem($card);

            $displayLink = $W->Link($W->Icon($Crm->translate('Detail'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS), $Crm->Controller()->Contact()->display($contact->id));

//			$mainOrganization = $contact->getMainOrganization();
//			if (isset($mainOrganization)) {
//				$orgs[] = $mainOrganization;
//			}
        }
    }

    $deals = $note->getLinkedDeals();
    if (count($deals) > 0) {

        $Crm->Ui()->includeDeal();

        $objectsFrame->addItem(
            $W->Title($Crm->translate('Deals linked to this note'), 4)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
        );

        foreach ($deals as $deal) {

            $dealFrame = $W->VBoxLayout()->setVerticalSpacing(1, 'em')->addClass('crm-card');
            $objectsFrame->addItem($dealFrame);

            /* @var $deal crm_Deal */
            $card = new crm_DealCardFrame($Crm, $deal);
            $dealFrame->addItem($card);

            $displayLink = $W->Link($W->Icon($Crm->translate('Detail'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS), $Crm->Controller()->Deal()->display($deal->id));

            $lead = $deal->lead();
            if (isset($lead)) {
                $orgs[] = $lead;
            }
        }
    }


    $orgs = $note->getLinkedOrganizations();
    if (count($orgs) > 0) {

        $Crm->Ui()->includeOrganization();
        $Crm->Ui()->includeContact();

        $objectsFrame->addItem(
            $W->Title($Crm->translate('Organizations linked to this note'), 4)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
        );

        foreach ($orgs as $org) {
            /* @var $org crm_Organization */

            $card = $Crm->Ui()->OrganizationCardFrame($org);

            $organizationFrame = $W->VBoxLayout()->setVerticalSpacing(1, 'em')->addClass('crm-card');
            $objectsFrame->addItem($organizationFrame);

            $organizationFrame->addItem($card);

            $orgContacts = $org->selectContacts();

            $contactsAccordion = $W->Accordions();
            foreach ($orgContacts as $orgContact) {
                $contact = $orgContact->getContact();
                /* @var $contact crm_Contact */
                $contactCard = $Crm->Ui()->contactCardFrame($contact, crm_ContactCardFrame::SHOW_ALL & ~(crm_ContactCardFrame::SHOW_NAME | crm_ContactCardFrame::SHOW_POSITION | crm_ContactCardFrame::SHOW_ORGANIZATION));
                $address = $contact->getMainAddress();
                $contactsAccordion->addPanel(
                    $contact->getFullName() . ' - ' . $Crm->translate($contact->getMainPosition()),
                    $W->VboxItems(
                        $contactCard,
                        $W->FlowLayout()->addClass('crm-context-actions')->addClass('icon-left-16')->addClass('icon-16x16')->addClass('icon-left')
                            ->addItem($W->Link($W->Icon($Crm->translate('Detail'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS), $Crm->Controller()->Contact()->display($contact->id)))
                    )->setVerticalSpacing(0.5, 'em')
                );
            }
            $organizationFrame->addItem($contactsAccordion);
        }
    }





    return $objectsFrame;
}




/**
 *
 * @param crm_Record $object
 *
 * @return widget_Frame
 */
function crm_notesForCrmObject(crm_Record $object, $linkType = null)
{
    $W = bab_Widgets();

    $Crm = $object->Crm();

    $notesFrame = $W->VBoxItems();
    $notesFrame->setVerticalSpacing(0.5, 'em');

    $noteSet = $Crm->NoteSet();

    $notes = $noteSet->select(
        $noteSet->all(
            $noteSet->isTargetOf($object, $linkType)
        )
    );

    $notes->orderDesc($noteSet->pinned);
    $notes->orderDesc($noteSet->modifiedOn);

    $nbNotes = 0;
    foreach ($notes as $note) {

        if (!$note->isReadable()) {
            continue;
        }

        $nbNotes++;


        $notePreview = new crm_HistoryNote($note);

        if ($note->isReadable()) {
            $notePreview->addAction(
                $W->Link(
                    $Crm->translate('Display'),
                    $Crm->Controller()->Note()->display($note->id)
                )->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            );
        }
        if ($note->isUpdatable()) {
            $notePreview->addAction(
                $W->Link(
                    $Crm->translate('Edit'),
                    $Crm->Controller()->Note()->edit($note->id)
                )->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            );
        }
        if ($note->isDeletable()) {
            $notePreview->addAction(
                $W->Link(
                    $Crm->translate('Delete...'),
                    $Crm->Controller()->Note()->confirmDelete($note->id)
                )->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            );
        }
        $notesFrame->addItem($notePreview);
    }

    if ($nbNotes === 0) {
        $notesFrame->addItem(
            $W->Label($Crm->translate('No note'))
                ->addClass('crm-empty')
        );
    }
    return $notesFrame;
}
