<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


bab_Widgets()->includePhpClass('widget_Frame');






class crm_ShoppingCartSteps extends crm_UiObject
{
	
	protected $step;
	
	/**
	 * 
	 * @param Func_Crm $Crm
	 * @param crm_ShoppingCart $cart
	 * @param int $step
	 * @param string $id
	 */
	public function __construct(Func_Crm $Crm, crm_ShoppingCart $cart, $step = null, $id = null)
	{
		parent::__construct($Crm, $id);
		$W = bab_Widgets();
		

		$this->step = $step;
		$this->shoppingCart = $cart;
		
		$this->setInheritedItem($W->Frame(null, $this->getItems()));
		$this->addClass('crm-shoppingcart-steps');
	}
	
	
	
	/**
	 * Add list of steps
	 * @return Widget_Layout
	 */
	protected function getItems()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		$layout = $W->HBoxLayout();
		
		for($s = 0; $s <= 3; $s++)
		{
			$name = 'step'.$s;
			
			$widget = $this->$name();
			if (!isset($widget))
			{
				continue;
			}
			
			if ($this->step === $s)
			{
				$widget->addClass('crm-shoppingcart-currentstep');
			}
			
			$layout->addItem($widget);
		}
		
		return $layout;
	}
	
	/**
	 * Display shopping cart summary
	 * @return Widget_link
	 */
	protected function step0()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		if ($this->step > 0)
		{
			return $W->Link(
					$Crm->translate('Summary'),
					$Crm->Controller()->ShoppingCart()->edit()
			);
		}
		
		return $W->Label($Crm->translate('Summary'));
	}
	
	
	
	/**
	 * Display login form or register
	 * @return Widget_link | Widget_Label
	 */
	protected function step1()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		if ($this->step > 1)
		{
			return $W->Link(
					$Crm->translate('Identify yourself'), // Login or create account
					$Crm->Controller()->MyContact()->loginCreate(1)
			);
		}
		
		return $W->Label($Crm->translate('Identify yourself'));
	}
	
	
	/**
	 * Addresses
	 * @return Widget_link | Widget_Label
	 */
	protected function step2()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		if ($this->step > 2)
		{
			return $W->Link(
					$Crm->translate('Addresses'),
					$Crm->Controller()->ShoppingCart()->setDeliveryAddress()
			);
		}
		
		return $W->Label($Crm->translate('Addresses'));
	}
	
	
	/**
	 * Payment
	 * @return Widget_link | Widget_Label
	 */
	protected function step3()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
	
		if ($this->step > 3)
		{
			return $W->Link(
					$Crm->translate('Payment'),
					$Crm->Controller()->ShoppingCart()->confirmShopTerms()
			);
		}
	
		return $W->Label($Crm->translate('Payment'));
	}
	
	
	
	/**
	 * used also in account creation confirmation page
	 * @return Widget_Link
	 */
	public function button_setDeliveryAddress()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		$contact = $Crm->getMyContact();
		$address = $contact->getMainAddress();
		
		if ($address->isEmpty())
		{
			$button_label = $Crm->translate('Set the addresses');
		} else {
			$button_label = $Crm->translate('Set delivery address');
		}
		
		return $W->Link(
			$W->Icon($button_label, Func_Icons::ACTIONS_DIALOG_OK),
			$Crm->Controller()->ShoppingCart()->setDeliveryAddress()
		)->addClass('crm-dialog-button');
	
	}
	
	/**
	 * @return Widget_Link
	 */
	public function button_creditCard()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		$addon = bab_getAddonInfosInstance('LibCrm');
	
		return $W->Link(
			$W->Icon($Crm->translate('Proceed to payment'), Func_Icons::ACTIONS_DIALOG_OK)
		          ->setImageUrl($addon->getStylePath().'images/credit_card3.png'),
			$Crm->Controller()->ShoppingCart()->creditCard()
		)->addClass('crm-dialog-button');
	}
	
	/**
	 * @return Widget_Link
	 */
	public function button_loginCreate()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
	
		return $W->Link(
			$W->Icon($Crm->translate('Confirm your order'), Func_Icons::ACTIONS_DIALOG_OK), // Login or create account
			$Crm->Controller()->MyContact()->loginCreate(1)
		)->addClass('crm-dialog-button');
	}
	
	/**
	 * @return Widget_Link
	 */
	public function button_confirmShopTerms()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
	
		return $W->Link(
			$W->Icon($Crm->translate('Confirm your order'), Func_Icons::ACTIONS_DIALOG_OK),
			$Crm->Controller()->ShoppingCart()->confirmShopTerms()
		)->addClass('crm-dialog-button');
	}
	
	
	
	public function getButtonsContainer()
	{
		$W = bab_Widgets();
		
		
		$buttons = $W->Frame(null, $W->FlowLayout()->setSpacing(2,'em'));
		$buttons->addClass(Func_Icons::ICON_LEFT_48)->addClass('crm-shoppingcart-buttons');
		
		return $buttons;
	}
	
	
	
	/**
	 * Get buttons according to step
	 * @return Widget_Link
	 */
	public function getButtons()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		$buttons = $this->getButtonsContainer();
		
		switch($this->step)
		{
			default:
			case 0:
				$buttons->addItem($this->button_loginCreate());
				break;
				
			case 1:
				$buttons->addItem($this->button_setDeliveryAddress());
				break;
				
			case 2:
				$buttons->addItem($this->button_confirmShopTerms());
				break;
				
			case 3:
				$buttons->addItem($this->button_creditCard());
				break;
		}
		
		return $buttons;
	}
	
}








class crm_ShoppingCartItemTableView extends crm_TableModelView
{
	/**
	 * 
	 * @var crm_ShoppingCart
	 */
	protected $shoppingCart = null;
	
	/**
	 * @var bool
	 */
	private $readOnly = null;
	
	
	public function __construct(Func_Crm $Crm, crm_ShoppingCart $cart, $id = null)
	{
		parent::__construct($Crm, $id);
		
		$this->addClass('crm-shoppingcart-tableview');
		$this->setShoppingCart($cart);
		
		// variables for javascript
		
		$this->setMetadata('tg', bab_rp('tg'));
		$this->setMetadata('selfpage', bab_getSelf());
		$this->setMetadata('cart', $cart->id);
		
		$this->addClass(Func_Icons::ICON_LEFT_16);
		
		
	}
	
	
	public function setShoppingCart(crm_ShoppingCart $cart)
	{
		$Crm = $this->Crm();
		$this->shoppingCart = $cart;
		
		$res = $cart->getItems();
		$this->setDataSource($res);

		$this->addDefaultColumns($cart->getParentSet());
	
		$this->setName('item');
	}
	
	
	public function addDefaultColumns(ORM_RecordSet $set)
	{
		$Crm = $this->Crm();
		$dvat = $this->displayVatColumn($this->shoppingCart);
		
		$this->addColumn(widget_TableModelViewColumn('_delete', '')->addClass('widget-column-thin'));
		$this->addColumn(widget_TableModelViewColumn('name', $Crm->translate('Product')));
		
		$addonname = $Crm->getAddonName();
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory("/$addonname/configuration/");
		
		$excltaxcart = $registry->getValue('excltaxcart', false);


		if ($excltaxcart) {
			$this->addColumn(widget_TableModelViewColumn('unit_cost_df', $dvat ? $Crm->translate('U. cost tax excl') : $Crm->translate('Unit cost'))->addClass('numeric')->addClass('widget-column-thin')->addClass('unitcostdf'));
		} else {
			$this->addColumn(widget_TableModelViewColumn('unit_cost_ti', $dvat ? $Crm->translate('U. cost tax incl') : $Crm->translate('Unit cost'))->addClass('numeric')->addClass('widget-column-thin')->addClass('unitcostti'));
		}
		
		$this->addColumn(widget_TableModelViewColumn('quantity', $Crm->translate('Quantity'))->addClass('numeric')->addClass('widget-column-thin')->addClass('quantity'));
		
		if ($dvat && $excltaxcart) {
			$this->addColumn(widget_TableModelViewColumn('vat', $Crm->translate('Vat'))->addClass('numeric')->addClass('widget-column-thin')->addClass('vat'));
		}
		
		if ($excltaxcart) {
			$this->addColumn(widget_TableModelViewColumn('total_df', $dvat ? $Crm->translate('Total tax excl') : $Crm->translate('Total'))->addClass('numeric')->addClass('widget-column-thin')->addClass('totaldf'));
		} else {
			$this->addColumn(widget_TableModelViewColumn('total_ti', $dvat ? $Crm->translate('Total tax incl') : $Crm->translate('Total'))->addClass('numeric')->addClass('widget-column-thin')->addClass('totalti'));
		}
	}
	

	protected function getExtraRows()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$variables = $Ui->ShoppingCartExtraRows($this->shoppingCart);
		
		$return = array();
		
		foreach($variables as $name => $arr)
		{
			if (null === $arr['value'])
			{
				// si mode readonly, retirer les regles qui ne doivent pas etre envoyes au client
				// en mode modification les regles a null sont envoyees car elle peuvent apparaitre si le panier est modifie
				
				if (!$this->isEditMode())
				{
					continue;
				}
				
				
				$value = '';
			} else {
				$value = $Crm->numberFormat($arr['value']);
			}
			
			$return[] = array(
					'name' => $W->Label($arr['title']),
					'total' => $W->Label($value.bab_nbsp().$Ui->Euro(), 'crm-shopping-cart-'.$name)
			);
		}
		
		return $return;
	}
	

	protected function getCouponRows()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$variables = $Ui->CouponRows($this->shoppingCart);
		
		$return = array();
		
		foreach($variables as $id_coupon => $arr)
		{
			
			if ($this->isEditMode())
			{
				$delete = $W->Link($W->Icon($Crm->translate('Remove'), Func_Icons::ACTIONS_EDIT_DELETE), $this->Crm()->Controller()->ShoppingCart()->removeShoppingCartCoupon($arr['id']))->addClass('crm-shoppingcart-removeitem');
			} else {
				$delete = null;
			}
			
			
			if (null === $arr['value'])
			{
				if (!$this->isEditMode())
				{
					continue;
				}
				
				$value = '0.00';
			} else {
				$value = $Crm->numberFormat($arr['value']);
			}
			
			
			$message = $W->Label($arr['message'], 'crm-coupon-message-'.$id_coupon);
			if (!empty($arr['message']))
			{
				$message->addClass('crm-coupon-message');
			}
			
			
			$return[] = array(
				'_delete' => $delete,
				'name' => $W->VBoxItems($W->Label($arr['ref'])->addClass('crm-strong'), $W->Label($arr['title']), $message),
				'total' => $W->Label($value.bab_nbsp().$Ui->Euro(), 'crm-coupon-'.$id_coupon)
			);
		}
		
		return $return;
	}
	
	
	/**
	 * append a list of rows to footer
	 * @param	int		&$row 		Current row
	 * @param	array	$rowList	
	 * @param	string	$class		CSS class specific to this row list
	 */
	private function appendFooterRows(&$row, $rowList, $class)
	{
		$W = bab_Widgets();
		
		foreach ($rowList as $extra) {
			$col = 0;
			$this->addRowClass($row, $class);
			foreach ($this->columns as $columnId => $column) {
		
				if ('total_ti' === $columnId || 'total_df' === $columnId)
				{
					$columnId = 'total';
				}
		
				if (isset($extra[$columnId])) {
					$this->addItem($extra[$columnId], $row, $col);
				} else {
					$this->addItem($W->Label(''), $row, $col);
				}
		
				$col++;
			}
			$row++;
		}
	}
	
	

	/**
	 * Called after data rows
	 *
	 * @param	int	$row		Last row number +1
	 *
	 * @return unknown_type
	 */
	protected function initFooterRow($row)
	{
		parent::initFooterRow($row);

		$couponRows = $this->getCouponRows();
		$this->appendFooterRows($row, $couponRows, 'crm-shoppingcart-coupon');
		
		$extraRows = $this->getExtraRows();
		$this->appendFooterRows($row, $extraRows, 'crm-shoppingcart-extra');
	}
	
	

	/**
	 * Display VAT column if one or more shopping cart item contain a VAT
	 * @return bool
	 */
	protected function displayVatColumn(crm_ShoppingCart $cart)
	{
		foreach($cart->getItems() as $item)
		{
			if ('0.0000' !== $item->vat)
			{
				return true;
			}
		}

		return false;
	}
	
	
	
	/**
	 * Handle cell content, add the cell widget the the tableview
	 *
	 * @param 	ORM_Record	$record			row record
	 * @param 	string		$fieldPath		collumn name as path is recordSet
	 * @param 	int			$row			row number in table
	 * @param 	int			$col			col number in table
	 * @param	string		$name			if name is set, the cell widget will be added with a namedContainer
	 *
	 * @return 	bool		True if a cell was added
	 */
	protected function handleCell(ORM_Record $record, $fieldPath, $row, $col, $name = null, $rowSpan = null, $colSpan = null)
	{
		return parent::handleCell($record, $fieldPath, $row, $col, (string) $record->id);
	}
	
	
	
	public function setReadOnly($val = true)
	{
		$this->readOnly = $val;
		return $this;
	}
	
	
	protected function isEditMode()
	{
		if (null !== $this->readOnly)
		{
			return !$this->readOnly;
		}
		
		return crm_ShoppingCart::EDIT === (int) $this->shoppingCart->status;
	}
	
	
	protected function computeCellContent(ORM_Record $record, $fieldPath)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
	
		
	
		if ('_delete' === $fieldPath && $this->isEditMode()) {
			return $W->Link($W->Icon($Crm->translate('Remove'), Func_Icons::ACTIONS_EDIT_DELETE), $this->Crm()->Controller()->ShoppingCart()->remove($record->id))->addClass('crm-shoppingcart-removeitem');
		}
	
		if ('name' === $fieldPath) {
			return $this->Crm()->Ui()->ShoppingCartItemCardFrame($record);
		}
	
	
		if ('unit_cost_df' === $fieldPath) {
			return $W->Label($this->Crm()->numberFormat($record->getUnitCostDF()).bab_nbsp().$Ui->Euro(), 'crm-item-'.$record->id.'-unit_cost-df');
		}
		
		if ('unit_cost_ti' === $fieldPath) {
			return $W->Label($this->Crm()->numberFormat($record->getUnitCostTI()).bab_nbsp().$Ui->Euro(), 'crm-item-'.$record->id.'-unit_cost-ti');
		}
	
		if ('quantity' === $fieldPath) {
			if ($this->isEditMode()) {
			    $min = $record->articlepackaging->packaging->getMinimalQuantity();
			    if (0 == $min) {
			        $min = 1;
			    }
			    
			    if (method_exists($W, 'NumberEdit')) {
			        $numberEdit = $W->NumberEdit()->setMin($min);
			    } else {
			        $numberEdit = $W->LineEdit();
			    }
			    
				return $numberEdit->setName('quantity')->setValue((int) $record->quantity)->addClass('crm-shoppingcart-quantity');
			} else {
				return $W->Label((int) $record->quantity);
			}
		}
	
	
		if ('vat' === $fieldPath) {
			return $W->Label($this->Crm()->numberFormat($record->vat).bab_nbsp().'%', 'crm-item-'.$record->id.'-vat');
		}
	
		if ('total_df' === $fieldPath) {
			return $W->Label($this->Crm()->numberFormat($record->getTotalDF()).bab_nbsp().$Ui->Euro(), 'crm-item-'.$record->id.'-total_df');
		}
		
		if ('total_ti' === $fieldPath) {
			return $W->Label($this->Crm()->numberFormat($record->getTotalTI()).bab_nbsp().$Ui->Euro(), 'crm-item-'.$record->id.'-total_ti');
		}
	
	
		return parent::computeCellContent($record, $fieldPath);
	}
	
}



/**
 * a frame to display on each catalog page, the number of items in shopping cart and a link to the shopping cart
 *
 */
class crm_ShoppingCartLinkFrame extends crm_UiObject
{
	public function __construct(Func_Crm $crm, $id = null, Widget_Layout $layout = null)
	{
		parent::__construct($crm);
		$W = bab_Widgets();
		$this->setInheritedItem($W->Frame($id, $layout));

		$this->addClass('crm-shoppingcart-linkframe');

		$this->addItems();
	}

	protected function addItems()
	{
		// $this->Title();
		$this->Items();
	}

	protected function Title()
	{
		$W = bab_Widgets();
		$this->addItem($W->label($this->Crm()->translate('Shopping cart'))->colon());
	}

	protected function CartIsEmpty()
	{
		$W = bab_Widgets();
		$this->addItem($W->Label($this->Crm()->translate('The shopping cart is empty')));
	}

	protected function Items()
	{
		$W = bab_Widgets();

		$cart = $this->Crm()->ShoppingCartSet()->getMyCart(false);
		if (!($cart instanceOf crm_ShoppingCart))
		{
			$this->CartIsEmpty();
			return;
		}

		$items = $cart->getItems();

		if (0 === $items->count()) {
			$this->CartIsEmpty();
			return;
		}

		if (1 === $items->count()) {
			$label = $this->Crm()->translate('One item in your shopping cart');
		} else {
			$label = sprintf($this->Crm()->translate('%d items in your shopping cart'), $items->count());
		}

		$this->addItem($W->Link($label, $this->Crm()->Controller()->ShoppingCart()->edit($cart->id)));
	}

}

class crm_ShoppingCartRecordEditor extends crm_Editor
{
	public function __construct(Func_Crm $Crm)
	{
		parent::__construct($Crm);

		$W = bab_Widgets();
		$this->colon();
		$this->setName('shoppingcart');

		$this->setHiddenValue('tg', bab_rp('tg'));

		$this->addItem($this->name());
		$this->addItem($this->recipients());
		$this->addItem($this->message());
		
		$this->addButton(
			$W->SubmitButton()->setLabel($Crm->translate('Save')),
			$Crm->Controller()->ShoppingCart()->recordShoppingCartEnd()
		);
		
		// set default values
		
		$this->setValues(
			array(
				'shoppingcart' => array(
					'name' => $this->getDefaultName(),
					'message' => $this->getDefaultMessage()
				)		
			)		
		);
	}
	
	/**
	 * @return string
	 */
	protected function getDefaultName()
	{
		$Crm = $this->Crm();
		
		return $Crm->translate('Wishlist');
	}
	
	/**
	 * @return string
	 */
	protected function getDefaultMessage()
	{
		$Crm = $this->Crm();
		
		return sprintf(
				$Crm->translate("Hello,")."\r\n".
				$Crm->translate("I let you discover these products on the site %s")."\r\n".
				$Crm->translate("Sincerely yours,")."\r\n".
				"%s", $_SERVER['HTTP_HOST'], $GLOBALS['BAB_SESS_FIRSTNAME']);
	}

	protected function name()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		return $W->FlowItems(
		$tmpLbl = $W->Label($Crm->translate('Name'))->colon(),
		$W->LineEdit()->setAssociatedLabel($tmpLbl)->setName('name')
		)->setSpacing(0,'em', .5,'em');
	}
	
	/**
	 * A list of email to notify about the recorded shopping cart
	 */
	protected function recipients()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		return $this->labelledField(
			$Crm->translate('You can fill in the e-mail addresses of people with whom you want to share this list of products'), 
			$W->TextEdit()->setLines(2)->setColumns(80),
			__FUNCTION__
		);
	}
	
	
	/**
	 * 
	 */
	protected function message()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		return $this->labelledField(
				$Crm->translate('Message'),
				$W->TextEdit()->setLines(8)->setColumns(80),
				__FUNCTION__
		);
	}
}

class crm_ShoppingCartCouponEditor extends crm_Editor
{
	public function __construct(Func_Crm $Crm)
	{
		$W = bab_Widgets();
		
		$this->setCrm($Crm);
		
		$W = $this->widgets = bab_Widgets();

		$this->buttonsLayout = $W->HBoxLayout()->setHorizontalSpacing(1, 'em');
		$this->innerLayout = $W->HBoxLayout()->setVerticalSpacing(1, 'em');

		$layout = $W->HBoxItems(
			$this->innerLayout->addClass('crm-loading-box'), // javascrit can add the crm-loading-on class to this container to show ajax loading progress in the form
			$this->buttonsLayout
		);
		$layout->setHorizontalSpacing(.25, 'em');
		
		$this->setInheritedItem($W->Form(null, $layout));
		if (isset($this->tmp_values))
		{
			$this->setValues($this->tmp_values[0], $this->tmp_values[1]);
		}

		$this->prependFields();

		$this->addClass('crm-editor');
		$this->colon();
		$this->setName('coupon');
		
		$this->setHiddenValue('tg', bab_rp('tg'));
		
		$this->addItem($this->code());
		$this->addButton(
			$W->SubmitButton()->setLabel($Crm->translate('OK')),
			$Crm->Controller()->ShoppingCart()->shoppingCartCoupon()
		);
	}

	protected function code()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		return $W->HBoxItems(
				$tmpLbl = $W->Label(crm_translate('If you have a coupon code enter it here'))->colon(),
				$W->LineEdit()->setName('code')->setAssociatedLabel($tmpLbl)->setSize(25)->setMandatory(true, crm_translate('The code has to be fill.'))
		)->setHorizontalSpacing(.25, 'em');
	}
}








/**
 * 
 *
 */
class crm_ShoppingCartCardFrame extends crm_CardFrame 
{
	/**
  	 * @var crm_ShoppingCart 
	 */
	protected $shoppingcart = null;
	
	public function __construct(Func_Crm $Crm, crm_ShoppingCart $shoppingcart)
	{
		parent::__construct($Crm);
		$W = bab_Widgets();
		$this->shoppingcart = $shoppingcart;
		
		$vbox = $W->VBoxLayout()->setVerticalSpacing(.5, 'em');
		$header = $W->HBoxLayout()->setHorizontalSpacing(3, 'em');
		$vbox->addItem($header);
		
		$createdOn = $this->labelStr($Crm->translate('Created on'), bab_shortDate(bab_mktime($shoppingcart->createdOn)));
		
		if ($expireOn = bab_shortDate(bab_mktime($shoppingcart->expireOn)))
		{
			$expireOn = $this->labelStr($Crm->translate('Expire on'), $expireOn);
		} else {
			$expireOn = null;
		}
		
		$header->addItem($W->VBoxItems($createdOn, $expireOn)->setVerticalSpacing(.5,'em'));
		
		$header->addItem($this->labelStr($Crm->translate('Status'), $shoppingcart->getParentSet()->status->output($shoppingcart->status)));
		
		if ($table = $this->getItemsTable())
		{
			$vbox->addItem($table);
		}
		
		
		$this->setInheritedItem($W->Frame(null, $vbox));
		$this->addClass('crm-shoppingcart-cardframe');
		$this->addClass(Func_Icons::ICON_LEFT_16);
		
		
		
		if ($Menu = $this->Menu())
		{
			$this->addItem($Menu);
		}
		
	}
	
	
	protected function Menu()
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		
		$Menu = $W->Menu(null, $W->VBoxLayout())->attachTo($this->item);
		
		$Menu->addItem(
				$W->Link(
						$W->Icon($Crm->translate('Display details'), Func_Icons::ACTIONS_DOCUMENT_PROPERTIES),
						$Crm->Controller()->ShoppingCartAdm()->display($this->shoppingcart->id)
				)
		);
		
		return $Menu;
	}
	
	
	/**
	 * Poducts list and total
	 */
	protected function getItemsTable()
	{
		
		$items = $this->shoppingcart->getItems();
		
		if (0 === $items->count())
		{
			return null;
		}
		
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		
		$table = $W->TableView();
		
		$i = 0;
		foreach($items as $scItem)
		{
			/*@var $scItem crm_ShoppingCartItem */
			$table->addItem($W->Label((int) $scItem->quantity), $i, 0);
			$table->addItem($W->Label($scItem->name), $i, 1);
			
			$i++;
		}
		
		$table->addItem($W->Label(''), $i, 0);
		
		$table->addItem(
			$W->Label(sprintf($Crm->translate('Total: %s incl tax'), $Crm->numberFormat($this->shoppingcart->getTotalTI()).$Ui->Nbsp().$Ui->Euro()))->addClass('crm-strong'),
			$i, 
			1
		);
		
		return $table;
	}
}






/**
 * Header frame for shopping cart products lists
 * 
 *
 */
class crm_ShoppingCartHeaderFrame extends crm_CardFrame
{
	/**
	 * @var crm_ShoppingCart
	 */
	protected $shoppingcart = null;

	public function __construct(Func_Crm $Crm, crm_ShoppingCart $shoppingcart)
	{
		parent::__construct($Crm);
		$W = bab_Widgets();
		$this->shoppingcart = $shoppingcart;

		$vbox = $W->VBoxLayout()->setVerticalSpacing(.5, 'em');
		
		$name = $shoppingcart->name;
		if (empty($name))
		{
			$name = $Crm->translate('Shopping cart');
		}
		
		$vbox->addItem($W->Title($name,3));
		
		
		$header = $W->HBoxLayout()->setHorizontalSpacing(6, 'em');
		$vbox->addItem($header);


		
		$header->addItem($this->labelStr($Crm->translate('Created by'), $W->Label(bab_getUserName($shoppingcart->user))));
		$header->addItem($this->labelStr($Crm->translate('Created on'), bab_shortDate(bab_mktime($shoppingcart->createdOn), false)));

		if ($link = $this->shoppingLink())
		{
			$hbox = $W->HBoxItems($W->Label(''), $link->setSizePolicy(Widget_SizePolicy::MINIMUM));
			$hbox->setCanvasOptions($hbox->Options()->width(100, '%'));
			$vbox->addItem($hbox);
		}

		$this->setInheritedItem($W->Frame(null, $vbox));
		$this->addClass('crm-shoppingcart-headerframe');
		$this->addClass(Func_Icons::ICON_LEFT_16);



	}


	protected function shoppingLink()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		return $W->Link(
				$Crm->translate('Add to shopping cart'), 
				$Crm->Controller()->ShoppingCart()->addWishList($this->shoppingcart->uuid)
			)
			->addClass('crm-shopping-link')
			->addAttribute('rel', 'nofollow');
	}
}






