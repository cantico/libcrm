<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




bab_Widgets()->includePhpClass('widget_TableView');



class crm_RoleTableView extends widget_TableModelView
{

	protected function computeCellContent($record, $fieldPath)
	{		
		$Crm = $record->Crm();
		$W = bab_Widgets();

		if ($fieldPath == '_actions_') {

			$cellContent = $W->Menu()
						->setLayout($W->FlowLayout())
						->addClass(Func_Icons::ICON_LEFT_16);
			$cellContent->addItem(
				$W->Link($W->Icon($Crm->translate('Edit...'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $Crm->Controller()->Admin()->editRole($record->id))
			);
			$cellContent->addItem(
				$W->Link($W->Icon($Crm->translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE), $Crm->Controller()->Admin()->deleteRole($record->id))
					->setConfirmationMessage($Crm->translate('Are you sure you want to delete this role?'))
			);
			
		} else if ($fieldPath == 'name') {
			$cellContent = $W->VBoxItems(
				$W->Title($record->name, 6),
				$W->Label($record->description)->addClass('crm-sub-label')
			);
			
		} else {
			$cellContent = parent::computeCellContent($record, $fieldPath);
		}
		
		return $cellContent;		
	}
	
}

