<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__).'/history.class.php';


bab_Widgets()->includePhpClass('widget_Form');
bab_Widgets()->includePhpClass('widget_Frame');
bab_Widgets()->includePhpClass('widget_InputWidget');
bab_Widgets()->includePhpClass('widget_TableModelView');


/**
 * @return Widget_Form
 *
 *
 * @method Widget_Form setReadOnly()
 * @method boolean isReadOnly()
 * @method boolean getReadOnly()
 * @method Widget_Form colon()
 * @method boolean isColon()
 * @method Widget_Form setHiddenValue()
 * @method string getHiddenValue()
 * @method array getHiddenFields()
 * @method Widget_Form setHiddenValues()
 * @method Widget_InputWidget[] getFields()
 * @method mixed getValue()
 * @method Widget_Form setValue()
 * @method Widget_Form setValues()
 * @method array getValues()
 * @method Widget_Form setAnchor()
 * @method Widget_Form setSelfPageHiddenFields()
 * @method boolean testMandatory()
 * @method Widget_Form checkUnsaved()
 * @method Widget_Form setValidateAction()
 * @method array getClasses()
 * @method array getFullName()
 * @method string display()
 */
class crm_Editor extends crm_UiObject
{
    /**
     * @var crm_Record
     */
    protected $record;

    protected $ctrl;

    protected $saveAction;
    protected $saveLabel;

    protected $cancelAction;
    protected $cancelLabel;

    /* @var $widgets Func_Widgets */
    protected $widgets;

    protected $buttonsLayout;
    protected $innerLayout;

    protected $failedAction = null;
    protected $successAction = null;

    /**
     * @var Widget_Section[] $sections
     */
    protected $sections = array();


    public $isAjax = false;

    /**
     *
     * @var array
     */
    private $tmp_values;


    /**
     * @param	Func_Crm		$crm
     * @param 	Widget_Layout 	$layout		The layout that will manage how widgets are displayed in this form.
     * @param 	string 			$id			The item unique id.
     */
    public function __construct(Func_Crm $crm, $id = null, Widget_Layout $layout = null)
    {
        parent::__construct($crm);
         // We simulate inheritance from Widget_VBoxLayout.
        $W = $this->widgets = bab_Widgets();


        $this->innerLayout = $layout;
        $this->buttonsLayout = $this->buttonsLayout();

        if (!isset($this->innerLayout)) {
            $this->innerLayout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        }

        $this->innerLayout->addClass('crm-loading-box'); // javascript can add the crm-loading-on class to this container to show ajax loading progress in the form

        $layout = $W->VBoxItems(
            $this->innerLayout,
            $this->buttonsLayout
        );
        $layout->setVerticalSpacing(2, 'em');
//		parent::__construct($id, $layout);

        // We simulate inheritance from Widget_Form.
        $this->setInheritedItem($W->Form($id, $layout));
        if (isset($this->tmp_values))
        {
            $this->setValues($this->tmp_values[0], $this->tmp_values[1]);
        }

        $this->setFormStyles();
        $this->prependFields();

    }


    /**
     * @return Widget_Layout
     */
    protected function buttonsLayout()
    {
        $W = bab_Widgets();
        return $W->HBoxLayout()->setHorizontalSpacing(1, 'em');
    }



    protected function setFormStyles()
    {
        $this->addClass('crm-editor');
    }


    /**
     * Set record and values to editor
     * @see crm_Editor::getRecord()
     *
     * @param crm_Record $record
     *
     * @return self
     */
    public function setRecord(crm_Record $record)
    {
        $this->record = $record;
        $this->recordSet = $record->getParentSet();

        if ($name = $this->getName()) {
            $values = $record->getFormOutputValues();
            $this->setValues($values, array($name));
        }

        return $this;
    }

    /**
     * @return crm_Record
     */
    protected function getRecord()
    {
        return $this->record;
    }


    /**
     * @return crm_RecordSet
     */
    protected function getRecordSet()
    {
        if ($this->record) {
            return $this->record->getParentSet();
        }
        return null;
    }


    public function setValues($row, $namePathBase = array())
    {
        if (isset($this->item)) {
            return $this->item->setValues($row, $namePathBase);
        } else {
            $this->tmp_values = array($row, $namePathBase);
            return true;
        }
    }

    /**
     *
     * @return crm_Editor
     */
    public function setController($ctrl)
    {
        $this->ctrl = $ctrl;
        return $this;
    }


    /**
     *
     * @return crm_Editor
     */
    public function setSaveAction($saveAction, $saveLabel = null)
    {
        $this->saveAction = $saveAction;
        $this->saveLabel = $saveLabel;
        return $this;
    }

    public function setSuccessAction($successAction)
    {
        $this->successAction = $successAction;
        return $this;
    }

    public function setFailedAction($failedAction)
    {
        $this->failedAction = $failedAction;
        return $this;
    }

    /**
     *
     * @return crm_Editor
     */
    public function setCancelAction($cancelAction, $cancelLabel = null)
    {
        $this->cancelAction = $cancelAction;
        $this->cancelLabel = $cancelLabel;
        return $this;
    }




    /**
     * Adds a button to button box of the form.
     *
     * @return crm_Editor
     */
    public function addButton(Widget_Item $button, Widget_Action $action = null)
    {
        $this->buttonsLayout->addItem($button);
        if (isset($action)) {
            $button->setAction($action);
        }

        return $this;
    }


    /**
     * Adds an item to the top part of the form.
     *
     * @return crm_Editor
     */
    public function addItem(Widget_Displayable_Interface $item = null, $position = null)
    {
        $this->innerLayout->addItem($item, $position);

        return $this;
    }

    /**
     *
     *
     */
    protected function appendButtons()
    {
        $W = $this->widgets;
        $Crm = $this->Crm();

        if (isset($this->saveAction)) {
            $saveLabel = isset($this->saveLabel) ? $this->saveLabel : $Crm->translate('Save');
            $submitButton = $W->SubmitButton();
            $submitButton->validate(true)
                ->setAction($this->saveAction)
                ->setFailedAction($this->failedAction)
                ->setSuccessAction($this->successAction)
                ->setLabel($saveLabel);
            if ($this->isAjax) {
                $submitButton->setAjaxAction();
            }
            $this->addButton($submitButton);
        }

        if (isset($this->cancelAction)) {
            $cancelLabel = isset($this->cancelLabel) ? $this->cancelLabel : $Crm->translate('Cancel');
            $this->addButton(
                $W->SubmitButton(/*'cancel'*/)
                     ->addClass('widget-close-dialog')
                    ->setAction($this->cancelAction)
                      ->setLabel($cancelLabel)
            );
        }

    }

    /**
     * Fields that will appear at the beginning of the form.
     *
     */
    protected function prependFields()
    {

    }


    /**
     * Fields that will appear at the end of the form.
     *
     */
    protected function appendFields()
    {

    }


    /**
     * Adds an item with a label and a description to the form.
     *
     * @param string                       $labelText
     * @param Widget_Displayable_Interface $item
     * @param string                       $fieldName
     * @param string                       $description
     * @param string						$suffix		 suffix for input field, example : unit
     *
     * @return Widget_LabelledWidget
     */
    public function labelledField($labelText, Widget_Displayable_Interface $item, $fieldName = null, $description = null, $suffix = null)
    {
        $W = $this->widgets;
        return $W->LabelledWidget($labelText, $item, $fieldName, $description, $suffix);
    }


    /**
     * Creates section in the editor.
     * If a section with the same header text (title) was already
     * created it is replaced by an empty section.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function createSection($headerText, $layout = null)
    {
        $W = bab_Widgets();
        if (!isset($layout)) {
            $layout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        }
        $this->sections[$headerText] = $W->Section(
            $headerText,
            $layout
        )->setFoldable(true);

        return $this->sections[$headerText];
    }

    /**
     * Retrieves a section in the editor.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function getSection($headerText)
    {
        if (!isset($this->sections[$headerText])) {
            return null;
        }
        return $this->sections[$headerText];
    }


    /**
     * Returns the list of sections created via createSection().
     *
     * @return Widget_Section[]
     */
    public function getSections()
    {
        return $this->sections;
    }


    /**
     *
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->appendFields();
        $this->appendButtons();

        if (false === $this->getReadOnly())
        {
            if ($bItem = crm_BreadCrumbs::getLastest())
            {
                $this->setHiddenValue('_ctrl_previous', $bItem->uid);
            }
        }


        return parent::display($canvas);
    }
}




/**
 * Editor for item with html metadata field
 *
 */
class crm_MetaEditor extends crm_Editor
{
    protected function MetaSection()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        return $W->Section(
            $Crm->translate('HTML page Metadata fields'),
            $W->VBoxItems(
                $this->page_title(),
                $this->page_description(),
                $this->page_keywords(),
                $this->image_alt()
            )->setVerticalSpacing(1,'em')
        )->setFoldable(true, true);
    }


    /**
     *
     * @return Widget_Item
     */
    protected function page_title()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        return $this->labelledField(
                $Crm->translate('Page title'),
                $W->LineEdit()->setSize(60),
                __FUNCTION__
        );
    }


    /**
     *
     * @return Widget_Item
     */
    protected function page_description()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        return $this->labelledField(
                $Crm->translate('Page description'),
                $W->TextEdit()->setColumns(60)->setLines(3),
                __FUNCTION__
        );
    }


    /**
     *
     * @return Widget_Item
     */
    protected function page_keywords()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        return $this->labelledField(
                $Crm->translate('Page keywords'),
                $W->TextEdit()->setColumns(60)->setLines(3),
                __FUNCTION__
        );
    }


    /**
     *
     * @return Widget_Item
     */
    protected function image_alt()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        return $this->labelledField(
                $Crm->translate('Alternate text of image'),
                $W->LineEdit()->setSize(60),
                __FUNCTION__
        );
    }
}





class crm_MergeEditor extends crm_Editor
{
    protected $mainRecord;
    protected $alternativeRecord = array();
    protected $alternative = array();


    public function setMainRecord(crm_Record $record)
    {
        $this->mainRecord = $record;
        $values = $this->getFormValues();

        $this->addAlternative($this->mainRecord);
        // $this->setValues($values);
    }

    public function addAlternativeRecord(crm_Record $record)
    {
        $this->alternativeRecord[] = $record;
        $this->addAlternative($record);
    }


    protected function addAlternative(crm_Record $record)
    {

        foreach($record->getValues() as $name => $dummy) {

            if (null !== $key = $this->handleAlternativeKey($record, $name)) {
                if (null !== $value = $this->handleAlternativeValue($record, $name)) {

                    if (!isset($this->alternative[$name])) {
                        $this->alternative[$name] = array();
                    }

                    $this->alternative[$name][$key] = $value;
                }
            }
        }
    }

    /**
     * return an alternative value for a record value
     * @param	ORM_Record 	$record
     * @param	string		$name
     * @return string | null
     */
    protected function handleAlternativeKey(ORM_Record $record, $name)
    {
        $param = $record->$name;

        if ($record instanceOf crm_TraceableRecord) {
            switch($name) {
                case 'uuid':
                case 'modifiedOn':
                case 'createdOn':
                case 'modifiedOn':
                case 'deletedOn':
                case 'createdBy':
                case 'modifiedBy':
                case 'deletedBy':
                    return null;
            }
        }


        if ($param instanceOf ORM_Record) {
            return $param->id;
        }

        if ('' === $param) {
            // empty alternative will not be available because of this
            return null;
        }

        return $param;
    }

    /**
     * return an alternative value for a record value
     * @param	ORM_Record 	$value
     * @param	string		$name
     * @return string | null
     */
    protected function handleAlternativeValue(ORM_Record $record, $name)
    {
        $param = $record->$name;

        if ($param instanceOf ORM_Record) {
            if (isset($param->name)) {
                return $param->name;
            }

            return $param->id;
        }

        $set = $record->getParentSet();
        return $set->$name->output($param);
    }


    /**
     * @return Array
     */
    protected function getFormValues()
    {
        return $this->mainRecord->getValues();
    }



    public function addFields()
    {
        $W = bab_Widgets();

        foreach($this->alternative as $name => $alternatives)
        {
            $field = $this->mainRecord->getParentSet()->$name;
            if (($description = $field->getDescription()) && count($alternatives) > 1)
            {

                $frame = $W->Frame()
                    ->addItem($W->Title($description, 4))
                    ->addItem($W->RadioSet()->setName($name)->setOptions($alternatives))
                ;

                $this->addItem($frame);
            }
        }
    }
}














///**
// * @param string	$id
// *
// * @return Widget_Frame
// */
//function crm_Toolbar($id = null)
//{
//	$W = bab_Widgets();
//
//	$toolbar = $W->Frame($id)
//					->setLayout($W->FlowLayout())
//					->addClass('widget-toolbar')
//					->addClass('icon-left-16')->addClass('icon-16x16')->addClass('icon-left')
//					;
//	return $toolbar;
//}



class crm_Toolbar extends Widget_Frame
{
    public $local = false;

    public function __construct($id = null, Widget_Layout $layout = null)
    {
        if (!isset($layout)) {
            $W = bab_Widgets();
            $layout = $W->FlowLayout()->setHorizontalSpacing(1, 'em');
            $layout->setVerticalAlign('top');
        }

        parent::__construct($id, $layout);
    }

    /**
     *
     * @param string		 	$labelText
     * @param string			$iconName
     * @param Widget_Action		$action
     * @param string			$id
     * @return crm_Toolbar
     */
    public function addButton($labelText = null, $iconName = null, $action = null, $id = null)
    {
        $W = bab_Widgets();
        $button = $W->Link($labelText, $action, $id);
        if (isset($iconName)) {
            $button->addClass('icon', $iconName);
        }

        $this->addItem($button);

        return $this;
    }


    public function addInstantForm(Widget_Displayable_Interface $form, $labelText = null, $iconName = null, $action = null, $id = null)
    {
        $W = bab_Widgets();
        if (isset($iconName)) {
            $content = $W->Icon($labelText, $iconName);
        } else {
            $content = $labelText;
        }
        $button = $W->Link($content, $action, $id);

        $this->addItem(
            $W->VBoxItems(
                $button->addClass('widget-instant-button'),
                $form->addClass('widget-instant-form')
            )->addClass('widget-instant-container')
        );

        if ($form->getTitle() === null) {
            $form->setTitle($labelText);
        }


        return $this;

    }


    public function display(Widget_Canvas $canvas)
    {
        if (!$this->local) {
            $this->addClass('widget-toolbar');
        } else {
            $this->addClass('crm-toolbar');
        }
        $this->addClass(Func_Icons::ICON_LEFT_16);

        return parent::display($canvas);
    }

}




class crm_RecordEditor extends crm_Editor
{

    /**
     * @var crm_RecordSet
     */
    public $recordSet = null;

    /**
     * Creates section in the editor.
     * If a section with the same header text (title) was already
     * created it is replaced by an empty section.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function addSection($id, $headerText, $layout = null)
    {
        $W = bab_Widgets();
        if (!isset($layout)) {
            $layout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        }
        $this->sections[$id] = $W->Section(
            $headerText,
            $layout
        )->setFoldable(true);

        return $this->sections[$id];
    }


    /**
     * Retrieves a section in the editor.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function getSection($id)
    {
        if (!isset($this->sections[$id])) {
            return null;
        }
        return $this->sections[$id];
    }


    /**
     * @param string $textLabel
     * @param mixed $value
     * @return Widget_Layout
     */
    function labelledWidget($textLabel, $value, crm_CustomSection $section = null)
    {
        $W = bab_Widgets();

        if ($value instanceof Widget_Displayable_Interface) {
            $widget = $value;
        } else {
            $widget = $W->Label($value);
        }

        if (isset($section)) {
            if ($textLabel === '__') {
                $fieldLayout = crm_CustomSection::FIELDS_LAYOUT_NO_LABEL;
            } else {
                $fieldLayout = $section->fieldsLayout;
            }
            switch ($fieldLayout) {
                case crm_CustomSection::FIELDS_LAYOUT_HORIZONTAL_LABEL:
                    return $W->FlowItems(
                        $W->Label($textLabel)->addClass('crm-horizontal-display-label', 'widget-strong')
                            ->setSizePolicy('widget-25pc'),
                        $widget->setSizePolicy('widget-75pc')
                    )->setHorizontalSpacing(1, 'ex')
                    ->setVerticalAlign('top');

                case crm_CustomSection::FIELDS_LAYOUT_WIDE_HORIZONTAL_LABEL:
                    return $W->FlowItems(
                        $W->Label($textLabel)->addClass('crm-horizontal-display-label', 'widget-strong')
                            ->setSizePolicy('widget-50pc'),
                        $widget
                    )->setHorizontalSpacing(1, 'ex')
                    ->setVerticalAlign('top');

                case crm_CustomSection::FIELDS_LAYOUT_NO_LABEL:
                    return $widget;

                case crm_CustomSection::FIELDS_LAYOUT_VERTICAL_LABEL:
                default:
                    return $W->LabelledWidget($textLabel, $widget);
            }
        }
    }


    function labelledWidgetOptional($textLabel, $displayedValue, $value = null, crm_CustomSection $section = null)
    {
        if (!isset($value)) {
            $value = $displayedValue;
        }
        if (!isset($value) || is_numeric($value) && $value == 0 || is_string($value) && trim($value) == '' || ($value instanceof Widget_Layout && count($value->getItems()) <= 0)) {
            return null;
        }
        return $this->labelledWidget($textLabel, $displayedValue, $section);
    }



    protected function fieldOutput($field, $record, $fieldName)
    {
        if ($field instanceof ORM_CurrencyField) {
            $Crm = $this->Crm();
            $value = $Crm->shortFormatWithUnit($this->record->$fieldName, $Crm->translate('_euro_'));
        } else {
            $value = $field->output($this->record->$fieldName);
        }
        if (is_array($value)) {
            $value = implode(', ', $value);
        }
        return $value;
    }


    protected function getValueItem($customSection, $displayField, $label, $value)
    {
//         if ($value instanceof Widget_Displayable_Interface) {
//             return $value;
//         }
//         $W = bab_Widgets();
//         $parameters = $displayField['parameters'];
//         if ($parameters['type'] === 'title1') {
//             $item = $W->Title($value, 1);
//         } elseif ($parameters['type'] === 'title2') {
//             $item = $W->Title($value, 2);
//         } elseif ($parameters['type'] === 'title3') {
//             $item = $W->Title($value, 3);
//         } else {
//             $item = $W->Label($value);
//         }
        return $this->labelledWidget(
            $label,
//            $item,
            $value,
            $customSection
        );
    }


    protected function addSections($view)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();


        $this->addItem($W->Hidden()->setName('id'));

        $recordClassName = $this->record->getClassName();

        $customSectionSet = $Crm->CustomSectionSet();
        $conditions = array(
            $customSectionSet->object->is($recordClassName),
            $customSectionSet->view->is($view)
        );

        $customSections = $customSectionSet->select(
            $customSectionSet->all($conditions)
        );
        $customSections->orderAsc($customSectionSet->rank);

        $currentColumn = 0;
        $row = $W->Items()->setSizePolicy('row');
        foreach ($customSections as $customSection) {

            if (isset($this->record) && !$customSection->isVisibleForRecord($this->record)) {
                continue;
            }

            list(, , $nbCol) = explode('-', $customSection->sizePolicy);

            if ($currentColumn + $nbCol > 12) {
                $this->addItem($row);
                $row = $W->Items()->setSizePolicy('row');
                $currentColumn = 0;
            }
            $currentColumn += $nbCol;

            $section = $this->getSection($customSection->id);
            if (!isset($section)) {
                $section = $this->addSection($customSection->id, $customSection->name);
                $section->addClass($customSection->classname);
                $section->setSizePolicy($customSection->sizePolicy);

                $section->setFoldable($customSection->foldable, $customSection->folded);
                $row->addItem($section);
            }

            $displayFields = $customSection->getFields();

            foreach ($displayFields as $displayField) {
                $widget = null;
                $item = null;
                $displayFieldName = $displayField['fieldname'];
                $parameters = $displayField['parameters'];
                $classname = isset($parameters['classname']) ? $parameters['classname'] : '';
                $label = isset($parameters['label']) && $parameters['label'] !== '__' ? $parameters['label'] : '';
                $displayFieldMethod = '_' . $displayFieldName;

                if (method_exists($this, $displayFieldMethod)) {
                    $widget = $this->$displayFieldMethod($customSection);
                    $item = $widget;
                } elseif ($this->recordSet->fieldExist($displayFieldName)) {
                    $field = $this->recordSet->getField($displayFieldName);
                    if ($label === '') {
                        $label = $field->getDescription();
                        if (substr($displayFieldName, 0, 1) !== '_') {
                            $label = $Crm->translate($label);
                        }
                    }
                    $widget = $field->getWidget();
                    if ($widget instanceof Widget_TextEdit || $widget instanceof Widget_Select) {
                        $widget->addClass('widget-100pc');
                    }
                    $item = $this->getValueItem($customSection, $displayField, $label, $widget);
                }

                if ($item) {
                    $item->addClass($classname);
                    $section->addItem($item);
                }
            }
        }

        if ($currentColumn + $nbCol> 0) {
            $this->addItem($row);
        }
    }

    public function sectionContent($customSectionId)
    {
        $Crm = $this->Crm();

        $customSectionSet = $Crm->CustomSectionSet();
        $customSection = $customSectionSet->get(
            $customSectionSet->id->is($customSectionId)
        );

        if (isset($this->record) && !$customSection->isVisibleForRecord($this->record)) {
            return null;
        }

        $W = bab_Widgets();

        $section = $W->VBoxLayout()->setVerticalSpacing(1, 'em');

        $displayFields = $customSection->getFields();

        $customSection->fieldsLayout = crm_CustomSection::FIELDS_LAYOUT_HORIZONTAL_LABEL;

        foreach ($displayFields as $displayField) {
            $widget = null;
            $item = null;
            $displayFieldName = $displayField['fieldname'];
            $parameters = $displayField['parameters'];
            $classname = isset($parameters['classname']) ? $parameters['classname'] : '';
            $label = isset($parameters['label']) && $parameters['label'] !== '__' ? $parameters['label'] : '';
            $displayFieldMethod = '_' . $displayFieldName;

            if (method_exists($this, $displayFieldMethod)) {
                $widget = $this->$displayFieldMethod($customSection);
                $item = $widget;
            } elseif ($this->recordSet->fieldExist($displayFieldName)) {
                $field = $this->recordSet->getField($displayFieldName);
                if ($label === '') {
                    $label = $field->getDescription();
                    if (substr($displayFieldName, 0, 1) !== '_') {
                        $label = $Crm->translate($label);
                    }
                }
                $widget = $field->getWidget();
                if ($widget instanceof Widget_TextEdit) {
                    $widget->addClass('widget-100pc widget-autoresize');

                }
                if ($widget instanceof Widget_Select) {
                    $widget->addClass('widget-100pc');
                }
                $item = $this->getValueItem($customSection, $displayField, $label, $widget);
            }

            if ($item) {
                $item->addClass($classname);
                $section->addItem($item);
            }
        }

        return $section;
    }
}



/**
 * A record frame is a frame displaying information about a crm record.
 *
 * @since 1.0.40
 */
class crm_RecordFrame extends crm_UiObject // extends Widget_Frame
{

    /**
     * @var crm_Record
     */
    protected $record = null;

    public function __construct(Func_Crm $crm, crm_Record $record, $id = null, Widget_Layout $layout = null)
    {
        parent::__construct($crm);

        $W = bab_Widgets();
        $this->setInheritedItem($W->Frame($id, $layout));
    }


    /**
     * Creates section in the editor.
     * If a section with the same header text (title) was already
     * created it is replaced by an empty section.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function addSection($id, $headerText, $layout = null)
    {
        $W = bab_Widgets();
        if (!isset($layout)) {
            $layout = $W->VBoxLayout()->setVerticalSpacing(0.6, 'em');
        }
        $this->sections[$id] = $W->Section(
            $headerText,
            $layout
        )->setFoldable(true);

        return $this->sections[$id];
    }


    /**
     * Retrieves a section in the editor.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function getSection($id)
    {
        if (!isset($this->sections[$id])) {
            return null;
        }
        return $this->sections[$id];
    }


    /**
     * @param string $textLabel
     * @param mixed $value
     * @return Widget_Layout
     */
    function labelledWidget($textLabel, $value, crm_CustomSection $section = null)
    {
        $W = bab_Widgets();

        if ($value instanceof Widget_Displayable_Interface) {
            $widget = $value;
        } else {
            $widget = $W->Label($value);
        }

        if (isset($section)) {
            if ($textLabel === '__') {
                $fieldLayout = crm_CustomSection::FIELDS_LAYOUT_NO_LABEL;
            } else {
                $fieldLayout = $section->fieldsLayout;
            }
            switch ($fieldLayout) {
                case crm_CustomSection::FIELDS_LAYOUT_HORIZONTAL_LABEL:
                    return $W->FlowItems(
                    $W->Label($textLabel)->addClass('crm-horizontal-display-label', 'widget-strong')
                        ->setSizePolicy('widget-25pc'),
                    $widget->setSizePolicy('widget-75pc')
                )->setHorizontalSpacing(1, 'ex')
                ->setVerticalAlign('top');

                case crm_CustomSection::FIELDS_LAYOUT_WIDE_HORIZONTAL_LABEL:
                    return $W->FlowItems(
                    $W->Label($textLabel)->addClass('crm-horizontal-display-label', 'widget-strong')
                        ->setSizePolicy('widget-50pc'),
                    $widget->setSizePolicy('widget-50pc')
                )->setHorizontalSpacing(1, 'ex')
                ->setVerticalAlign('top');

                case crm_CustomSection::FIELDS_LAYOUT_VERY_WIDE_HORIZONTAL_LABEL:
                    return $W->FlowItems(
                    $W->Label($textLabel)->addClass('crm-horizontal-display-label', 'widget-strong')
                        ->setSizePolicy('widget-75pc'),
                    $widget->setSizePolicy('widget-25pc')
                )->setHorizontalSpacing(1, 'ex')
                ->setVerticalAlign('top');


                case crm_CustomSection::FIELDS_LAYOUT_NO_LABEL:
                    return $widget;

                case crm_CustomSection::FIELDS_LAYOUT_VERTICAL_LABEL:
                default:
                    return $W->LabelledWidget($textLabel, $widget);
            }
        }
    }


    function labelledWidgetOptional($textLabel, $displayedValue, $value = null, crm_CustomSection $section = null)
    {
        if (!isset($value)) {
            $value = $displayedValue;
        }
        if (!isset($value) || is_numeric($value) && $value == 0 || is_string($value) && trim($value) == '' || ($value instanceof Widget_Layout && count($value->getItems()) <= 0)) {
            return null;
        }
        $labelledWidget = $this->labelledWidget($textLabel, $displayedValue, $section);
        return $labelledWidget;
    }



    protected function fieldOutput($field, $record, $fieldName)
    {
        if ($field instanceof ORM_CurrencyField) {
            $Crm = $this->Crm();
            $value = $Crm->shortFormatWithUnit($this->record->$fieldName, $Crm->translate('_euro_'));
        } else {
            $value = $field->output($this->record->$fieldName);
        }
        if (is_array($value)) {
            $value = implode(', ', $value);
        }
        return $value;
    }


    protected function getValueItem($customSection, $displayField, $label, $value)
    {
        if ($value instanceof Widget_Displayable_Interface) {
            return $value;
        }
        $W = bab_Widgets();
        $parameters = $displayField['parameters'];
        if (isset($parameters['type'])) {
            if ($parameters['type'] === 'title1') {
                $item = $W->Title($value, 1);
            } elseif ($parameters['type'] === 'title2') {
                $item = $W->Title($value, 2);
            } elseif ($parameters['type'] === 'title3') {
                $item = $W->Title($value, 3);
            } else {
                $item = $W->Label($value);
            }
        } else {
            $item = $W->Label($value);
        }
        $labelledWidgetOptional = $this->labelledWidget( //Optional(
            $label,
            $item,
//            $value,
            $customSection
        );

        if (isset($labelledWidgetOptional)) {
            $labelledWidgetOptional->addClass($displayField['fieldname']);
        }

        return $labelledWidgetOptional;
    }


    protected function addSections($view)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $recordClassName = $this->record->getClassName();

        $customSectionSet = $Crm->CustomSectionSet();
        $customSections = $customSectionSet->select(
            $customSectionSet->object->is($recordClassName)->_AND_(
                $customSectionSet->view->is($view)
            )
        );
        $customSections->orderAsc($customSectionSet->rank);

        $currentColumn = 0;
        $row = $W->Items()->setSizePolicy('row');
        foreach ($customSections as $customSection) {

            if (isset($this->record) && !$customSection->isVisibleForRecord($this->record)) {
                continue;
            }

            list(, , $nbCol) = explode('-', $customSection->sizePolicy);

            if ($currentColumn + $nbCol > 12) {
                $this->addItem($row);
                $row = $W->Items()->setSizePolicy('row');
                $currentColumn = 0;
            }
            $currentColumn += $nbCol;

            $section = $this->getSection($customSection->id);
            if (!isset($section)) {
                $section = $this->addSection($customSection->id, $customSection->name);
                $section->addClass($customSection->classname);
                $section->setSizePolicy($customSection->sizePolicy);

                $section->setFoldable($customSection->foldable, $customSection->folded);
                $row->addItem($section);
            }

            $displayFields = $customSection->getFields();

            foreach ($displayFields as $displayField) {
                $value = null;
                $displayFieldName = $displayField['fieldname'];
                $parameters = $displayField['parameters'];
                $classname = isset($parameters['classname']) ? $parameters['classname'] : '';
                $label = isset($parameters['label']) ? $parameters['label'] : '';
                $displayFieldMethod = '_' . $displayFieldName;

                if (method_exists($this, $displayFieldMethod)) {
                    $value = $this->$displayFieldMethod($customSection);
                } elseif ($this->recordSet->fieldExist($displayFieldName)) {
                    $field = $this->recordSet->getField($displayFieldName);
                    if ($label === '') {
                        $label = $field->getDescription();
                        if (substr($displayFieldName, 0, 1) !== '_') {
                            $label = $Crm->translate($label);
                        }
                    }
                    $value = $this->fieldOutput($field, $this->record, $displayFieldName);
                }

                if (isset($value)) {
                    $item = $this->getValueItem($customSection, $displayField, $label, $value);
                    if ($item) {
                        $item->addClass($classname);
                        $section->addItem($item);
                    }
                }
            }
        }

        if ($currentColumn + $nbCol> 0) {
            $this->addItem($row);
        }
    }
}



class crm_RecordView extends crm_UiObject
{
    /**
     * @var crm_Record
     */
    protected $record = null;

    protected $view = '';

    protected $recordController = null;

    /**
     * @param Func_Crm $crm
     * @param string $id
     * @param Widget_Layout $layout
     */
    public function __construct(Func_Crm $crm, $id = null, Widget_Layout $layout = null)
    {
        parent::__construct($crm);

        if (!isset($layout)) {
            $W = bab_Widgets();
            $layout = $W->Items();
        }

        $this->setInheritedItem($layout);
    }


    /**
     * @param crm_Record $record
     * @return self
     */
    public function setRecord(crm_Record $record)
    {
        $this->record = $record;
        $this->recordSet = $record->getParentSet();


        return $this;
    }


    /**
     * @param string $view
     * @return self
     */
    public function setView($view)
    {
        $this->view = $view;
        return $this;
    }


    /**
     * @return string
     */
    public function getView()
    {
        return $this->view;
    }



    /**
     * Creates section in the editor.
     * If a section with the same header text (title) was already
     * created it is replaced by an empty section.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function addSection($id, $headerText, $layout = null)
    {
        $W = bab_Widgets();
        if (!isset($layout)) {
            $layout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        }
        $this->sections[$id] = $W->Section(
            $headerText,
            $layout
        )->setFoldable(true);

        return $this->sections[$id];
    }

    /**
     * Creates section in the editor.
     * If a section with the same header text (title) was already
     * created it is replaced by an empty section.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function createSection($headerText, $layout = null)
    {
        return $this->addSection($headerText, $headerText, $layout);
    }


    /**
     * Retrieves a section in the editor.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function getSection($id)
    {
        if (!isset($this->sections[$id])) {
            return null;
        }
        return $this->sections[$id];
    }


    /**
     * Returns the list of sections created via createSection().
     *
     * @return Widget_Section[]
     */
    public function getSections()
    {
        return $this->sections;
    }




    /*
     * @param string $textLabel
     * @param mixed $value
     * @return Widget_Layout
     */
    function labelledWidget($textLabel, $value, crm_CustomSection $section = null)
    {
        $W = bab_Widgets();

        if ($value instanceof Widget_Displayable_Interface) {
            $widget = $value;
        } else {
            $widget = $W->Label($value);
        }

        if (isset($section)) {
            if ($textLabel === '__') {
                $fieldLayout = crm_CustomSection::FIELDS_LAYOUT_NO_LABEL;
            } else {
                $fieldLayout = $section->fieldsLayout;
            }
            switch ($fieldLayout) {
                case crm_CustomSection::FIELDS_LAYOUT_HORIZONTAL_LABEL:
                    return $W->FlowItems(
                    $W->Label($textLabel)->addClass('crm-horizontal-display-label', 'widget-strong')
                        ->setSizePolicy('widget-25pc'),
                    $widget
                )->setHorizontalSpacing(1, 'ex')
                ->setVerticalAlign('top');

                case crm_CustomSection::FIELDS_LAYOUT_WIDE_HORIZONTAL_LABEL:
                    return $W->FlowItems(
                    $W->Label($textLabel)->addClass('crm-horizontal-display-label', 'widget-strong')
                        ->setSizePolicy('widget-50pc'),
                    $widget
                )->setHorizontalSpacing(1, 'ex')
                ->setVerticalAlign('top');

                case crm_CustomSection::FIELDS_LAYOUT_NO_LABEL:
                    return $widget;

                case crm_CustomSection::FIELDS_LAYOUT_VERTICAL_LABEL:
                default:
                    return $W->LabelledWidget($textLabel, $widget);
            }
        }
    }


    function labelledWidgetOptional($textLabel, $displayedValue, $value = null, crm_CustomSection $section = null)
    {
        if (!isset($value)) {
            $value = $displayedValue;
        }
        if (!isset($value) || is_numeric($value) && $value == 0 || is_string($value) && trim($value) == '' || ($value instanceof Widget_Layout && count($value->getItems()) <= 0)) {
            return null;
        }
        return $this->labelledWidget($textLabel, $displayedValue, $section);
    }



    protected function fieldOutput($field, $record, $fieldName)
    {
        if ($field instanceof ORM_CurrencyField) {
            $Crm = $this->Crm();
            $value = $Crm->shortFormatWithUnit($this->record->$fieldName, $Crm->translate('_euro_'));
        } else {
            $value = $field->output($this->record->$fieldName);
        }
        return $value;
    }

    protected function addSections($view = '')
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $objectName = $this->record->getClassName();

        $customSectionSet = $Crm->CustomSectionSet();
        $customSections = $customSectionSet->select(
            $customSectionSet->object->is($objectName)->_AND_($customSectionSet->view->is($view))
        );
        $customSections->orderAsc($customSectionSet->rank);

        $currentColumn = 0;
        $nbCol = 0;
        $row = $W->Items()->setSizePolicy('row');
        foreach ($customSections as $customSection) {

            list(, , $nbCol) = explode('-', $customSection->sizePolicy);

            if ($currentColumn + $nbCol > 12) {
                $this->addItem($row);
                $row = $W->Items()->setSizePolicy('row');
                $currentColumn = 0;
            }
            $currentColumn += $nbCol;

            $section = $this->getSection($customSection->id);
            if (!isset($section)) {
                $section = $this->addSection($customSection->id, $customSection->name);
                $section->addClass($customSection->classname);
                $section->setSizePolicy($customSection->sizePolicy);

                if ($customSection->editable) {
                    $menu = $section->addContextMenu('inline');
                    $menu->addClass(Func_Icons::ICON_LEFT_16);
                    $menu->addItem(
                        $W->Link(
                            '',
                            $this->record->getController()->editSection($this->record->id, $customSection->id)
                        )->addClass('widget-actionbutton', 'section-button', 'icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                        ->setOpenMode(Widget_Link::OPEN_DIALOG)
                    );
                }

                $section->setFoldable($customSection->foldable, $customSection->folded);
                $row->addItem($section);
            }



            $displayFields = $customSection->getFields();

            foreach ($displayFields as $displayField) {
                $item = null;
                $displayFieldName = $displayField['fieldname'];
                $parameters = $displayField['parameters'];
                $classname = isset($parameters['classname']) ? $parameters['classname'] : '';
                $label = isset($parameters['label']) ? $parameters['label'] : null;

                $displayFieldMethod = '_' . $displayFieldName;
                if (method_exists($this, $displayFieldMethod)) {
                    $item = $this->$displayFieldMethod($customSection, $label);
                } else {
                    $field = $this->recordSet->getField($displayFieldName);
                    $value = $this->fieldOutput($field, $this->record, $displayFieldName);
                    if (!isset($label) || empty($label)) {
                        $label = $Crm->translate($field->getDescription());
                    }

                    //$value = $field->output($this->record->$displayFieldName);
                    $item = $this->labelledWidgetOptional(
                        $label,
                        $W->Label($value),
                        $value,
                        $customSection
                    );
                }
                if (isset($item)) {
                    $item->addClass($classname);
                    $section->addItem($item);
                }
            }
        }

        if ($currentColumn + $nbCol> 0) {
            $this->addItem($row);
        }
    }
}


/**
 * A card frame is a frame with contact informations
 *
 */
class crm_CardFrame extends crm_UiObject // extends Widget_Frame
{

    const IMAGE_WIDTH = 40;
    const IMAGE_HEIGHT = 40;


    const MAPS_URL = '//maps.google.com/?q=%s';

    const MAPS_ROUTE_URL = '//maps.google.com/maps?saddr=%s&daddr=%s';

    const STATIC_MAPS_URL = '//maps.google.com/maps/api/staticmap?';

    /**
     * @var Widget_Section[] $sections
     */
    protected $sections = array();


    protected $menu = null;





    public function __construct(Func_Crm $crm, $id = null, Widget_Layout $layout = null)
    {
        parent::__construct($crm);

        $W = bab_Widgets();

        $this->setInheritedItem($W->Frame($id, $layout));
    }


    /**
     * render a labeled string
     *
     * @param	string | Widget_Item	$label
     * @param	string | Widget_Item	$value
     *
     * @return Widget_Item
     */
    protected function labelStr($label, $value)
    {
        $W = bab_Widgets();

        if (!($label instanceOf Widget_Item)) {
            $label = $W->Label($label);
        }

        if (!($value instanceOf Widget_Displayable_Interface)) {
            $value = $W->Label($value);
        }


        return $W->VBoxItems(
            $label->colon(false)->addClass('crm-display-label'),
            $value->addClass('crm-display-value')
        )->setVerticalAlign('middle')->setVerticalSpacing(3, 'px');
    }

    /**
     * render a labeled string only if value set
     *
     * @param	string | Widget_Item	$label
     * @param	string | Widget_Item	$value
     *
     * @return Widget_Item | null
     */
    protected function labelStrSet($label, $value)
    {
        if (!isset($value) || '' === $value) {
            return null;
        }

        return $this->labelStr($label, $value);
    }


    /**
     * render a labeled string with a field value, if the value is not set, return null
     *
     *
     * @param string $label
     * @param crm_Record $record
     * @param ORM_Field $field
     */
    protected function labeledField($label, crm_Record $record, ORM_Field $field)
    {
        $W = bab_Widgets();

        $fieldName = $field->getName();
        $value = $record->$fieldName;

        if (!$field->isValueSet($value)) {
            return null;
        }

        $displayable = $field->output($value);

        switch(true) {
            case ($field instanceof ORM_TextField):
               $displayable = $W->RichText($displayable)->setRenderingOptions(BAB_HTML_ALL ^ BAB_HTML_P);
               break;

            case ($field instanceof ORM_UrlField):
               $displayable = $W->Link($displayable, $displayable);
               break;

            case ($field instanceof ORM_EmailField):
               $displayable = $W->Link($displayable, 'mailto:'.$displayable);
               break;

            case ($field instanceof ORM_FkField):
                $record = $record->$fieldName();
                if (!isset($record)) {
                    return null;
                }
                $displayable = $record->getRecordTitle();
                break;
        }

        return $this->labelStr($label, $displayable);
    }




    /**
     * @return crm_AddressFrame
     */
    protected function AddressFrame(crm_Address $address)
    {
        require_once FUNC_CRM_UI_PATH . 'address.ui.php';
        $addressFrame = new crm_AddressFrame($address);
        return $addressFrame;
    }

    protected function AddressStr($name = null, crm_Address $address)
    {
        if (empty($address->city)) {
            return null;
        }

        $query = '';

        if (!empty($address->street)) {
            $query = str_replace("\n", ' ', $address->street);
        }

        if (isset($name)) {
            $query = $name.', '. $query;
        }

        $query .= $address->postalCode ? ', '.$address->postalCode : '';
        $query .= ', '.$address->city;
        $query .= $address->cityComplement ? ' '.$address->cityComplement : '';
        if ($country = $address->getCountry()) {
            $query .= ', '.$country->getName();
        }

        return $query;
    }

    /**
     * @return Widget_Link | null
     */
    protected function MapLink($name = null, crm_Address $address)
    {
        $query = $this->AddressStr($name, $address);

        if (null === $query) {
            return null;
        }

        $url = sprintf(self::MAPS_URL, urlencode($query));
        $W = bab_Widgets();

        $vbox = $W->VBoxLayout();

        $vbox->addItem($this->MapLinkDisplay($address));

        $vbox->additem($W->Link($W->Icon($this->Crm()->translate('Open in google map'), Func_Icons::STATUS_DIALOG_INFORMATION), $url)->setOpenMode(Widget_Link::OPEN_POPUP));

        if ($GLOBALS['BAB_SESS_USERID'])
        {
            $set = $this->Crm()->ContactSet();
            $set->address();
            $mycontact = $set->get($set->user->is($GLOBALS['BAB_SESS_USERID']));
            if ($mycontact)
            {
                $myaddress = $mycontact->getMainAddress();

                if (!$myaddress->isEmpty())
                {
                    $start = $this->AddressStr(null, $myaddress);

                    if ($start !== $query)
                    {
                        $route_url = sprintf(self::MAPS_ROUTE_URL, urlencode($start), urlencode($query));
                        $vbox->additem($W->Link($W->Icon($this->Crm()->translate('Route from my address'), Func_Icons::ACTIONS_GO_NEXT), $route_url)->setOpenMode(Widget_Link::OPEN_POPUP));
                    }
                }
            }
        }

        return $vbox;
    }


    protected function staticMapUrl(crm_Address $address, $zoom = 15, $width = 400, $height = 300)
    {
        if (empty($address->city)) {
            return null;
        }
        if (empty($address->street)) {
            $zoom = 11;
        } else {
            $query = str_replace("\n", ' ', $address->street);
            $query = str_replace("\r", ' ', $address->street);
        }


        $query = str_replace("\n", ' ', $address->street);
        $query = str_replace("\r", ' ', $address->street);

        $query .= $address->postalCode ? ', '.$address->postalCode : '';
        $query .= ', '.$address->city;
//		$query .= $address->cityComplement ? ' '.$address->cityComplement : '';
        if ($country = $address->getCountry()) {
            $query .= ', '.$country->getName();
        }

        $url = self::STATIC_MAPS_URL . 'zoom=' . $zoom . '&size=' . $width . 'x' . $height . '&maptype=roadmap&sensor=false&markers=color:green|' . urlencode(bab_removeDiacritics($query));
        $url.= isset($GLOBALS['babGoogleApiKey']) ? '&key='.$GLOBALS['babGoogleApiKey'] : '';

        return $url;
    }

    /**
     * @return string | Widget_Item
     */
    protected function MapLinkDisplay(crm_Address $address)
    {
        $W = bab_Widgets();

        return $W->Image(
            $this->staticMapUrl($address)
        );

//		return $W->Icon($this->Crm()->translate('Display on a map'), Func_Icons::STATUS_DIALOG_INFORMATION);
    }






    /**
     * Display address with title and map link in a menu
     *
     * @param	crm_Address	$address
     * @param	string		$title
     * @param	string		$recipient
     *
     * @return Widget_Item
     */
    protected function TitledAddress(crm_Address $address, $title = null, $recipient = null)
    {
        $W = bab_Widgets();

        $addressFrame = $this->AddressFrame($address);

        $layout = $W->VBoxLayout()->SetVerticalSpacing(.3, 'em');

        if ($recipient) {
            $layout->addItem(
                $W->Label($recipient)->addClass('crm-address-recipient')
            );
        }

        $layout->addItem($addressFrame);

        $layout =
            $W->HBoxLayout()->setVerticalAlign('top')->setHorizontalSpacing(1, 'em')
            ->addItem($layout->setSizePolicy(Widget_SizePolicy::MAXIMUM));

        if ($maplink = $this->MapLink(null, $address)) {

            $menu = $W->Menu(null, $W->FlowLayout())
                    ->attachTo($addressFrame)
                    ->addClass('icon-left-16 icon-16x16 icon-left')
                    ->addItem($maplink);

            $layout->addItem($menu);
        }

        if ($title) {
            $layout = $this->LabelStr($title, $layout);
        }
        return $W->Frame(null, $layout->addClass('crm-titled-address'));
    }



    /**
     * Creates section in the editor.
     * If a section with the same header text (title) was already
     * created it is replaced by an empty section.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function addSection($id, $headerText, $layout = null)
    {
        $W = bab_Widgets();
        if (!isset($layout)) {
            $layout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        }
        $this->sections[$id] = $W->Section(
            $headerText,
            $layout
            )->setFoldable(true);

            return $this->sections[$id];
    }

    /**
     * Creates section in the editor.
     * If a section with the same header text (title) was already
     * created it is replaced by an empty section.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function createSection($headerText, $layout = null)
    {
        return $this->addSection($headerText, $headerText, $layout);
    }


    /**
     * Retrieves a section in the editor.
     *
     * @param string $headerText
     * @return Widget_Section
     */
    protected function getSection($id)
    {
        if (!isset($this->sections[$id])) {
            return null;
        }
        return $this->sections[$id];
    }


    /**
     * Returns the list of sections created via createSection().
     *
     * @return Widget_Section[]
     */
    public function getSections()
    {
        return $this->sections;
    }


    /**
     * @param Widget_Displayable_Interface $item
     * @return crm_CardFrame
     */
    public function addAction(Widget_Displayable_Interface $item)
    {
        if (!isset($this->menu)) {
            $W = bab_Widgets();
            $this->menu = $W->Menu()
                ->setLayout($W->FlowLayout())
                ->addClass(Func_Icons::ICON_LEFT_SYMBOLIC);

        }
        $this->menu->addItem($item);
        return $this;
    }
}









/**
 * Actions frame for context panel
 * @see crm_page::addContextItem()
 *
 */
class crm_Actions extends Widget_Frame
{
    public function __construct($id = null)
    {
        $W = bab_Widgets();
        parent::__construct($id, $W->VBoxLayout()->setVerticalSpacing(0.5, 'em'));

        $this->addClass('icon-left-16 icon-left icon-16x16')
            ->addClass('crm-actions');
    }
}















/**
 * Generic frame for notes history
 * the record must have a selectNotes method
 *
 *
 *
 * @param crm_Record $record
 *
 * @return widget_Frame
 */
function crm_recordHistory(crm_Record $record, $title)
{
    require_once dirname(__FILE__) . '/note.class.php';

    $W = bab_Widgets();
    $Crm = crm_Crm();

    $Crm->Ui()->Note();

    // Recent general history
    $historyFrame = $W->VBoxLayout()
//				->addClass('icon-left-16')->addClass('icon-16x16')->addClass('icon-left')
                ->addClass('crm-recent-history')
        ->addItem($W->Title($title, 2));

    $notes = $record->selectNotes();

    $notes->orderDesc($notes->getSet()->targetId->createdOn);

    foreach ($notes as $note) {

        $note = $note->targetId;

        $noteBox = $Crm->HistoryNote();
        $noteBox->setNote($note);

        $historyFrame->addItem($noteBox);
    }

    return $historyFrame;
}








/**
 *
 * @param Widget_FilePickerItem $file
 * @param int                   $width
 * @param int                   $height
 * @return Widget_Icon
 */
function crm_fileAttachementIcon($file, $width, $height)
{
    $Crm = crm_Crm();
    $W = bab_Widgets();
    $T = @bab_functionality::get('Thumbnailer');
    $F = @bab_functionality::get('FileInfos');

    $fileSize = filesize($file->getFilePath()->tostring());
    if ($fileSize > 1024 * 1024) {
        $fileSizeText = round($fileSize / (1024 * 1024), 1) . ' ' . $Crm->translate('MB');
    } elseif ($fileSize > 1024) {
        $fileSizeText = round($fileSize / 1024) . ' ' . $Crm->translate('KB');
    } else {
        $fileSizeText = $fileSize . ' ' . $Crm->translate('Bytes');
    }

    /*

    $imageUrl = null;
    if ($T && $F) {

        $mimetype = $F->getGenericClassName($file->getFilePath()->tostring());

        $T->setSourceFile($file->getFilePath()->tostring());
        $T->setBorder(1, '#aaaaaa', 1, '#ffffff');
        $imageUrl = $T->getThumbnail($width, $height);

    }
    if ($imageUrl) {
        $fileIcon = $W->Icon($file->toString() . "\n" . $fileSizeText);
        $fileIcon->setImageUrl($imageUrl);
    } else {
        if ($F) {
            $mimetype = $F->getGenericClassName($file->tostring());
        } else {
            $mimetype = 'mimetypes-unknown';
        }
        $fileIcon = $W->Icon($file->toString() . "\n" . $fileSizeText, $mimetype);
    }

    */

    $fileIcon = $W->FileIcon($file->toString() . "\n" . $fileSizeText, $file->getFilePath())
        ->setThumbnailSize($width, $height);

    return $fileIcon;
}










/**
 * Table model view with Crm() method
 *
 *
 */
class crm_TableModelView extends widget_TableModelView
{

    /**
     * Filter form reset button
     */
    protected $reset = null;


    /**
     * @var crm_RecordController
     */
    protected $recordController = null;

    /**
     * @param Func_Crm $crm
     * @param string $id
     */
    public function __construct(Func_Crm $crm = null, $id = null)
    {
        parent::__construct(null, $id);
        $this->setCrm($crm);
    }

    /**
     * Forces the Func_Crm object to which this object is 'linked'.
     *
     * @param Func_Crm	$crm
     * @return crm_RecordSet
     */
    public function setCrm(Func_Crm $crm = null)
    {
        $this->crm = $crm;
        return $this;
    }

    /**
     * Get CRM object to use with this SET
     *
     * @return Func_Crm
     */
    public function Crm()
    {
        if (!isset($this->crm)) {
            // If the crm object was not specified (through the setCrm() method)
            // we try to select one according to the classname prefix.
            list($prefix) = explode('_', get_class($this));
            $functionalityName = ucwords($prefix);
            $this->crm = @bab_functionality::get('Crm/' . $functionalityName);
            if (!$this->crm) {
                $this->crm = @bab_functionality::get('Crm');
            }
        }
        return $this->crm;
    }


    /**
     * @param crm_RecordController $recordController
     * @return crm_TableModelView
     */
    public function setRecordController(crm_RecordController $recordController)
    {
        $this->recordController = $recordController;
        return $this;
    }


    /**
     * @return crm_RecordController
     */
    public function getRecordController()
    {
        return $this->recordController;
    }


    /**
     * @param crm_RecordSet $recordSet
     */
	protected function addCustomFields(crm_RecordSet $recordSet)
	{
		$customFields = $recordSet->getCustomFields();

        foreach ($customFields as $customField) {
            $fieldname = $customField->fieldname;
            $this->addColumn(
                widget_TableModelViewColumn($recordSet->$fieldname, $customField->name)
                  ->setSortable(true)
                  ->setExportable(true)
                  ->setSearchable($customField->searchable)
				  ->setVisible($customField->visible)
            );
        }
	}


    /**
     * Get a generic filter panel
     * Use setPageLength to define the default number of items per page
     *
     * @param	array	$filter		Filter values
     * @param	string	$name		filter form name
     * @param	string	$anchor		anchor in destination page of filter, the can be a Widget_Tab id
     *
     * @return Widget_Filter
     */
    public function filterPanel($filter = null, $name = null)
    {
        $W = bab_Widgets();

        $filterPanel = $W->Filter();
        $filterPanel->setLayout($W->VBoxLayout());
        if (isset($name)) {
            $filterPanel->setName($name);
        }

        $pageLength = $this->getPageLength();
        if (null === $pageLength) {
            $pageLength = 15;
        }

        $pageNumber = $this->getCurrentPage();
        if (null === $pageNumber) {
            $pageNumber = 0;
        }

        $this->setPageLength(isset($filter['pageSize']) ? $filter['pageSize'] : $pageLength);
        $this->setCurrentPage(isset($filter['pageNumber']) ? $filter['pageNumber'] : $pageNumber);

        if (isset($name)) {
            $this->sortParameterName = $name . '[filter][sort]';
        } else {
            $this->sortParameterName = 'filter[sort]';
        }

        if (isset($filter['sort'])) {
            $this->setSortField($filter['sort']);
        } elseif (!isset($this->sortField)) {

            if (method_exists($this, 'getDefaultSortField'))
            {
                $this->setSortField($this->getDefaultSortField());
            } else {
                $columns = $this->getVisibleColumns();
                list($sortField) = each($columns);
                $this->setSortField($sortField);
            }
        }

        $form = $this->getFilterForm();

        if (isset($filter)) {
            if (isset($name)) {
                $path = array($name, 'filter');
            } else {
                $path = array('filter');
            }
            $form->setValues($filter, $path);
        }


        $filterPanel->setFilter($form);
        $filterPanel->setFiltered($this);

        return $filterPanel;
    }


    /**
     * Add the filter fields to the filter form
     * @param Widget_Form $form
     *
     */
    protected function handleAdvancedFilterFields(Widget_Item $form)
    {

    }




    protected function isFilterFieldSpecified($filter, $fieldPath)
    {
        if (isset($filter[$fieldPath])) {
            $value = $filter[$fieldPath];
            if (is_array($value)) {
                foreach ($value as $val) {
                    $val = trim($val);
                    if (!empty($val)) {
                        return true;
                    }
                }
                return false;
            }

            if (trim($value) !== '') {
                return true;
            }
        }

        return false;
    }



    /**
     * Handle label and input widget merge in one item before adding to the filter form
     * default is a vertical box layout
     *
     * @param Widget_Label                  $label
     * @param Widget_Displayable_Interface  $input
     * @return Widget_Item
     */
    protected function handleFilterLabel(Widget_Label $label, Widget_Displayable_Interface $input)
    {
        $W = bab_Widgets();
        if ($input instanceof Widget_CheckBox) {
            return $W->HBoxItems(
                $input,
                $label
            )->setVerticalAlign('middle')
            ->setHorizontalSpacing(1, 'ex');
        }
        return $W->VBoxItems(
            $label,
            $input
        );
    }

    protected function getSearchItem()
    {
        $W = bab_Widgets();

        return $W->LineEdit()->setName('search')->addClass('widget-100pc');
    }

    /**
     * Get an advanced form filter
     * @see Widget_Filter
     *
     * @param   string          $id
     * @param   array           $filter
     *
     * @return Widget_Form
     */
    public function getAdvancedFilterForm($id = null, $filter = null)
    {
        $Crm = $this->Crm();

        $W = bab_Widgets();

        $formItem = $this->getSearchItem();

        $activeFiltersFrame = $W->Frame(
            null,
            $W->FlowItems(
                $W->VBoxItems(
                    $W->Label($Crm->translate('Search'))->setAssociatedWidget($formItem),
                    $formItem
                )->setSizePolicy('col-lg-2 col-md-3 col-sm-6 col-xs-12')
            )
            ->setHorizontalSpacing(1, 'em')
            ->setVerticalSpacing(1, 'em')
            ->setVerticalAlign('bottom')
        );

        $advancedFiltersFrame = $W->Section(
            $Crm->translate('Advanced filters'),
            $W->FlowLayout()
                ->setHorizontalSpacing(1, 'em')
                ->setVerticalSpacing(1, 'em')
                ->setVerticalAlign('bottom'),
            7
        )->setFoldable(true, true)
        ->setSizePolicy('row');


        $columns = $this->getVisibleColumns();

        foreach ($columns as $fieldName => $column) {
            $field = $column->getField();
            if (! $column->isSearchable()) {
                continue;
            }

            if (! ($field instanceof ORM_Field)) {
                $field = null;
            }

            $label = $this->handleFilterLabelWidget($fieldName, $field);
            $input = $this->handleFilterInputWidget($fieldName, $field);


            if (isset($input) && isset($label)) {

                $input->setName($fieldName);
                $label->setAssociatedWidget($input);

                $input->addClass('widget-100pc');

                $formItem = $this->handleFilterLabel($label, $input);
                $formItem->setSizePolicy('col-lg-2 col-md-3 col-sm-6 col-xs-12');
                $formItem->addClass('field_' . $fieldName);

                $mainSearch = (method_exists($column, 'isMainSearch') && $column->isMainSearch());

                if ($mainSearch || $this->isFilterFieldSpecified($filter, $column->getFieldPath())) {
                    $activeFiltersFrame->addItem($formItem);
                } else {
                    $advancedFiltersFrame->addItem($formItem);
                }
            }
        }


        if (! $this->submit) {
            $this->submit = $W->SubmitButton();
            $this->submit->setLabel(widget_translate('Filter'));
        }



        if ($controller = $this->getRecordController()) {
            $proxy = $controller->proxy();
            $this->reset = $W->Link(
                $Crm->translate('Reset'),
                $proxy->resetFilters()
            )->addClass('icon', Func_Icons::ACTIONS_VIEW_REFRESH, 'widget-actionbutton')
            ->setTitle($Crm->translate('Reset filter to default values'))
            ->setAjaxAction();

            $this->save = $W->Link(
                $Crm->translate('Save'),
                $proxy->confirmSaveCurrentFilter()
            )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_SAVE, 'widget-actionbutton')
            ->setTitle($Crm->translate('Save current filter'))
            ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD);

            $this->select = null;

            $filterNames = $controller->getFilterNames();
            if (count($filterNames) > 0) {
                $this->select = $W->Menu(null, $W->VBoxItems());
                $this->select->setButtonClass('widget-link icon actions-list-filter');
                $this->select->setButtonLabel($Crm->translate('Saved filters'));
                $this->select->addClass(Func_Icons::ICON_LEFT_16);


                foreach ($filterNames as $filterName) {
                    $filter = $W->getUserConfiguration($controller->getModelViewDefaultId() . '/filters/' . $filterName, 'widgets', false);

                    $this->select->addItem(
                        $W->Link(
                            $filterName,
                            $proxy->setCurrentFilterName($filterName)
                        )->setTitle($filter['description'])
                    );
                }
                $this->select->addSeparator();
                $this->select->addItem(
                    $W->Link(
                        $Crm->translate('Manage filters'),
                        $proxy->manageFilters()
                    )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_PROPERTIES)
                    ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                );
            }
        }


//         $this->showTotal = $W->LabelledWidget(
//             $Crm->translate('Show total/subtotals'),
//             $W->CheckBox(),
//             'showTotal'
//         );

//         $activeFiltersFrame->addItem($this->showTotal);


        $form = $W->Form($id);
        $form->setReadOnly(true);
        $form->setName('filter');
        $form->colon();

        $form->setLayout(
            $W->VBoxItems(
                $activeFiltersFrame
                    ->setSizePolicy('row'),
                $buttonsBox = $W->FlowItems(
                    $advancedFiltersFrame,
                    $this->submit,
                    $W->FlowItems(
                        $this->reset,
                        $this->save,
                        $this->select
                    )->setHorizontalSpacing(0.5, 'em')
                )->setHorizontalSpacing(2, 'em')
                ->setSizePolicy('row')
            )->setVerticalSpacing(1, 'em')
            ->setVerticalAlign('bottom')
        );

        $form->setHiddenValue('tg', $Crm->controllerTg);
        $form->setAnchor($this->getAnchor());
        $form->addClass(Func_Icons::ICON_LEFT_16);


//         if ($this->isColumnSelectionAllowed()) {
//             $columnSelectionMenu = $this->columnSelectionMenu($this->columns);
//             $buttonsBox->addItem($columnSelectionMenu);
//         }

        return $form;
    }


    /**
     * Get a advanced filter panel
     * Use setPageLength to define the default number of items per page
     *
     * @param	array	$filter		Filter values
     * @param	string	$name		filter form name
     * @param	string	$anchor		anchor in destination page of filter, the can be a Widget_Tab id
     *
     * @return Widget_Filter
     */
    public function advancedFilterPanel($filter = null, $name = null)
    {
        $W = bab_Widgets();

        $filterPanel = $W->Filter();
        $filterPanel->setLayout($W->VBoxLayout());
        if (isset($name)) {
            $filterPanel->setName($name);
        }

        $pageLength = $this->getPageLength();
        if (null === $pageLength) {
            $pageLength = 15;
        }

        $pageNumber = $this->getCurrentPage();
        if (null === $pageNumber) {
            $pageNumber = 0;
        }

        $this->setPageLength(isset($filter['pageSize']) ? $filter['pageSize'] : $pageLength);
//        $this->setCurrentPage(isset($filter['pageNumber']) ? $filter['pageNumber'] : $pageNumber);

        if (isset($name)) {
            $this->sortParameterName = $name . '[filter][sort]';
        } else {
            $this->sortParameterName = 'filter[sort]';
        }

        if (isset($filter['sort'])) {
            $this->setSortField($filter['sort']);
        } elseif (!isset($this->sortField)) {

            if (method_exists($this, 'getDefaultSortField'))
            {
                $this->setSortField($this->getDefaultSortField());
            } else {
                $columns = $this->getVisibleColumns();
                list($sortField) = each($columns);
                $this->setSortField($sortField);
            }
        }

        $form = $this->getAdvancedFilterForm(null, $filter);

        if (isset($filter)) {
            if (isset($name)) {
                $path = array($name, 'filter');
            } else {
                $path = array('filter');
            }
            $form->setValues($filter, $path);
        }

        $filterPanel->setFilter($form);
        $filterPanel->setFiltered($this);

        return $filterPanel;
    }
}





/**
 * Cards model view with Crm() method
 *
 *
 */
class crm_CardsModelView extends crm_TableModelView
{

}




/**
 * Creates a specific filter panel containing the table and an auto-generated form.
 *
 * @param Func_Crm                     $Crm
 * @param crm_TableModelView           $tableview
 * @param Widget_Displayable_Interface $formContent
 * @param array                        $filter
 * @param string                       $name
 * @return Widget_Filter
 */
function crm_filteredTableView(Func_Crm $Crm, crm_TableModelView $tableview, Widget_Displayable_Interface $formContent, $filterValues = null, $name = null)
{
    $W = bab_Widgets();

    $filterPanel = $W->Filter();
    $filterPanel->setLayout($W->VBoxLayout());
    if (isset($name)) {
        $filterPanel->setName($name);
    }

    if (isset($filterValues['pageSize'])) {
        $tableview->setPageLength($filterValues['pageSize']);
    } else if ($tableview->getPageLength() === null) {
        $tableview->setPageLength(12);
    }

    $tableview->setCurrentPage(isset($filterValues['pageNumber']) ? $filterValues['pageNumber'] : 0);

    $tableview->sortParameterName = $name . '[filter][sort]';

    if (isset($filterValues['sort'])) {
        $tableview->setSortField($filterValues['sort']);
    } else {
        $columns = $tableview->getVisibleColumns();
        foreach ($columns as $sort => $column) {
            if ($sortField = $column->getField()) {
                $tableview->setSortField($sort);
                break;
            }
        }
    }


    if ($formContent instanceof Widget_Form) {
        $form = $formContent;
    } else {
        $form = $W->Form()->setLayout($W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $form->setReadOnly(true);
        $form->setName('filter');
        $form->colon();
        $form->addItem($formContent);

        $form->addItem($W->SubmitButton()->setLabel($Crm->translate('Filter')));
    }


    if (isset($filterValues) && is_array($filterValues)) {
        $form->setValues($filterValues, array($name, 'filter'));
    }
    $form->setHiddenValue('tg', bab_rp('tg'));
    $form->setHiddenValue('idx', bab_rp('idx'));

    $filterPanel->setFilter($form);
    $filterPanel->setFiltered($tableview);

    return $filterPanel;
}



/**
 * @param string|ORM_Field $field
 * @param string|null $description
 *
 * @return widget_TableModelViewColumn
 */
function crm_TableModelViewColumn($field, $description = null)
{
    if (null === $field) {
        return null;
    }

    if (!isset($description) && $field instanceof ORM_Field) {
        $Crm = $field->getParentSet()->Crm();
        $description = $Crm->translate($field->getDescription());
    }

    return new widget_TableModelViewColumn($field, $description);
}
