<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__).'/discountbase.ui.php';


/**
 * list of discount from back office
 *
 */
class crm_DiscountTableView extends crm_TableModelView
{
	
	public function addDefaultColumns(crm_DiscountSet $set)
	{
		$Crm = $this->Crm();
		
		$this->addColumn(widget_TableModelViewColumn($set->description, $Crm->translate('Description')));
		$this->addColumn(widget_TableModelViewColumn($set->start_date, $Crm->translate('Start date'))->setSearchable(false));
		$this->addColumn(widget_TableModelViewColumn($set->end_date, $Crm->translate('Expiration date')));
		$this->addColumn(widget_TableModelViewColumn($set->threshold, $Crm->translate('Threshold')) );
		$this->addColumn(widget_TableModelViewColumn($set->shipping, $Crm->translate('On shipping'))->setVisible(false) );
		
	}
	
	
	
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(crm_Discount $record, $fieldPath)
	{
		$W = bab_Widgets();
		$Crm = $record->Crm();
		$Ui = $Crm->Ui();
		
		/*@var $Ui crm_Ui */
		
		$displayAction = $Crm->Controller()->Discount()->display($record->id);
		
		if ('description' === $fieldPath)
		{
			return $W->Link(
				$record->description,
				$displayAction
			);
		}
		
		
		return parent::computeCellContent($record, $fieldPath);
	}

}




/**
 * @return Widget_Form
 */
class crm_DiscountEditor extends crm_DiscountBaseEditor
{
	
	protected $suggestArticlePackaging = null;
	

	public function __construct(Func_Crm $crm, crm_Discount $discount = null)
	{
	    
		parent::__construct($crm, $discount);
		
		$this->setName('discount');

		if (isset($discount)) {
			$values = $this->getDiscountValues();
			$this->setValues($values, array('discount'));
			$this->setHiddenValue('discount[id]', $discount->id);
		}
	}


	


	/**
	 * @return Array
	 */
	protected function getDiscountValues()
	{
	    if (null === $this->discount)
	    {
	        return array();
	    }
	
	
	    $values = $this->discount->getFormOutputValues();
	
	    $values['opt'] = 'total';
	
	    if ($this->discount->shipping) {
	        $values['opt'] = 'shipping';
	    }
	
	    return $values;
	}
}




/**
 * Display discount
 *
 */
class crm_DiscountDisplay extends crm_DiscountBaseDisplay {


	

}