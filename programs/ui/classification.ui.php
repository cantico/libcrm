<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 *
 * @param Func_Crm		$Crm
 * @param crm_Deal		$deal 		The deal
 *
 * @return Widget_FlowLayout
 */
function crm_classificationPreview(Func_Crm $Crm, crm_Deal $deal)
{
    $W = bab_Widgets();

    $Crm = $deal->Crm();

    $Crm->includeClassificationSet();

    $dealClassificationSet = $Crm->DealClassificationSet();
    $dealClassificationSet->join('classification');
    $dealClassifications = $dealClassificationSet->selectForDeal($deal->id);

    $classifications = $W->FlowLayout();

    foreach ($dealClassifications as $dealClassification) {
        /* @var $dealClassification crm_DealClassification */

        $classificationLabel = $W->Label($dealClassification->classification->name)
                                    ->setTitle(implode(' > ', $dealClassification->classification->getPathname()))
                                    ->addClass('crm-codification');
        $classifications->addItem($classificationLabel);
    }

    return $classifications;
}



/**
 * @return crm_Editor
 *
 * @param Func_Crm		$Crm
 * @param crm_Deal		$deal 		The deal
 */
function crm_validatorSelector(Func_Crm $Crm, crm_Deal $deal)
{
    require_once dirname(__FILE__) . '/classification.class.php';

    $Crm = $deal->Crm();
    $Crm->Ui()->includeDeal();
    $W = bab_Widgets();

    $validatorSelector = new crm_Editor($Crm);

    $validatorSelector->addItem($W->UserPicker()->setName('validator'));

    $validatorSelector->setHiddenValue('tg', bab_rp('tg'));
    $validatorSelector->setHiddenValue('deal', $deal->id);

    return $validatorSelector;
}





/**
 * @param Func_Crm		$Crm
 *
 * @return widget_SimpleTreeView
 */
function crm_classificationEditor(Func_Crm $Crm)
{
    $Crm->Ui()->includeDeal();

    $W = bab_Widgets();


//	$root = crm_Classification(1);

    $treeview = $W->SimpleTreeView();

    $set = $Crm->ClassificationSet();
    $nodes = $set->select($set->id_parent->greaterThan('0'));

    $element = $treeview->createElement('1', '', $Crm->translate('Classifications'), '', '');
    $element->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/category.png');
    $treeview->appendElement($element, null);
    $element->addAction(
        'appendChild',
        $Crm->translate('Add subclassification'),
        $GLOBALS['babSkinPath'] . 'images/Puces/edit_add.png',
        $Crm->Controller()->Admin()->addClassification('1')->url(),
        ''
    );

    foreach ($nodes as $node) {
        $element = $treeview->createElement(
            $node->id,
            '',
            $node->name,
            '',
            $Crm->Controller()->Admin()->editClassification($node->id)->url()
        );
        $element->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/folder.png');

        $element->addAction(
            'appendChild',
            $Crm->translate('Add subclassification'),
            $GLOBALS['babSkinPath'] . 'images/Puces/edit_add.png',
            $Crm->Controller()->Admin()->addClassification($node->id)->url(),
            ''
        );
        $element->addAction(
            'delete',
            $Crm->translate('Delete'),
            $GLOBALS['babSkinPath'] . 'images/Puces/delete.png',
            $Crm->Controller()->Admin()->proposeDeleteClassification($node->id)->url(),
            ''
        );
        $parentId = $node->id_parent;
        $treeview->appendElement($element, $parentId);
    }

    return $treeview;
}

/**
 * @param Func_Crm		$Crm
 * @param array	$classifications
 *
 * @return crm_Editor
 */
function crm_classificationSelector(Func_Crm $Crm, $classifications)
{
    $Crm->Ui()->includeDeal();

    $classificationSelector = new crm_Editor($Crm);

    $W = bab_Widgets();

    $set = $Crm->ClassificationSet();

    $root = $set->get(1);

    $treeview = $W->SimpleTreeView();

//	$set = new crm_ClassificationSet();
    $nodes = $set->select();

    foreach ($nodes as $node) {
        $element = $treeview->createElement($node->id, '', $node->name, '', '');
        $element->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/folder.png');

        $selected = array_key_exists($node->id, $classifications);

        $element->addCheckBox('classifications[' . $node->id . ']', $selected);
        $parentId = ($node->id_parent == 1) ? null : $node->id_parent;
        $treeview->appendElement($element, $parentId);
    }

    $classificationSelector->addItem($treeview);


    $classificationSelector->setHiddenValue('tg', bab_rp('tg'));


    return $classificationSelector;
}


