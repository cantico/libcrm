<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


class crm_shopTermsEditor extends crm_Editor
{
	public function __construct(Func_Crm $Crm)
	{
		parent::__construct($Crm);
		$this->setName('terms');
		$this->colon();
	
		$this->setHiddenValue('tg', bab_rp('tg'));
	
		$this->addFields();
		$this->addButtons();
	}
	
	
	protected function addFields()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		$addonname = $Crm->getAddonName();
		
		if (!$addonname)
		{
			throw new ErrorException('failed to get the addon name');
		}
		
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory("/$addonname/configuration/");
		

	
		$this->addItem($W->RichText($registry->getValue('terms'))
		        ->setRenderingOptions(BAB_HTML_ENTITIES | BAB_HTML_P | BAB_HTML_BR | BAB_HTML_TAB)
		        ->addClass('crm-shop-terms'));
	
		$this->addItem($this->labelledField(
				$Crm->translate('Accept the terms and conditions'),
				$W->Checkbox(),
				'accept'
		));
	}
	
	
	protected function addButtons()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		$this->addItem(
			$W->SubmitButton()
				->setLabel($Crm->translate('Continue'))
				->setAction($Crm->Controller()->ShoppingCart()->confirmMyCart())
				->setSuccessAction(crm_BreadCrumbs::lastAction())
				->setFailedAction(crm_BreadCrumbs::getPosition(-1))
		);
	}
}