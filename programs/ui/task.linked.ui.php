<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * Get a frame with parent task if exists
 * @param crm_Task $task
 *
 * @return widget_Frame
 */
function crm_taskLinkedAncestor(crm_Task $task)
{
    $W = bab_Widgets();
    $Crm = $task->Crm();
    $Ui = $Crm->Ui();
    
    $parent = $task->getScalarValue('parent');
    
    if (!$parent) {
        return null;
    }

    $frame = $W->Frame();
    $parentTask = $task->parent();
    
    $frame->addItem(
        $W->Title($Crm->translate('Set of tasks'), 4)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
    );
    $frame->addItem($Ui->TaskCardFrame($parentTask));

    return $frame;
}



/**
 * Frame with contacts linked to a task
 * 
 * @param crm_Task $task
 *
 * @return widget_Frame
 */
function crm_taskLinkedContacts(crm_Task $task)
{
    $W = bab_Widgets();
    $Crm = $task->Crm();
    $Ui = $Crm->Ui();
    
    $contacts = $task->getLinkedContacts();
    
    
    if (0 === count($contacts)) {
        return null;
    }
    
    $frame = $W->Frame();
    
    $frame->addItem(
        $W->Title($Crm->translate('Contacts linked to this task'), 4)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
    );

    foreach ($contacts as $contact) {
        $contactFrame = $W->VBoxLayout()->setVerticalSpacing(1, 'em')->addClass('crm-card');
        $frame->addItem($contactFrame);
        /* @var $contact crm_Contact */
        $card = $Ui->contactCardFrame($contact);
        $contactFrame->addItem($card);

    }
    
    return $frame;
}





/**
 * Frame with deals linked to a task
 *
 * @param crm_Task $task
 * @param Array &$orgs
 *
 * @return widget_Frame
 */
function crm_taskLinkedDeals(crm_Task $task, &$orgs)
{
    $W = bab_Widgets();
    $Crm = $task->Crm();
    $Ui = $Crm->Ui();
    $Access = $Crm->Access();
    
    
    $deals = $task->getLinkedDeals();
    if (0 === count($deals) > 0) {
        return null;
    }
    
    $frame = $W->Frame();
    
    $frame->addItem(
        $W->Title($Crm->translate('Deals linked to this task'), 4)
        ->setSizePolicy(Widget_SizePolicy::MAXIMUM)
    );

    foreach ($deals as $deal) {


        /* @var $deal crm_Deal */
        $card = new crm_DealCardFrame($Crm, $deal);
        $dealFrame = $W->VBoxLayout()->setVerticalSpacing(1, 'em')->addClass('crm-card');
        $frame->addItem($dealFrame);

        $dealFrame->addItem($card);


        $contact = $deal->referralContact();

        $contactsLayout = $W->VBoxLayout()->setVerticalSpacing(2, 'px');

        if ($contact) {

            $contactsLayout->addItem($W->Label($Crm->translate('Referral contact'))->addClass('crm-display-label '));

            $contactCard = $Ui->ContactCardFrame(
                $contact,
                crm_ContactCardFrame::SHOW_ALL & ~(crm_ContactCardFrame::SHOW_ORGANIZATION | crm_ContactCardFrame::SHOW_NAME | crm_ContactCardFrame::SHOW_POSITION | crm_ContactCardFrame::SHOW_PHOTO)
            );

            $contactSection = $W->Section(
                $contact->getFullName(),
                $contactCard,
                6
            )->setFoldable(true, true);
            $contactSection->setSubHeaderText($contact->getMainPosition());
            $contactSection->addClass('crm-team-member');

            $contactInfo = $contactSection->addContextMenu('inline')
            ->addItem($Ui->ContactPhoto($contact, 24, 24));

            $contactMenu = $contactSection->addContextMenu('popup');

            $contactMenu->addItem(
                $W->Link(
                    $W->Icon($Crm->translate('View contact page'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                    $Crm->Controller()->Contact()->display($contact->id)
                )
            );
            if ($Access->updateContact($contact)) {
                $contactMenu->addItem(
                    $W->Link(
                        $W->Icon($Crm->translate('Edit contact'), Func_Icons::ACTIONS_DOCUMENT_EDIT),
                        $Crm->Controller()->Contact()->edit($contact->id)
                    )
                );
            }
            if ($Access->emailContact($contact) && ($email = $contact->getMainEmail())) {
                $contactMenu->addItem(
                    $W->Link(
                        $W->Icon($Crm->translate('Send an email'), Func_Icons::ACTIONS_MAIL_SEND),
                        $Crm->Controller()->Email()->edit($contact->getMainEmail(), null, null, $contact->getRef() . ',' . $deal->getRef())
                    )
                );
            }

            $contactsLayout->addItem($contactSection);

        }
        $dealFrame->addItem($contactsLayout);

        $displayLink = $W->Link(
            $W->Icon($Crm->translate('Detail'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
            $Crm->Controller()->Deal()->display($deal->id)
        );

        $lead = $deal->lead();
        if (isset($lead)) {
            $orgs[] = $lead;
        }
        
        return $frame;
    }
   
}










/**
 * Frame with organizations linked to a task
 *
 * @param crm_Task $task
 * @param Array $orgs   additional organizations
 *
 * @return widget_Frame
 */
function crm_taskLinkedOrganizations(crm_Task $task, $orgs)
{
    $W = bab_Widgets();
    $Crm = $task->Crm();
    $Ui = $Crm->Ui();
    $Ui->includeOrganization();
    $Access = $Crm->Access();
    
    $orgs += $task->getLinkedOrganizations();
    if (0 === count($orgs)) {
        return null;
    }
    
    $objectsFrame = $W->Frame();
    
    $objectsFrame->addItem(
        $W->Title($Crm->translate('Organizations linked to this task'), 4)
        ->setSizePolicy(Widget_SizePolicy::MAXIMUM)
    );

    foreach ($orgs as $org) {
        /* @var $org crm_Organization */

        $card = $Ui->OrganizationCardFrame($org);

        $organizationFrame = $W->VBoxLayout()->setVerticalSpacing(1, 'em')->addClass('crm-card');
        $objectsFrame->addItem($organizationFrame);

        $organizationFrame->addItem($card);

        $orgContacts = $org->selectContacts();

        $contactsLayout = $W->Section(
            $Crm->translate('Contacts in this organization'),
            $W->VBoxLayout()->setVerticalSpacing(2, 'px'),
            4
        )->setFoldable(true, true);

        foreach ($orgContacts as $orgContact) {

            $contact = $orgContact->getContact();

            $contactCard = $Ui->ContactCardFrame(
                $contact,
                crm_ContactCardFrame::SHOW_ALL & ~(crm_ContactCardFrame::SHOW_ORGANIZATION | crm_ContactCardFrame::SHOW_NAME | crm_ContactCardFrame::SHOW_POSITION | crm_ContactCardFrame::SHOW_PHOTO)
            );

            $contactSection = $W->Section(
                $contact->getFullName(),
                $contactCard,
                6
            )->setFoldable(true, true);
            $contactSection->setSubHeaderText($contact->getMainPosition());
            $contactSection->addClass('crm-team-member');

            $contactInfo = $contactSection->addContextMenu('inline')
            ->addItem($Ui->ContactPhoto($contact, 24, 24));

            $contactMenu = $contactSection->addContextMenu('popup');

            $contactMenu->addItem(
                $W->Link(
                    $W->Icon($Crm->translate('View contact page'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                    $Crm->Controller()->Contact()->display($contact->id)
                )
            );
            if ($Access->updateContact($contact)) {
                $contactMenu->addItem(
                    $W->Link(
                        $W->Icon($Crm->translate('Edit contact'), Func_Icons::ACTIONS_DOCUMENT_EDIT),
                        $Crm->Controller()->Contact()->edit($contact->id)
                    )
                );
            }
            if ($Access->emailContact($contact) && ($email = $contact->getMainEmail())) {
                $contactMenu->addItem(
                    $W->Link(
                        $W->Icon($Crm->translate('Send an email'), Func_Icons::ACTIONS_MAIL_SEND),
                        $Crm->Controller()->Email()->edit($contact->getMainEmail(), null, null, $contact->getRef() . ',' . $org->getRef())
                    )
                );
            }

            /* @var $contact crm_Contact */
            // 				$contactCard = $Crm->Ui()->contactCardFrame($contact, crm_ContactCardFrame::SHOW_ALL & ~(crm_ContactCardFrame::SHOW_NAME | crm_ContactCardFrame::SHOW_POSITION | crm_ContactCardFrame::SHOW_ORGANIZATION));
            // 				$address = $contact->getMainAddress();
            $contactsLayout->addItem($contactSection);

        }
        $organizationFrame->addItem($contactsLayout);
    }
    
    return $objectsFrame;
}




