<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




bab_Widgets()->includePhpClass('widget_TableView');


/**
 * List of catalog for administration
 *
 */
class crm_CatalogTableView extends crm_TableModelView
{
	

	public function setDataSource(ORM_Iterator $iterator)
	{
		$this->addClass('icon-left-16 icon-16x16 icon-left');
		return parent::setDataSource($iterator);
	}
	
	
	public function addDefaultColumns(crm_CatalogSet $set)
	{
		$Crm = $this->Crm();
		
		$this->addColumn(widget_TableModelViewColumn('image', '')->addClass('widget-column-thin')->addClass('widget-column-center'));
		$this->addColumn(widget_TableModelViewColumn($set->sortkey, $Crm->translate('Full pathname'))->setSearchable(false));
		$this->addColumn(widget_TableModelViewColumn($set->name, $Crm->translate('Name'))->setSearchable(true)->setVisible(false));
		$this->addColumn(widget_TableModelViewColumn($set->description, $Crm->translate('Description')));
		$this->addColumn(widget_TableModelViewColumn('articles', $Crm->translate('Articles'))->setSearchable(false));
		// $this->addColumn(widget_TableModelViewColumn('refcount', $Crm->translate('Visible articles in sub-tree'))->setVisible(false)->setSearchable(false));
		$this->addColumn(widget_TableModelViewColumn($set->disabled, $Crm->translate('Disabled'))->setVisible(false));
		$this->addColumn(widget_TableModelViewColumn($set->catalog_displaymode, $Crm->translate('Categories display'))->setVisible(false)->setSearchable(false));
		$this->addColumn(widget_TableModelViewColumn($set->catalogitem_displaymode, $Crm->translate('Products display'))->setVisible(false)->setSearchable(false));
	}


	/**
	 *
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item	The item that will be placed in the cell
	 */
	protected function computeCellContent(crm_Catalog $record, $fieldPath)
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		$Ui = $Crm->Ui();
		
		$displayAction = $Crm->Controller()->Catalog()->display($record->id);
		
		if ('image' === $fieldPath)
		{
			return $W->Link(
				$Ui->CatalogPhoto($record, 22, 22),
				$displayAction
			);
		}
		
		if ('sortkey' === $fieldPath)
		{
			$ancestors = $record->getAncestors();
			
			$path = array();
			foreach($ancestors as $ancestor)
			{
				$path[] = $ancestor->name;
			}
			
			return $W->Link(implode(' > ' , $path), $displayAction);
		}

		if ('name' === $fieldPath) {

			$value = self::getRecordFieldValue($record, $fieldPath);
			return $W->Link(
				$value,
				$displayAction
			);
		}

		if ('articles' === $fieldPath) {
			return $W->Label(sprintf($Crm->translate('%d articles'), $record->getArticles()->count()));
		}
		
		if ('refcount' === $fieldPath) {
			return $W->Label(sprintf($Crm->translate('%d articles'), $record->refcount));
		}
		
		if ('description' === $fieldPath) {
			return $W->Label(bab_abbr($record->description, BAB_ABBR_FULL_WORDS, 170));
		}
		
		if ('disabled' === $fieldPath) {
			return $record->disabled ? $W->Label($Crm->translate('Yes')) : $W->Label($Crm->translate('No'));
		}

		return parent::computeCellContent($record, $fieldPath);
	}
	
	
	
	protected function handleRow(crm_Catalog $record, $row)
	{
		if ($record->disabled)
		{
			$this->addRowClass($row, 'disabled');
		}
		
		if (0 === (int) $record->refcount && !$record->getParentSet()->displayEmpty())
		{
			$this->addRowClass($row, 'empty');
		}
	
		return parent::handleRow($record, $row);
	}

}











/**
 *
 * @param crm_Catalog 	$catalog
 * @param int			$width
 * @param int			$height
 * @param bool			$border
 * @return Widget_Icon
 */
function crm_catalogPhoto(crm_Catalog $catalog, $width, $height)
{
	$W = bab_Widgets();

	$path = $catalog->getPhotoPath();

	if (null === $path)
	{
		require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
		$addon = bab_getAddonInfosInstance('LibCrm');
		$path = new bab_Path($addon->getStylePath() . 'images/photo-default.png');
	} 
	
	$image = $W->ImageThumbnail($path)->setThumbnailSize($width, $height);

	$image->setTitle($catalog->name);
	$image->addClass('crm-element-image small');

	return $image;
}








/**
 * @return Widget_Form
 */
class crm_CatalogEditor extends crm_MetaEditor
{
	/**
	 * @var crm_Catalog
	 */
	protected $catalog;


	public function __construct(Func_Crm $crm, crm_Catalog $catalog = null)
	{
		parent::__construct($crm);

		$this->setHiddenValue('tg', bab_rp('tg'));
		$this->setName('catalog');
		$this->catalog = $catalog;

		$this->colon();
		$this->addFields();
		
		if (isset($_POST['catalog']))
		{
			$this->setValues(array('catalog' => $_POST['catalog']));
			
		} else if (isset($catalog))
		{
			$values = $catalog->getValues();
			$this->setValues(array('catalog' => $values));
		} else {
			$this->setValues(array('catalog' => array(
					'catalog_displaymode' => 0,
					'catalogitem_displaymode' => 0
			)));
		}
		

		$W = $this->widgets;
	}



	/**
	 * Add a default field set to form
	 *
	 *
	 */
	protected function addFields()
	{
		$W = bab_Widgets();

		$this->addItem($W->Hidden()->setName('id'));

		$this->addItem(
			$W->HBoxItems(
				$W->VBoxItems(
					$this->Name(),
					$this->Description(),
					$this->Disabled(),
					$this->parentcatalog(),
					$this->catalog_displaymode(),
					$this->catalogitem_displaymode() 
				)->setSpacing(1, 'em'),
				$this->Photo()
			)->setHorizontalSpacing(5, 'em')
		);
		
		
		$this->addItem(
			$this->MetaSection()		
		);


		$this->addItem($W->HBoxItems(
			$this->Save(),
			$this->Cancel()
		)->setHorizontalSpacing(1,'em'));
	}

	
	
	protected function catalog_displaymode()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		return $this->labelledField(
				$Crm->translate('Categories display'),
				$W->RadioSet()
					->setHorizontalView()
					->setOptions(crm_CatalogSet::getDisplayModes()),
				__FUNCTION__
		);
	}
	
	
	protected function catalogitem_displaymode()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
	
		return $this->labelledField(
				$Crm->translate('Products display'),
				$W->RadioSet()
					->setHorizontalView()
					->setOptions(crm_CatalogSet::getDisplayModes()),
				__FUNCTION__
		);
	}



	/**
	 *
	 * @return Widget_Item
	 */
	protected function Name()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		return $this->labelledField(
			$Crm->translate('Name'),
			$W->LineEdit()->setSize(30)->setMandatory(),
			'name'
		);
	}


	/**
	 *
	 * @return Widget_Item
	 */
	protected function Description()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		return $this->labelledField(
			$Crm->translate('Description'),
			$W->TextEdit(),
			'description'
		);
	}


	/**
	 *
	 * @return Widget_Item
	 */
	protected function Disabled()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		return $this->labelledField(
			$Crm->translate('Disable category public access'),
			$W->CheckBox(),
			'disabled'
		);
	}


	/**
	 *
	 * @return Widget_Item
	 */
	protected function Photo()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		$filePicker = $W->FilePicker()
				->setTitle($Crm->translate('Set photo'))
				->oneFileMode();
		
		if (isset($this->catalog))
		{
			$filePicker->setFolder($this->catalog->getPhotoFolder());
		}

		return $this->labelledField(
			$Crm->translate('Category image'),
			$filePicker,
			'photo'
		);

	}
	
	
	
	protected function parentcatalog()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		$set = $Crm->CatalogSet();
		
		return $this->labelledField(
				$Crm->translate('Parent category'),
				$W->Select()->setOptions($set->getAllOptions()),
				__FUNCTION__
		);
	}



	public function Save()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		return $W->SubmitButton()
				->validate()
				->setlabel($Crm->translate('Save'))
				->setAction($this->Crm()->Controller()->Catalog()->save())
				->setSuccessAction($this->Crm()->Controller()->Catalog()->displayList())
				->setFailedAction($this->Crm()->Controller()->Catalog()->edit());
	}


	public function Cancel()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		return $W->SubmitButton()
				->setlabel($Crm->translate('Cancel'))
				->setAction($this->Crm()->Controller()->Catalog()->cancel());
	}
}










/**
 * 
 * @param Func_Crm $Crm
 * @param ORM_Iterator $res
 * @return Widget_Form
 */
function crm_SortCatalogForm(Func_Crm $Crm, ORM_Iterator $res, $saveaction, $nextaction)
{
	$W = bab_Widgets();
	
	
	$childNodesForm = $W->Form(null, $W->VBoxLayout()->setVerticalSpacing(.5,'em'));
	$childNodesForm
		->setHiddenValue('tg', bab_rp('tg'));
	
	$childNodesFrame = $W->Frame(null, $W->VBoxLayout()->sortable())
	->setName('sortkeys')
	->addClass(Func_Icons::ICON_LEFT_16);
	
	foreach($res as $childNode) {
		$childNodesFrame->addItem(
				$W->FlowItems(
						$W->Hidden()->setName($childNode->id),
						$W->Icon($childNode->name, Func_Icons::PLACES_FOLDER)
				)
		);
	}
	
	
	$childNodesForm->addItem($childNodesFrame);
	$childNodesForm->addItem($W->SubmitButton()
			->setLabel($Crm->translate('Save sort'))
			->setAction($saveaction)
			->setSuccessAction($nextaction)
			->setFailedAction($nextaction)
	);
	
	return $childNodesForm;
}









/**
 * Display catalog
 *
 */
class crm_CatalogDisplay {


	public $Crm;

	/**
	 * @var crm_Catalog | crm_SearchCatalog
	 */
	protected $catalog = null;

	/**
	 *
	 * @param crm_Catalog $catalog
	 *
	 */
	public function __construct(Func_Crm $Crm, $catalog)
	{
		$this->Crm = $Crm;
		$this->catalog = $catalog;
	}


	protected function Crm()
	{
		if (isset($this->Crm)) {
			return $this->Crm;
		}

		return crm_Crm();
	}
	
	
	/**
	 * Small display
	 * @return Widget_Item
	 */
	public function getCardFrame()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
	
	
		$layout = $W->VBoxItems(
				$W->Link($W->Title($this->catalog->name, 3), $this->Crm()->Controller()->Catalog()->display($this->catalog->id)),
				$this->Photo(64, 64)->setTitle(bab_abbr($this->catalog->description, BAB_ABBR_FULL_WORDS, 100)),
				$W->Label(sprintf($Crm->translate('%d articles'),$this->catalog->getArticles()->count()))
		)->setVerticalSpacing(.2,'em');
	
		if ($this->catalog->disabled)
		{
			$layout->addItem($this->Disabled());
		}
	
		return $W->Frame(null, $layout)->addClass('crm-catalog-cardframe');
	}
	
	
	public function getShopHeader()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		$vbox = $W->VBoxItems(
				$W->Title($this->catalog->name, 3),
				$W->RichText($this->catalog->description)->setRenderingOptions(BAB_HTML_ALL)
		)->setVerticalSpacing(.2,'em');
		
		$layout = $W->HBoxItems(
				$this->Photo(200, 150),
				$vbox
		)->setHorizontalSpacing(1,'em');
		
		return $W->Frame(null, $layout)->addClass('crm-catalog-header');
	}
	
	
	
	/**
	 * result frame for online shop
	 * @param string $mode	user parameter 1:list | 0:card
	 */
	public function getShopResultFrame($mode)
	{
		switch((int) $mode)
		{
			case 1:
				return $this->getShopListFrame();
				break;
	
			default:
			case 0:
				return $this->getShopCardFrame();
			break;
		}
	}
	
	
	
	

	/**
	 * Online shop icon result Frame
	 * @return Widget_Item
	 */
	public function getShopCardFrame()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		$url = $this->catalog->getRewritenUrl();
		$nb = $this->getContentNumber();

		$layout = $W->VBoxItems(
			$W->Link($W->Title($this->catalog->name, 3), $url),
			$W->Link($this->Photo(200, 150), $url)->setTitle(bab_abbr($this->catalog->description, BAB_ABBR_FULL_WORDS, 170)),
			$nb ? $W->Label(implode(', ',$nb))->addClass('crm-catalog-articlecount') : null
		)->setVerticalSpacing(.2,'em');

		if ($this->catalog->disabled)
		{
			$layout->addItem($this->Disabled());
		}

		return $W->Frame(null, $layout)->addClass('crm-catalog-cardframe');
	}
	
	
	/**
	 * @return array
	 */
	protected function getContentNumber()
	{
		$Crm = $this->Crm();
		$nb = array();
		
		$articles = (int) $this->catalog->getVisibleArticles()->count();
		$catalogs = count($this->catalog->getParentSet()->selectFromParent($this->catalog->id)); // include dynamic folders
		
		if (0 === $articles)
		{
			if (0 === $catalogs)
			{
				$nb[] = $Crm->translate('This category is empty');
			}
		}
		else if (1 === $articles)
		{
			$nb[] = $Crm->translate('1 article');
		} else {
			$nb[] = sprintf($Crm->translate('%d articles'), $articles);
		}
		
		
		if ($catalogs)
		{
			if (1 === $catalogs)
			{
				$nb[] = $Crm->translate('1 category');
			} else {
				$nb[] = sprintf($Crm->translate('%d categories'), $catalogs);
			}
		}
		
		return $nb;
	}
	
	
	
	/**
	 * Online shop list result frame
	 * @return Widget_Item
	 */
	public function getShopListFrame()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		
		$url = $this->catalog->getRewritenUrl();
		$nb = $this->getContentNumber();
		
		
		$vbox = $W->VBoxItems(
				$W->Link($W->Title($this->catalog->name, 3), $url),
				$W->RichText(bab_abbr($this->catalog->description, BAB_ABBR_FULL_WORDS, 170))->setRenderingOptions(BAB_HTML_ENTITIES),
				$nb ? $W->Label(implode(', ',$nb))->addClass('crm-catalog-articlecount') : null
		)->setVerticalSpacing(.2,'em');
		
		$layout = $W->HBoxItems(
				$W->Link($this->Photo(200, 150), $url)->setTitle(bab_abbr($this->catalog->description, BAB_ABBR_FULL_WORDS, 100)),
				$vbox
		)->setHorizontalSpacing(1,'em');
		
		if ($this->catalog->disabled)
		{
			$layout->addItem($this->Disabled());
		}
		
		return $W->Frame(null, $layout)->addClass('crm-catalog-searchresult');
	}
	
	
	
	protected function description()
	{
		if (empty($this->catalog->description))
		{
			return null;
		}
		
		$Crm = $this->Crm();
		$W = bab_Widgets();
		
		return $W->Items(
			$W->Title($Crm->translate('Description'), 5),
			$W->RichText($this->catalog->description)
		);
	}
	



	/**
	 * Large display
	 * @return Widget_Item
	 */
	public function getFullFrame()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		$layout = $W->VBoxItems(
			$W->Title($this->catalog->name, 2),
			$W->Label(sprintf($Crm->translate('%d articles'),$this->catalog->getArticles()->count())),
			$W->Label(sprintf($Crm->translate('%d visibles articles'),$this->catalog->getVisibleArticles()->count())),
			$W->Label(sprintf($Crm->translate('%d visibles articles in sub-tree'),$this->catalog->refcount)),
			$this->description()
		)->setVerticalSpacing(.6,'em');


		if ($this->catalog->disabled) {
			$layout->addItem($this->Disabled());
		}

		$frame = $W->Frame(null ,$W->HBoxItems(
			$this->Photo(128, 128),
			$layout
		)->setHorizontalSpacing(1,'em'));
		
		if ($meta = $this->getMetaFields())
		{
			$layout->addItem($meta);
		}
		
		if ($sortChildnodes = $this->getSortChildNodes())
		{
			$layout->addItem($W->Title($Crm->translate('Sort sub-categories'), 5));
			$layout->addItem($sortChildnodes);
		}
		
		$frame->addClass('crm-detailed-info-frame');
		
		return $frame;
	}


	/**
	 * Large display
	 * @return Widget_Item
	 */
	public function getPublicFrame()
	{
		$W = bab_Widgets();

		if ($this->catalog->disabled) {
			return null;
		}

		$search = array(
				'advanced' => array(
					'catalog' => $this->catalog->id
				)
			);

		$action = $this->Crm()->Controller()->CatalogItem()->displayList($search);

		$layout = $W->VBoxItems(
			$W->Link($this->Photo(64, 64), $action),
			$W->Link($W->Title($this->catalog->name, 2), $action),
			$W->RichText($this->catalog->description)
		)->setSpacing(.2,'em');


		return $W->Frame(null, $layout)->addClass('crm-catalog-public-frame');
	}



	/**
	 *
	 * @return Widget_Item
	 */
	protected function Disabled()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		return $W->Label($Crm->translate('This catalog is disabled'));
	}



	/**
	 * Get original main photo path
	 * @return bab_Path
	 */
	protected function getMainPhotoPath()
	{
		return $this->catalog->getPhotoPath();
        
	}

	/**
	 * 
	 * @return bab_Path
	 */
	protected function getDefaultImagePath()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
		$addon = bab_getAddonInfosInstance('LibCrm');
		return new bab_Path($addon->getStylePath() . 'images/article-default.png');
	}


	/**
	 *
	 *
	 * @param int			$width
	 * @param int			$height
	 * @return Widget_ImageThumbnail
	 */
	public function Photo($width, $height)
	{
		$W = bab_Widgets();

		$path = $this->getMainPhotoPath();
		if (empty($path)) {
			$path = $this->getDefaultImagePath();
		}
		
		$image = $W->ImageThumbnail($path, $this->catalog->image_alt)->setThumbnailSize($width, $height);


		return $image;
	}
	
	
	
	
	protected function getSortChildNodes()
	{
		$res = $this->catalog->getCatalogs();
		if ($res->count() === 0)
		{
			return null;
		}
		
		$Crm = $this->Crm();
		
		$saveaction = $Crm->Controller()->Catalog()->saveSort();
		$nextaction = $Crm->Controller()->Catalog()->display($this->catalog->id);
		
		$form = crm_SortCatalogForm($this->Crm(), $res, $saveaction, $nextaction);
		$form->setHiddenValue('catalog', $this->catalog->id);
		
		return $form;
	}
	
	
	
	
	
	
	
	
	
	protected function getMetaFields()
	{
		if (!$this->catalog->page_title && !$this->catalog->page_description && !$this->catalog->page_keywords)
		{
			return null;
		}
	
		$W = bab_Widgets();
		$Crm = $this->Crm();
	
		$frame = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(.4,'em'))->addClass('crm-metafields');
		$frame->addItem($W->Title($Crm->translate('HTML page Metadata fields'), 5));
	
		if ($this->catalog->page_title)
		{
			$frame->addItem($W->FlowItems(
					$W->Label($Crm->translate('Page title'))->colon(),
					$W->Label($this->catalog->page_title)
			)->setSpacing(0,'em',.5,'em'));
		}
	
	
		if ($this->catalog->page_description)
		{
			$frame->addItem($W->FlowItems(
					$W->Label($Crm->translate('Page description'))->colon(),
					$W->Label($this->catalog->page_description)
			)->setSpacing(0,'em',.5,'em'));
		}
	
		if ($this->catalog->page_keywords)
		{
			$frame->addItem($W->FlowItems(
					$W->Label($Crm->translate('Page keywords'))->colon(),
					$W->Label($this->catalog->page_keywords)
			)->setSpacing(0,'em',.5,'em'));
		}
		
		if ($this->catalog->image_alt)
		{
			$frame->addItem($W->FlowItems(
					$W->Label($Crm->translate('Alternate text of image'))->colon(),
					$W->Label($this->catalog->image_alt)
			)->setSpacing(0,'em',.5,'em'));
		}
	
		return $frame;
	}


}














