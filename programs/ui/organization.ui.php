<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */






/**
 * organization card frame
 *
 */
class crm_OrganizationCardFrame extends crm_CardFrame
{
    const SHOW_PHOTO = 1;
    const SHOW_LOGO = 1;
    const SHOW_NAME = 2;
    const SHOW_ADDRESS = 32;
    const SHOW_EMAIL = 64;
    const SHOW_PHONE = 128;
    const SHOW_MOBILE = 256;
    const SHOW_FAX = 512;

    const SHOW_ALL = 0xFFFF;

    const SHOW_MINIMUM = 0x1F;

    const IMAGE_WIDTH = 48;
    const IMAGE_HEIGHT = 24;

    protected $organization;

    protected $attributes;

    public function __construct(Func_Crm $crm, crm_Organization $org, $attributes = crm_OrganizationCardFrame::SHOW_ALL, $id = null)
    {
        $this->organization = $org;
        $this->setCrm($crm);

        $W = bab_Widgets();
        $cardLayout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
        parent::__construct($crm, $id, $cardLayout);

        $this->attributes = $attributes;
        $this->init();

        $this->addClass('vcard');

        require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
        $this->item->setCacheHeaders(Widget_Item::CacheHeaders()->setLastModified(BAB_DateTime::fromIsoDateTime($this->organization->modifiedOn)));

    }



    public function init()
    {
        $W = bab_Widgets();


        if (($this->attributes & self::SHOW_LOGO) && $logo = $this->Logo()) {
            $contentlayout = $W->HBoxItems(
                $this->handleContent()->setSizePolicy(Widget_SizePolicy::MAXIMUM),
                $logo->setSizePolicy(Widget_SizePolicy::MINIMUM)
            )->setHorizontalSpacing(1, 'em');
            $this->addItem($contentlayout);
        } else {
            $this->addItem($this->handleContent()->setSizePolicy(Widget_SizePolicy::MAXIMUM));
        }

    }


    public function section($title, Widget_Displayable_Interface $content, $level = 5)
    {
        $W = bab_Widgets();
        return $W->Section(
            $title,
            $content,
            $level
        );
    }


    /**
     * Placeholder for content next to organization logo
     * @return Widget_Item
     */
    protected function handleContent()
    {
        $W = bab_Widgets();
        $layout = $W->VBoxItems(
            $this->Title(),
            $this->handleData()
        )->setVerticalSpacing(1,'em');

        return $layout;
    }



    /**
     * Placeholder for data under organization name
     * this method can return info in multiple columns
     * @return Widget_Item
     */
    protected function handleData()
    {
        $W = bab_Widgets();
        $layout = $W->VBoxLayout()->setVerticalSpacing(0.5, 'em');


        if ($parents = $this->parents()) {
            $layout->addItem($parents);
        }

        if ($type = $this->type()) {
            $layout->addItem($type);
        }

        if ($activity = $this->activity()) {
            $layout->addItem($activity);
        }

        if (($this->attributes & self::SHOW_ADDRESS) && ($address = $this->Address())) {
            $layout->addItem($address);
        }

        if ($web = $this->Web()) {
            $layout->addItem($web);
        }

        if ($phones = $this->Phones()) {
            $layout->addItem($phones);
        }

        if ($description = $this->Description()) {
            $layout->addItem($description);
        }


        return $layout;
    }


    /**
     * Widget_Item
     */
    protected function parents()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();
        $parentOrganizations = $this->organization->getAscendants();
        array_shift($parentOrganizations);
        $parentOrganizations = array_reverse($parentOrganizations);
        if (count($parentOrganizations) > 0) {
            $parentsBox = $W->FlowLayout()->setHorizontalSpacing(1, 'em');
            foreach ($parentOrganizations as $parentOrganization) {
                $parentsBox->addItem($W->Label('>'));
                $parentsBox->addItem($W->Link($parentOrganization->name, $Crm->Controller()->Organization()->display($parentOrganization->id)));
            }
            return $this->labelStr(
                $Crm->translate('Parent organization'),
                $parentsBox
            );
        }
        return null;
    }

    /**
     * Widget_Item
     */
    protected function type()
    {
        $W = bab_Widgets();

        if (!isset($this->Crm()->OrganizationType)) {
            return null;
        }
        $this->Crm()->includeOrganizationTypeSet();

        if (!($type = $this->organization->type())) {
            return null;
        }

        return $this->labelStr(
            $W->Label($this->Crm()->translate('Type')),
            $W->Label($type->name)
        );
    }


    /**
     * Widget_Item
     */
    protected function Title()
    {
        $W = bab_Widgets();

        $access = $this->Crm()->Access();

        if (!$access->viewOrganization($this->organization)) {
            return $W->Label($this->organization->getName())->addClass('fn crm-card-title');
        }
        return $W->Link(
            $this->organization->getName(),
            $this->Crm()->Controller()->Organization()->display($this->organization->id)
        )->addClass('fn crm-card-title');
    }


    /**
     * @return Widget_Image | null
     */
    protected function Logo()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $access = $Crm->Access();
        $logo = $Crm->Ui()->OrganizationLogo($this->organization, self::IMAGE_WIDTH, self::IMAGE_HEIGHT);

        if (!$access->viewOrganization($this->organization)) {
            return $logo;
        }
        return $W->Link(
            $logo,
            $Crm->Controller()->Organization()->display($this->organization->id)
        );
    }

    /**
     * display address or addresses
     * the default display only the main address without title for card frame, the full frame will overwrite this method to display more informations
     *
     * @return Widget_Item | null
     */
    protected function Address()
    {
        return $this->MainAddress(false);
    }

    /**
     * display main address
     * @param	bool	$displayTitle
     * @return Widget_Item | null
     */
    protected function MainAddress($displayTitle = true)
    {
        $Crm = $this->Crm();
        $mainAddress = $this->organization->getMainAddress();
        if ($mainAddress && !$mainAddress->isEmpty()) {

            $title = $displayTitle ? $Crm->translate('Main address') : null;
            return parent::TitledAddress($mainAddress, $title);
        }
        return null;
    }


    /**
     * display billing address
     * @param	bool	$displayTitle
     * @return Widget_Item | null
     */
    protected function BillingAddress($displayTitle = true)
    {
        $Crm = $this->Crm();
        $billingAddress = $this->organization->getBillingAddress();
        if ($billingAddress) {
            $title = $displayTitle ? $Crm->translate('Billing address') : null;
            $recipient = $this->organization->getBillingAddressRecipient();

            return parent::TitledAddress($billingAddress, $title, $recipient);
        }
        return null;
    }



    /**
     * Display main address and billing address if availables
     *
     * @return Widget_FlowLayout | null
     */
    protected function AllAddresses()
    {
        $mainAddress = $this->organization->getMainAddress();
        $billingAddress = $this->organization->getBillingAddress();

        if ($mainAddress instanceOf crm_Address && $mainAddress->isEmpty()) {
            $mainAddress = null;
        }

        if ($billingAddress instanceOf crm_Address && $billingAddress->isEmpty()) {
            $billingAddress = null;
        }

        if (!$mainAddress && !$billingAddress) {
            return null;
        }

        $W = bab_Widgets();

        $layout = $W->FlowLayout()->setHorizontalSpacing(2.5, 'em')->setVerticalAlign('top');




        if (null !== $mainAddress && null !== $billingAddress) {
            $layout->addItem($this->MainAddress(true));
            $layout->addItem($this->BillingAddress(true));
        }

        else if (null !== $mainAddress) {
            $layout->addItem($this->MainAddress(true));

        }

        else if (null !== $billingAddress) {
            $layout->addItem($this->BillingAddress(true));
        }


        return $layout;
    }



    /**
     * Placeholder for phone numbers, mobile, fax ...
     *
     * @return Widget_Item | null
     */
    protected function Phones()
    {
        $W = bab_Widgets();

        $n = 0;
        $phones = $W->FlowLayout()->setHorizontalSpacing(2, 'em')->setVerticalSpacing(1, 'em');

        if ($phone = $this->organization->getMainPhone()) {
            $phones->addItem($this->labelStr($W->Label($this->Crm()->translate('Phone')), $W->Label($phone)->addClass('tel')));
            $n++;
        }

        if ($mobile = $this->organization->getMainMobile()) {
            $phones->addItem($this->labelStr($W->Label($this->Crm()->translate('Mobile')), $W->Label($mobile)));
            $n++;
        }

        if ($fax = $this->organization->getMainFax()) {
            $phones->addItem($this->labelStr($W->Label($this->Crm()->translate('Fax')),$W->Label($fax)));
            $n++;
        }

        return $n > 0 ? $phones : null;
    }


    protected function activity()
    {
        $W = bab_Widgets();

        if ($activity = $this->organization->activity) {
            return $this->labelStr($W->Label($this->Crm()->translate('Activity')), $W->Label($activity));
        }

        return null;
    }




    /**
     * Placeholder for email address, web url, IM ...
     *
     * @return Widget_Item | null
     */
    protected function Web()
    {
        $W = bab_Widgets();

        $n = 0;
        $web = $W->VBoxLayout()->setVerticalSpacing(0.5, 'em')->addClass('icon-left-16 icon-left icon-16x16');

        if (($email = $this->organization->getMainEmail())) {
            $web->addItem(
                $this->labelStr($W->label($this->Crm()->translate('Email')), $W->Link($email, $this->Crm()->mailTo($email))->addClass('email'))
            );
            $n++;
        }

        if (($website = $this->organization->getMainWebsite())) {
            $p = @parse_url($website);
            if (!isset($p['scheme'])) {
                $p['scheme'] = 'http://';
                $website = 'http://' . $website;
            }
            $link = $W->Link($website, $website);
            $link->addClass('url');
            $link->setOpenMode(Widget_Link::OPEN_POPUP);

            if (isset($p['host'])) {
                $favicon = $W->Image($p['scheme'].'://'.$p['host'].'/favicon.ico')
                    ->addClass('widget-1em');
                $link = $W->FlowItems($favicon, $link)
                    ->setVerticalAlign('middle')
                    ->setHorizontalSpacing(4, 'px');
            }

            $web->addItem(
                $this->labelStr($this->Crm()->translate('Web site'), $link)
            );
            $n++;
        }

        return $n > 0 ? $web : null;
    }


    /**
     * Placeholder for organization description
     * no description in card frame
     * use crm_OrganizationFullFrame to display the description
     * @see crm_OrganizationFullFrame::Description()
     * @return Widget_item | null
     */
    protected function Description()
    {
        return null;
    }

    /**
     *
     * @return Widget_FlowLayout
     */
    protected function Codifications()
    {
        $W = bab_Widgets();
        $crm = $this->Crm();

        $crm->includeClassificationSet();

        $orgClassificationSet = $crm->OrganizationClassificationSet();
        $orgClassificationSet->join('classification');
        $orgClassifications = $orgClassificationSet->selectForOrganization($this->organization->id);

        $classifications = $W->FlowLayout()->setSpacing(3, 'px');

        foreach ($orgClassifications as $orgClassification) {
            /* @var $orgClassification crm_OrganizationClassification */

            $classificationLabel = $W->Label($orgClassification->classification->name)
                                        ->setTitle(implode(' > ', $orgClassification->classification->getPathname()))
                                        ->addClass('crm-codification');
            $classifications->addItem($classificationLabel);
        }

        return $this->labelStr(
            $crm->translate('Codification'),
            $classifications
        );
    }
}




/**
 * Organization full frame
 *
 */
class crm_OrganizationFullFrame extends crm_OrganizationCardFrame
{
    public function init()
    {
        $W = bab_Widgets();

        if ($logo = $this->Logo()) {
            $contentlayout = $W->HBoxLayout()->setHorizontalSpacing(1, 'em');
            $contentlayout->addItem($logo->setSizePolicy(Widget_SizePolicy::MINIMUM));
            $this->addItem($contentlayout);
        }
        $contentlayout->addItem($this->handleContent()->setSizePolicy(Widget_SizePolicy::MAXIMUM));

// 		if ($description = $this->Description()) {
// 			$this->addItem($description);
// 		}

    }


    /**
     * Placeholder for content next to organization logo
     * @return Widget_Item
     */
    protected function handleContent()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $layout = $W->VBoxItems(
            $this->Title(),
            $this->handleData()


        )->setVerticalSpacing(1,'em');

        if (isset($Crm->Tag) && $tags = $this->tags()) {
            $layout->addItem($tags);
        }



        if (isset($Crm->Codification) && isset($Crm->OrganizationCodification))
        {
            $layout->addItem($this->Codifications());
        }

        return $layout;
    }


    /**
     * Widget_Item
     */
    protected function Title()
    {
        $W = bab_Widgets();
        return $W->Title($this->organization->getName(), 1)->addClass('fn');
    }


    /**
     * @return Widget_Image | null
     */
    protected function Logo()
    {
        return $this->Crm()->Ui()->OrganizationLogo($this->organization, 180, 180);
    }

    /**
     * Placeholder for organization description
     * @return Widget_Item | null
     */
    protected function Description()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        if (empty($this->organization->description)) {
            return $W->Html('');
        }
        return $this->labelStr(
            $Crm->translate('Description'),
            $W->Html(bab_toHtml($this->organization->description, BAB_HTML_ALL))
        );
    }


    protected function Address()
    {
        return $this->AllAddresses();
    }


    protected function tags()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $Crm->Ui()->includeTag();

        $tagSet = $Crm->TagSet();
        $tags = $tagSet->selectFor($this->organization);
        $tagsDisplay = new crm_TagsDisplay($Crm, $tags, $this->organization, $Crm->Controller()->Organization()->displayListForTag());

        $addTagForm = $W->Form()
            ->setLayout($W->FlowLayout()->setVerticalAlign('middle')->setHorizontalSpacing(0.5, 'em'))
            ->setName('tag');
        $addTagForm->addItem($Crm->Ui()->SuggestTag()->setName('label'));
        $addTagForm->addItem($W->SubmitButton()->setAction($Crm->Controller()->Tag()->link())->setLabel($Crm->translate('Add')));
        $addTagForm->setHiddenValue('tg', bab_rp('tg'));
        $addTagForm->setHiddenValue('to', $this->organization->getRef());

        return $W->FlowItems(
            $W->Label($Crm->translate('Tags'))->colon()->addClass('crm-display-label'),
            $tagsDisplay,
            $addTagForm
        )->setHorizontalSpacing(0.5, 'em');

//		return $this->section(
//			$crm->translate('Tags'),
//			$W->FlowItems(
//				$tagsDisplay,
//				$addTagForm
//			)->setHorizontalSpacing(0.5, 'em'),
//			3
//		);
    }

}





/**
 * @deprecated use crm_OrganizationFullFrame class
 * @param $organization
 * @return Widget_Frame
 */
function crm_fullOrganizationFrame(Func_Crm $crm, crm_Organization $organization)
{
    return new crm_OrganizationFullFrame($crm, $organization);
}







/**
 *
 * @param crm_Organization 	$org
 * @param int				$width
 * @param int				$height
 * @param bool				$border
 * @return Widget_Image
 */
function crm_organizationLogo(crm_Organization $org, $width, $height, $border = false)
{
    $W = bab_Widgets();
    $T = @bab_functionality::get('Thumbnailer');

    $image = $W->Image();

    if ($T) {
        $logo = $org->getLogoPath();
        if (!empty($logo)) {
             $T->setSourceFile($logo->toString());
             $T->setResizeMode(Func_Thumbnailer::KEEP_ASPECT_RATIO);
        } else {
            $addon = bab_getAddonInfosInstance('LibCrm');
             $T->setSourceFile($addon->getStylePath() . 'images/organization-default.png');
        }

        if ($border) {
            $padWidth = min(array(2, round(min(array($width, $height)) / 24)));
            $T->setBorder(1, '#cccccc', $padWidth, '#ffffff');
        }
        $imageUrl = $T->getThumbnail($width, $height);
        $image->setUrl($imageUrl);
    }

    if ($org->name) {
        $image->setTitle($org->name);
    }

    $image->addClass('crm-organization-image crm-element-image small');

    return $image;
}





/**
 * List of contacts for organization display page
 */
class crm_organizationContacts extends crm_UiObject
{

    protected function Title()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();
        return $W->Title($Crm->translate('Contacts in this organization'), 4)->setSizePolicy(Widget_SizePolicy::MAXIMUM);
    }


    protected function ContactTitle(crm_OrganizationPerson $organizationPerson)
    {
        $return = $organizationPerson->getContact()->getFullName();

        $position = $organizationPerson->getMainPosition();
        if (!empty($position)) {
            $return  .= ' - ' . $this->Crm()->translate($position);
        }

        return $return;
    }





    /**
     *
     * @param crm_Organization 	$organization
     * @param	int				$limit			Maximum contact to display (default 50)
     *
     * @return widget_Frame
     */
    public function getFrame(crm_Organization $organization, $limit = 50)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        require_once dirname(__FILE__) . '/contact.ui.php';

        $W = bab_Widgets();

        $contactFrame = $W->Section(
            $Crm->translate('Contacts in this organization'),
            $W->VBoxLayout()->addClass('crm-team-section'),
            4
        );

        $contactFrame->addContextMenu()
            ->addItem(
                $W->Link(
                    $Crm->translate('Add'),
                    $this->Crm()->Controller()->Contact()->add($organization->id)
                )
                ->addClass('widget-actionbutton')
                ->setTitle($Crm->translate('Add a contact to this organization'))
            );

        $contactFrame;


        $max = $limit;
        $contacts = $organization->selectContacts();

        foreach ($contacts as $organizationPerson) {

            $contact = $organizationPerson->getContact();
            $contactCard = $Ui->ContactCardFrame(
                $organizationPerson,
                crm_ContactCardFrame::SHOW_ALL & ~(crm_ContactCardFrame::SHOW_NAME | crm_ContactCardFrame::SHOW_POSITION | crm_ContactCardFrame::SHOW_PHOTO)
            );

            $fullName = $contact->getFullName();

            $position = $organizationPerson->position;
            if (empty($position)) {
                $position = '';
            }
            $contactSection = $W->Section(
                $fullName,
                $contactCard,
                6
            )->setFoldable(true, true);
            $contactSection->setSubHeaderText($position);
            $contactSection->addClass('crm-team-member');
            $contactFrame->addItem(
                $contactSection
            );

            $contactSection->addContextMenu('inline')
                ->addItem($Ui->ContactPhoto($contact, 24, 24));

            $contactMenu = $contactSection->addContextMenu('popup');
            $this->loadMenu($contactMenu, $organization, $contact);


            $max--;

            $remains = $contacts->count() - $limit;

            if ($max <= 0 && $remains > 1) {

                $item = $this->Remains($organization, $remains);

                if (null !== $item) {
                    $contactFrame->addItem($item);
                }

                break;
            }
        }

        return $contactFrame;
    }


    /**
     * Contact context menu
     *
     * @return Widget_Menu
     */
    protected function loadMenu(Widget_Menu $contactMenu, crm_Organization $organization, crm_Contact $contact)
    {
        $Crm = $this->Crm();

        $Access = $Crm->Access();
        $W = bab_Widgets();



        $contactMenu->addItem(
            $W->Link(
                $W->Icon($Crm->translate('View contact page'), Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                $Crm->Controller()->Contact()->display($contact->id)
                )
            );
        if ($Access->updateContact($contact)) {
            $contactMenu->addItem(
                $W->Link(
                    $W->Icon($Crm->translate('Edit contact'), Func_Icons::ACTIONS_DOCUMENT_EDIT),
                    $Crm->Controller()->Contact()->edit($contact->id)
                    )
                );
        }

        $email = $contact->getMainEmail();

        if ($Access->emailContact($contact) && $email) {
            $contactMenu->addItem(
                $W->Link(
                    $W->Icon($Crm->translate('Send an email'), Func_Icons::ACTIONS_MAIL_SEND),
                    $Crm->Controller()->Email()->edit($email, null, null, $contact->getRef() . ',' . $organization->getRef()))
                );
        }

        return $contactMenu;
    }



    /**
     * Called if the getFrame method return not all the contacts
     *
     * @param	crm_Organization	$organization
     * @param 	int 				$remains
     * @return Widget_Item
     */
    protected function Remains(crm_Organization $organization, $remains)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();
        return $W->Label(sprintf($Crm->translate('And %d contacts not displayed'), $remains));
    }

}


/**
 *
 * @param crm_Organization $organization
 *
 * @return widget_Frame
 */
function crm_organizationDeals(crm_Organization $organization)
{
    require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
    $W = bab_Widgets();
    $Crm = $organization->Crm();

    $Crm->Ui()->includeDeal();

    $dealsFrame = $W->Section(
        $Crm->translate('Deals with this organization'),
        $W->VBoxLayout()->setVerticalSpacing(6, 'px'),
        4
    );

    $dealsFrame->addContextMenu()
        ->addItem(
            $W->Link(
                $Crm->translate('Add'),
                $Crm->Controller()->Deal()->add($organization->id)
            )
            ->addClass('widget-actionbutton')
            ->setTitle($Crm->translate('Add a deal with this organization'))
        );

    $dealSet = $Crm->DealSet();
    $dealSet->status();
    $deals = $dealSet->select($dealSet->lead->is($organization->id));

    if ($deals->count() == 0) {
        $dealsFrame->addItem($W->Label($Crm->translate('No deal associated with this organization')));
    }
    foreach ($deals as $deal) {
        $amount = '';
        if ($deal->amount) {
            $amount = number_format((float)$deal->amount, 0, ',', ' ') . $Crm->translate('_euro_') . ' / ';
        }
        $dealsFrame->addItem(
            $W->HBoxItems(
                $W->VBoxItems(
                    $W->Link($W->Title($deal->name, 6), $Crm->Controller()->Deal()->display($deal->id)),
                    $W->Label(
                        $amount . $deal->status->name
                    )
                )->setSizePolicy(Widget_SizePolicy::MAXIMUM),
                $Crm->Ui()->DealPhoto($deal, 24, 24)
            )
            ->setSizePolicy('crm-list-element')
            ->setVerticalAlign('middle')
            ->setHorizontalSpacing(8, 'px')
            ->setCacheHeaders(Widget_Item::CacheHeaders()->setLastModified(BAB_DateTime::fromIsoDateTime($deal->modifiedOn)))
        );
    }
    return $dealsFrame;
}











/**
 * Organization editor
 */
class crm_OrganizationEditor extends crm_Editor
{

    protected $nameItem = null;

    /**
     * @var crm_SuggestOrganization
     */
    protected $suggestParentOrganization = null;

    /**
     * @var crm_Organization
     */
    protected $organization;

    /**
     * @param Widget_Layout $layout	The layout that will manage how widgets are displayed in this form.
     * @param string $id			The item unique id.
     */
    public function __construct(Func_Crm $crm, crm_Organization $organization = null, $id = null, Widget_Layout $layout = null)
    {
        if (null === $layout) {
            $W = bab_Widgets();
            $layout = $W->VBoxLayout()->setVerticalSpacing(2, 'em');
        }

        parent::__construct($crm, $id, $layout);
        $this->organization = $organization;

        $this->colon();
        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->setName('organization');

        $this->addFields();

        $this->addButtons();

        if (isset($organization)) {
            $this->setHiddenValue('organization[id]', $organization->id);
            $this->setValues($organization->getValues(), array('organization'));
        }

    }




    /**
     * Returns an array containing field that can be used to search google for an image.
     */
    public function getGoogleSearchFields()
    {
        return array($this->nameItem);
    }




    /**
     * Add a default field set to form
     *
     *
     */
    protected function addFields()
    {

        $this->addItem($this->name());
        $this->addItem($this->description());
        $this->addItem($this->activity());
        $this->addItem($this->getContactInformationsSection());
        $this->addItem($this->addresses());
    }


    protected function getContactInformationsSection()
    {
        $W = $this->widgets;
        $Crm = $this->Crm();

        return $W->Section(
            $Crm->translate('Contact information'),
            $W->VBoxItems(
                $W->FlowItems(
                    $this->phone(),
                    $this->fax()
                    )->setHorizontalSpacing(1, 'em'),
                $this->email(),
                $this->website()
                )->setVerticalSpacing(1, 'em')
            )
            ->setFoldable(true);
    }



    protected function addButtons()
    {
        $Crm = $this->Crm();

        $this->setCancelAction($Crm->Controller()->Organization()->cancel());
        $this->setSaveAction($Crm->Controller()->Organization()->save());
    }


    protected function name()
    {
        $Crm = $this->Crm();
        return $this->labelledField(
            $Crm->translate('Name'),
            $this->nameItem = $Crm->Ui()->SuggestOrganization()
            //$W->LineEdit()
                ->setSize(75)
                ->setMaxSize(255)
                ->addClass('widget-fullwidth')
                ->setMandatory(true, $Crm->translate('You must specify the organization name.')),
            'name'
        );
    }

    protected function description()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;
        return $this->labelledField(
            $Crm->translate('Description'),
            $W->TextEdit()
                ->setLines(2)
                ->setColumns(73)
                ->addClass('widget-fullwidth', 'widget-autoresize'),
            'description'
        );
    }


    protected function activity()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;
        return $this->labelledField(
            $Crm->translate('Activity'),
            $W->LineEdit()
                ->setSize(52)
                ->setMaxSize(255)
                ->addClass('widget-fullwidth'),
            'activity'
        );
    }



    protected function parentOrganization()
    {
        $Crm = $this->Crm();

        $organizationId = null;
        if (isset($this->organization)) {
            $organizationId = $this->organization->id;
        }
        $this->suggestParentOrganization = $Crm->Ui()->SuggestParentOrganization($organizationId)
            ->setMinChars(0)
            ->setSize(75)
            ->addClass('widget-fullwidth');

        $parentOrganizationFormItem = $this->labelledField(
            $Crm->translate('Parent organization'),
            $this->suggestParentOrganization,
            'parent/name'
        );
        return $parentOrganizationFormItem;
    }



    /**
     *
     * @return Widget_ImagePicker
     */
    protected function logo()
    {
        /* @var $W Func_Widgets */
        $W = $this->widgets;

        $addon = bab_getAddonInfosInstance('LibCrm');

        $imagePicker = $W->ImagePicker()
            ->setName('logo')
            ->oneFileMode()
            ->setDefaultImage(new bab_Path($addon->getStylePath() . 'images/organization-default.png'))
            ->setTitle($this->Crm()->translate('Set Logo'));

        return $imagePicker;
    }




    protected function email()
    {
        $W = bab_Widgets();
        return $this->labelledField(
                $this->Crm()->translate('Email'),
                $W->EmailLineEdit()
                ->setSize(75)
                ->setMaxSize(255)
                ->addClass('widget-fullwidth'),
                'email'
        );
    }



    protected function phone()
    {
        $W = $this->widgets;
        return $this->labelledField(
                $this->Crm()->translate('Phone'),
                $W->TelLineEdit()->setSize(20)->setMaxSize(30),
                'phone'
        );
    }



    protected function fax()
    {
        $W = $this->widgets;
        return $this->labelledField(
                $this->Crm()->translate('Fax'),
                $W->TelLineEdit()
                ->setSize(20)
                ->setMaxSize(30),
                'fax'
        );
    }



    protected function website()
    {
        $W = $this->widgets;
        return $this->labelledField(
                $this->Crm()->translate('Web site'),
                $W->UrlLineEdit()
                ->setSchemeCheck(false)
                ->setSize(75)
                ->setMaxSize(255)
                ->addClass('widget-fullwidth'),
                'website'
        );
    }


    protected function addresses()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;
        $Ui = $Crm->Ui();

        return 	$this->addressSection = $W->Section(
                $Crm->translate('Address'),
                $W->VBoxItems($Ui->AddressEditor(true)->setName('address'))
        )->setFoldable(true);
    }


    protected function collaborator()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $orgs = array();

        $mainOrganizationId = $Crm->getMainOrganization();
        if (isset($mainOrganizationId)) {
            $mainOrganization = $Crm->OrganizationSet()->get($mainOrganizationId);
            if (isset($mainOrganization)) {
                $orgs[$mainOrganization->id] = $mainOrganization->name;

                $descendantIds = array();
                $mainOrganization->getDescendants($descendantIds);
                $organizationSet = $Crm->OrganizationSet();
                $descendants = $organizationSet->select($organizationSet->id->in($descendantIds));

                foreach ($descendants as $descendant) {
                    $orgs[$descendant->id] = $descendant->name;
                }
            }
        }

        return $this->labelledField(
            $Crm->translate('Responsible collaborator'),
            $Crm->Ui()->SuggestContact()
                ->setRelatedOrganization($orgs)
                ->addClass('widget-fullwidth')
                ->setMinChars(0)
                ->setSize(75),
            'responsible_name'
        );
    }



    /**
     * @param	array	$row
     * @param	array 	$namePathBase
     * @return 	crm_OrganizationEditor
     */
    public function setValues($organization, $namePathBase = array())
    {
        if (!($organization instanceof crm_Organization)) {
            return parent::setValues($organization, $namePathBase);
        }

        $name = $this->getName();
        $organizationValues = $organization->getValues();
        parent::setValues($organizationValues, $namePathBase);

        if ($organization->responsible && ($responsible = $organization->responsible()) instanceof crm_Contact) {
            $this->setValue(array($name, 'responsible_name'), $responsible->getFullName());
        }

        if (!empty($organization->id)) {
            $this->setHiddenValue($name . '[id]', $organization->id);
            $parentOrganization = $organization->parent();
            $this->suggestParentOrganization->excludeDescendantsOf($organization->id);
            $this->suggestParentOrganization->setValue($parentOrganization);
        }
        return $this;
    }

}

/**
 * @param Func_Crm	$Crm
 * @param crm_Organization $organization
 * @return Widget_Timeline
 */
function crm_organizationTimeline(Func_Crm $Crm, crm_Organization $organization)
{
    $W = bab_Widgets();

    require_once $GLOBALS['babInstallPath'] . 'utilit/dateTime.php';

    $now = BAB_DateTime::now();
    $nowIso = $now->getIsoDate();

    $taskSet = $Crm->TaskSet();
    $orgTasks = $taskSet->selectLinkedTo($organization);

    $orgContacts = $organization->selectContacts();
    $contactTasks = $taskSet->selectLinkedTo($orgContacts);

    $timeline = $W->Timeline();

    $timeline->addBand(new Widget_TimelineBand('20%', Widget_TimelineBand::INTERVAL_MONTH, 150, Widget_TimelineBand::TYPE_OVERVIEW));
    $timeline->addBand(new Widget_TimelineBand('80%', Widget_TimelineBand::INTERVAL_WEEK, 150, Widget_TimelineBand::TYPE_ORIGINAL));


    foreach ($orgTasks as $task) {
        $task = $task->targetId;
        $start = BAB_DateTime::fromIsoDateTime($task->dueDate);
        $period = $timeline->createMilestone($start);
        $period->start = $task->dueDate . ' 12:00';
        if ($task->completion >= 100) {
            $period->color = '#282';
            $period->setTitle($task->summary);
        } else  {
            if ($nowIso > $task->dueDate) {
                $period->color = '#b22';
            }
            $period->setTitle($task->summary);
        }
        $period->setBubbleContent(
            $W->VBoxItems(
                $W->Link($W->Title($task->summary, 2), $GLOBALS['babUrl'] . $Crm->Controller()->Task()->display($task->id)->url()),
                $W->Html($Crm->translate('Responsible:') . ' <b>' . bab_toHtml(bab_getUserName($task->responsible, true)) . '</b>'),
                $W->Html(bab_toHtml($task->description, BAB_HTML_ALL))
            )->setVerticalSpacing(0.5, 'em')
        );

        $timeline->addPeriod($period);
    }

    $T = @bab_functionality::get('Thumbnailer');
    if ($T) {
        $addon = bab_getAddonInfosInstance('LibCrm');
         $T->setSourceFile($addon->getStylePath() . 'images/contact-default.png');
        $imageUrl = $T->getThumbnail(12, 12);
    } else {
        $imageUrl = null;
    }

    $contactSet = $Crm->ContactSet();
    foreach ($contactTasks as $linkTask) {
        $task = $linkTask->targetId;
        $contact = $contactSet->get($linkTask->sourceId);
        $start = BAB_DateTime::fromIsoDateTime($task->dueDate);
        $period = $timeline->createMilestone($start);
        $period->start = $task->dueDate . ' 12:00';
        if ($task->completion >= 100) {
            $period->color = '#282';
            $period->setTitle($contact->getFullName() . ' - ' . $task->summary);
        } else  {
            if ($nowIso > $task->dueDate) {
                $period->color = '#b22';
            }
            $period->setTitle($contact->getFullName() . ' - ' . $task->summary);
        }
        $period->description = $W->VBoxItems(
            $W->RichText($task->description),
            $Crm->Ui()->ContactCardFrame($contact)
        );
        $period->link = $GLOBALS['babUrl'] . $Crm->Controller()->Task()->display($task->id)->url();
        $period->icon = $imageUrl;
        $timeline->addPeriod($period);
    }

    return $timeline;
}




class crm_OrganizationTableView extends crm_TableModelView
{
    /**
     * @var crm_TagSet
     */
    protected $tagSet = null;

    /**
     * @param Func_Crm $Crm
     * @param string $id
     */
    public function __construct(Func_Crm $Crm = null, $id = null)
    {
        parent::__construct($Crm, $id);

        $Crm = $this->Crm();

        if (isset($Crm->Tag)) {
            $this->tagSet = $Crm->TagSet();
            $Crm->Ui()->includeTag();
        }
    }


    /**
     * {@inheritDoc}
     * @see crm_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $organizationSet = null)
    {
        $Crm = $this->Crm();

        if (!isset($organizationSet)) {
            $organizationSet = $this->getRecordSet();
        }

        $organizationSet->address();
        $organizationSet->address->country();
        $organizationSet->responsible();
        if (isset($organizationSet->type)) {
            $organizationSet->type();
        }

        $this->addColumn(widget_TableModelViewColumn('image', ' ')->setSortable(false)->setExportable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));
        $this->addColumn(widget_TableModelViewColumn($organizationSet->name, $Crm->translate('Organization')));
        if (isset($organizationSet->type)) {
            $this->addColumn(widget_TableModelViewColumn($organizationSet->type->name, $Crm->translate('Type')));
        }
        $this->addColumn(widget_TableModelViewColumn($organizationSet->activity, $Crm->translate('Activity')));

        $this->addColumn(widget_TableModelViewColumn($organizationSet->phone, $Crm->translate('Phone'))->setSearchable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($organizationSet->fax, $Crm->translate('Fax'))->setSearchable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($organizationSet->email, $Crm->translate('Email'))->setSearchable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($organizationSet->website, $Crm->translate('Web site'))->setSearchable(false)->setVisible(false));

        $this->addColumn(widget_TableModelViewColumn($organizationSet->address->street, $Crm->translate('Number / pathway'))->setSearchable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($organizationSet->address->postalCode, $Crm->translate('Postal code')));
        $this->addColumn(widget_TableModelViewColumn($organizationSet->address->city, $Crm->translate('City')));
        $this->addColumn(widget_TableModelViewColumn($organizationSet->address->cityComplement, $Crm->translate('Complement'))->setSearchable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($organizationSet->address->country->getNameField(), $Crm->translate('Country'))->setSearchable(true)->setVisible(false));
        if (isset($Crm->Tag)) {
            $this->addColumn(widget_TableModelViewColumn('tag', $Crm->translate('Tags'))->setSearchable(true)->setVisible(true));
        }
    }


    /**
     *
     */
    protected function getDisplayAction(crm_Organization $record)
    {
        return $this->Crm()->Controller()->Organization()->display($record->id);
    }

    /**
     * @param ORM_Record    $record
     * @param string        $fieldPath
     * @return Widget_Item
     */
    protected function computeCellTextContent(ORM_Record $record, $fieldPath)
    {
        $Crm = $this->Crm();

        switch ($fieldPath) {
            case 'tag':
                if (isset($Crm->Tag)) {
                    $tags = $this->tagSet->selectFor($record);
                    $values = array();
                    foreach ($tags as $tag) {
                        $values[] = $tag->targetId->label;
                    }
                    return implode(', ', $values);
                }
                break;
        }

        return parent::computeCellTextContent($record, $fieldPath);
    }

    /**
     * @param ORM_Record    $record
     * @param string        $fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        switch ($fieldPath) {

            case 'image':
                $displayAction = $this->getDisplayAction($record);
                return $W->Link(
                    $Crm->Ui()->OrganizationLogo($record, 22, 22),
                    $displayAction
                );

            case 'name':
                $displayAction = $this->getDisplayAction($record);
                return $W->Link(
                    self::getRecordFieldValue($record, $fieldPath),
                    $displayAction
                );
        }

        return parent::computeCellContent($record, $fieldPath);
    }


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::getFilterCriteria()
     */
    public function getFilterCriteria($filter = null)
    {
        $Crm = $this->Crm();
        $Access = $Crm->Access();

        $recordSet = $this->getRecordSet();

        $conditions = $Access->canPerformActionOnSet($recordSet, 'organization:read');

        if (isset($filter['import']) && !empty($filter['import'])) {
            $importRecord = $Crm->ImportSet()->get($filter['import']);
            if ($importRecord) {
                $linkSet = $Crm->LinkSet();

                $linkSet->joinTarget($recordSet->getRecordClassName());

                $conditions = $conditions->_AND_(
                    $recordSet->id->in(
                        $linkSet->sourceId->is($importRecord->id)->_AND_(
                            $linkSet->sourceClass->is(get_class($importRecord))
                        )->_AND_(
                            $linkSet->targetClass->is($recordSet->getRecordClassName())
                        ),
                        'targetId'
                    )
                );
            }
            unset($filter['import']);
        }

        $conditions = $conditions->_AND_(parent::getFilterCriteria($filter));

        return $conditions;
    }



    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleFilterInputWidget()
     */
    protected function handleFilterInputWidget($name, ORM_Field $field = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        if ($name === 'name') {
            return $Ui->SuggestOrganization();
        }
        if ($name === 'tag') {
            if (isset($Crm->Tag)) {
                return $Ui->SuggestTag();
            }
        }
        return parent::handleFilterInputWidget($name, $field);
    }
}









/**
 * Creates a generic form fragment for the specified set.
 *
 * @return Widget_Item
 */
function crm_organizationFilterForm(Func_Crm $Crm, $classifications = array())
{
    $W = bab_Widgets();
    $Ui = $Crm->Ui();

//	require_once dirname(__FILE__) . '/classification.ui.php';

    $layout = $W->FlowLayout();
    $layout->setVerticalSpacing(1, 'em')->setHorizontalSpacing(1, 'em');


    $layout->addItem(
        $W->VBoxItems(
            $W->Label($Crm->translate('Name')),
            $Ui->SuggestOrganization()->setSize(20)->setName('name')
        )
    );
    $layout->addItem(
        $W->VBoxItems(
            $W->Label($Crm->translate('Activity')),
            $W->LineEdit()
                ->setSize(20)->setName('activity')
        )
    );

    if (isset($Crm->OrganizationType))
    {
        $layout->addItem(
            $W->VBoxItems(
                $W->Label($Crm->translate('Type')),
                $W->LineEdit()->setSize(20)->setName('type/name')
            )
        );
    }

    $layout->addItem(
        $W->VBoxItems(
            $W->Label($Crm->translate('Postal code')),
            $Ui->SuggestPostalCode()->setSize(5)->setName('address/postalCode')
        )
    );
    $layout->addItem(
        $W->VBoxItems(
            $W->Label($Crm->translate('City')),
            $Ui->SuggestPlaceName()
                ->setMinChars(1)
                ->setSize(20)->setName('address/city')
        )
    );

    if (isset($Crm->Classification))
    {
        $layout->addItem(
            $W->Frame(null,
                $qualificationsVBox = $W->VBoxItems(
                    $W->Label($Crm->translate('Codification'))
                )
            )->setName('classification')
        );
    }

    if (isset($Crm->Tag))
    {
        $layout->addItem(
            $W->Frame(
                null,
                $W->VBoxItems(
                    $W->Label($Crm->translate('Tag')),
                    $Ui->SuggestTag()->setName('tag')
                )
            )
        );
    }

    $i = 0;
    if (isset($qualificationsVBox))
    {
        $qualificationsVBox->addItem($Ui->SuggestClassification()->setMinChars(0)->setSize(50)->setName('' . $i));
    }

    $layout->setVerticalAlign('bottom');

    return $layout;
}


/**
 * Creates a specific filter panel containing the table and an auto-generated form.
 *
 * @param widget_TableModelView $table
 * @param array					$filter
 * @param string				$name
 * @return Widget_Filter
 */
function crm_organizationFilteredTableView(Func_Crm $Crm, crm_TableModelView $table, $filter = null, $name = null)
{
    $W = bab_Widgets();

    $filterPanel = $W->Filter();
    $filterPanel->setLayout($W->VBoxLayout());
    if (isset($name)) {
        $filterPanel->setName($name);
    }

    $table->setPageLength(isset($filter['pageSize']) ? $filter['pageSize'] : 12);

    $table->setCurrentPage(isset($filter['pageNumber']) ? $filter['pageNumber'] : 0);

    $table->sortParameterName = $name . '[filter][sort]';

    if (isset($filter['sort'])) {
        $table->setSortField($filter['sort']);
    } else {
        $columns = $table->getVisibleColumns();
        foreach ($columns as $columnId => $column) {
            /* @var $column widget_TableModelViewColumn */
            if ($column->isSortable()) {
                $table->setSortField($columnId);
                break;
            }
        }
    }


    $classifications = isset($filter['classification']) ? $filter['classification'] : array();


    $layout = crm_organizationFilterForm($Crm, $classifications);

    $form = $W->Form();
    $form->setReadOnly(true);
    $form->setName('filter');
    $form->setLayout($layout);
    $form->colon();

    $form->addItem($W->SubmitButton()->setLabel($Crm->translate('Filter')));

    if (isset($filter) && is_array($filter)) {
        $form->setValues($filter, array($name, 'filter'));
    }
    $form->setHiddenValue('tg', bab_rp('tg'));
    $form->setHiddenValue('idx', bab_rp('idx'));

    $filterPanel->setFilter($form);
    $filterPanel->setFiltered($table);

    return $filterPanel;
}


/**
 *
 * @param bab_OrgChart $orgChart
 * @param crm_Organization $org
 */
function crm_appendOrgChartElement(Widget_OrgchartView $orgChart, crm_Organization $org)
{
    $Crm = $org->Crm();
    $Crm->includeAddressSet();

    $element = $orgChart->createElement($org->id, 'entity', $org->name, '', '');
    $address = $org->address();
    if ($address) {
        $element->setInfo($address->postalCode . ' ' . $address->city);
    }
    $orgChart->appendElement($element, $org->parent == 0 ? null : $org->parent);


    $T = @bab_functionality::get('Thumbnailer');

    if ($T) {
        $logoPath = $org->getLogoPath();
        if (!empty($logoPath)) {
            $T->setSourceFile($org->getLogoPath());
            $imageUrl = $T->getThumbnail(48, 48);
            $element->setIcon($imageUrl);
        }
    }

    $element->addAction(
        'toggle_members',
        $Crm->translate('Show/Hide organization contacts'),
        $GLOBALS['babSkinPath'] . 'images/Puces/members.png',
        '',
        'toggleMembers'
    );
    $element->addAction(
        'close_all_members',
        $Crm->translate('Hide all contacts from this organization'),
        $GLOBALS['babSkinPath'] . 'images/Puces/PuceCollapse.gif',
        '',
        'closeAllMembers'
    );
    $element->addAction(
        'open_all_members',
        $Crm->translate('Show all contacts from this organization'),
        $GLOBALS['babSkinPath'] . 'images/Puces/PuceExpand.gif',
        '',
        'openAllMembers'
    );

    $element->setLinkEntity($Crm->Controller()->Organization()->display($org->id)->url());

    $contactOrganizationSet = $Crm->ContactOrganizationSet();

    $today = date('Y-m-d');
    $contactOrganizations = $contactOrganizationSet->select(
        $contactOrganizationSet->organization->is($org->id)->_AND_(
            $contactOrganizationSet->history_to->is('0000-00-00')->_OR_($contactOrganizationSet->history_to->greaterThan($today))
        )->_AND_(
            $contactOrganizationSet->history_from->is('0000-00-00')->_OR_($contactOrganizationSet->history_from->lessThanOrEqual($today))
        )
    );

    foreach ($contactOrganizations as $contactOrganization) {
        /* @var $contact crm_OrganizationPerson */
        if ($contact = $contactOrganization->contact()) {
            $element->addMember(
                $contact->getFullName(),
                $contactOrganization->position,
                $Crm->Controller()->Contact()->display($contact->id)->url()
            );
        }
    }
}



/**
 *
 * @param Func_Crm $Crm
 * @param int      $orgs
 *
 * @return bab_OrgChart
 */
function crm_orgChart(Func_Crm $Crm, $orgs, $threshold = null)
{
    $W = bab_Widgets();

    $organizationSet = $Crm->OrganizationSet();

    if (!is_array($orgs)) {
        $orgs = array($orgs);
    }

    $orgChartId = 'crm_orgChart_' . implode('_', $orgs);
    $orgChart = $W->OrgchartView($orgChartId);

    $nodeIds = array();
    $nbEstablishments = 0;
    foreach ($orgs as $org) {
        $groupOrg = $organizationSet->get($org);
        if (!isset($groupOrg)) {
            return $orgChart;
        }
        $ancestors = array();
        while ($groupOrg->parent != 0) {
            $groupOrg = $organizationSet->get($groupOrg->parent);
            if (isset($ancestors[$groupOrg->parent])) {
                break;
            }
            $ancestors[$groupOrg->parent] = $groupOrg->parent;
        }

        $nodeIds[] = $orgChartId. '.' . $groupOrg->id;
        crm_appendOrgChartElement($orgChart, $groupOrg);

        $accountOrgs = $groupOrg->getChildren();
        foreach ($accountOrgs as $accountOrg) {
            $nodeIds[] = $orgChartId. '.' . $accountOrg->id;
            crm_appendOrgChartElement($orgChart, $accountOrg);

            $establishmentOrgs = $accountOrg->getChildren();
            foreach ($establishmentOrgs as $establishmentOrg) {
                $nodeIds[] = $orgChartId . '.' . $establishmentOrg->id;
                crm_appendOrgChartElement($orgChart, $establishmentOrg);
                $nbEstablishments++;

                $subEstablishmentOrgs = $establishmentOrg->getChildren();
                foreach ($subEstablishmentOrgs as $subEstablishmentOrg) {
                    $nodeIds[] = $orgChartId . '.' . $subEstablishmentOrg->id;
                    crm_appendOrgChartElement($orgChart, $subEstablishmentOrg);
                    $nbEstablishments++;
                }
            }
        }
        if (!isset($threshold)) {
            $threshold = ($nbEstablishments > 8 ? 3 : 4);
        }
        $orgChart->setVerticalThreshold($threshold);

        $orgChart->highlightElement($org);
    }

    $orgChart->setPersistent(true, Widget_Widget::STORAGE_LOCAL);

    $orgChart->setOpenNodes($nodeIds);

    $orgChart->setOpenMembers($nodeIds);

    return $orgChart;
}





/**
 * Returns a Widget_Treeview corresponding to the organizations hierarchy of $organizationId.
 *
 * @param Func_Crm 		$Crm
 * @param int			$organizationId	The id of the root organization of the treeview.
 * @param Widget_Action	$action			An optional action.
 * 										If set, the treeview node will link to the corresponding url
 * 										taking a parameter containing the organization id.
 * @param string		$paramName		The name of the parameter containing the organization id.
 *
 * @return Widget_SimpleTreeView
 */
function crm_organizationTreeView(Func_Crm $Crm, $organizationId, Widget_Action $action = null, $paramName = 'organization')
{
    $W = bab_Widgets();


    $organizationSet = $Crm->OrganizationSet();

    /* @var $mainOrganization betom_Organization */
    $mainOrganization = $organizationSet->get($organizationId);

    $organizations = array($organizationId => $organizationId);
    if (isset($mainOrganization)) {
       $mainOrganization->getDescendants($organizations);
    }

    $organizationTreeview = $W->SimpleTreeView();

    $displayedOrgs = array();


    foreach ($organizations as $organizationId) {

        if (isset($action)) {
            $action->setParameter($paramName, $organizationId);
            $displayOrganizationDocumentsUrl = $action->url();
        } else {
            $displayOrganizationDocumentsUrl = null;
        }


        $organization = $organizationSet->get($organizationId);

        if (!isset($organization)) {
            continue;
        }


        $element = $organizationTreeview->createElement($organizationId, 'organization', $organization->name, '', $displayOrganizationDocumentsUrl);
        $element->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/folder.png');

        if ($organization->parent && isset($displayedOrgs[$organization->parent])) {
            $organizationTreeview->appendElement($element, $organization->parent);
        } else {
            $organizationTreeview->appendElement($element, null);
        }

        $displayedOrgs[$organization->id] = $organization->id;
    }

    return $organizationTreeview;
}



