<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * list of packaging of articles from back office
 *
 */
class crm_PackagingTableView extends crm_TableModelView
{
	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(ORM_Record $record, $fieldPath)
	{
		$W = bab_Widgets();
		$Crm = $record->Crm();
		$Ui = $Crm->Ui();

		/*@var $Ui crm_Ui */

		$editAction = $Crm->Controller()->Packaging()->edit($record->id);

		switch ($fieldPath) {

			case '_edit_':
				return $W->Link($W->Icon($Crm->translate('Edit'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $editAction);
				break;
		}
		
		

		return parent::computeCellContent($record, $fieldPath);
	}







}







/**
 * 
 */
class crm_PackagingEditor extends crm_Editor
{
	/**
	 * 
	 * @var crm_Packaging
	 */
	protected $packaging = null;
	
	
	public function __construct(Func_Crm $Crm, crm_Packaging $packaging = null, $id = null, Widget_Layout $layout = null)
	{
		$this->packaging = $packaging;
		
		parent::__construct($Crm, $id, $layout);
		$this->setName('packaging');
		
		$this->addFields();
		$this->addButtons();
		
		$this->setHiddenValue('tg', bab_rp('tg'));
		
		if (isset($packaging)) {
			$this->setHiddenValue('packaging[id]', $packaging->id);
			$this->setValues($packaging->getValues(), array('packaging'));
		}
	}
	
	
	protected function addFields()
	{
		$this->addItem($this->name());
		$this->addItem($this->description());
		$this->addItem($this->min());
	}
	
	
	protected function addButtons()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
		
		$id = null !== $this->packaging ? $this->packaging->id : null;
		
		$this->addButton(
				$W->SubmitButton()
				->setLabel($Crm->translate('Save'))
				->validate(true)
				->setAction($Crm->Controller()->Packaging()->save())
				->setSuccessAction(crm_BreadCrumbs::getPosition(-1))
				->setFailedAction($Crm->Controller()->Packaging()->edit($id))
		);
		
		
		
		$this->addButton(
				$W->SubmitButton()
				->setLabel($Crm->translate('Cancel'))
				->setAction(crm_BreadCrumbs::getPosition(-1))
		);
	}
	
	
	protected function name()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
	
		return $this->labelledField(
				$Crm->translate('Name'),
				$W->LineEdit()->setSize(70)->setMaxSize(100)->setMandatory(true, $Crm->translate('The name is mandatory')),
				__FUNCTION__
		);
	}
	
	
	protected function description()
	{
		$Crm = $this->Crm();
		$W = $this->widgets;
	
		return $this->labelledField(
				$Crm->translate('Description'),
				$W->TextEdit(),
				__FUNCTION__
		);
	}
	
	
	protected function min()
	{
	    $Crm = $this->Crm();
	    $W = $this->widgets;
	
	    return $this->labelledField(
	        $Crm->translate('Minimal order quantity'),
	        $W->LineEdit()->setSize(4),
	        __FUNCTION__
	    );
	}
	
}



