<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




class crm_ImportTableView extends crm_TableModelView
{

	/**
	 * Handle filter field input widget
	 * If the method return null, no filter field is displayed for the field
	 *
	 * @param	string			$name		table field name
	 * @param 	ORM_Field		$field		ORM field
	 *
	 * @return Widget_InputWidget | null
	 */
	protected function handleFilterInputWidget($name, ORM_Field $field = null) {

		if ('itemcount' === $name)
		{
			return null;
		}

		return parent::handleFilterInputWidget($name, $field);
	}



	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(ORM_Record $record, $fieldPath)
	{
		$W = bab_Widgets();

		if ($fieldPath == 'title') {
			return $W->Link($W->Icon($record->title, Func_Icons::ACTIONS_DOCUMENT_PROPERTIES), $this->Crm()->Controller()->Import()->Display($record->id));
		}

		if ($fieldPath == 'createdOn') {
			require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
			return $W->Label(BAB_DateTimeUtil::relativePastDate($record->createdOn));
		}


		return parent::computeCellContent($record, $fieldPath);
	}

}




/**
 * List of imported article linked to a crm_Import
 *
 */
class crm_ImportedArticleTableView extends crm_TableModelView
{




	/**
	 * Handle filter field input widget
	 * If the method return null, no filter field is displayed for the field
	 *
	 * @param	string			$name		table field name
	 * @param 	ORM_Field		$field		ORM field
	 *
	 * @return Widget_InputWidget | null
	 */
	protected function handleFilterInputWidget($name, ORM_Field $field = null) {

		$widget = parent::handleFilterInputWidget($name, $field);

		if ('targetId/reference' === $name)
		{
			$widget->setSize(10);
		}

		if ('targetId/name' === $name)
		{
			$widget->setSize(40);
		}


		return $widget;
	}



	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(ORM_Record $record, $fieldPath)
	{
		$W = bab_Widgets();

		if ('targetId/reference' === $fieldPath)
		{
			return $W->Link(
				$W->Icon($record->targetId->reference, Func_Icons::ACTIONS_DOCUMENT_PROPERTIES),
				$this->Crm()->Controller()->Article()->display($record->targetId->id)
			);
		}

		return parent::computeCellContent($record, $fieldPath);
	}

}







class crm_ImportFullFrame extends crm_CardFrame
{
	protected $import;

	public function __construct(Func_Crm $Crm, crm_Import $import)
	{
		$this->import = $import;
		parent::__construct($Crm, null, $this->getLayout());

	}


	protected function getLayout()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		$layout = $W->VBoxLayout()->setVerticalSpacing(.5,'em');

		if ($title = $this->import->title) {
			$layout->addItem($W->Title($title, 2));
		}

		$layout->addItem($this->labelStr($Crm->translate('File name'), $this->import->filename));
		$layout->addItem($this->labelStr($Crm->translate('Date'), bab_longDate(bab_mktime($this->import->createdOn))));
		$layout->addItem($this->labelStr($Crm->translate('Number of imported item'), $this->import->itemcount));

		return $layout;
	}


}









/**
 * @return Widget_Form
 */
class crm_ImportEditor extends crm_Editor
{

	protected $import;


	public function __construct(Func_Crm $crm, crm_Import $import = null)
	{
		parent::__construct($crm);

		$this->setName('import');
		$this->setHiddenValue('tg', bab_rp('tg'));
		$this->import = $import;

		$this->setSaveAction($this->Crm()->Controller()->Import()->save());
		$this->setCancelAction($this->Crm()->Controller()->Import()->cancel());


		$this->colon();
		$this->addFields();

		if ($import)
		{
			$this->setValues(array('import' => $import->getValues()));
		}


	}



	/**
	 * Add a default field set to form
	 *
	 *
	 */
	protected function addFields()
	{
		$W = bab_Widgets();

		$this->addItem($W->Hidden()->setName('id'));

		$this->addItem($this->Title());
		$this->addItem($this->Attachment());


	}



	/**
	 *
	 * @return Widget_Item
	 */
	protected function Title()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		return $this->labelledField(
			$Crm->translate('Title'),
			$W->LineEdit()->setSize(50),
			'title'
		);
	}


	/**
	 *
	 * @return Widget_Item
	 */
	protected function Attachment()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();

		if (null === $this->import || empty($this->import->filename)) {

			return $this->labelledField(
				$Crm->translate('Import file'),
				$W->FilePicker()->oneFileMode(),
				'attachment'
			);
		}

		// the file can be uploaded once

		return $W->HBoxItems($W->Label($Crm->translate('Import file'))->colon(), $W->Label($this->import->filename))->setHorizontalSpacing(.5,'em');
	}
}
















/**
 *
 * @param crm_Import $import
 *
 * @return Widget_Item
 */
function crm_importHistory(crm_Import $record)
{
	$W = bab_Widgets();
	$Crm = $record->Crm();

	$Crm->includeNoteSet();

	// Recent general history
	$historyFrame = $W->VBoxLayout()
				->addClass('icon-left-16 icon-16x16 icon-left')
				->addClass('crm-recent-history')
		->addItem($W->Title($Crm->translate('Import report'), 2));

	$notes = $record->selectNotes();

	$notes->orderAsc($notes->getSet()->targetId->createdOn);
	$notes->orderAsc($notes->getSet()->id);


	if (0 === $notes->count()) {

		$historyFrame->addItem($W->RichText($Crm->translate('This import is successfull and has no messages registerd in the report')));
	} else {

		foreach ($notes as $note) {
			$noteDate = bab_shortDate(bab_mktime($note->targetId->createdOn), true);

			$historyFrame->addItem(
				$W->FlowItems(
				    $W->Label($noteDate)->addClass('widget-strong', 'icon', Func_Icons::STATUS_DIALOG_INFORMATION),
				    $W->Label($note->targetId->summary)
				)->setVerticalAlign('top')->setHorizontalSpacing(2, 'em')
				->setSizePolicy('widget-list-element crm-note')
			);
		}
	}

	$historyFrame->addItem(
		$W->HBoxItems(
			$W->Link($W->Icon($Crm->translate('Imported items'), Func_Icons::ACTIONS_EDIT_FIND), $record->getListAction())
		)
		->addClass('icon-left-16 icon-left icon-16x16')
		->setHorizontalSpacing(0.5, 'em')
	);

	return $historyFrame;
}





