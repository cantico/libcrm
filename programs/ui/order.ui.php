<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */



bab_Widgets()->includePhpClass('widget_TableView');

class crm_OrderTableView extends crm_TableModelView
{
    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $orderSet)
    {
        $Crm = $this->Crm();

        $this->addColumn(widget_TableModelViewColumn($orderSet->name, $Crm->translate('Number')));
        $this->addColumn(widget_TableModelViewColumn($orderSet->type, $Crm->translate('Document type'))->setSearchable(true)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($orderSet->request_date, $Crm->translate('Date')));
        $this->addColumn(widget_TableModelViewColumn($orderSet->billingperson, $Crm->translate('Billing recipient')));
        $this->addColumn(widget_TableModelViewColumn($orderSet->deliveryperson, $Crm->translate('Delivery recipient'))->setSearchable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($orderSet->deliveryaddress->postalCode, $Crm->translate('Delivery postal code'))->setSearchable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($orderSet->deliveryaddress->city, $Crm->translate('Delivery city')));
        $this->addColumn(widget_TableModelViewColumn($orderSet->total_df, $Crm->translate('Total exl. tax'))->setSearchable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($orderSet->total_ti, $Crm->translate('Total incl. tax'))->setSearchable(false));
        if (isset($Crm->ShoppingCart))
        {
            $this->addColumn(widget_TableModelViewColumn($orderSet->shoppingcart->id, $Crm->translate('Shopping cart / Order'))->setSearchable(false)->setVisible(false));
            $this->addColumn(widget_TableModelViewColumn($orderSet->shoppingcart->status, $Crm->translate('Shopping cart / status'))->setSearchable(false));
            $this->addColumn(widget_TableModelViewColumn($orderSet->shoppingcart->paymentToken, $Crm->translate('Payment token'))->setSearchable(false)->setVisible(false));
            $this->addColumn(widget_TableModelViewColumn($orderSet->autorization_id, $Crm->translate('Autorization ID'))->setSearchable(false)->setVisible(false));
            $this->addColumn(widget_TableModelViewColumn($orderSet->transaction_id, $Crm->translate('Transaction ID'))->setSearchable(false)->setVisible(false));
        }
    }


    /**
     * Get first type in the list
     * @return int
     */
    public function getFirstType()
    {
        $res = $this->getDataSource();
        foreach($res as $order)
        {
            /*@var $order crm_Order */
            return $order->type;
        }

        return null;
    }



    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
        $W = bab_Widgets();

        if ($fieldPath == 'name') {
            $value = self::getRecordFieldValue($record, $fieldPath);
            return $W->Link($W->Icon($value, Func_Icons::ACTIONS_DOCUMENT_PROPERTIES), $this->Crm()->Controller()->Order()->display($record->id));
        }

        if ($fieldPath == 'createdOn') {
            return $W->Label(BAB_DateTimeUtil::relativePastDate($record->createdOn));
        }

        if ('billingrecipient' === $fieldPath) {
            if (!empty($record->billingrecipient)) {
                return $W->Label($record->billingrecipient);
            }

            if (!empty($record->billingorganization->name)) {
                return $W->Link($W->Icon($record->billingorganization->name, Func_Icons::ACTIONS_USER_GROUP_PROPERTIES), $this->Crm()->Controller()->Organization()->display($record->billingorganization->id));
            }
        }

        if ($fieldPath == 'shoppingcart/id')
        {
            $value = self::getRecordFieldValue($record, $fieldPath);
            return $W->Link($W->Icon($value, Func_Icons::ACTIONS_DOCUMENT_PROPERTIES), $this->Crm()->Controller()->ShoppingCartAdm()->display($record->shoppingcart->id));
        }

        return parent::computeCellContent($record, $fieldPath);
    }




    public function handleFilterFields(Widget_Item $form)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        parent::handleFilterFields($form);

        $label = $W->Label($Crm->translate('Sold item name or reference'));
        $input = $W->LineEdit()->setName('item')->setSize(30);

        $label->setAssociatedWidget($input);

        $form->addItem($this->handleFilterLabel($label, $input));
    }

}




class crm_OrderItemTableView extends crm_TableModelView
{
    protected $order = null;


    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ORM_RecordSet $set)
    {
        $Crm = $this->Crm();

        $this->addColumn(widget_TableModelViewColumn($set->parentorder, $Crm->translate('Invoice')));
        $this->addColumn(widget_TableModelViewColumn($set->reference, $Crm->translate('Reference')));
        $this->addColumn(widget_TableModelViewColumn($set->name, $Crm->translate('Name')));
        $this->addColumn(widget_TableModelViewColumn($set->packaging, $Crm->translate('Packaging')));
        $this->addColumn(widget_TableModelViewColumn($set->unit_cost, $Crm->translate('Unit cost'))->setSearchable(false));
        $this->addColumn(widget_TableModelViewColumn($set->quantity, $Crm->translate('Quantity'))->setSearchable(false));
        $this->addColumn(widget_TableModelViewColumn($set->vat, $Crm->translate('VAT'))->setSearchable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn('total_df', $Crm->translate('Total tax excl'))->setSearchable(false));
        $this->addColumn(widget_TableModelViewColumn('total_ti', $Crm->translate('Total tax incl'))->setSearchable(false)->setVisible(false));
        $this->addColumn(widget_TableModelViewColumn($set->reduction, $Crm->translate('Reduction'))->setSearchable(false)->setVisible(false));
    }


    public function setOrder(crm_Order $order)
    {
        $set = $order->getParentSet();
        /*@var $set crm_OrderSet */
        if (null !== $order->currency && !($set->currency instanceof crm_CurrencySet)) {
            throw new Exception('missing join on currency');
        }
        
        $this->order = $order;
        $this->setDataSource($order->getItems());

        $this->addClass('icon-16x16')->addClass('icon-left')->addClass('icon-left-16')
            ->addClass('crm-orderitem-edit-tableview')
            ->setName('items')
        ;

        return $this;
    }








    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        if (isset($record->parentorder) && $record->parentorder instanceof crm_Order)
        {
            $order = $record->parentorder;
        } else if ($this->order instanceof crm_Order)
        {
            $order = $this->order;
        } else {
            throw new ErrorException('Missing order');
        }

        if ('name' === $fieldPath && $record->subtitle) {
            return $W->VBoxItems(
                $W->label($record->name)->addClass('crm-strong'),
                $W->label($record->subtitle),
                $W->label($record->packaging)
            );
        }

        if ('unit_cost' === $fieldPath) {
            return $W->label($order->moneyFormat($record->unit_cost));
        }

        if ('quantity' === $fieldPath) {
            return $W->label($Crm->numberFormat($record->quantity));
        }

        if ('vat' === $fieldPath) {
            return $W->label($Crm->numberFormat($record->vat));
        }


        if ('total_df' === $fieldPath) {
            return $W->label($order->moneyFormat($record->getTotalDF()));
        }

        if ('total_ti' === $fieldPath) {
            return $W->label($order->moneyFormat($record->getTotalTI()));
        }

        if ('edit' === $fieldPath) {
            return $W->Link($W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT), $this->Crm()->Controller()->Order()->editItem($record->id));
        }

        return parent::computeCellContent($record, $fieldPath);
    }


    /**
     *
     * @return Widget_Item
     */
    protected function computeTotalDfCellContent()
    {
        $W = bab_Widgets();

        return $W->Label($this->order->moneyFormat($this->order->total_df));
    }


    /**
     *
     * @return Widget_Item
     */
    protected function computeTaxNameCellContent(crm_OrderTax $taxRecord)
    {
        $W = bab_Widgets();
        return $W->Label($taxRecord->name);
    }



    /**
     *
     * @return Widget_Item
     */
    protected function computeTaxCellContent(crm_OrderTax $taxRecord)
    {
        $W = bab_Widgets();
        return $W->Label($this->order->moneyFormat($taxRecord->amount));
    }


    /**
     *
     * @return Widget_Item
     */
    protected function computeTotalTiCellContent()
    {
        $W = bab_Widgets();
        return $W->Label($this->order->moneyFormat($this->order->total_ti));
    }



    protected function getExtraRows()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        if (!isset($this->order))
        {
            return array();
        }

        $cols = $this->getVisibleColumns();
        if (!isset($cols['total_df'])) {
            return array();
        }

        $extra = array(
            array(
                'name' => $W->Label($Crm->translate('Total excluding taxes')),
                'total_df' => $this->computeTotalDfCellContent()
            )
        );

        $cols = $this->getVisibleColumns();

        foreach($this->order->getTaxes() as $taxRecord) {

            $arr = array(
                'name' => $this->computeTaxNameCellContent($taxRecord),
                'total_df' => $this->computeTaxCellContent($taxRecord)
            );

            if (isset($cols['edit']))
            {
                $arr['edit'] = $W->Link($W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT), $this->Crm()->Controller()->Order()->editTax($taxRecord->id));
            }

            $extra[] = $arr;
        }


        $label = $Crm->translate('Total taxes included');

        if ($this->order->ignorevalue) {
            $label .= ' '.$Crm->translate('(Ignored in sales)');
        }

        $extra[] = array(
            'name' => $W->Label($label),
            'total_df' => $this->computeTotalTiCellContent()
        );


        return $extra;
    }


    protected function initFooterRow($row)
    {
        parent::initFooterRow($row);

        $extraRows = $this->getExtraRows();
        $W = bab_Widgets();



        foreach ($extraRows as $extra) {
            $col = 0;
            foreach ($this->columns as $columnId => $column) {

                if (isset($extra[$columnId])) {
                    $this->addItem($extra[$columnId], $row, $col);
                } else {
                    $this->addItem($W->Label(''), $row, $col);
                }

                $col++;
            }




            $row++;
        }
    }

}







class crm_OrderItemEditTableView extends crm_OrderItemTableView
{

    public function setOrder(crm_Order $order)
    {
        parent::setOrder($order);
        $this->addClass('crm-orderitem-edit-tableview')->setName('items');

        return $this;
    }



    /**
     *
     * {@inheritDoc}
     * @see widget_TableModelView::handleCell()
     */
    protected function handleCell(ORM_Record $record, $fieldPath, $row, $col, $name = null, $rowSpan = null, $colSpan = null)
    {
        return parent::handleCell($record, $fieldPath, $row, $col, $record->id);
    }



    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {
        $W = bab_Widgets();

        if ('name' === $fieldPath) {
            return $W->LineEdit()->setSize(30)->setName('name')->setValue($record->name);
        }

        if ('unit_cost' === $fieldPath) {
            return $W->LineEdit()->setSize(6)->setName('unit_cost')->setValue($this->Crm()->numberFormat($record->unit_cost));
        }

        if ('quantity' === $fieldPath) {
            return $W->LineEdit()->setSize(6)->setName('quantity')->setValue($this->Crm()->numberFormat($record->quantity));
        }

        if ('vat' === $fieldPath) {
            return $W->LineEdit()->setSize(6)->setName('vat')->setValue($this->Crm()->numberFormat($record->vat));
        }


        if ('total_df' === $fieldPath) {

            $total = $record->unit_cost * $record->quantity;
            return $W->LineEdit()->setSize(6)->setName('total_df')->setValue($this->Crm()->numberFormat($total))->disable();
        }

        return parent::computeCellContent($record, $fieldPath);
    }


    /**
     *
     * @return Widget_Item
     */
    protected function computeTotalDfCellContent()
    {
        $W = bab_Widgets();
        return $W->LineEdit()->setSize(6)->setName('total_df')->setValue($this->Crm()->numberFormat($this->order->total_df))->disable();
    }


    protected function computeTaxNameCellContent(crm_OrderTax $taxRecord)
    {
        $W = bab_Widgets();
        $input = $W->LineEdit()->setSize(30)->setName('tax]['.$taxRecord->id.'][name')->setValue($taxRecord->name);
        return $input;
    }

    /**
     *
     * @return Widget_Item
     */
    protected function computeTaxCellContent(crm_OrderTax $taxRecord)
    {
        $W = bab_Widgets();
        $input = $W->LineEdit()->setSize(6)->setName('tax]['.$taxRecord->id.'][amount')->setValue($this->Crm()->numberFormat($taxRecord->amount));

        if ($taxRecord->readonly) {
            $input->disable();
        }

        return $input;
    }

    /**
     *
     * @return Widget_Item
     */
    protected function computeTotalTiCellContent()
    {
        $W = bab_Widgets();
        return $W->LineEdit()->setSize(6)->setName('total_ti')->setValue($this->Crm()->numberFormat($this->order->total_ti))->disable();
    }

}












/**
 *
 */
class crm_OrderEditor extends crm_Editor
{
    private $order = null;

    public function setOrder(crm_Order $order)
    {
        $arr = $order->getValues();

        $arr['billingorganization'] = $order->billingorganization->id;
        $arr['billingorganizationname'] = $order->billingorganization->name;

        $arr['shippingorganization'] = $order->shippingorganization->id;
        $arr['shippingorganizationname'] = $order->shippingorganization->name;

        $arr['deliveryorganization'] = $order->deliveryorganization->id;
        $arr['deliveryorganizationname'] = $order->deliveryorganization->name;

        $this->setValues(array('order' => $arr));
        $this->setHiddenValue('order[id]', $order->id);

        $this->order = $order;
    }


    /**
     * Fields that will appear at the beginning of the form.
     *
     */
    protected function prependFields()
    {
        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->colon();

        $this->setSaveAction($this->Crm()->Controller()->Order()->save())
            ->setCancelAction($this->Crm()->Controller()->Order()->cancel());
    }


    public function addFields()
    {
        $W = $this->widgets;

        $this->setName('order');

        $this
            ->addItem($W->HBoxItems(
                $this->name(),
                $this->request_date(),
                $this->formal_notice_date(),
                $this->currency()
            )->setHorizontalSpacing(2,'em'))

            ->addItem($W->HBoxItems($this->type(), $this->recipienttype())->setHorizontalSpacing(2,'em'))

            ->addItem($this->Addresses())
            ->addItem($this->header())
            ->addItem($this->ItemEditTableView())
            ->addItem($this->footer())
            ->addItem($this->ignorevalue())

        ;


    }


    /**
     *
     * @return Widget_Item
     */
    protected function name()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('Number'),
            $W->LineEdit()->setSize(15), // not mandatory to allow automatic number creation
            __FUNCTION__
        );
    }

    /**
     *
     * @return Widget_Item
     */
    protected function request_date()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('Date'),
            $W->DatePicker(),
            __FUNCTION__
        );
    }


    /**
     *
     * @return Widget_Item
     */
    protected function formal_notice_date()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('Formal notice date'),
            $W->DatePicker(),
            __FUNCTION__
        );
    }




    /**
     *
     * @return Widget_Item
     */
    protected function type()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('The document is a'),
            $W->Select()->setOptions($Crm->translateArray(crm_Order::getTypes())),
            __FUNCTION__
        );
    }




    /**
     *
     * @return Widget_Item
     */
    protected function currency()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;
        $set = $Crm->CurrencySet();

        return $this->labelledField(
            $Crm->translate('Currency'),
            $W->Select()->setOptions($set->getOptions()),
            __FUNCTION__
        );
    }



    /**
     *
     * @return Widget_Item
     */
    protected function recipienttype()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('The recipient is a'),
            $W->Select()->setOptions(crm_Order::getRecipientTypes()),
            __FUNCTION__
        );
    }




    protected function Addresses()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        $tabs = $W->Tabs();

        $tabs
            ->addItem($W->Tab()->noTitle()->setLabel($Crm->translate('Billing address'))->addItem($this->Address('billing')))
            ->addItem($W->Tab()->noTitle()->setLabel($Crm->translate('Shipping address'))->addItem($this->Address('shipping')))
            ->addItem($W->Tab()->noTitle()->setLabel($Crm->translate('Delivery address'))->addItem($this->Address('delivery')))
        ;

        return $tabs;
    }


    /**
     * Address contener
     * the address form is filled with organization billing informations
     *
     * @param	string	$prefix
     * @param	array	$fields		List of form fields to add in ajax query
     *
     * @return Widget_Item
     */
    protected function Address($prefix, $fields = array())
    {
        $Crm = $this->Crm();
        $W = $this->widgets;
        $Crm->Ui()->includeAddress();

        $layout = $W->VBoxLayout()->setVerticalSpacing(.5,'em')->addClass('crm-loading-box');

        /* @var $suggestOrganization crm_SuggestOrganization */
        $suggestOrganization = $Crm->Ui()->SuggestOrganization()
            ->setIdName($prefix.'organization')
            ->setSize(50)
            ->onKeyUp('orderAddress', 'window.babAddonLibCrm')
            ->setMetadata('selfpage', bab_getSelf())
            ->setMetadata('tg', bab_rp('tg'))
            ->setMetadata('type', $prefix)
            ->setMetadata('action', $Crm->Controller()->Order()->organizationFormData()->getMethod())
        ;

        $layout->addItem($this->labelledField($Crm->translate('Organization'), $suggestOrganization, $prefix.'organizationname'));
        $layout->addItem($this->labelledField($Crm->translate('Recipient') ,$W->LineEdit()->setSize(50), $prefix.'person'));
        $layout->addItem(crm_addressEditorFrame($Crm, true)->setName($prefix.'address'));

        return $layout;
    }





    /**
     * Address contener without organization information
     *
     * @return Widget_Item
     */
    protected function RecipientAddress($prefix)
    {
        $Crm = $this->Crm();
        $W = $this->widgets;
        $Crm->Ui()->includeAddress();

        $layout = $W->VBoxLayout()->setVerticalSpacing(.5,'em')->addClass('crm-loading-box');

        $layout->addItem($this->labelledField($Crm->translate('Recipient'), $W->LineEdit()->setSize(50), $prefix.'person'));
        $layout->addItem(crm_addressEditorFrame($Crm, true)->setName($prefix.'address'));

        return $layout;
    }







    /**
     *
     * @return crm_OrderItemEditTableView
     */
    protected function ItemEditTableView()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        if (!$this->order) {
            return $W->Label('');
        }


        $table = new crm_OrderItemEditTableView($Crm);
        $table->setOrder($this->order);




        $table->setVisibleColumns(array(
            'name' 			=> $Crm->translate('Name'),
            'quantity' 		=> $Crm->translate('Quantity'),
            'unit_cost' 	=> $Crm->translate('Unit cost'),
            'vat' 			=> $Crm->translate('VAT'),
            'total_df'		=> $Crm->translate('Total excluding taxes')
        ));


        $table->addColumnClass(1, 'numeric');
        $table->addColumnClass(2, 'numeric');
        $table->addColumnClass(3, 'numeric');
        $table->addColumnClass(4, 'numeric');

        return $table;
    }



    /**
     * Header
     * @return Widget_Item
     */
    protected function header()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        return $this->labelledField(
            $Crm->translate('Header introduction'),
            $W->TextEdit()->setColumns(80)->setLines(2),
            __FUNCTION__
        );
    }

    /**
     * Footer
     * @return Widget_Item
     */
    protected function footer()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('Footer comments'),
            $W->TextEdit()->setColumns(80)->setLines(2),
            __FUNCTION__
        );
    }



    /**
     * Footer
     * @return Widget_Item
     */
    protected function ignorevalue()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('Ignore amount in sales (not for value)'),
            $W->CheckBox(),
            __FUNCTION__
        );
    }
}











class crm_OrderItemEditor extends crm_Editor
{
    /**
     *
     * @var int
     */
    protected $order;


    /**
     *
     * @var crm_OrderItem
     */
    protected $orderItem;


    public function setOrderItem(crm_OrderItem $orderItem)
    {
        $arr = $orderItem->getFormOutputValues();

        if ($arr['catalogitem']) {
            $this->Crm()->includeArticleSet();
            $set = $this->Crm()->CatalogItemSet()->join('article');
            $catalogitem = $set->get($arr['catalogitem']);

            if ($catalogitem) {
                $arr['catalogitemreference'] = $catalogitem->article->reference;
            }

        }

        $this->setValues(array('orderItem' => $arr));
        $this->setHiddenValue('orderItem[id]', $orderItem->id);

        $this->orderItem = $orderItem;
        $this->order = $orderItem->parentorder;
    }


    /**
     * @param	int		$order
     */
    public function setOrder($order)
    {
        $this->order = $order;
        $this->setHiddenValue('order', $order);

        // default quantity value

        if (null === $this->getValue(array('orderItem', 'quantity')))
        {
            $this->setValue(array('orderItem', 'quantity'), 1);
        }
    }


    /**
     * Fields that will appear at the beginning of the form.
     *
     */
    protected function prependFields()
    {
        $this->setName('orderItem');


        $this->setHiddenValue('tg', bab_rp('tg'));
        $this
            ->setSaveAction($this->Crm()->Controller()->Order()->saveItem())
            ->setCancelAction($this->Crm()->Controller()->Order()->cancel())
            ->colon()
        ;
    }


    public function addFields()
    {
        $W = $this->widgets;


        $this
            ->addItem($this->name())
            ->addItem($this->description())
            ->addItem(
                $W->HBoxItems(
                    $this->unit_cost(),
                    $this->quantity(),
                    $this->vat()
                )->setHorizontalSpacing(2,'em')
            )

            ->addItem($this->catalogitem())
        ;


    }


    /**
     *
     * @return Widget_Item
     */
    protected function name()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('Name'),
            $W->LineEdit()->setSize(50),
            __FUNCTION__
        );
    }



    protected function description()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        return $this->labelledField(
            $Crm->translate('Description'),
            $W->TextEdit()->setColumns(70)->setLines(5),
            __FUNCTION__
        );
    }



    /**
     *
     * @return Widget_Item
     */
    protected function quantity()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('Quantity'),
            $W->LineEdit()->setSize(6),
            __FUNCTION__
        );
    }




    /**
     *
     * @return Widget_Item
     */
    protected function unit_cost()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('Unit cost'),
            $W->LineEdit()->setSize(6),
            __FUNCTION__
        );
    }



    /**
     *
     * @return Widget_Item
     */
    protected function vat()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('VAT'),
            $W->LineEdit()->setSize(6),
            __FUNCTION__
        );
    }




    protected function catalogitem()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        return $this->labelledField(
            $Crm->translate('Article reference'),
            $this->Crm()->Ui()->SuggestCatalogItem()
                ->setIdName(__FUNCTION__)
                ->onKeyUp('orderItemCatalogItem', 'window.babAddonLibCrm')
                ->setMetadata('selfpage', bab_getSelf())
                ->setMetadata('tg', bab_rp('tg'))
                ->setMetadata('order', $this->order)
                ->setMetadata('action', $this->Crm()->Controller()->Order()->catalogItemFormData()->getMethod())
                ,
            'catalogitemreference',
            $Crm->translate('Search an article from visible catalogs by name or reference')
        );
    }
}














class crm_OrderTaxEditor extends crm_Editor
{
    /**
     *
     * @var int
     */
    protected $order;


    /**
     *
     * @var crm_OrderTax
     */
    protected $orderTax;


    public function setOrderTax(crm_OrderTax $orderTax)
    {
        $arr = $orderTax->getFormOutputValues();

        $this->setValues(array('orderTax' => $arr));
        $this->setHiddenValue('orderTax[id]', $orderTax->id);

        $this->orderTax = $orderTax;
        $this->order = $orderTax->parentorder;
    }


    /**
     * @param	int		$order
     */
    public function setOrder($order)
    {
        $this->order = $order;
        $this->setHiddenValue('order', $order);
    }


    protected function prependFields()
    {
        $this->setName('orderTax');


        $this->setHiddenValue('tg', bab_rp('tg'));
        $this
            ->setSaveAction($this->Crm()->Controller()->Order()->saveTax())
            ->setCancelAction($this->Crm()->Controller()->Order()->cancel())
            ->colon()
        ;
    }


    public function addFields()
    {
        $W = $this->widgets;


        $this
            ->addItem($this->name())
            ->addItem($this->description())
            ->addItem($this->amount())
        ;


    }


    /**
     *
     * @return Widget_Item
     */
    protected function name()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('Name'),
            $W->LineEdit()->setSize(50),
            __FUNCTION__
        );
    }



    protected function description()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        return $this->labelledField(
            $Crm->translate('Description'),
            $W->TextEdit()->setColumns(70)->setLines(5),
            __FUNCTION__
        );
    }





    /**
     *
     * @return Widget_Item
     */
    protected function amount()
    {
        $Crm = $this->Crm();
        $W = $this->widgets;

        return $this->labelledField(
            $Crm->translate('Amount'),
            $W->LineEdit()->setSize(6),
            __FUNCTION__
        );
    }



}




















/**
 * Display order
 *
 */
class crm_OrderCardFrame extends crm_CardFrame {


    /**
     * @var crm_Order
     */
    protected $order = null;

    protected $orderSet = null;

    protected $readOnly = false;


    public function __construct($crm, crm_Order $order = null, $readOnly = false)
    {
        $W = bab_Widgets();
        $this->readOnly = $readOnly;

        parent::__construct($crm, null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));

        if (null !== $order)
        {
            $this->setOrder($order);
        }


    }



    /**
     *
     * @param crm_Order $order
     *
     */
    public function setOrder(crm_Order $order)
    {




        $this->order = $order;
        $this->orderSet = $order->getParentSet();
    }

    /**
     * Get base instance of crm_OrderItemTableView
     * @see self::ItemTableView()
     * @return crm_OrderItemTableView
     */
    protected function OrderItemTableView()
    {
        return $this->Crm()->Ui()->OrderItemTableView();
    }


    protected function defaultColumns($table)
    {
        $Crm = $this->Crm();

        $cols = array(
                'reference'		=> $Crm->translate('Reference'),
                'name' 			=> $Crm->translate('Name'),
                'quantity' 		=> $Crm->translate('Quantity'),
                'unit_cost' 	=> $Crm->translate('Unit cost'),
                'vat' 			=> $Crm->translate('VAT'),
                'total_df'		=> $Crm->translate('Total excluding taxes')
        );

        $table->addColumnClass(0, 'widget-column-thin');
        $table->addColumnClass(2, 'numeric widget-column-thin');
        $table->addColumnClass(3, 'numeric widget-column-thin');
        $table->addColumnClass(4, 'numeric widget-column-thin');
        $table->addColumnClass(5, 'numeric widget-column-thin');

        return $cols;
    }


    /**
     * display table view parameters
     *
     * @return crm_OrderItemTableView
     */
    protected function ItemTableView()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $table = $this->OrderItemTableView();
        $table->setOrder($this->order);
        $cols = $this->defaultColumns($table);


        // do not display the vat column if no vat amount
        $taxSet = $Crm->OrderTaxSet();
        $taxRecord = $taxSet->get($taxSet->parentorder->is($this->order->id)->_AND_($taxSet->reference->is('vat')));
        if (null === $taxRecord)
        {
            unset($cols['vat']);
        } else {
            $table->addColumnClass(5, 'numeric');
        }


        if (false === $this->readOnly && $Crm->Access()->editOrder($this->order)) {
            $cols['edit'] = $Crm->translate('Edit');
        }

        $table->setVisibleColumns($cols);



        $layout = $W->VBoxLayout();

        if (false === $this->readOnly && $Crm->Access()->editOrder($this->order)) {
            $layout->addItem($this->TopButtons());
        }

        $layout->addItem($table);


        if (false === $this->readOnly && $Crm->Access()->editOrder($this->order)) {
            $layout->addItem($this->BottomButtons());
        }


        return $layout;
    }


    /**
     * @return Widget_Layout
     */
    protected function TopButtons()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $layout = $W->FlowLayout()->setSizePolicy(Widget_SizePolicy::MAXIMUM)->setHorizontalSpacing(1, 'em')
        ->addClass('icon-left-16')->addClass('icon-16x16')->addClass('icon-left')
        ->addItem(
            $W->Link(
                $W->Icon($this->Crm()->translate('Add a row'), Func_Icons::ACTIONS_LIST_ADD),
                $Crm->Controller()->Order()->editItem(null, $this->order->id)
            )
            ->setTitle($this->Crm()->translate('Add an item to this order'))
        )
        ->addClass('crm-order-add-item');

        return $layout;
    }

    /**
     * @return Widget_Layout
     */
    protected function BottomButtons()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $layout = $W->FlowLayout()->setSizePolicy(Widget_SizePolicy::MAXIMUM)->setHorizontalSpacing(1, 'em')
        ->addClass('icon-left-16')->addClass('icon-16x16')->addClass('icon-left')
        ->addItem(
            $W->Link(
                $W->Icon($this->Crm()->translate('Add tax or discount'), Func_Icons::ACTIONS_LIST_ADD),
                $Crm->Controller()->Order()->editTax(null, $this->order->id)
            )
        )
        ->addClass('crm-order-add-item');

        return $layout;
    }






    protected function Title()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        if ($this->order->type)
        {
            $type = $this->orderSet->type->output($this->order->type);
        } else {
            $type = $Crm->translate('Order');
        }


        return $W->Title(sprintf($Crm->translate('Accounting document: %s %s'),$type, $this->order->name), 2);
    }




    /**
     *
     * @return Widget_Item
     */
    protected function Infos()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $layout = $W->VBoxItems(
                $this->Title()
        )->setSpacing(.5,'em');

        $layout->addItem($horizontal = $W->FlowLayout()->setVerticalAlign('top')->setSpacing(.5,'em', 5,'em'));


        if ($this->order->contact) {
            $Crm->includeContactSet();
            if ($contact = $this->order->contact())
            {
                $Crm->Ui()->includeContact();
                $horizontal->addItem($Crm->Ui()->ContactCardFrame($contact, crm_ContactCardFrame::SHOW_ALL ^ crm_ContactCardFrame::SHOW_ADDRESS));
            }
        }

        $horizontal->addItem($infos = $W->VBoxLayout()->setSpacing(.5,'em'));





        if ($this->order->request_date) {
            $infos->addItem($this->labelStr($Crm->translate('Date'), $this->order->getParentSet()->request_date->output($this->order->request_date)));
        }

        if (($this->order->shoppingcart instanceof crm_ShoppingCart) && $this->order->shoppingcart->id) {
            $infos->addItem($W->Link($this->order->shoppingcart->getTitle(), $Crm->Controller()->ShoppingCartAdm()->display($this->order->shoppingcart->id) ));
        }

        return $layout;
    }

    /**
     * invoice/bill recipient contact information title
     * @return Widget_Item | null
     */
    protected function Billing()
    {
        return $this->OrderAddress('billing', $this->Crm()->translate('Billing'));
    }

    /**
     * expeditor contact information title
     * @return Widget_Item | null
     */
    protected function Shipping()
    {
        return $this->OrderAddress('shipping', $this->Crm()->translate('Shipping'));
    }

    /**
     * delivery contact information title
     * @return Widget_Item | null
     */
    protected function Delivery()
    {
        return $this->OrderAddress('delivery', $this->Crm()->translate('Delivery'));
    }


    /**
     * Generic method for person and address
     */
    protected function OrderAddress($prefix, $title)
    {
        $Crm = $this->Crm();
        $Crm->includeAddressSet();
        $W = bab_Widgets();

        $_person = $prefix.'person';
        $_orgname = $prefix.'organizationname';
        $_organization = $prefix.'organization';
        $_address = $prefix.'address';

        $person = $this->order->$_person;
        $orgname = $this->order->$_orgname;
        $address = $this->order->$_address();

        if (isset($Crm->Organization))
        {
            $Crm->includeOrganizationSet();
            $organization = $this->order->$_organization();

            if (!$person && (!$organization || !$organization->id) && (!$address || $address->isEmpty()))
            {
                return null;
            }
        } else {

            $organization = null;

            if (!$person && (!$address || $address->isEmpty()))
            {
                return null;
            }
        }

        $layout = $W->VBoxLayout()->SetVerticalSpacing(.3,'em');

        if ($title)
        {
            $layout->addItem($W->Title($title, 4));
        }

        if ($orgname)
        {
            $layout->addItem($this->OrganizationName($orgname, $organization));
        }


        if ($person)
        {
            $layout->addItem($W->Title($person, 6));
        }



        if ($address)
        {
            $layout->addItem($this->AddressFrame($address));
        }


        $layout->addClass('crm-order-address')->addClass('icon-16x16')->addClass('icon-left')->addClass('icon-left-16');

        return $layout;
    }

    /**
     * Display of organization name
     *
     * @param	string				$orgname		organization name stored in order
     * @param	crm_Organization	$organization	organization object linked to order (can be null)
     *
     * @return Widget_Item
     */
    protected function OrganizationName($orgname, crm_Organization $organization = null)
    {
        $W = bab_Widgets();

        if ($organization && $organization->id) {

            return $W->Link(
                $W->Icon($orgname, Func_Icons::ACTIONS_USER_GROUP_PROPERTIES),
                $this->Crm()->Controller()->Organization()->display($organization->id));
        } else {

            return $W->Label($orgname);
        }
    }


    /**
     * @return Widget_HboxLayout
     */
    protected function Addresses()
    {
        $W = bab_Widgets();
        $layout = $W->HBoxLayout()->setHorizontalSpacing(2,'em')->setVerticalAlign('top');

        if ($billing = $this->Billing())
        {
            $layout->addItem($billing);
        }

        if ($shipping = $this->Shipping())
        {
            $layout->addItem($shipping);
        }

        if ($delivery = $this->Delivery())
        {
            $layout->addItem($delivery);
        }

        return $layout;
    }



    public function Header()
    {
        $W = bab_Widgets();
        return $W->RichText($this->order->header, null, BAB_HTML_ALL);
    }

    public function Footer()
    {
        $W = bab_Widgets();
        return $W->RichText($this->order->footer, null, BAB_HTML_ALL);
    }



    /**
     *
     * @return crm_OrderCardFrame
     */
    public function CardFrame()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $layout = $this->getLayout();

        $layout->addItem($W->Link($this->Title(), $Crm->Controller()->Order()->display($this->order->id)));
        $total_ti = $this->order->moneyFormat($this->order->total_ti).' '.$Crm->translate('incl tax');
        $layout->addItem($W->Label(sprintf($Crm->translate('Total amount: %s'), $total_ti)));

        return $this;
    }





}





/**
 * Display order
 *
 */
class crm_OrderFullFrame extends crm_OrderCardFrame {

    /**
     *
     * @return crm_OrderFullFrame
     */
    public function FullFrame()
    {
        $layout = $this->getLayout();

        $layout->addItem($this->Infos());
        $layout->addItem($this->Addresses());
        $layout->addItem($this->Header());
        $layout->addItem($this->ItemTableView());
        $layout->addItem($this->Footer());

        return $this;
    }

    /**
     *
     * @return crm_OrderFullFrame
     */
    public function HistoryShortFrame()
    {
        $layout = $this->getLayout();
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $order = $this->order;
        $total_ti = $order->moneyFormat($order->total_ti).' '.$Crm->translate('incl tax');
        $layout->addItem($W->Label(sprintf($Crm->translate('Total amount: %s'), $total_ti)));
        return $this;
    }

    /**
     *
     * @return crm_OrderFullFrame
     */
    public function HistoryFullFrame()
    {
        $layout = $this->getLayout();
        $layout->addItem($this->ItemTableView());
        return $this;
    }

}















class crm_HistoryOrder extends crm_HistoryElement
{
    public function __construct(crm_Order $order, $mode = null, $id = null)
    {
        $Crm = $order->Crm();
        parent::__construct($Crm, $mode, $id);
        $W = bab_Widgets();

        require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
        $this->setElement($order);

        $access = $Crm->Access();

        $contactSet = $Crm->ContactSet();



        if ($order->createdBy) {
            $creator = $contactSet->get($contactSet->user->is($order->createdBy));
            if ($creator && $access->viewContact($creator)) {
                $creatorName = $creator->getFullName();
                $this->addIcon(
                        $W->Link(
                                $Crm->Ui()->ContactPhoto($creator, 32, 32, 0),
                                $Crm->Controller()->Contact()->display($creator->id)
                        )
                );
            } else {
                $creatorName = bab_getUserName($order->createdBy, true);
            }
        } else {
            $creatorName = '';
        }


        $subtitleLayout = $W->FlowLayout()
        ->setHorizontalSpacing(1, 'ex')
        ->addClass(Func_Icons::ICON_LEFT_24)
        ->addClass('crm-small');


        if ($creatorName)
        {
            $subTitle = sprintf($Crm->translate('%s created by %s'),  $order->getParentSet()->type->output($order->type), $creatorName);
        } else {
            $subTitle = '';
        }

        $this->addDefaultLinkedElements();


        $this->addTitle(
                $W->Title(
                        $W->Link(
                                $order->getParentSet()->type->output($order->type).' '.$order->name,
                                $Crm->Controller()->Order()->display($order->id)
                        ),
                        4
                )
        );

        $date = $this->getMode() == self::MODE_SHORT ?
        BAB_DateTimeUtil::relativePastDate($order->createdOn, true)
        : bab_longDate(bab_mktime($order->createdOn), true);
        $icon = $W->Icon($subTitle . "\n" . $date, Func_Icons::OBJECTS_INVOICE);
        $subtitleLayout->addItem($icon);


        $this->addTitle($subtitleLayout);

        $fullframe = new crm_OrderFullFrame($Crm, $order);

        switch ($this->getMode()) {

            case self::MODE_COMPACT :
                break;

            case self::MODE_SHORT:
                $this->addContent($fullframe->HistoryShortFrame());
                break;

            case self::MODE_FULL:
            default:
                $this->addContent($fullframe->HistoryFullFrame());
                break;
        }

    }

    /**
     * Add a contextual menu with several actions applicable
     *
     *
     * @return crm_historyTask
     */
    public function addActions()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $this->addAction(
                $W->Link(
                        $W->Icon($Crm->translate('Display details'), Func_Icons::ACTIONS_DOCUMENT_EDIT),
                        $Crm->Controller()->Order()->display($this->element->id)
                )
        );

        $this->addAction(
                $W->Link(
                        $W->Icon($Crm->translate('Download PDF'), Func_Icons::MIMETYPES_APPLICATION_PDF),
                        $Crm->Controller()->Order()->pdf($this->element->id)
                )
        );

        return $this;
    }
}









