<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


$Crm = crm_Crm();
$Crm->includeRecordController();


/**
 * This controller manages actions that can be performed on tasks.
 *
 * @method crm_TaskSet getRecordSet()
 */
class crm_CtrlTask extends crm_RecordController
{


    protected function toolbar(widget_TableModelView $tableView)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $toolbar = new crm_Toolbar();
        $toolbar->addItem(
            $W->Link(
                $Crm->translate('Export list'),
                $this->proxy()->exportSelect()
            )->addClass('icon',  Func_Icons::ACTIONS_ARROW_DOWN)
            ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );

        return $toolbar;
    }


    /**
     * Displays a form to edit the specified task or an new task.
     *
     * @param int    $task The task id or null for a new task.
     * @param string $for  A reference to a crm object as returned by crm_Object::getRef().
     * @return Widget_Page
     */
    public function edit($task = null, $for = null, $date = null, $time = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $page = $Ui->Page();
        $page->addClass('crm-page-editor');

        if (isset($task)) {
            $taskSet = $Crm->TaskSet();
            $task = $taskSet->get($task);
            if (!isset($task)) {
                throw new crm_Exception('Trying to access task with wrong (unknown) id.');
            }
            $page->setTitle($Crm->translate('Edit task'));
        } else {
            $page->setTitle($Crm->translate('Create a new task'));
        }

        $form = $Ui->TaskEditor($task);

        if ($task instanceof crm_Task) {
            $linkedObjectsFrame = crm_taskLinkedObjects($task);
            $page->addContextItem($linkedObjectsFrame);
        } else {
            $page->addContextItem($W->Label(''));
        }


        if (isset($date)) {
            $form->setValue(array('task', 'dueDate'), $date);
        }

        if (isset($for)) {
            $form->setHiddenValue('task[for]', $for);
        }

        $page->addItem($form);

        return $page;
    }



    /**
     * Displays a form to edit a clone of the specified task.
     *
     * @param int    $task The task id or null for a new task.
     * @return Widget_Action
     */
    public function duplicate($task = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $Crm->Ui()->includeTask();


        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        if (!isset($task)) {
            throw new crm_Exception('Trying to access task with wrong (unknown) id.');
        }

        $taskSet = $Crm->TaskSet();
        $task = $taskSet->get($task);
        if (!isset($task)) {
            throw new crm_Exception('Trying to access task with wrong (unknown) id.');
        }

        $page->addItem($W->Title($Crm->translate('Duplicate task'), 1));


        $linkedObjectsFrame = crm_taskLinkedObjects($task);

        $page->addContextItem($linkedObjectsFrame);

        $linkedObjectsReferences = array();
        $contacts = $task->getLinkedContacts();
        foreach ($contacts as $contact) {
            $linkedObjectsReferences[$contact->getRef()] = $contact->getRef();
        }
        $deals = $task->getLinkedDeals();
        foreach ($deals as $deal) {
            $linkedObjectsReferences[$deal->getRef()] = $deal->getRef();
        }
        $organizations = $task->getLinkedOrganizations();
        foreach ($organizations as $organization) {
            $linkedObjectsReferences[$organization->getRef()] = $organization->getRef();
        }

        $task->id = null;

        $form = new crm_TaskEditor($Crm, $task);

        if ($task instanceof crm_Task) {
            $form->setValues(array('task'), $task->getValues());
        }


        if (!empty($linkedObjectsReferences)) {
            $form->setHiddenValue('task[for]', implode(',', $linkedObjectsReferences));
        }

        $page->addItem($form);


        return $page;
    }



    /**
     * Actions frame for task display
     * @param crm_Task $task
     * @return Widget_VBoxLayout
     */
    protected function getActionsFrame(crm_Task $task)
    {
        $Crm = $this->Crm();
        $Access = $Crm->Access();
        $Ui = $Crm->Ui();
        $W = bab_Widgets();
        $actionsFrame = $W->VBoxLayout()
            ->setVerticalSpacing(1, 'px')
            ->addClass(Func_Icons::ICON_LEFT_16)
            ->addClass('crm-actions');

        $Ctrl = $Crm->Controller()->Task();


        if ($Access->updateTask($task->id)) {
            $actionsFrame->addItem($W->Link($W->Icon($Crm->translate('Edit this task'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $Ctrl->edit($task->id)));
        }

        $actionsFrame->addItem($W->Link($W->Icon($Crm->translate('Duplicate this task'), Func_Icons::ACTIONS_EDIT_COPY), $Ctrl->duplicate($task->id)));

        if ($Access->deleteTask($task->id)) {
            $actionsFrame->addItem(
                $W->Link($W->Icon($Crm->translate('Delete this task'), Func_Icons::ACTIONS_EDIT_DELETE), $Ctrl->confirmDelete($task->id))->setOpenMode(Widget_Link::OPEN_DIALOG)
            );
        }

        return $actionsFrame;
    }


    /**
     * Displays a form to edit the task.
     *
     * @param int $task The task id
     * @return Widget_Action
     */
    public function display($task)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $Access = $Crm->Access();
        $Crm->includeStatusSet();

        $taskSet = $Crm->TaskSet();
        $task = $taskSet->request($task);

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($task->id), $task->summary);



        $page = $Ui->Page();

        $mainFrame = $Ui->TaskFullFrame($task);

        $page->addItem($mainFrame);
        if ($task->completion < 100 && $task->canUserWork()) {
            $mainFrame->addItem($this->getCompleteWidget($task));
        }


        $mainFrame->addItem(
            $W->VboxItems(
                $W->Link($Crm->translate('Add a note'), $Crm->Controller()->Note()->edit(null, $task->getRef()))
                    ->addClass('widget-instant-button widget-actionbutton'),
                $Crm->Ui()->AddNoteEditor($task)
                    ->setTitle($Crm->translate('Add a comment'))
                    ->addClass('widget-instant-form')
            )->addClass('widget-instant-container')
        );


        $mainFrame->addItem($Ui->History($task));

        $page->addContextItem($this->getActionsFrame($task));

        $linkedObjectsFrame = crm_taskLinkedObjects($task);

        $page->addContextItem($linkedObjectsFrame);

        return $page;
    }



    /**
     * @return Widget_Displayable_Interface
     */
    protected function getCompleteWidget(crm_Task $task)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $editor = new crm_Editor($Crm);
        $editor->addItem(
            $editor->labelledField(
                $Crm->translate('Comment'),
                $W->TextEdit()->addClass('widget-fullwidth'),
                'comment'
            )
        );
        $editor->setHiddenValue('tg', bab_rp('tg'));
        $editor->setHiddenValue('task', $task->id);

        $editor->setSaveAction($Crm->Controller()->Task()->complete());
        $completeButton = $W->Accordions()->addClass('crm-action-accordion');
        $completeButton->addPanel($Crm->translate('I completed this task'), $editor);

        return $completeButton;
    }


    protected function completeWithWork(crm_Task $task)
    {
        $message = crm_translate('Please fill the actual work');
        throw new crm_SaveException($message);
    }



    /**
     * Updates the completion status of a task.
     *
     * @param int $task       The task id.
     * @param int $percent    The new completion rate (defaults to 100 which marks the task as completed
     *                        and sets the completedOn field to now()).
     * @param string $comment An optional comment text that will be appended to the task description.
     * @return Widget_Action
     */
    public function complete($task = null, $completion = 100, $comment = null)
    {
        $Crm = $this->Crm();
        $Crm->Ui()->includeTask();

        $taskSet = $Crm->TaskSet();
        $task = $taskSet->request($task);

        if ($task->isPlanned()) {
            return $this->completeWithWork($task);
        }



        if ($task->completion < 100) {

            $task->completion = $completion;
            $task->remainingWork = 0;

            if (isset($comment)) {
                $task->description .= "\n\n" . $comment;
            }

            $task->save();

            if ($task->completion >= 100 && !empty($task->completedAction)) {
                crm_redirect($task->completedAction);
            }
        }
        crm_redirect(crm_BreadCrumbs::last(), $Crm->translate('The task has been saved.'));
    }




    /**
     * Saves the task.
     *
     * @param array	$task
     * @return Widget_Action
     */
    public function save($task = null)
    {
        $Crm = $this->Crm();
        $set = $Crm->TaskSet();

        $task['scheduledStart'] = crm_fixDateTime($task['scheduledStart'], $task['scheduledAllDay']);
        $task['scheduledFinish'] = crm_fixDateTime($task['scheduledFinish'], $task['scheduledAllDay']);

        if (isset($task['id'])) {
             $record = $set->request($task['id']);
        } else {
             $record = $set->newRecord();
        }
        $record->setFormInputValues($task);

        if (!isset($task['responsible']) || empty($task['responsible'])) {
            $record->responsible = $GLOBALS['BAB_SESS_USERID'];
        }
        $record->save();

        if (isset($task['for']) && $task['for'] !== '') {
            $references = explode(',', $task['for']);
            foreach ($references as $reference) {
                $subject = $Crm->getRecordByRef($reference);
                $record->linkTo($subject);
            }
        }

        if (isset($task['sendInvitation']) && $task['sendInvitation']) {
            $record->sendNotification();
        }

        return true;
    }



    /**
     * @return Widget_DisplayableInterface
     */
    public function myTasks($tasks = null, $itemId = null)
    {
        $Crm = $this->Crm();

        $Crm->Ui()->includeTask();

        $userTasksItem = new crm_UserTasks($Crm);
        $userTasksItem->setUser($GLOBALS['BAB_SESS_USERID']);
        if (isset($itemId)) {
            $userTasksItem->setId($itemId);
        }

        $userTasksItem->setReloadAction($this->proxy()->myTasks($tasks, $userTasksItem->getId()));
        return $userTasksItem;
    }




    /**
     *
     * @param array $tasks  array(taskId => '0' or '1')
     */
    public function markDone($tasks = null)
    {
        $Crm = $this->Crm();

        $recordSet = $this->getRecordSet();
        if (isset($tasks) && !is_array($tasks)) {
            $tasks = array($tasks => true);
        }

        foreach ($tasks as $taskId => $checked) {
            $record = $recordSet->get($taskId);

            if (!$record->isUpdatable()) {
                continue;
            }

            if ($record->isPlanned()) {
                continue;
            }

            if ($checked) {
                $record->setCompletion(100);
                $this->addMessage($Crm->translate('The task has been marked as done.'));
            } else {
                $record->setCompletion(0);
            }
        }


        return true;
    }


    /**
     * @return Widget_DisplayableInterface
     */
    public function timeline($select = null)
    {
        $Crm = $this->Crm();
        $Access = $Crm->Access();
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';

        $W = bab_Widgets();
        $Ui = $Crm->Ui();
        $Ui->includeContact();

        $timeline = $W->Timeline();
        $timeline->setAutoWidth(true);

        $timeline->addBand(new Widget_TimelineBand('5%', Widget_TimelineBand::INTERVAL_DAY, 45, Widget_TimelineBand::TYPE_OVERVIEW));
        $timeline->addBand(new Widget_TimelineBand('80%', Widget_TimelineBand::INTERVAL_DAY, 45, Widget_TimelineBand::TYPE_ORIGINAL));
        $timeline->addBand(new Widget_TimelineBand('15%', Widget_TimelineBand::INTERVAL_MONTH, 150, Widget_TimelineBand::TYPE_OVERVIEW));


        $now = BAB_DateTime::now();
        $nowIso = $now->getIsoDate();

        $taskStart = $now->cloneDate();
        $taskStart->add(-1, BAB_DATETIME_YEAR);

        $taskSet = $Crm->TaskSet();


        $taskSelection = $taskSet->dueDate->greaterThan($taskStart->getIsoDate());

        switch ($select) {
            case 'user':
                $taskSelection = $taskSelection->_AND_($taskSet->responsible->is($GLOBALS['BAB_SESS_USERID']));
                break;

            case 'all':
            default:
                break;
        }

        $tasks = $taskSet->select($taskSelection);

        $widgetAddonInfo = bab_getAddonInfosInstance('widgets');
        $baseIconPath = $GLOBALS['babUrl'] . $widgetAddonInfo->getTemplatePath() . 'timeline/timeline_js/images/';
        $myNormalIcon = $baseIconPath . 'dark-blue-circle.png';
        $otherNormalIcon = $baseIconPath . 'dull-blue-circle.png';
        $myOverdueIcon = $baseIconPath . 'dark-red-circle.png';
        $otherOverdueIcon = $baseIconPath . 'dull-red-circle.png';
        $myCompleteIcon = $baseIconPath . 'dark-green-circle.png';
        $otherCompleteIcon = $baseIconPath . 'dull-green-circle.png';



        foreach ($tasks as $task) {
            /* @var $task crm_Task */
            $start = BAB_DateTime::fromIsoDateTime($task->dueDate);
            $period = $timeline->createMilestone($start);
            $period->start = $task->dueDate . ' 12:00';
            $period->setTitle($task->summary);
            $period->setBubbleContent(
                $W->VBoxItems(
                    $W->Link($W->Title($task->summary, 2), $GLOBALS['babUrl'] . $Crm->Controller()->Task()->display($task->id)->url()),
                    $W->Html($Crm->translate('Responsible:') . ' <b>' . bab_toHtml(bab_getUserName($task->responsible, true)) . '</b>'),
                    $W->Html($task->description),
                    $taskActions = $W->FlowItems(
                        $W->Link($Crm->translate('Details...'), $Crm->Controller()->Task()->display($task->id))
                            ->addClass('widget-actionbutton')
                    )
                    ->setHorizontalSpacing(1, 'em')
                )->setVerticalSpacing(0.5, 'em')
            );


            if ($Access->updateTask($task)) {
                $taskActions->addItem(
                    $W->Link($Crm->translate('Edit...'), $Crm->Controller()->Task()->edit($task->id))
                        ->addClass('widget-actionbutton')
                );
            }


            if ($task->responsible == $GLOBALS['BAB_SESS_USERID']) {
                $period->classname = 'crm-personal-task';
                if ($task->completion >= 100) {
                    $period->icon = $myCompleteIcon;
                } else  {
                    if ($nowIso > $task->dueDate) {
                        $period->icon = $myOverdueIcon;
                    } else {
                        $period->icon = $myNormalIcon;
                    }
                    $period->setTitle($task->summary);
                }
            } else {
                $period->classname = 'crm-other-task';
                if ($task->completion >= 100) {
                    $period->icon = $otherCompleteIcon;
                } else  {
                    if ($nowIso > $task->dueDate) {
                        $period->icon = $otherOverdueIcon;
                    } else {
                        $period->icon = $otherNormalIcon;
                    }
                    $period->setTitle($task->summary);
                }
            }

            $timeline->addPeriod($period);
        }

        return $timeline;
    }



    /**
     *
     * @param string $start			ISO formatted datetime
     * @param string $end			ISO formatted datetime
     */
    public function ical($start = null, $end = null)
    {
        $Crm = $this->Crm();
        $Access = $Crm->Access();
        require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';
        require_once $GLOBALS['babInstallPath'] . '/utilit/cal.calendarperiod.class.php';


        if (!isset($start)) {
            $start = bab_DateTime::now();
            $start->add(-1, BAB_DATETIME_MONTH);
            $start = $start->getIsoDateTime();
            $end = bab_DateTime::now();
            $end->add(6, BAB_DATETIME_MONTH);
            $end = $end->getIsoDateTime();
        }

        $caldav = bab_functionality::get('CalendarBackend/Caldav');

        $caldav->includeCalendarPeriod();

        $taskSet = $Crm->TaskSet();


        $tasks = $taskSet->select(
            $taskSet->scheduledStart->lessThanOrEqual($end)
            ->_AND_($taskSet->scheduledFinish->greaterThanOrEqual($start))
        );

        $icalEvents = array();
        foreach ($tasks as $task) {
            $start = BAB_DateTime::fromIsoDateTime($task->scheduledStart);
            $end = BAB_DateTime::fromIsoDateTime($task->scheduledFinish);

            $calendarPeriod = new bab_CalendarPeriod();
            $calendarPeriod->setBeginDate($start);
            $calendarPeriod->setEndDate($end);
            $calendarPeriod->setProperty('SUMMARY', $task->summary);
            //	        $calendarPeriod->setProperty('DESCRIPTION', $reservation->longDescription);

            $icalEvents[] = caldav_CalendarPeriod::toIcal($calendarPeriod);
        }

        header('Content-type: text/calendar; charset=utf-8');
        header('Content-Disposition: inline; filename=calendar.ics');

        echo 'BEGIN:VCALENDAR' . "\r\n"
            . 'VERSION:2.0' . "\r\n"
                . $caldav->getProdId() . "\r\n"
                    . $caldav->getTimeZone()
                    . implode("\r\n", $icalEvents) . "\r\n"
                        . 'END:VCALENDAR';

        die;
    }


    public function userTasks($itemId = null)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $taskFrame = new crm_UserTasks($Crm);
        $taskFrame->setCrm($Crm);
        $taskFrame->setUser($Crm->Access()->currentUser());
        $box = $W->VBoxItems(
            $taskFrame
        );

        if (isset($itemId)) {
            $box->setId($itemId);
        }

        $box->setReloadAction($this->proxy()->userTasks($box->getId()));

        return $box;
    }


    /**
     * Does nothing and return to the previous page.
     *
     * @param array	$message
     * @return Widget_Action
     */
    public function cancel()
    {
        crm_redirect(crm_BreadCrumbs::last());
    }


    /**
     * Displays help on this page.
     *
     * @return Widget_Action
     */
    public function help()
    {

    }
}