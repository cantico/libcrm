<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


$Crm = crm_Crm();
$Crm->includeRecordController();

/**
 * This controller manages actions that can be performed on organizations.
 */
class crm_CtrlOrganization extends crm_RecordController implements crm_ShopAdminCtrl
{

    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param	crm_OrganizationSet	$organizationSet
     * @param	array				$filter
     * @return ORM_Criteria
     */
    protected function getFilterCriteria(crm_OrganizationSet $organizationSet, $filter)
    {
        $Crm = $this->Crm();

        // Initial conditions are base on read access rights.
        $conditions = $Crm->Access()->canPerformActionOnSet($organizationSet, 'organization:read');


        if (isset($filter['import']) && !empty($filter['import'])) {
            $linkSet = $Crm->LinkSet();

            $importRecord = $Crm->ImportSet()->get($filter['import']);
            $linkSet->joinTarget($Crm->OrganizationClassName());

            $conditions = $conditions->_AND_(
                $organizationSet->id->in(
                    $linkSet->sourceId->is($importRecord->id)
                    ->_AND_(
                        $linkSet->sourceClass->is(get_class($importRecord))
                        ->_AND_(
                            $linkSet->targetClass->is($Crm->OrganizationClassName())
                        )
                    ),
                    'targetId'
                )
            );
        }


        if (!empty($filter['name'])) {
            $conditions = $conditions->_AND_($organizationSet->name->contains($filter['name']));
        }
        if (!empty($filter['type/name'])) {
            $conditions = $conditions->_AND_($organizationSet->type->name->contains($filter['type/name']));
        }
        if (!empty($filter['activity'])) {
            $conditions = $conditions->_AND_($organizationSet->activity->contains($filter['activity']));
        }
        if (!empty($filter['address/postalCode'])) {
            $conditions = $conditions->_AND_($organizationSet->address->postalCode->startsWith($filter['address/postalCode']));
        }
        if (!empty($filter['address/city'])) {
            $conditions = $conditions->_AND_($organizationSet->address->city->contains($filter['address/city']));
        }
        if (isset($filter['classification']) && !empty($filter['classification'][0])) {
            $classificationSet = $Crm->ClassificationSet();
            $classification = $classificationSet->get($classificationSet->name->is($filter['classification'][0]));
            if (isset($classification)) {
                $parent = $classification->id_parent();
                $conditions = $conditions
                    ->_AND_(
                        $organizationSet->hasClassification($filter['classification'][0], 'implied')
                        ->_OR_($organizationSet->hasClassification($parent->name, 'implied'))
                    );
            }
        }
        if (isset($filter['tag']) && !empty($filter['tag'])) {
            $conditions = $conditions->_AND_($organizationSet->haveTagLabels($filter['tag']));
        }

        if (!empty($filter['responsible'])) {
            $conditions = $conditions->_AND_($organizationSet->responsible->name->is($filter['responsible']));
        }

        return $conditions;
    }





    /**
     * @param array $filter
     *
     * @return crm_OrganizationTableView
     */
    protected function organizationTableModelView($filter)
    {
        $Crm = $this->Crm();

        $organizationSet = $Crm->OrganizationSet();
        $organizationSet->address();
        $organizationSet->address->country();

        $organizationSet->responsible();

        if (isset($organizationSet->type))
        {
            $organizationSet->type();
        }

        $conditions = $this->getFilterCriteria($organizationSet, $filter);

        $organizationSelection = $organizationSet->select($conditions);


        $tableView = $Crm->Ui()->OrganizationTableView();
        $tableView->addDefaultColumns($organizationSet);
        $tableView->setDataSource($organizationSelection);
        $tableView->allowColumnSelection();

        return $tableView;
    }



    /**
     * @return Widget_Action
     */
    public function displayList($organizations = null, $type = null)
    {
        $Crm = $this->Crm();
        $Crm->Ui()->includeContact();

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList($organizations), $Crm->translate('Organizations'));

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-list');


        $filter = isset($organizations['filter']) ? $organizations['filter'] : array();


        $tableView = $this->organizationTableModelView($filter);

        $toolbar = new crm_Toolbar();
        $toolbar->addButton($Crm->translate('Add organization'), Func_Icons::ACTIONS_USER_GROUP_NEW, $this->proxy()->edit());
        $toolbar->addButton($Crm->translate('Export in CSV'), Func_Icons::ACTIONS_ARROW_DOWN, $this->proxy()->exportList($organizations));

        $filterPanel = crm_organizationFilteredTableView($Crm, $tableView, $filter, 'organizations');

        $page->addToolbar($toolbar);
        $page->addItem($filterPanel);
        return $page;
    }


    /**
     * @param array	 $contacts		List parameters
     * @param bool   $inline
     * @param string $filename
     *
     * @return Widget_Action
     */
    public function export($export = null)
    {
        $organizations = unserialize($export['organizations']);
        $filter = isset($organizations['filter']) ? $organizations['filter'] : array();

        switch($export['format']) {

            case 'csv':
                $tableView = $this->organizationTableModelView($filter);
                $tableView->downloadCsv('export-organization.csv', ',', false, $export['charset']);
                break;

            case 'xls':
                $tableView = $this->organizationTableModelView($filter);
                $tableView->downloadExcel('export-organization.xls');
                break;

            case 'xlsx':
            default:
                $tableView = $this->organizationTableModelView($filter);
                $tableView->downloadXlsx('export-organization.xlsx');
                break;
        }
    }






//     /**
//      * @param array	 $contacts		List parameters
//      * @param bool   $inline
//      * @param string $filename
//      *
//      * @return Widget_Action
//      */
//     public function exportList($organizations = null, $inline = false, $filename = 'export-organization.csv')
//     {
//         $Crm = $this->Crm();
//         $Crm->Ui()->includeOrganization();
//         $access = $Crm->Access();


//         $page = $Crm->Ui()->Page();
//         $page->addClass('crm-page-list');

//         $filter = isset($organizations['filter']) ? $organizations['filter'] : array();

//         $tableView = $this->organizationTableModelView($filter);


//         $csvExport = $tableView->exportCsv(',');

//         //DOWNLOAD
//         bab_setTimeLimit(3600);

//         if (mb_strtolower(bab_browserAgent()) == 'msie') {
//             header('Cache-Control: public');
//         }

//         if ($inline) {
//             header('Content-Disposition: inline; filename="'.$filename.'"'."\n");
//         } else {
//             header('Content-Disposition: attachment; filename="'.$filename.'"'."\n");
//         }

//         $mime = 'text/csv';

//         if (function_exists('mb_strlen')) {
//             $size = mb_strlen($csvExport, '8bit');
//         } else {
//             $size = strlen($csvExport);
//         }


//         header('Content-Type: '.$mime."\n");
//         header('Content-Length: '.$size."\n");
//         header('Content-transfert-encoding: binary'."\n");

//         echo $csvExport;

//         die;
//     }



    /**
     * Edit / Create an organization
     * @param int $organization
     * @return Widget_Action
     */
    public function edit($organization = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $page = $Ui->Page();
        $page->addClass('crm-page-editor');



        if (isset($organization)) {
            $page->addItem($W->Title($Crm->translate('Edit organization'), 1));
            $organization = $Crm->OrganizationSet()->join('address')->get($organization);

        } else {
            $page->addItem($W->Title($Crm->translate('Create a new organization'), 1));
            $organization = null;
        }

        /*@var $organization crm_Organization */

        $form = $Ui->OrganizationEditor($organization);

        if (!$form->getValue(array('organization', 'address', 'country')) && $Crm->CountrySet() && $Crm->CountrySet()->guessCountryFromBrowser('FR')) {
            $form->setValue(array('organization', 'address', 'country'), $Crm->CountrySet()->guessCountryFromBrowser('FR')->id);
        }

        $page->addItem($form);

        $addon = bab_getAddonInfosInstance('LibCrm');
        $defaultImage = $addon->getStylePath() . 'images/organization-default.png';

        $imagesFormItem = $W->ImagePicker()
        ->setDimensions(256, 256)
        ->oneFileMode()
        ->setDefaultImage(new bab_Path($defaultImage));

        if ($organization) {
            $logoUploadPath = $organization->getLogoUploadPath();
            $logoUploadPath->createDir();

            $imagesFormItem->setFolder($logoUploadPath);
        } else {
            $imagesFormItem->setName('logo');
        }

        $page->addContextItem($W->Title($Crm->translate('Logo'), 4));
        $page->addContextItem($imagesFormItem);

        return $page;
    }



    /**
     *
     * @param crm_Organization $organization
     */
    protected function saveLogo(crm_Organization $organization)
    {
        $W = bab_Widgets();

        $res = $W->ImagePicker()->getTemporaryFiles('logo');

        if (isset($res)) {
            foreach($res as $filePickerItem) {
                /*@var $filePickerItem Widget_FilePickerItem */
                $organization->importLogo($filePickerItem->getFilePath());
            }
        }


    }



    /**
     * Saves the organization and returns to the previous page.
     *
     * @param array $organization
     * @return Widget_Action
     */
    public function save($organization = null)
    {
        $Crm = $this->Crm();
        $set = $Crm->OrganizationSet();
        $set->address();

        /* @var $record crm_Organization */
        if (!empty($organization['id'])) {
            $record = $set->get($organization['id']);
        } else {
            $record = $set->newRecord();
        }

        if (isset($organization['parent/name']) && $organization['parent/name'] !== '') {
            $parentSet = $Crm->OrganizationSet();
            $parents = $parentSet->select($parentSet->name->is($organization['parent/name']));
            foreach ($parents as $parent) {
                $organization['parent'] = $parent->id;
                break;
            }

        }

        if (isset($organization['responsible_name'])) {
            if (trim($organization['responsible_name']) === '') {
                $organization['responsible'] = 0;
            } else {
                $contactSet = $Crm->ContactSet();
                list($namestart) = explode(' ', $organization['responsible_name']);
                $responsibles = $contactSet->select($contactSet->lastname->startsWith($namestart)->_OR_($contactSet->firstname->startsWith($namestart)));
                foreach ($responsibles as $responsible) {
                    if ($responsible->getFullName() == $organization['responsible_name']) {
                        $organization['responsible'] = $responsible->id;
                        break;
                    }
                }
            }
        }


        $organizationAddresses = null;

        if (isset($organization['addresses'])) {
            $organizationAddresses = $organization['addresses'];
            unset($organization['addresses']);
            unset($organization['address']);
        }

        $record->setValues($organization);
        $record->save();


        if (is_array($organizationAddresses)) {

            $addressSet = $Crm->AddressSet();

            foreach ($organizationAddresses as $key => $address) {

                /* @var $newAddress crm_Address */
                if (strpos($key, '.') === false) {
                    $newAddress = $addressSet->newRecord();
                    $newLink = true;
                } else {
                    list($previousAddressType, $addressId) = explode('.', $key);
                    $newAddress = $addressSet->get($addressId);
                    $newLink = false;
                }
                $newAddress->setValues($address);

                if (!$newAddress->isEmpty()) {
                    $newAddress->save();
                    if ($newLink) {
                        $record->addAddress($newAddress, $address['type']);
                    } elseif ($previousAddressType !== $address['type']) {
                        $record->updateAddress($newAddress, $previousAddressType, $address['type']);
                    }
                }
            }
        }

        $this->saveLogo($record);

        crm_redirect($this->proxy()->display($record->id), $Crm->translate('The organization has been saved.'));
    }





    /**
     * Displays the organization information page.
     *
     * @param int	$org
     * @return Widget_Action
     */
    public function display($org)
    {
        $Crm = $this->Crm();
        $page = $Crm->Ui()->Page();

        $organizationSet = $Crm->OrganizationSet();

        /* @var $org crm_Organization */
        $org = $organizationSet->get($org);

        if (!isset($org)) {
            throw new crm_Exception($Crm->translate('Trying to access an organization with a wrong (unknown) id.'));
        }

        $Access = $Crm->Access();

        if (!$Access->readOrganization($org)) {
            throw new crm_Exception($Crm->translate('You do not have access to this page.'));
        }


        crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($org->id), $org->name);

        $W = bab_Widgets();
        $Crm->Ui()->includeOrganization();
        $Crm->Ui()->includeTask();


        $mainFrame = $Crm->Ui()->OrganizationFullFrame($org);
        $mainFrame->addClass('crm-detailed-info-frame');


        $maxHistory = 8;
        $historyFrame = $W->VBoxItems(
            $Crm->Controller()->Search(false)->history($org->getRef(), $maxHistory, 0, true)
        );
        $mainFrame->addItem(
            $W->Section(
                $Crm->translate('History'),
                $historyFrame,
                2
            )->setFoldable(true)
            ->addClass('compact')
        );

        $page->addItem($mainFrame);


        // Tell clipboard what actions are possible on this page.

        $this->addAcceptedClipboardObject(
            'Email',
            $Crm->Controller()->Organization()->CopyFromClipboard('__1__', $org->id),
            $Crm->translate('Copy email in this organization history'),
            Func_Icons::ACTIONS_EDIT_PASTE
        );

        // Actions

        $actionsFrame = $page->ActionsFrame();

        $page->addContextItem($actionsFrame);

        $contactFrame = new crm_organizationContacts($Crm);

        $page->addContextItem($contactFrame->getFrame($org));

        if (isset($Crm->Deal))
        {
            $dealFrame = crm_organizationDeals($org);
            $page->addContextItem($dealFrame);
        }


        // Tasks
        if (isset($Crm->Task))
        {
            $tasksSection = $W->Section(
                $Crm->translate('Incoming tasks for this organization'),
                crm_userTasksForCrmObject($org),
                4
            );

            $tasksSectionMenu = $tasksSection->addContextMenu('inline');

            $tasksSectionMenu->addItem(
                $W->VboxItems(
                    $W->Link($Crm->translate('Add'), $Crm->Controller()->Task()->edit(null, $org->getRef()))
                        ->setTitle($Crm->translate('Add a task'))
                        ->addClass('widget-instant-button widget-actionbutton'),
                    $taskEditor = $Crm->Ui()->TaskEditor()
                        ->associateTo($org)
                        ->addClass('widget-instant-form')
                )->addClass('widget-instant-container')
            );

            $page->addContextItem($tasksSection);
        }



        $this->addActions($actionsFrame, $org);


        // $cacheHeaders = $page->getCacheHeaders();
        // $cacheHeaders->setTTL(3600);

        return $page;
    }


    /**
     * @param Widget_Item $actionFrame
     * @param crm_Organization $org
     */
    protected function addActions(Widget_Item $actionFrame, crm_Organization $org)
    {
        $Crm = $this->Crm();
        $Access = $Crm->Access();
        $W = bab_Widgets();

        if ($Access->updateOrganization($org)) {
            $actionFrame->addItem($W->Link($W->Icon($Crm->translate('Edit this organization'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $Crm->Controller()->Organization()->edit($org->id)));

            if (isset($Crm->OrganizationClassification)) {
                $actionFrame->addItem($W->Link($W->Icon($Crm->translate('Edit organization codification'), Func_Icons::ACTIONS_VIEW_LIST_TREE), $Crm->Controller()->Organization()->classify($org->id)));
            }
        }
        if ($Access->deleteOrganization($org)) {
            $actionFrame->addItem($W->Link($W->Icon($Crm->translate('Delete this organization'), Func_Icons::ACTIONS_EDIT_DELETE), $Crm->Controller()->Organization()->confirmDelete($org->id)));
        }

        if ($Access->viewOrganizationGroup($org)) {
            $actionFrame->addItem($W->Link($W->Icon($Crm->translate('View situation in group'), Func_Icons::APPS_ORGCHARTS), $Crm->Controller()->Organization()->orgChart($org->id)));
        }
    }



    /**
     * Link the specified object to this organization.
     *
     * @param string $ref A reference to the object to copy.
     * @param int    $organization     The destination organization id.
     */
    public function CopyFromClipboard($ref = null, $organization = null)
    {
        $Crm = $this->Crm();

        $organizationSet = $Crm->OrganizationSet();
        /* @var $organization crm_Organization */
        $organization = $organizationSet->get($organization);
        if (!isset($organization)) {
            throw new crm_Exception($Crm->translate('Trying to access an organization with a wrong (unknown) id.'));
        }

        $object = $Crm->getRecordByRef($ref);

        if ($object instanceof crm_Email) {
            $object->linkTo($organization);
        }

        return true;
    }


    /**
     * Deletes the specified organization and all related data.
     *
     * @param int	$organization
     * @return Widget_Action
     */
    public function delete($organization)
    {
        $Crm = $this->Crm();
        $set = $Crm->OrganizationSet();

        /* @var $organization crm_Organization */
        $organization = $set->get($organization);

        if (!isset($organization)) {
            throw new crm_Exception($Crm->translate('Trying to access an organization with a wrong (unknown) id.'));
        }

        $Access = $Crm->Access();

        if (!$Access->deleteOrganization($organization)) {
            throw new crm_Exception($Crm->translate('You do not have access to this action.'));
        }

        $organization->delete();

        crm_redirect($this->proxy()->displayList(), $Crm->translate('The organization has been deleted.'));
    }


    /**
     * Deletes the specified organization and all related data.
     *
     * @param int	$organization
     * @return Widget_Action
     */
    public function deleteWithContacts($organization)
    {
        $Crm = $this->Crm();
        $set = $Crm->OrganizationSet();

        /* @var $organization crm_Organization */
        $organization = $set->get($organization);

        if (!isset($organization)) {
            throw new crm_Exception($Crm->translate('Trying to access an organization with a wrong (unknown) id.'));
        }

        $Access = $Crm->Access();

        if (!$Access->deleteOrganization($organization)) {
            throw new crm_Exception($Crm->translate('You do not have access to this action.'));
        }

        $contactSet = $Crm->ContactSet();
        $contacts = $contactSet->select($contactSet->hasOnlyOrganization($organization->id));
        $nbContacts = $contacts->count();
        $contactSet->delete($contactSet->hasOnlyOrganization($organization->id));

        $organization->delete();

        crm_redirect($this->proxy()->displayList(), sprintf($Crm->translate('The organization and the %d contacts have been deleted.'), $nbContacts));
    }


    /**
     * Displays an information page about elements linked to an organization
     * before deleting it, and propose the user to confirm or cancel
     * the organization deletion.
     *
     * @param int $organization
     *
     * @return Widget_Action
     */
    public function confirmDelete($organization = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $Crm->Ui()->includeContact();

        $set = $Crm->OrganizationSet();
        /* @var $organization crm_Organization */
        $organization = $set->get($organization);
        if (!isset($organization)) {
            throw new crm_Exception($Crm->translate('Trying to delete an organization with a wrong (unknown) id.'));
        }

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->confirmDelete($organization->id), $Crm->translate('Delete organization'));

        $page = $Crm->Ui()->Page();
        $page->setMainPanelLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
        $page->addClass('crm-page-editor');

        $page->addItem($W->Title($Crm->translate('Delete organization'), 1));
//		$page->addItem($W->Title($organization->name));

        $page->addContextItem(
            $Crm->Ui()->OrganizationCardFrame($organization),
            $Crm->translate('Organization')
        );

        $buttonbar = new crm_Toolbar();
        $buttonbar->local = true;

        $relatedRecords = $organization->getRelatedRecords();

        if (isset($Crm->Task)) {
            $tasks = $organization->selectTasks();
            $nbTasks = $tasks->count();
        } else {
            $nbTasks = 0;
        }

        $notes = $organization->selectNotes();
        $nbNotes = $notes->count();

        if (isset($Crm->Email)) {
            $emails = $organization->selectEmails();
            $nbEmails = $emails->count();
        } else {
            $nbEmails = 0;
        }

        if ($nbTasks > 0 || $nbNotes > 0 || $nbEmails > 0 || count($relatedRecords) > 0) {

            $relatedRecordSection = $W->Section(
                $Crm->translate('Several CRM elements are related to this organization'),
                $W->VBoxItems()->setVerticalSpacing(1, 'em')
            );
            $page->addItem($relatedRecordSection);

            if ($nbTasks > 0) {
                $relatedRecordSection->addItem($W->Label($Crm->translate('Tasks:') . ' ' . $nbTasks));
            }
            if ($nbNotes > 0) {
                $relatedRecordSection->addItem($W->Label($Crm->translate('Notes:') . ' ' . $nbNotes));
            }
            if ($nbEmails > 0) {
                $relatedRecordSection->addItem($W->Label($Crm->translate('Emails:') . ' ' . $nbEmails));
            }

            foreach ($relatedRecords as $setClassName => $records) {

                $set = new $setClassName($Crm);
                $relatedRecordSection->addItem(
                    $section = $W->Section(
                        $Crm->translate($set->getDescription()),
                        $W->VBoxItems()->setVerticalSpacing(1, 'em'),
                        4
                    )->setFoldable(true)
                );

                $section->addContextMenu('inline')->addItem($W->Label($records->count())->addClass('crm-counter-label'));

                foreach ($records as $record) {
                    if($sectionItem = $this->linkedItem($record)){
                        $section->addItem($sectionItem);
                    }
                }
            }

            $buttonbar->addItem(
                $W->Link(
                    $Crm->translate('Replace with an existing organization...'),
                    $Crm->Controller()->Organization()->searchReplacement($organization->id)
                )->addClass('icon', 'widget-actionbutton', Func_Icons::ACTIONS_EDIT_COPY)
            );
            $buttonbar->addItem(
                $W->Link(
                    $Crm->translate('Delete only organization'),
                    $Crm->Controller()->Organization()->delete($organization->id)
                )->addClass('icon', 'widget-actionbutton', Func_Icons::ACTIONS_EDIT_DELETE)
            );
            $buttonbar->addItem(
                $W->Link(
                    $Crm->translate('Delete organization and contacts'),
                    $Crm->Controller()->Organization()->deleteWithContacts($organization->id)
                    )->addClass('icon', 'widget-actionbutton', Func_Icons::ACTIONS_EDIT_DELETE)
                );

        } else {

            $page->addItem($W->Label($Crm->translate('This organization is not associated with any CRM element.')));
            $buttonbar->addItem(
                $W->Link(
                    $Crm->translate('Delete organization'),
                    $Crm->Controller()->Organization()->delete($organization->id)
                )->addClass('icon', 'widget-actionbutton', Func_Icons::ACTIONS_EDIT_DELETE)
            );
        }

        $buttonbar->addItem(
            $W->Link(
                $Crm->translate('Cancel'),
                $Crm->Controller()->Organization()->display($organization->id)
            )->addClass('icon', 'widget-actionbutton', Func_Icons::ACTIONS_GO_PREVIOUS)
        );

        $page->addItem($buttonbar);


        return $page;
    }




    protected function linkedItem($record)
    {
        $Crm = $this->Crm();

        $Crm->Ui()->includeContact();

        switch (true) {
            case $record instanceof crm_Deal:
                return $Crm->Ui()->DealCardFrame($record);
                break;
            case $record instanceof crm_Contact:
            case $record instanceof crm_ContactOrganization:
                return $Crm->Ui()->ContactCardFrame(
                    $record,
                    crm_ContactCardFrame::SHOW_ALL
                    & ~(crm_ContactCardFrame::SHOW_ADDRESS | crm_ContactCardFrame::SHOW_FAX | crm_ContactCardFrame::SHOW_PHONE | crm_ContactCardFrame::SHOW_MOBILE)
                );
                break;
            case $record instanceof crm_Organization:
                return $Crm->Ui()->OrganizationCardFrame($record);
                break;
        }
    }


    /**
     * Display a page showing the organizational chart
     * @param int|array $orgs
     * @param int $threshold
     *
     * @return Widget_Action
     */
    public function orgChart($orgs, $threshold = null)
    {
        $Crm = $this->Crm();

        if (!is_array($orgs)) {
            $orgs = array($orgs);
        }
        $page = $Crm->Ui()->Page();

        $Crm->Ui()->includeOrganization();
        $orgChart = crm_orgChart($Crm, $orgs, $threshold);

        $orgChart->setOpenMembers(array());

        $page->addItem($orgChart);

        return $page;
    }


    /**
     *
     * @param int $organization
     *
     * @return Widget_Action
     */
    public function searchReplacement($organization = null, $keyword = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $organizationSet = $Crm->OrganizationSet();
        $organization = $organizationSet->get($organization);
        if (!isset($organization)) {
            throw new crm_Exception($Crm->translate('Trying to access an or with a wrong (unknown) id.'));
        }


        if (!isset($keyword)) {
            $keyword = $organization->name();
            $keywords = array();
            foreach (explode(' ', $keyword) as $k) {
                if (mb_strlen($k) > 3) {
                    $keywords[] = $k;
                }
            }
        } else {
            $keywords = explode(' ', $keyword);
        }


        $page = $Crm->Ui()->Page();
        $page->setMainPanelLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));

        $page->addClass('crm-page-editor');

        $page->addItem($W->Title($Crm->translate('Search replacement for organization'), 1));

        $page->addContextItem($Crm->Ui()->OrganizationCardFrame($organization), $Crm->translate('Organization to replace'));


        $alreadyMatchedIds = array($organization->id => $organization->id);


        $editor = new crm_Editor($Crm, null, $W->VBoxLayout());
        $editor->colon();

        $editor->addItem($editor->labelledField($Crm->translate('Keywords'), $W->LineEdit()->setSize(80), 'keyword'));
//		$editor->setCancelAction($Crm->Controller()->Deal()->cancel());
        $editor->setSaveAction($Crm->Controller()->Organization()->searchReplacement(), $Crm->translate('Search'));
        $editor->setReadOnly();
        $editor->setHiddenValue('tg', bab_rp('tg'));
        $editor->setHiddenValue('organization', $organization->id);
        $editor->setValue('keyword', implode(' ', $keywords));
        $page->addItem($editor);


        if (count($keywords) > 0) {

            $similarOrganizationsSection = $W->Section(
                $Crm->translate('Similar organizations'),
                $W->VBoxLayout()
                    ->setVerticalSpacing(1, 'em'),
                3
            )->addClass('compact');

            $page->addItem($similarOrganizationsSection);

            $criteria = new ORM_FalseCriterion();
            foreach ($keywords as $k) {
                $criteria = $criteria->_OR_($organizationSet->name->contains($k));
            }

            $organizations = $organizationSet->select(
                $organizationSet->id->notIn($alreadyMatchedIds)
                ->_AND_($criteria)
            );


            $similarOrganizationsTable = $W->TableView();
            $similarOrganizationsSection->addItem($similarOrganizationsTable);

            $similarOrganizationsTable->addClass(Func_Icons::ICON_LEFT_16);

            $row = 0;
            foreach ($organizations as $newOrganization) {

                $similarOrganizationsTable->addItem(
                    $W->Link(
                        $W->FlowItems(
                            $Crm->Ui()->OrganizationLogo($newOrganization, 32, 32),
                            $W->Label($newOrganization->name)
                        )->setHorizontalSpacing(1, 'em')->setVerticalAlign('middle'),
                        $Crm->Controller()->Organization()->display($newOrganization->id)
                    ),
                    $row, 0
                );
                $similarOrganizationsTable->addItem(
                    $W->Link(
                        $Crm->translate('Replace with this organization'),
                        $Crm->Controller()->Organization()->replace($organization->id, $newOrganization->id)
                    )->addClass('widget-nowrap icon ' . Func_Icons::ACTIONS_ARROW_RIGHT),
                    $row, 1
                );
                $alreadyMatchedIds[$organization->id] = $organization->id;

                $row++;
            }

            $similarOrganizationsTable->addColumnClass(1, 'widget-column-thin');
        }

        return $page;
    }





    /**
     * Reassociates elements of $oldOrganization to $newOrganization.
     *
     * @param int $oldOrganization
     * @param int $newOrganization
     */
    public function replace($oldOrganization, $newOrganization)
    {
        $Crm = $this->Crm();

        $organizationSet = $Crm->OrganizationSet();
        /* @var $oldOrganization crm_Organization */
        $oldOrganization = $organizationSet->get($oldOrganization);
        if (!isset($oldOrganization)) {
            throw new crm_Exception($Crm->translate('Trying to access an or with a wrong (unknown) id.'));
        }
        $newOrganization = $organizationSet->get($newOrganization);
        if (!isset($newOrganization)) {
            throw new crm_Exception($Crm->translate('Trying to access an or with a wrong (unknown) id.'));
        }

        $Access = $Crm->Access();
        if (!($Access->updateOrganization($oldOrganization) & $Access->updateOrganization($newOrganization))) {
            throw new crm_Exception($Crm->translate('You do not have access to this action.'));
        }

        $oldOrganization->replaceWith($newOrganization->id);


        $organizationSet->delete($organizationSet->id->is($oldOrganization->id));

        $message = sprintf(
            $Crm->translate('The organization %s has been deleted and all its elements reattached to %s.'),
            $oldOrganization->name, $newOrganization->name
        );

        crm_redirect($Crm->Controller()->Organization()->display($newOrganization->id), $message);
    }


    /**
     * Display the form to select the organization classification.
     *
     * @param int   $id
     *
     * @return crm_Page
     */
    public function classify($id = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);

        $record->requireUpdatable($Crm->translate('You are not allowed to access this page.'));

        $Ui->includeOrganization();
        $Ui->includeClassification();

        $page = $Ui->Page();

        $page->setTitle($Crm->translate('Edit organization codification'));

        $orgClassificationSet = $Crm->OrganizationClassificationSet();
        $orgClassifications = $orgClassificationSet->selectForOrganization($record->id, 'defined');

        $classifications = array();
        foreach ($orgClassifications as $orgClassification) {
            $classifications[$orgClassification->classification] = $orgClassification->classification;
        }

        $classificationSelector = crm_classificationSelector($Crm, $classifications);

        $classificationSelector->setName('organization');
        $classificationSelector->setHiddenValue('organization', $record->id);

        $classificationSelector->setCancelAction($Crm->Controller()->Organization()->cancel());
        $classificationSelector->setSaveAction($Crm->Controller()->Organization()->saveClassification(), $Crm->translate('Save the organization codification'));

        $page->addItem($classificationSelector);

        return $page;
    }


    /**
     * Saves the organization classification
     *
     * @param int	$organization		the organization to classify
     * @param array	$classifications	array where keys correspond to selected classification ids.
     * @return Widget_Action
     */
    public function saveClassification($organization = null, $classifications = array())
    {
        require_once dirname(__FILE__) . '/organizationclassification.class.php';
        require_once dirname(__FILE__) . '/classification.class.php';

        $Crm = $this->Crm();

        $organizationSet = $Crm->OrganizationSet();
        /* @var $organization crm_Organization */
        $organization = $organizationSet->get($organization);
        if (!isset($organization)) {
            throw new crm_Exception($Crm->translate('Trying to access an organization with a wrong (unknown) id.'));
        }


        $classificationSet = $Crm->ClassificationSet();

        $orgClassificationSet = $Crm->OrganizationClassificationSet();
        $orgClassificationSet->delete($orgClassificationSet->organization->is($organization->id));

        foreach (array_keys($classifications) as $classification) {
            $orgClassification = $orgClassificationSet->newRecord();
            $orgClassification->organization = $organization->id;
            $orgClassification->classification = $classification;
            $orgClassification->type = 'defined';
            $orgClassification->save();

            $orgClassification = $orgClassificationSet->newRecord();
            $orgClassification->organization = $organization->id;
            $orgClassification->classification = $classification;
            $orgClassification->type = 'implied';
            $orgClassification->save();

            $classification = $classificationSet->get($classification);
            $parents = $classification->getAscendants();
            foreach ($parents as $parent) {
                $orgClassification = $orgClassificationSet->newRecord();
                $orgClassification->organization = $organization->id;
                $orgClassification->classification = $parent->id;
                $orgClassification->type = 'implied';
                $orgClassification->save();
            }
        }

        crm_redirect(crm_BreadCrumbs::last(), $Crm->translate('The organization codification has been saved.'));
    }




    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggest($search = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        bab_requireCredential($Crm->translate('You must be logged'), 'Basic');

        $suggest = $Ui->SuggestOrganization();
        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();

        die;
    }



    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggestParent($child = null, $search = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        bab_requireCredential($Crm->translate('You must be logged'), 'Basic');

        $suggest = $Ui->SuggestParentOrganization($child);
        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();

        die;
    }



    public function create($redirect = false)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->addItem(
            $W->Title($Crm->translate('Create organization'), 1)
        );

        $editor = new crm_Editor($Crm, null, $W->VBoxLayout());
        $editor->colon();

        $editor->setName('organization');
        $editor->addItem(
            $editor->labelledField(
                $Crm->translate('Organization name'),
                $W->LineEdit()->setSize(80),
                'name'
            )
        );
        //		$editor->setCancelAction($Crm->Controller()->Deal()->cancel());
//        $editor->setSaveAction($Crm->Controller()->Organization()->save(), $Crm->translate('Create'));
        $editor->setHiddenValue('tg', bab_rp('tg'));

        $saveButton = $W->SubmitButton()
            ->setLabel($Crm->translate('Save'));

        if ($redirect) {
            $saveButton->setAction($Crm->Controller()->Organization()->save());
        } else {
            $saveButton->setAjaxAction($Crm->Controller()->Organization()->save());
        }
        $editor->addItem($saveButton);
        $page->addItem($editor);

        return $page;
    }


    /**
     *
     * @param $tag
     * @return Widget_Action
     */
    public function displayListForTag($tag = null)
    {
        crm_redirect($this->proxy()->displayList(array('filter' => array('tag' => $tag))));
    }



    /**
     * Redirects to societe.com search page based on data in form.
     *
     * @param array $data
     */
    public function checkSiret($data = null)
    {
        if (isset($data['siret'])) {
            $siret = str_replace(' ', '', $data['siret']);
            if (!empty($siret)) {
                header('Location: http://www.societe.com/cgi-bin/search?champs=' . urlencode($siret));
                die;
            }
        }

        if (isset($data['name'])) {
            $name = trim($data['name']);
            if (!empty($name)) {
                header('Location: http://www.societe.com/cgi-bin/mainsrch?champ=' . urlencode($name));
                die;
            }
        }
        die;
    }


    /**
     * Does nothing and returns to the previous page.
     *
     * @param array	$message
     * @return Widget_Action
     */
    public function cancel()
    {
        crm_redirect(crm_BreadCrumbs::last());
    }



    /**
     * Displays help on this page.
     *
     * @return Widget_Action
     */
    public function help()
    {

    }
}
