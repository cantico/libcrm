<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

$Crm = crm_Crm();
$Crm->includeRecordController();



/**
 * This controller manages actions that can be performed on contacts.
 */
class crm_CtrlContact extends crm_RecordController implements crm_ShopAdminCtrl
{



    protected function populateTableModelView($tableView, $filter)
    {
        $Crm = $this->Crm();

        $contactSet = $Crm->ContactSet();
        $contactSet->address();
        $contactSet->address->country();

        if (isset($contactSet->deliveryaddress))
        {
            $contactSet->deliveryaddress();
            $contactSet->deliveryaddress->country();
        }

        $tableView->addDefaultColumns($contactSet);
        $conditions = $tableView->getFilterCriteria($filter, $contactSet);
        $contactSelection = $contactSet->select($conditions);
        $tableView->setDataSource($contactSelection);
    }


    /**
     * @param array $filter
     *
     * @return crm_ContactTableView
     */
    protected function contactTableModelView($filter)
    {
        $Crm = $this->Crm();

        $tableView = $Crm->Ui()->ContactTableView();
        $recordSet = $this->getRecordSet();
        $tableView->setRecordSet($recordSet);
        $this->populateTableModelView($tableView, $filter);

        return $tableView;
    }





    public function addMailinglist($contacts = null)
    {
        $Crm = $this->Crm();
        $access = $Crm->Access();

        if (!$access->manageMailingLists())
        {
            throw new crm_AccessException($Crm->translate('Access denied'));
        }

        $Crm->includeAddressSet();
        $Crm->Ui()->includeContact();

        $page = $Crm->Ui()->Page();

        $contactSet = $Crm->ContactSet();
        $contactSet->address();
        $contactSet->address->country();
        $tableView = $Crm->Ui()->ContactTableView();
        $tableView->addDefaultColumns($contactSet);

        $filter = isset($contacts['filter']) ? $contacts['filter'] : array();
        $_SESSION['CRM-FILTER-MAILINGLIST'] = $filter;

        $conditions = $tableView->getFilterCriteria($filter, $contactSet);
        $contactsOrm = $contactSet->select($conditions);

        $page->addClass('crm-page-editor');

        $page->setTitle(sprintf($Crm->translate('Manage %s contacts'),$contactsOrm->count()));

        $contactEditor = $Crm->Ui()->MailinglistEditor();

        $page->addItem($contactEditor);

        return $page;
    }



    /**
     * Add current search contact user on a mailing list
     */
    public function saveMailinglist($contacts = null)
    {
        $Crm = $this->Crm();
        $access = $Crm->Access();

        if (!$access->manageMailingLists()
            || ($contacts['action'] == 1 && (!isset($contacts['addmailinglist']) || !$contacts['addmailinglist']))
            ||  ($contacts['action'] == 2 && (!isset($contacts['removemailinglist']) || !$contacts['removemailinglist']))) {
                throw new crm_AccessException($Crm->translate('Access denied'));
            }

            /* @var $ML Func_MailingList */
            $ML = crm_MailingList();
            if ($contacts['action'] == 1) {
                if ($contacts['addmailinglist'] == '-1') {
                    if(!isset($contacts['name']) || $contacts['name'] == '') {
                        throw new crm_AccessException($Crm->translate('Access denied'));
                    }
                    $contacts['addmailinglist'] = $ML->addList($contacts['name'], $contacts['type']);
                    if ($contacts['addmailinglist'] === false) {
                        $e = new crm_SaveException($Crm->translate('This name is already used.'));
                        $e->redirect = false;
                        throw $e;
                    }
                }
            }

            $filter = $_SESSION['CRM-FILTER-MAILINGLIST'];

            $contactSet = $Crm->ContactSet();
            $contactSet->address();
            $contactSet->address->country();
            $tableView = $Crm->Ui()->ContactTableView();
            $tableView->addDefaultColumns($contactSet);

            $conditions = $tableView->getFilterCriteria($filter, $contactSet);
            $contactsOrm = $contactSet->select($conditions);

            $contactlist = array();
            foreach ($contactsOrm as $contact) {
                if($contact->email) {
                    $contactlist[$contact->email] = array(
                        'email' => $contact->email,
                        'name' => bab_composeUserName($contact->firstname, $contact->lastname)
                    );
                }
            }



            if (!empty($contactlist)) {
                if ($contacts['action'] == 1) {
                    $ML->subscribeContacts($contactlist, $contacts['addmailinglist']);
                    crm_addPageInfo($Crm->translate('Contacts added.'));
                } elseif ($contacts['action'] == 2) {
                    $ML->unsubscribeContacts($contactlist, $contacts['removemailinglist']);
                    crm_addPageInfo($Crm->translate('Contacts removed.'));
                }
            }

            return true;
    }



    /**
     * Remove a contact from a mailing list.
     *
     * Ajax method
     *
     * @return bool
     */
    public function deleteFromMailinglist($contact, $idlist)
    {
        $Crm = $this->Crm();
        $access = $Crm->Access();

        if (!$access->manageMailingLists() || !$contact || !$idlist) {
            throw new crm_AccessException($Crm->translate('Access denied'));
        }

        $contactSet = $Crm->ContactSet();
        $contact = $contactSet->get($contactSet->id->is($contact));

        if (!$contact || !$contact->email) {
            throw new crm_AccessException($Crm->translate('Contact not found (wrong ID).'));
        }

        /* @var $ML Func_MailingList */
        $ML = crm_MailingList();
        $c = $ML->newContact();
        $c->setEmail($contact->email);
        return $ML->unsubscribeContact($c, $idlist);
    }

    public function sendEmail($contacts = null)
    {
        $Crm = $this->Crm();

        $access = $Crm->Access();

        if (!$access->listContact()) {
            throw new crm_AccessException($Crm->translate('Access denied'));
        }

        $filter = isset($contacts['filter']) ? $contacts['filter'] : array();


        $contactSet = $Crm->ContactSet();
        $contactSet->address();
        $contactSet->address->country();

        $tableView = $Crm->Ui()->ContactTableView();
        $tableView->addDefaultColumns($contactSet);

        $conditions = $tableView->getFilterCriteria($filter, $contactSet);
        $contacts = $contactSet->select($conditions);

        $emails = array();
        foreach ($contacts as $contact) {
            $email = trim($contact->getMainEmail());
            if ($email === '') {
                continue;
            }
            $emails[] = $email;
        }

        return $Crm->Controller()->Email(false)->edit(implode(', ',$emails));
    }



    /**
     * Editor contact form
     *
     * @param int | null $contact
     * @return Widget_Action
     */
    public function edit($contact = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $access = $Crm->Access();

        $Crm->includeAddressSet();
        $Crm->Ui()->includeContact();

        $page = $Crm->Ui()->Page();

        $contactSet = $Crm->ContactSet();
        $contactSet->join('address');

        if ($Crm->onlineShop && !$Crm->noDelivery)
        {
            $contactSet->join('deliveryaddress');
        }

        $page->addClass('crm-page-editor');

        if(isset($_POST['contact']) && isset($_POST['contact']['id'])){
            $contact = $_POST['contact']['id'];
        }

        if (isset($contact)) {
            $page->addItem($W->Title($Crm->translate('Edit contact'), 1));
            if (is_array($contact)) {

            }else{
                $contact = $contactSet->get($contact);
                if (!isset($contact)) {
                    throw new crm_Exception('Trying to access a contact with a wrong (unknown) id.');
                }

                if (!$access->updateContact($contact))
                {
                    throw new crm_AccessException($Crm->translate('Access denied'));
                }
            }

        } else {
            $page->addItem($W->Title($Crm->translate('Create a new contact'), 1));

            if (!$access->createContact())
            {
                throw new crm_AccessException($Crm->translate('Access denied'));
            }
        }

        $contactEditor = $Crm->Ui()->ContactEditor($contact);
        $contactEditor->setName('contact');

        $page->addItem($contactEditor);

        $addon = bab_getAddonInfosInstance('LibCrm');
        $defaultImage = $addon->getStylePath() . 'images/contact-default.png';

        $imagesFormItem = $W->ImagePicker()
        ->setDimensions(256, 256)
        ->setDefaultImage(new bab_Path($defaultImage))
        ->oneFileMode()
        ;

        /*@var $imagesFormItem Widget_ImagePicker */

        $imagesFormItem->setAssociatedDropTarget($imagesFormItem);



        if ($contact) {

            $photoUploadPath = $contact->getPhotoUploadPath();
            if (!is_dir($photoUploadPath->toString())) {
                $photoUploadPath->createDir();
            }

            $imagesFormItem->setFolder($photoUploadPath);

        } else {

            $imagesFormItem->setName('photo');

        }


        $searchable = array();

        foreach($contactEditor->getFields() as $widget) {

            if ('email' === $widget->getName() && method_exists($imagesFormItem, 'setGravatarEmailField'))
            {
                /*@var $widget Widget_LabelledWidget */
                $imagesFormItem->setGravatarEmailField($widget->getInputWidget());
            }
        }



        $page->addContextItem(
            $imagesFormItem,
            $Crm->translate('Photo')
            );

        if(isset($_POST['contact'])){
            $contactEditor->setValues($_POST['contact'], array('contact'));
        }

        return $page;
    }




    /**
     *
     * @param crm_Contact $contact
     */
    protected function savePhoto(crm_Contact $contact)
    {
        $Crm = $this->Crm();
        // attach photo to contact.

        $W = bab_Widgets();

        $res = $W->ImagePicker()->getTemporaryFiles('photo');

        if (isset($res)) {
            foreach($res as $filePickerItem) {
                /*@var $filePickerItem Widget_FilePickerItem */
                $contact->importPhoto($filePickerItem->getFilePath());
            }
        }


    }



    /**
     * Launch the save contact action and redirect
     * @requireSaveMethod
     * @param array	$contact
     * @return Widget_Action
     */
    public function save($contact = null)
    {
        $this->requireSaveMethod();
        $Crm = $this->Crm();

        $this->saveAction($contact);

        crm_redirect(crm_BreadCrumbs::last(), $Crm->translate('The contact has been saved.'));
    }




    /**
     * Save the contact
     *
     * @param unknown $contact
     * @return crm_Contact
     */
    public function saveAction($contact = null)
    {
        $this->requireSaveMethod();
        $Crm = $this->Crm();
        $W = bab_Widgets();
        $set = $Crm->ContactSet();
        $access = $Crm->Access();

        if (isset($contact['address']) && is_array($contact['address'])) {
            $set->join('address');
        }

        if (isset($contact['deliveryaddress']) && is_array($contact['deliveryaddress'])) {
            $set->join('deliveryaddress');
        }


        if (isset($Crm->Organization) && isset($contact['contactOrganization'])) {

            $organizationSet = $this->Crm()->OrganizationSet();

            //  verify organization link before saving

            foreach ($contact['contactOrganization'] as $id => $contactOrgData) {

                if (is_numeric($id) && empty($contactOrgData['organization']) && !empty($contactOrgData['organizationname'])) {
                    if (null === $organizationSet->get($organizationSet->name->is($contactOrgData['organizationname']))) {
                        $e = new crm_SaveException(sprintf($Crm->translate('The organization %s does not exists'), $contactOrgData['organizationname']));
                        $e->redirect = false;
                        throw $e;
                        continue;
                    }
                }
            }
        }

        if (isset($contact['photo'])) {
            $contact['photo'] = reset($contact['photo']);
        }


        /* @var $record crm_Contact */
        if (!empty($contact['id']))
        {
            $record = $set->get($contact['id']);

            if (!$access->updateContact($record))
            {
                throw new crm_AccessException($Crm->translate('Access denied'));
            }

        } else {

            if (!$access->createContact())
            {
                throw new crm_AccessException($Crm->translate('Access denied'));
            }

            $record = $set->newRecord();
        }

        if (isset($contact['addresses'])) {
            $contactAddresses = $contact['addresses'];
            unset($contact['addresses']);
            unset($contact['address']);
        } else {
            $contactAddresses = null;
        }

        $oldEmail = $record->email;

        $record->setFormInputValues($contact);

        //		if (isset($contact['usertype']) && $contact['usertype'] == 3) {
        $record->manageUserAccount($contact);
        //		}


        $record->save();

        if ($oldEmail && $oldEmail != $record->email) {
            $ML = crm_MailingList();
            if (false !== $ML) {
                $oldContact = $ML->newContact();
                $oldContact->setEmail($oldEmail);
                $newContact = $ML->newContact();
                $newContact->setEmail($record->email);
                try {
                    $ML->updateContact($oldContact, $newContact);
                } catch (LibNewsletterException $e) {
                    // if newsletter installed but not configured
                    bab_debug($e->getMessage());
                }
            }
        }

        $this->savePhoto($record);

        if (is_array($contactAddresses)) {

            $addressSet = $Crm->AddressSet();

            foreach ($contactAddresses as $key => $address) {

                if (!is_array($address)) {
                    continue;
                }

                /* @var $newAddress crm_Address */
                if (strpos($key, '.') === false) {
                    $newAddress = $addressSet->newRecord();
                    $newLink = true;
                } else {
                    list($previousAddressType, $addressId) = explode('.', $key);
                    $newAddress = $addressSet->get($addressId);
                    $newLink = false;
                }
                $newAddress->setValues($address);

                if (!$newAddress->isEmpty()) {
                    $newAddress->save();
                    if ($newLink) {
                        $record->addAddress($newAddress, $address['type']);
                    } elseif ($previousAddressType !== $address['type']) {
                        $record->updateAddress($newAddress, $address['type'], $previousAddressType);
                    }
                }
            }
        }





        if ($record->user && isset($contact['user_extra']['roles'])) {
            $contactRoleSet = $Crm->ContactRoleSet();
            $contactRoleSet->delete($contactRoleSet->contact->is($record->id));
            foreach ($contact['user_extra']['roles'] as $roleId => $checked) {
                if ($checked) {
                    $contactRole = $contactRoleSet->newRecord();
                    $contactRole->contact = $record->id;
                    $contactRole->role = $roleId;
                    $contactRole->save();
                }
            }
        }

        if (isset($contact['photo'])) {

            if ($iterator = $W->ImagePicker()->getTemporaryFiles('photo')) {

                foreach($iterator as $file) {
                    $record->importPhoto($file->getFilePath());
                    break;
                }

                $W->ImagePicker()->cleanup();
            }
        }


        /*
         if (isset($Crm->Organization) && isset($contact['contactOrganization'])) {
         $this->addOrganizationProcess($record, $contact);
         }
         */

        // contactorganization


        if (isset($Crm->Organization) && isset($contact['contactOrganization'])) {



            $contactOrganizationSet = $this->Crm()->ContactOrganizationSet();

            $contactOrganizations = $contact['contactOrganization'];
            reset($contactOrganizations);

            $main = isset($contactOrganizations['main']) ? $contactOrganizations['main'] : key($contactOrganizations);

            // For each specified organization we save the position of the contact.




            foreach ($contactOrganizations as $id => $contactOrgData) {



                if (is_numeric($id)) {

                    if ($id > 0) {
                        $contactOrganization = $contactOrganizationSet->get($id);
                    } else {
                        $contactOrganization = $contactOrganizationSet->newRecord();
                    }

                    if (!empty($contactOrgData['organization'])) {
                        $contactOrganization->organization = $contactOrgData['organization'];
                    } else if (!empty($contactOrgData['organizationname'])) {
                        if ($organization = $organizationSet->get($organizationSet->name->is($contactOrgData['organizationname']))) {
                            $contactOrganization->organization = $organization->id;
                        } else {
                            throw new crm_SaveException(sprintf($Crm->translate('The organization %s does not exists'), $contactOrgData['organizationname']));
                            continue;
                        }
                    }
                    $contactOrganization->contact = $record->id;

                    $contactOrganization->position = $contactOrgData['position'];
                    if (isset($contactOrgData['history_to']))
                    {
                        $contactOrganization->history_to = $contactOrganizationSet->history_to->input($contactOrgData['history_to']);
                    }
                    $contactOrganization->main = (($id == $main) ? 1 : 0);

                    $contactOrganization->save();

                    if (isset($Crm->Entry))
                    {
                        // If the contact position is not one in the dedicated entries table, we add it.
                        $entrySet = $Crm->EntrySet();
                        $entries = $entrySet->select($entrySet->category->is(crm_Entry::CONTACT_POSITIONS)->_AND_($entrySet->text->is($contactOrgData['position'])));
                        if ($entries->count() === 0) {
                            $entry = $entrySet->newRecord();
                            $entry->text = $contactOrgData['position'];
                            $entry->category = crm_Entry::CONTACT_POSITIONS;
                            $entry->save();
                        }
                    }

                }

            }

            // Ensure the contact organization field is up to date
            $record->updateMainOrganization();
        }

        // update directory entry from contact if possible
        $record->updateUser();


        return $record;
    }


    /**
     * Display the form to select the contact classification.
     *
     * @param int   $id
     * @return crm_Page
     */
    public function classify($id = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->ui();

        $Ui->includeContact();
        $Ui->includeClassification();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);

        $record->requireUpdatable($Crm->translate('You are not allowed to access this page.'));

        $page = $Ui->Page();

        $page->setTitle($Crm->translate('Edit contact codification'));

        $contactClassificationSet = $Crm->ContactClassificationSet();
        $contactClassifications = $contactClassificationSet->selectForContact($record->id, 'defined');

        $classifications = array();
        foreach ($contactClassifications as $contactClassification) {
            $classifications[$contactClassification->classification] = $contactClassification->classification;
        }

        $classificationSelector = crm_classificationSelector($Crm, $classifications);

        $classificationSelector->setHiddenValue('contact', $record->id);

        $classificationSelector->setCancelAction($Crm->Controller()->Contact()->cancel());
        $classificationSelector->setSaveAction($Crm->Controller()->Contact()->saveClassification(), $Crm->translate('Save the contact codification'));

        $page->addItem($classificationSelector);

        return $page;
    }



    /**
     * Saves the contact classification
     *
     * @param int	$contact			the contact to classify
     * @param array	$classifications	array where keys correspond to selected classification ids.
     * @return Widget_Action
     */
    public function saveClassification($contact = null, $classifications = array())
    {
        $Crm = $this->Crm();

        $contactSet = $Crm->ContactSet();
        $contact = $contactSet->get($contact);
        if (!isset($contact)) {
            throw new crm_Exception('Trying to access a contact with a wrong (unknown) id.');
        }

        $classificationSet = $Crm->ClassificationSet();

        $contactClassificationSet = $Crm->ContactClassificationSet();
        $contactClassificationSet->delete($contactClassificationSet->contact->is($contact->id));

        foreach (array_keys($classifications) as $classification) {
            $contactClassification = $contactClassificationSet->newRecord();
            $contactClassification->contact = $contact->id;
            $contactClassification->classification = $classification;
            $contactClassification->type = 'defined';
            $contactClassification->save();

            $contactClassification = $contactClassificationSet->newRecord();
            $contactClassification->contact = $contact->id;
            $contactClassification->classification = $classification;
            $contactClassification->type = 'implied';
            $contactClassification->save();

            $classification = $classificationSet->get($classification);
            $parents = $classification->getAscendants();
            foreach ($parents as $parent) {
                $contactClassification = $contactClassificationSet->newRecord();
                $contactClassification->contact = $contact->id;
                $contactClassification->classification = $parent->id;
                $contactClassification->type = 'implied';
                $contactClassification->save();
            }
        }

        crm_redirect(crm_BreadCrumbs::last(), $Crm->translate('The contact codification has been saved.'));
    }





    /**
     * Displays a form to assign sectors to a specific user.
     *
     * @param int	$contact
     * @return Widget_Action
     */
    public function assignSectors($contact)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $contactSet = $Crm->ContactSet();
        $contact = $contactSet->get($contact);
        if (!isset($contact)) {
            throw new crm_Exception('Trying to access a contact with a wrong (unknown) id.');
        }

        $page = $this->Crm()->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->addItem($W->Title($this->Crm()->translate('Assign sectors'), 1));

        $page->addContextItem($Crm->Ui()->ContactCardFrame($contact));
        $editor = crm_contactSectorEditor($Crm, $contact);

        $editor->setSaveAction($Crm->Controller()->Contact()->saveAssignedSectors());

        $page->addItem($editor);
        return $page;
    }


    /**
     * Saves assignation of sectors to a specific user.
     *
     * @param int	$contact
     * @param array	$sectors
     * @return Widget_Action
     */
    public function  saveAssignedSectors($contact = null, $sectors = null)
    {
        $Crm = $this->Crm();

        $contactSet = $Crm->ContactSet();
        $contact = $contactSet->get($contact);
        if (!isset($contact)) {
            throw new crm_Exception('Trying to access a contact with a wrong (unknown) id.');
        }


        if ($contact->user) {
            $contactSectorSet = $Crm->ContactSectorSet();
            $contactSectorSet->delete($contactSectorSet->contact->is($contact->id));
            foreach ($sectors as $sectorId => $checked) {
                if ($checked) {
                    $contactSector = $contactSectorSet->newRecord();
                    $contactSector->contact = $contact->id;
                    $contactSector->sector = $sectorId;
                    $contactSector->save();
                }
            }
        }

        crm_redirect(crm_BreadCrumbs::last());
    }



    /**
     *
     * @param int $contact
     * @throws crm_Exception
     * @return crm_Page
     */
    public function editAcquaintance($contact)
    {
        $Crm = $this->Crm();
        $Crm->Ui()->includeContact();

        if (!isset($contact)) {
            throw new crm_Exception('Trying to access a contact with a wrong (unknown) id.');
        }

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $contact = $Crm->ContactSet()->request($contact);
        $page->setTitle($Crm->translate('Edit contact acquaintance'), 1);

        $editor = crm_acquaintanceEditor($Crm, $contact->id);
        $editor->isAjax = $this->isAjaxRequest();

        $page->addItem($editor);

        return $page;
    }



    /**
     * Saves contact acquaintance.
     *
     * @param int	$contact
     * @param int	$level
     * @return Widget_Action
     */
    public function saveAcquaintance($contact = null, $level = null)
    {
        $Crm = $this->Crm();

        $contactSet = $Crm->ContactSet();
        $contact = $contactSet->request($contact);

        $user = $Crm->Access()->currentContact();

        if ($user) {
            $contact->setKnownBy($user, $level);
            $this->addMessage($Crm->translate('The contact acquaintance has been saved.'));
        }

        return true;
    }






    /**
     * Display contact information page.
     *
     * @param $contact
     * @return Widget_Action
     */
    public function display($contact)
    {
        /* @var $crm Func_Crm */
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $Access = $Crm->Access();

        $contactSet = $Crm->ContactSet();
        $contact = $contactSet->get($contact);
        if (!isset($contact)) {
            throw new crm_Exception('Trying to access a contact with a wrong (unknown) id.');
        }

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($contact->id), $contact->getFullName());


        if (!$Access->viewContact($contact))
        {
            throw new crm_AccessException($Crm->translate('Access denied'));
        }


        $W = bab_Widgets();
        $Crm->Ui()->includeContact();
        $Crm->Ui()->includeTask();
        $Crm->Ui()->includeTag();

        $page = $Crm->Ui()->Page();


        $mainFrame = $W->Frame()->setLayout($W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $mainFrame->addItem($Crm->Ui()->ContactFullFrame($contact));
        $mainFrame->addClass('crm-detailed-info-frame');

        // add note


        $maxHistory = 8;
        $historyFrame = $W->VBoxItems(
            $Crm->Controller()->Search(false)->history($contact->getRef(), $maxHistory, 0, true)
            );
        $mainFrame->addItem(
            $W->Section(
                $Crm->translate('History'),
                $historyFrame,
                2
                )->setFoldable(true)
            ->addClass('compact')
            );


        $page->addItem($mainFrame);


        // Tell clipboard what actions are possible on this page.

        $this->addAcceptedClipboardObject(
            'Email',
            $Crm->Controller()->Contact()->CopyFromClipboard('__1__', $contact->id),
            $Crm->translate('Copy mail in this contact history'),
            Func_Icons::ACTIONS_EDIT_PASTE
            );

        // Actions

        $actionsFrame = $page->ActionsFrame();
        $page->addContextItem($actionsFrame);

        $actions = $this->getActions($contact);

        foreach($actions as $action)
        {
            $actionsFrame->addItem($action);
        }

        // Tasks

        if (isset($Crm->Task))
        {

            $tasksSection = $W->Section(
                $Crm->translate('Incoming tasks for this contact'),
                crm_userTasksForCrmObject($contact),
                4
                );

            $tasksSectionMenu = $tasksSection->addContextMenu('inline');

            $tasksSectionMenu->addItem(
                $W->VboxItems(
                    $W->Link($Crm->translate('Add'), $Crm->Controller()->Task()->edit(null, $contact->getRef()))
                    ->setTitle($Crm->translate('Add a task'))
                    ->addClass('widget-instant-button widget-actionbutton'),
                    $taskEditor = $Crm->Ui()->TaskEditor()
                    ->associateTo($contact)
                    ->addClass('widget-instant-form')
                    )->addClass('widget-instant-container')
                );

            $page->addContextItem($tasksSection);
        }


        $contactAcquaintances = new crm_contactAcquaintances($Crm);
        $acquaintanceFrame = $contactAcquaintances->getFrame($contact);
        if (isset($acquaintanceFrame)) {
            $page->addContextItem($acquaintanceFrame);
        }

        if (isset($Crm->Deal))
        {

            $dealsSection = $W->Section(
                $Crm->translate('Deals with this contact'),
                crm_dealsWithContact($contact),
                4
                );

            $dealsSectionMenu = $dealsSection->addContextMenu('inline');

            if ($Access->createDeal()) {
                $dealsSectionMenu->addItem(
                    $W->Link(
                        $Crm->translate('Add'),
                        $Crm->Controller()->Deal()->addForContact($contact->id)
                        )->setTitle($Crm->translate('Add a deal with this contact'))
                    ->addClass('widget-actionbutton')
                    );
            }

            $page->addContextItem($dealsSection);

        }


        if (isset($Crm->ShoppingCart))
        {
            $Crm->includeShoppingCartSet();

            if ($section = $this->shoppingCartByStatus($contact, $Crm->translate('Ongoing shopping cart'), array(crm_ShoppingCart::EDIT, crm_ShoppingCart::CONFIRMED)))
            {
                $page->addContextItem($section);
            }

            // produit un segmentation fault sur amedesvins.com :

            /*
             if ($section = $this->shoppingCartByStatus($contact, $Crm->translate('Order(s) received / in preparation'), array(crm_ShoppingCart::ARCHIVED, crm_ShoppingCart::DELIVERY)))
             {
             $page->addContextItem($section);
             }
             */
        }

        return $page;
    }



    /**
     * Link the specified object to this contact.
     *
     * @param string $ref A reference to the object to copy.
     * @param int    $contact     The destination contact id.
     */
    public function CopyFromClipboard($ref = null, $contact = null)
    {
        $Crm = $this->Crm();

        $contactSet = $Crm->ContactSet();
        /* @var $contact crm_Contact */
        $contact = $contactSet->get($contact);
        if (!isset($contact)) {
            throw new crm_Exception($Crm->translate('Trying to access a contact with a wrong (unknown) id.'));
        }

        $object = $Crm->getRecordByRef($ref);

        if ($object instanceof crm_Email) {
            $object->linkTo($contact);
        }

        return true;
    }






    /**
     * Section with a list of shopping carts
     * @param array $status
     * @return Widget_Section
     */
    protected function shoppingCartByStatus($contact, $title, $status)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $W = bab_Widgets();

        $scSet = $Crm->ShoppingCartSet();
        $res = $scSet->select($scSet->contact->is($contact->id)->_AND_($scSet->status->in($status)));

        if (0 !== $res->count())
        {
            $scSection = $W->Section(
                $title,
                $vbox = $W->VBoxLayout()->setVerticalSpacing(.5,'em'),
                4
                )->setFoldable(true);

                foreach($res as $shoppingcart)
                {
                    $scSection->addItem($Ui->ShoppingCartCardFrame($shoppingcart));
                }

                return $scSection;
        }

        return null;
    }



    protected function getActions(crm_Record $contact)
    {
        /* @var $crm Func_Crm */
        $Crm = $this->Crm();
        $Access = $Crm->Access();
        $W = bab_Widgets();

        $actionsFrame = array();

        if ($Access->updateContact($contact)) {
            $actionsFrame[] = $W->Link(
                $W->Icon($Crm->translate('Edit this contact'), Func_Icons::ACTIONS_DOCUMENT_EDIT),
                $Crm->Controller()->Contact()->edit($contact->id)
                );
            if (isset($Crm->ContactClassification)) {
                $actionsFrame[] = $W->Link(
                    $W->Icon($Crm->translate('Edit contact codification'), Func_Icons::ACTIONS_VIEW_LIST_TREE),
                    $Crm->Controller()->Contact()->classify($contact->id)
                    );
            }
        }

        if ($Access->deleteContact($contact)) {
            $actionsFrame[] = $W->Link(
                $W->Icon($Crm->translate('Delete this contact'), Func_Icons::ACTIONS_EDIT_DELETE),
                $Crm->Controller()->Contact()->confirmDelete($contact->id)
                );
        }

        if (isset($Crm->ContactAcquaintance)) {
            $actionsFrame[] = $W->Link(
                $W->Icon($Crm->translate('I know this contact'), Func_Icons::ACTIONS_DIALOG_OK),
                $Crm->Controller()->Contact()->editAcquaintance($contact->id)
                )->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD);
        }

        if (isset($Crm->Email)) {
            $actionsFrame[] = $W->Link(
                $W->Icon($Crm->translate('Send an email'), Func_Icons::ACTIONS_MAIL_SEND),
                $Crm->Controller()->Email()->edit($contact->getMainEmail(), null, null, $contact->getRef())
                );
        }

        if (isset($Crm->Sector) && $Crm->Access()->assignSectors($contact)) {
            $actionsFrame[] = $W->Link($W->Icon($Crm->translate('Assign sectors'), Func_Icons::APPS_STATISTICS), $Crm->Controller()->Contact()->assignSectors($contact->id));
        }

        return $actionsFrame;
    }


    /**
     * Displays a form to create a new contact for the specified organization.
     *
     * @param int	$organization
     * @param array	$contact
     *
     * @return Widget_Action
     */
    public function add($organization = null, $contact = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Crm->Ui()->includeContact();

        $Crm->includeAddressSet();

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');



        $page->addItem($W->Title($Crm->translate('Create a new contact'), 1));



        $contactSet = $Crm->ContactSet();
        $contactSet->join('address');
        $newContact = $contactSet->newRecord();

        $organizationRecord = null;

        if (isset($organization)) {

            $organizationSet = $Crm->OrganizationSet();
            $organizationRecord = $organizationSet->get($organization);
            if (!isset($organizationRecord)) {
                throw new crm_Exception('Trying to access an organization with a wrong (unknown) id.');
            }

            $page->addContextItem(
                $Crm->Ui()->OrganizationCardFrame($organizationRecord),
                $Crm->translate('Organization')
                );

            $newContact->organization = $organizationRecord->id;
        }

        $form = $Crm->Ui()->ContactEditor($newContact, $organizationRecord);
        if (isset($organizationRecord)) {
            $form->setId('crm-add-contact-' . $organizationRecord->id);
        }

        // attention au bug si 2 creation de contact a la suite
        // $form->setPersistent(true);

        if (isset($contact)) {
            $form->setValues($contact);
        }

        $page->addItem($form);

        return $page;
    }




    /**
     * Display a form used to merge multipe contacts
     *
     * @param	int		$contact
     * @param	array	$duplicates		array of contact id to merge into the contact
     *
     * @return Widget_Action
     */
    public function merge($contact = null, $duplicates = null)
    {
        $page = $this->Crm()->Ui()->Page();

        $contactSet = $this->Crm()->ContactSet();
        $contact = $contactSet->get($contact);

        $form = $this->Crm()->Ui()->ContactMergeEditor();

        $form->setMainRecord($contact);

        foreach($duplicates as $key => $id) {
            $form->addAlternativeRecord($contactSet->get($id));
        }

        $form->addFields();

        $page->addItem($form);

        return $page;
    }



    /**
     * Downlaod a vCard
     * @param int $contact
     * @return Widget_Action
     */
    public function vCard($contact = null)
    {
        $contact = $this->Crm()->ContactSet()->get($contact);

        if (!isset($contact)) {
            die('missing contact');
        }

        $filename = bab_removeDiacritics(bab_convertStringFromDatabase($contact->lastname.'_'.$contact->firstname, 'ISO-8859-15'));
        $filename = str_replace(' ', ' _', $filename);
        $filename .= '.vcf';

        $output = $contact->vCard()->toString();

        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Length: ".strlen($output));
        header("Connection: close");
        header("Content-Type: text/x-vCard; name=$filename");

        echo $output;

        die();
    }


    /**
     * Download a vCard
     * @param int $contact
     * @return Widget_Action
     */
    public function sendVcard($id = null)
    {
        $Crm = $this->Crm();
        $recordSet = $this->getRecordSet();

        $record = $recordSet->request($id);

        $mail = bab_mail();

        if (!$mail) {
            return;
        }

        $emailTitle = $record->getFullName();

        $emailMessage = sprintf($Crm->translate('Please, find attached the vcard of %s.'), $record->getFullName());

        $mail->mailSubject($emailTitle);

        $htmlMessage = $mail->mailTemplate(bab_toHtml($emailMessage, BAB_HTML_BR));
        $mail->mailBody($htmlMessage, 'html');
        $mail->mailAltBody($emailMessage);

        $mail->clearAllRecipients();

        $vCard = $record->vCard()->toString();

        $contactSet = $Crm->ContactSet();
        $recipient = $contactSet->get($contactSet->user->is(bab_getUserId()));

        if (!$recipient) {
            return;
        }
        $mail->mailTo($recipient->getMainEmail());
        $filename = bab_removeDiacritics(bab_convertStringFromDatabase($record->lastname.'_'.$record->firstname, 'ISO-8859-15'));
        $filename = str_replace(' ', '_', $filename);
        $filename .= '.vcf';

        $mail->mailStringAttach($vCard, $filename, 'text/x-vCard');

        /* @var $Spooler Func_Mailspooler */
        if ($Spooler) {
            $Spooler->save($mail);
        }
        $mail->send();

        $this->addMessage(sprintf($Crm->translate('The vcard of %s has been sent to your email address.'), $record->getFullName()));

        return true;
    }


    /**
     * @param	crm_Contact		$contactRecord
     * @param 	array			$contact
     * @return unknown_type
     */
    protected function addOrganizationProcess(crm_Contact $contactRecord, array $contact)
    {
        $Crm = $this->Crm();

        $organization = 0;
        $position = isset($contact['contactOrganization'][-1]['position']) ?
        $contact['contactOrganization'][-1]['position'] : '';


        if (0 === $organization)
        {
            if (!empty($contact['contactOrganization'][-1]['organizationname'])) {
                $set = $Crm->OrganizationSet();
                if ($organizationRecord = $set->get($set->name->is($contact['contactOrganization'][-1]['organizationname']))) {
                    $organization = (int) $organizationRecord->id;
                } else {
                    throw new crm_SaveException(sprintf($Crm->translate('The organization %s does not exists'), $contact['contactOrganization'][-1]['organizationname']));
                }
            }
        }

        if (0 !== $organization)
        {
            try {

                $this->addOrganizationToContact($contactRecord, $organization, $contact['contactOrganization'][-1]);

            } catch(ORM_BackEndSaveException $e) {
                global $babBody;
                bab_debug($e->getMessage());
                $babBody->addError($this->Crm()->translate('This organization already exists'));
                return false;
            }
        }


        return true;
    }


    /**
     *
     * @param crm_Contact	$contactRecord
     * @param int			$organization
     * @param array			$contactOrganization		array of infos to add within the organization link
     *
     * @throws ORM_BackEndSaveException
     *
     * @return unknown_type
     */
    protected function addOrganizationToContact(crm_Contact $contactRecord, $organization, $contactOrganization)
    {
        $contactRecord->addOrganization($organization, $contactOrganization['position']);
    }





    /**
     * OK button to add an organization from crm_ContactEditor
     * @param	array	$contact
     *
     * @see crm_ContactEditor::Organizations
     *
     * @return Widget_Action
     */
    public function addOrganization($contact = null)
    {
        $contactRecord = $this->Crm()->ContactSet()->get($contact['id']);
        if ($this->addOrganizationProcess($contactRecord, $contact)) {
            crm_redirect($this->proxy()->edit($contact['id']));
        }
    }


    /**
     * @see crm_ContactEditor::Organizations
     * @param int $contactorganization
     * @return Widget_Action
     */
    public function removeOrganization($contactorganization = null)
    {
        $Crm = $this->Crm();

        $set = $Crm->ContactOrganizationSet();
        if ($contactorganization) {
            $record = $set->get($contactorganization);
            $Crm->includeContactSet();
            $contact = $record->contact();
            if (!$contact) {
                throw new crm_Exception('');
            }
            $set->delete($set->id->is($record->id));
            $contact->updateMainOrganization();
            crm_redirect($this->proxy()->edit($contact->id));
        }
    }




    public function removeAddress($contact = null, $address = null, $type = null)
    {
        $Crm = $this->Crm();

        $contactSet = $Crm->ContactSet();
        /* @var $contact crm_Contact */
        $contact = $contactSet->get($contact);

        $addressSet = $Crm->AddressSet();
        $address = $addressSet->get($address);

        $contact->removeAddress($address, $type);
    }

    /**
     *
     * @param $contact
     * @return Widget_Action
     */
    public function displayListForTag($tag = null)
    {
        crm_redirect($this->proxy()->displayList(array('filter' => array('tag' => $tag))));
    }



    /**
     * Displays an information page about elements linked to an organization
     * before deleting it, and propose the user to confirm or cancel
     * the organization deletion.
     *
     * @param int $contact
     *
     * @return Widget_Action
     */
    public function confirmDelete($contact = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $set = $Crm->ContactSet();
        /* @var $contact crm_Contact */
        $contact = $set->get($contact);
        if (!isset($contact)) {
            throw new crm_Exception($Crm->translate('Trying to delete an contact with a wrong (unknown) id.'));
        }

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->confirmDelete($contact->id), $Crm->translate('Delete contact'));

        $page = $Crm->Ui()->Page();
        $page->setMainPanelLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
        $page->addClass('crm-page-editor');

        $page->addItem($W->Title($Crm->translate('Delete contact'), 1));

        $page->addContextItem(
            $Crm->Ui()->ContactCardFrame($contact),
            $Crm->translate('Contact')
            );

        $buttonbar = new crm_Toolbar();
        $buttonbar->local = true;

        $relatedRecords = $contact->getRelatedRecords();





        $nbTasks = 0;
        if (isset($Crm->Task))
        {
            $tasks = $contact->selectTasks();
            $nbTasks = $tasks->count();
        }

        $nbNotes = 0;
        if (isset($Crm->Note))
        {
            $notes = $contact->selectNotes();
            $nbNotes = $notes->count();
        }

        $nbEmails = 0;
        if (isset($Crm->Email))
        {
            $emails = $contact->selectEmails();
            $nbEmails = $emails->count();
        }

        if ($nbTasks > 0 || $nbNotes > 0 || $nbEmails > 0 || count($relatedRecords) > 0) {

            $relatedRecordSection = $W->Section(
                $Crm->translate('Several CRM elements are related to this contact'),
                $W->VBoxItems()->setVerticalSpacing(1, 'em')
                );
            $page->addItem($relatedRecordSection);

            if ($nbTasks > 0) {
                $relatedRecordSection->addItem($W->Label($Crm->translate('Tasks:') . ' ' . $nbTasks));
            }
            if ($nbNotes > 0) {
                $relatedRecordSection->addItem($W->Label($Crm->translate('Notes:') . ' ' . $nbNotes));
            }
            if ($nbEmails > 0) {
                $relatedRecordSection->addItem($W->Label($Crm->translate('Emails:') . ' ' . $nbEmails));
            }

            foreach ($relatedRecords as $setClassName => $records) {

                $method = mb_substr($setClassName, mb_strlen($Crm->classPrefix));
                $set = $Crm->$method();
                // $set = new $setClassName;

                $relatedRecordSection->addItem(
                    $section = $W->Section(
                        $Crm->translate($set->getDescription()),
                        $W->VBoxItems()->setVerticalSpacing(1, 'em'),
                        4
                        )->setFoldable(true)
                    );

                $section->addContextMenu('inline')->addItem($W->Label($records->count())->addClass('widget-counter-label'));

                foreach ($records as $record) {
                    switch (true) {
                        case $record instanceof crm_Deal:
                            $section->addItem($Crm->Ui()->DealCardFrame($record));
                            break;
                        case $record instanceof crm_Contact:
                            $section->addItem(
                            $Crm->Ui()->ContactCardFrame(
                            $record,
                            crm_ContactCardFrame::SHOW_ALL
                            & ~(crm_ContactCardFrame::SHOW_ADDRESS | crm_ContactCardFrame::SHOW_FAX | crm_ContactCardFrame::SHOW_PHONE | crm_ContactCardFrame::SHOW_MOBILE)
                            )
                            );
                            break;

                        case $record instanceof crm_Order:
                            $section->addItem($Crm->Ui()->OrderCardFrame($record));
                            break;

                        case $record instanceof crm_ShoppingCart:
                            $section->addItem($Crm->Ui()->ShoppingCartAdmCardFrame($record));
                            break;
                    }
                }
            }

            $buttonbar->addItem(
                $W->Link(
                    $Crm->translate('Replace with an existing contact...'),
                    $Crm->Controller()->Contact()->searchReplacement($contact->id)
                    )->addClass('icon', 'widget-actionbutton', Func_Icons::ACTIONS_EDIT_COPY)
                );
            $buttonbar->addItem(
                $W->Link(
                    $Crm->translate('Delete anyway'),
                    $Crm->Controller()->Contact()->delete($contact->id)
                    )->addClass('icon', 'widget-actionbutton', Func_Icons::ACTIONS_EDIT_DELETE)
                );

        } else {

            $page->addItem($W->Label($Crm->translate('This contact is not associated with any CRM element.')));
            $buttonbar->addItem(
                $W->Link(
                    $Crm->translate('Delete contact'),
                    $Crm->Controller()->Contact()->delete($contact->id)
                    )->addClass('icon', 'widget-actionbutton', Func_Icons::ACTIONS_EDIT_DELETE)
                );
        }

        if ($previousAction = crm_BreadCrumbs::getPosition(-1)) {
            $buttonbar->addItem(
                $W->Link(
                    $Crm->translate('Cancel'),
                    $previousAction
                    )->addClass('icon', 'widget-actionbutton', Func_Icons::ACTIONS_GO_PREVIOUS)
                );
        }

        $page->addItem($buttonbar);

        return $page;
    }



    /**
     *
     * @param int $contact
     *
     * @return Widget_Action
     */
    public function searchReplacement($contact = null, $keyword = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $contactSet = $Crm->ContactSet();
        $contact = $contactSet->get($contact);
        if (!isset($contact)) {
            throw new crm_Exception($Crm->translate('Trying to access a contact or with a wrong (unknown) id.'));
        }


        if (!isset($keyword)) {
            $keyword = $contact->getFullName();
            $keywords = array();
            foreach (explode(' ', $keyword) as $k) {
                if (mb_strlen($k) > 2) {
                    $keywords[] = $k;
                }
            }
        } else {
            $keywords = explode(' ', $keyword);
        }


        $page = $Ui->Page();
        $page->setMainPanelLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));

        $page->addClass('crm-page-editor');

        $page->addItem($W->Title($Crm->translate('Search replacement for contact'), 1));

        $page->addContextItem($Ui->ContactCardFrame($contact), $Crm->translate('Contact to replace'));


        $alreadyMatchedIds = array($contact->id => $contact->id);


        $editor = new crm_Editor($Crm, null, $W->VBoxLayout());
        $editor->colon();

        $editor->addItem($editor->labelledField($Crm->translate('Keywords'), $W->LineEdit()->setSize(80), 'keyword'));
        //		$editor->setCancelAction($Crm->Controller()->Deal()->cancel());
        $editor->setSaveAction($Crm->Controller()->Contact()->searchReplacement(), $Crm->translate('Search'));
        $editor->setReadOnly();
        $editor->setHiddenValue('tg', bab_rp('tg'));
        $editor->setHiddenValue('contact', $contact->id);
        $editor->setValue('keyword', implode(' ', $keywords));
        $page->addItem($editor);


        if (count($keywords) > 0) {

            $similarContactsSection = $W->Section(
                $Crm->translate('Similar contacts'),
                $W->VBoxLayout()
                ->setVerticalSpacing(1, 'em'),
                3
                );
            $page->addItem($similarContactsSection);

            $criteria = new ORM_FalseCriterion();
            foreach ($keywords as $k) {
                $criteria = $criteria->_OR_($contactSet->firstname->contains($k))->_OR_($contactSet->lastname->contains($k));
            }

            $contacts = $contactSet->select(
                $contactSet->id->notIn($alreadyMatchedIds)
                ->_AND_($criteria)
                );


            $similarContactsTable = $W->TableView();
            $similarContactsSection->addItem($similarContactsTable);

            $row = 0;
            foreach ($contacts as $newContact) {

                $similarContactsTable->addItem(
                    $W->FlowItems(
                        $Ui->ContactPhoto($newContact, 32, 32),
                        $W->Label($newContact->getFullName())
                        )->setHorizontalSpacing(1, 'em')->setVerticalAlign('middle'),
                    $row, 0
                    );
                $similarContactsTable->addItem(
                    $W->Link(
                        $W->Icon($Crm->translate('Replace with this contact'), Func_Icons::ACTIONS_ARROW_RIGHT),
                        $Crm->Controller()->Contact()->replace($contact->id, $newContact->id)
                        ),
                    $row, 1
                    );
                $alreadyMatchedIds[$contact->id] = $contact->id;

                $row++;
            }
        }

        return $page;
    }



    public function replace($oldContact, $newContact)
    {
        $Crm = $this->Crm();

        $contactSet = $Crm->ContactSet();
        /* @var $oldContact crm_Contact */
        $oldContact = $contactSet->get($oldContact);
        if (!isset($oldContact)) {
            throw new crm_Exception($Crm->translate('Trying to access a contact  with a wrong (unknown) id.'));
        }
        $newContact = $contactSet->get($newContact);
        if (!isset($newContact)) {
            throw new crm_Exception($Crm->translate('Trying to access a contact with a wrong (unknown) id.'));
        }

        $Access = $Crm->Access();
        if (!($Access->updateContact($oldContact) & $Access->updateContact($newContact))) {
            throw new crm_Exception($Crm->translate('You do not have access to this action.'));
        }

        $oldContact->replaceWith($newContact->id);

        $this->cancel();
    }



    /**
     * Deletes the specified contact and all related data.
     *
     * @param int	$contact
     * @return Widget_Action
     */
    public function delete($contact)
    {
        $Crm = $this->Crm();
        $this->deleteAction($contact);

        crm_redirect($this->proxy()->displayList(), $Crm->translate('The contact has been deleted.'));
    }



    protected function deleteAction($contact)
    {
        $Crm = $this->Crm();
        $set = $Crm->ContactSet();

        /* @var $organization crm_Organization */
        $contact = $set->get($contact);

        if (!isset($contact)) {
            throw new crm_AccessException($Crm->translate('Trying to access an contact with a wrong (unknown) id.'));
        }

        $Access = $Crm->Access();

        if (!$Access->deleteContact($contact)) {
            throw new crm_AccessException($Crm->translate('You do not have access to this action.'));
        }

        // try to delete contact in the newsletters webservice if possible
        $ML = crm_MailingList();
        if (false !== $ML)
        {
            try {
                $c = $ML->newContact();
                $c->setEmail($contact->email);
                $ML->removeContact($c);
            } catch (LibNewsletterException $e) {
                // if newsletter installed but not configured
                bab_debug($e->getMessage());
            }
        }



        $contact->delete();


    }



    /**
     * Send a password token to the contact
     * @param int $contact
     * @throws crm_AccessException
     */
    public function sendPasswordToken($contact = null)
    {
        $Crm = $this->Crm();
        $Access = $Crm->Access();
        $contact = $Crm->ContactSet()->get($contact);



        if (!$contact)
        {
            throw new crm_AccessException($Crm->translate('The contact does not exists'));
        }

        if (!$Access->ContactPasswordToken($contact))
        {
            throw new crm_AccessException($Crm->translate('Access denied to contact password token'));
        }

        if (empty($contact->email))
        {
            throw new crm_SaveException($Crm->translate('The contact does not contain an email'));
        }


        // il ne doit pas existe d'autre compte avec le meme identifiant/email
        if (!$contact->user)
        {
            require_once $GLOBALS['babInstallPath'].'utilit/loginIncl.php';

            if (null !== bab_getUserByNickname($contact->email))
            {
                throw new crm_SaveException(sprintf($Crm->translate('A user with the nickname %s allready exists'), $contact->email));
            }

            if (0 !== bab_getUserIdByEmail($contact->email))
            {
                throw new crm_SaveException(sprintf($Crm->translate('A user with same email allready exists'), $contact->email));
            }
        }

        $notif = $Crm->Notify()->lostPassword_sendToken($contact);
        if (false === $notif->send())
        {
            throw new crm_SaveException('Failed to process the request : '.strip_tags($notif->getError()));
        }

        return true;
    }


    /**
     * Delete current password token associated to the user
     * @param int $contact
     * @throws crm_AccessException
     */
    public function deletePasswordToken($contact = null)
    {
        $Crm = $this->Crm();
        $Access = $Crm->Access();
        $contact = $Crm->ContactSet()->get($contact);

        if (!$contact)
        {
            throw new crm_AccessException($Crm->translate('The contact does not exists'));
        }

        if (!$Access->ContactPasswordToken($contact))
        {
            throw new crm_AccessException($Crm->translate('Access denied to contact password token'));
        }

        $set = $Crm->PasswordTokenSet();
        $set->delete($set->contact->is($contact->id));

        return true;
    }



    /**
     * A display of subscriptions based on LibMailingList
     *
     * @param int $contact
     *
     * @return Widget_Item
     */
    public function newsletterSubscriptions($contact = null)
    {
        $ML = crm_MailingList();
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Access = $Crm->Access();

        if (!$Access->viewShopAdmin())
        {
            throw new crm_AccessException('Access denied');
        }

        $set = $Crm->ContactSet();

        $contact = $set->get($contact);

        if (null === $contact)
        {
            throw new crm_Exception('This contact does not exists');
        }

        $c = $ML->newContact();
        $c->setEmail($contact->email);
        $arr = $ML->getContactSubscriptions($c);

        bab_functionality::includeOriginal('Icons');

        $layout = $W->VBoxLayout()->setVerticalSpacing(.5,'em');
        $layout->addClass(Func_Icons::ICON_LEFT_16);

        if (empty($arr))
        {
            $layout->addItem($W->Icon($Crm->translate('No registration recorded for this contact'), Func_Icons::STATUS_DIALOG_INFORMATION));
            return $layout;
        }

        foreach($arr as $id_list => $name)
        {
            $menu = $W->Menu()->setLayout($W->FlowLayout())->addItem(
                $W->Link($W->Icon($Crm->translate('Unsubscribe'), Func_Icons::ACTIONS_EDIT_DELETE), $this->proxy()->newsletterUnsuscribe($contact->email, $id_list))
                ->setConfirmationMessage(sprintf($Crm->translate('Are you sure you want to unsubscribe %s from the list %s?'), $contact->email, $name))
                );


            $layout->addItem($W->FlowItems($W->Icon($name, Func_Icons::APPS_MAIL), $menu)->setVerticalAlign('middle'));
        }

        return $layout;
    }



    public function newsletterUnsuscribe($email, $id_list)
    {
        $ML = crm_MailingList();
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Access = $Crm->Access();

        if (!$Access->viewShopAdmin())
        {
            throw new crm_AccessException('Access denied');
        }

        try {
            $NL->unsubscribeContact($email, $id_list);
        } catch (LibNewsletterException $e)
        {
            throw new crm_SaveException($e->getMessage());
        }

        return true;
    }




    /**
     *
     * @param string $search
     *
     * @return void
     */
    public function suggest($search = null, $relatedOrganizations = null, $relatedOrganizationNames = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $Access = $Crm->Access();


        bab_requireCredential($Crm->translate('You must be logged'), 'Basic');

        $suggest = $Ui->SuggestContact();

        if (isset($relatedOrganizations)) {
            $relatedOrganizations = explode(',', $relatedOrganizations);
            $orgs = array();
            $organizationSet = $Crm->OrganizationSet();
            foreach ($relatedOrganizations as $relatedOrganizationId) {
                $relatedOrganization = $organizationSet->get($relatedOrganizationId);
                if ($relatedOrganization) {
                    $orgs[$relatedOrganizationId] = $relatedOrganization->name;
                }
            }
            $suggest->setRelatedOrganization($orgs);
        } elseif (isset($relatedOrganizationNames)) {
            $relatedOrganizationNames = explode(',', $relatedOrganizationNames);
            $orgs = array();
            $organizationSet = $Crm->OrganizationSet();
            foreach ($relatedOrganizationNames as $relatedOrganizationName) {
                $relatedOrganizationName = bab_getStringAccordingToDataBase($relatedOrganizationName, 'UTF-8');
                $relatedOrganization = $organizationSet->get($organizationSet->name->is($relatedOrganizationName));
                if ($relatedOrganization) {
                    $orgs[$relatedOrganization->id] = $relatedOrganization->name;
                }
            }
            $suggest->setRelatedOrganization($orgs);
        }


        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();

        die;
    }


    public function suggestCollaborator($search = null)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        bab_requireCredential($Crm->translate('You must be logged'), 'Basic');

        $suggest = $Ui->SuggestCollaborator();

        $suggest->setMetadata('suggestparam', 'search');
        $suggest->suggest();

        die;
    }


    /**
     * Does nothing and returns to the previous page.
     *
     * @param array	$message
     * @return Widget_Action
     */
    public function cancel($message = null)
    {
        crm_redirect(crm_BreadCrumbs::last(), $message);
    }



    /**
     * Displays help on this page.
     *
     * @return Widget_Action
     */
    public function help()
    {

    }
}
