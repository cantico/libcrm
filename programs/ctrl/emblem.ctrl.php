<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * This controller manages actions that can be performed on emblems.
 */
class crm_CtrlEmblem extends crm_Controller
{


	/**
	 * Deletes the specified emblem
	 *
	 * @param int	$emblem
	 * @return Widget_Action
	 */
	public function delete($emblem)
	{
		$set = $this->Crm()->EmblemSet();

		$set->delete($set->id->is($emblem));

		crm_redirect(crm_BreadCrumbs::last(), $this->Crm()->translate('The emblem has been deleted.'));
	}


	/**
	 * Unlinks the specified emblem from the specified object
	 *
	 * @param int		$emblem	The emblem id
	 * @param string	$from	The object string identifier (e.g. Contact:12)
	 * @return Widget_Action
	 */
	public function unlink($emblem, $from)
	{
		$Crm = $this->Crm();
		$emblem = $Crm->EmblemSet()->get($emblem);

		// $from contains a string like Contact:123 or Organization:654
		list($classname, $id) = explode(':', $from);
		$classSet = $classname . 'Set';
		$subject = $Crm->$classSet()->get($id);

		$emblem->unlinkFrom($subject);

		crm_redirect(crm_BreadCrumbs::last());
	}



	public function link($emblem = null, $to = null)
	{
		$Crm = $this->Crm();
		$emblemSet = $Crm->EmblemSet();

		/* @var $newEmblem crm_Emblem */
		$newEmblem = $emblemSet->get($emblemSet->label->is($emblem['label']));

		if (!isset($newEmblem)) {
			if (trim($emblem['label']) === '') {
				crm_redirect(crm_BreadCrumbs::last());
			}
			$newEmblem = $emblemSet->newRecord();
			$newEmblem->label = $emblem['label'];
			$newEmblem->save();
		}

		// $from contains a string like Contact:123 or Organization:654
		list($classname, $id) = explode(':', $to);
		$classSet = $classname . 'Set';
		$subject = $Crm->$classSet()->get($id);

		if (!$newEmblem->isLinkedTo($subject)) {
			$newEmblem->linkTo($subject);
		}

		crm_redirect(crm_BreadCrumbs::last());
	}



	/**
	 *
	 * @param string $search
	 *
	 * @return void
	 */
	public function suggest($search = null)
	{
	    $Crm = $this->Crm();
	    $Ui = $Crm->Ui();

	    $suggestEmblem = $Ui->SuggestEmblem();
	    $suggestEmblem->setMetadata('suggestparam', 'search');
	    $suggestEmblem->suggest();

	    die;
	}
}
