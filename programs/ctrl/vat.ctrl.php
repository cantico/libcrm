<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * This controller manages actions that can be performed on vat rates.
 */
class crm_CtrlVat extends crm_Controller implements crm_ShopAdminCtrl
{
	
	/**
	 * @return Widget_Action
	 */
	public function displayList($vat = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList($vat), $Crm->translate('VAT rates'));
		
		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-list');
		
		
		$page->setTitle($Crm->translate('VAT rates configuration'));
		
		
		$filter = isset($vat['filter']) ? $vat['filter'] : array();
		
		$set = $Crm->VatSet();

		$table = $Crm->Ui()->VatTableView();
		
		$table->addDefaultColumns($set);
		
		
		
		$criteria = $table->getFilterCriteria($filter);
		$table->setDataSource($set->select($criteria));
		
		$table->addClass(Func_Icons::ICON_LEFT_16);
		
		$filterPanel = $table->filterPanel($filter, 'vat');
		$page->addItem($filterPanel);
		
		$toolbar = new crm_Toolbar();
		$toolbar->addButton($Crm->translate('Add a VAT rate'), Func_Icons::ACTIONS_LIST_ADD, $this->proxy()->edit());
		
		$page->addToolbar($toolbar);
		
		return $page;
	}
	
	/**
	 * @return Widget_Action
	 */
	public function display()
	{
		// Not used
	}
	
	/**
	 * @return Widget_Action
	 */
	public function edit($vat = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit($vat), $Crm->translate('Edit VAT rate'));
		
		/* @var $Ui crm_Ui */
		$Ui = $Crm->Ui();
		$page = $Ui->Page();
		
		$page->addClass('crm-page-editor');
		$page->setTitle($Crm->translate('Edit VAT rate'));
		
		if (null !== $vat)
		{
			$set = $Crm->VatSet();
			$vat = $set->get($vat);
		
		}
		
		
		
		$form = $Ui->VatEditor($vat);
		
		$page->addItem($form);
		
		if ($vat instanceof crm_Vat)
		{
			$actionsFrame = $page->ActionsFrame();
			$page->addContextItem($actionsFrame);
			
			$actionsFrame->addItem($W->Link($W->Icon($Crm->translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE), $this->proxy()->delete($vat->id)));
		}
		
		return $page;
	}
	
	/**
	 * @return Widget_Action
	 */
	public function save($vat = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		$set = $Crm->VatSet();
		
		if (empty($vat['id']))
		{
			$record = $set->newRecord();
		} else {
			$record = $set->get($vat['id']);
			if (null == $record)
			{
				throw new crm_AccessException($Crm->translate('This VAT rate does not exists'));
			}
		}
		
		$record->setValues($vat);
		
		$record->save();
		
		return true;
	}
	
	
	
	public function delete($vat = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		if (!$vat)
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$set = $Crm->VatSet();
		$set->delete($set->id->is($vat));
		
		crm_redirect($this->proxy()->displayList());
	}
	
	
	
	
	public function setDefault($vat = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		
		
		$set = $Crm->VatSet();
		
		foreach($set->select($set->default->is(1)) as $other)
		{
			$other->default = 0;
			$other->save();
		}
		
		
		$vat = $set->get($vat);
		
		if (!$vat)
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$vat->default = 1;
		$vat->save();
		
		crm_redirect($this->proxy()->displayList());
	}
}
