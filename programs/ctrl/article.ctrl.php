<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * This controller manages actions that can be performed on articles.
 */
class crm_CtrlArticle extends crm_Controller implements crm_ShopAdminCtrl
{
    /**
     *
     * @param array $filter
     * @return crm_ArticleTableView
     */
    protected function articleTableModelView($filter)
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $set = $this->Crm()->ArticleSet();
        $set->joinHasOneRelations();
        $set->mainpackaging->packaging();

        $table = $Ui->ArticleTableView();
        $table->addDefaultColumns($set);

        $criteria = $table->getFilterCriteria($filter);

        if (!empty($filter['catalog']))
        {
            $catalogItemSet = $Crm->CatalogItemSet();
            $criteria = $criteria->_AND_($set->id->in($catalogItemSet->catalog->is($filter['catalog'])));
        }


        $articleSelection = $set->select($criteria);

        $table->setDataSource($articleSelection);

        return $table;
    }


    /**
     * Display the list of article for administration purpose
     * @return Widget_Action
     */
    public function displayList($articles = null)
    {
        $Crm = $this->Crm();
        $access = $Crm->Access();

        if (!$access->listArticles())
        {
            $message = $Crm->translate('Access denied to online shop administration');
            throw new crm_AccessException($message);
        }

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList($articles), $Crm->translate('Articles'));

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-list');


        $page->setTitle($Crm->translate('Articles'));


        $filter = isset($articles['filter']) ? $articles['filter'] : array();

        $table = $this->articleTableModelView($filter);
        $table->addClass(Func_Icons::ICON_LEFT_16);

        $filterPanel = $table->filterPanel($filter, 'articles');
        $page->addItem($filterPanel);

        $toolbar = new crm_Toolbar();
        $toolbar->addButton($Crm->translate('Add an article'), Func_Icons::ACTIONS_LIST_ADD, $this->proxy()->edit());
        $toolbar->addButton($Crm->translate('Export in CSV'), Func_Icons::ACTIONS_ARROW_DOWN, $this->proxy()->exportList($articles));

        if ($access->exportArticlesPhotos())
        {
            $toolbar->addButton($Crm->translate('Download photos'), Func_Icons::ACTIONS_ARCHIVE_EXTRACT, $this->proxy()->exportPhotos());
        }

        if ($access->importArticles())
        {
            $toolbar->addButton($Crm->translate('Import'), Func_Icons::ACTIONS_DOCUMENT_UPLOAD, $this->proxy()->importArticles());
        }

        if ($access->importArticlesPhotos())
        {
            $toolbar->addButton($Crm->translate('Upload photos'), Func_Icons::MIMETYPES_IMAGE_X_GENERIC, $this->proxy()->importPhotos());
        }
        $page->addToolbar($toolbar);

        return $page;
    }


    /**
     * @param array	 $articles		List parameters
     *
     * @return Widget_Action
     */
    public function exportList($articles = null)
    {
        $Crm = $this->Crm();
        $Access = $Crm->Access();

        if (!$Access->viewShopAdmin())
        {
            $message = $Crm->translate('Access denied');
            throw new crm_AccessException($message);
        }

        $filter = isset($articles['filter']) ? $articles['filter'] : array();

        $tableView = $this->articleTableModelView($filter);
        $tableView->downloadCsv('export.csv');
    }


    /**
     * Display one article for administration purpose
     * @return Widget_Action
     */
    public function display($article = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $set = $Crm->ArticleSet();
        $set->joinHasOneRelations();

        $article = $set->get($article);

        if (null == $article)
        {
            $message = $Crm->translate('Article not found');
            throw new crm_AccessException($message);
        }

        $access = $Crm->Access();
        if (!$access->viewArticle($article)) {
            $message = $Crm->translate('Access denied');
            throw new crm_AccessException($message);
        }

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->display($article->id), $article->name);

        $Ui = $Crm->Ui();
        $page = $Ui->Page();


        $page->setTitle($article->name);

        $articleDisplay = $Ui->ArticleDisplay($article);


        $fullFrame = $articleDisplay->getFullFrame();
        $page->addItem($fullFrame);


        // add note

        $addNote = $W->VboxItems(
                        $W->Link($Crm->translate('Add a note'), $Crm->Controller()->Note()->edit(null, $article->getRef()))
                        ->addClass('widget-instant-button widget-actionbutton'),
                        $Crm->Ui()->AddNoteEditor($article, 'Article')
                        ->setTitle($Crm->translate('Add a note'))
                        ->addClass('widget-instant-form')
                )->addClass('widget-instant-container');

        // Recent history
        $historyFrame = $W->Section(
                        $Crm->translate('History'),
                        $Crm->Ui()->History($article),
                        2
                )->setFoldable(true)
                ->addClass('compact');

        $page->addItem($W->VBoxItems($addNote, $historyFrame)->addClass('crm-detailed-info-frame'));


        $actionsFrame = $page->ActionsFrame();
        $page->addContextItem($actionsFrame);

        $actions = $this->getActionFrame($article);
        foreach($actions as $action){
            $actionsFrame->addItem($action);
        }

        return $page;
    }


    /**
     * @param crm_Article $article
     */
    protected function getActionFrame(crm_Article $article)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Access = $Crm->Access();

        $returnObjets = array();

        if ($Access->updateArticle($article))
        {
            $returnObjets[] = $W->Link($W->Icon($Crm->translate('Edit article'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $Crm->Controller()->Article()->edit($article->id));
        }

        if ($Access->createArticle())
        {
            $returnObjets[] = $W->Link($W->Icon($Crm->translate('Copy article'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $Crm->Controller()->Article()->copy($article->id));
        }

        if ($Access->classifyArticle($article))
        {
            $returnObjets[] = $W->Link($W->Icon($Crm->translate('Set article categories'), Func_Icons::OBJECTS_PUBLICATION_CATEGORY), $Crm->Controller()->Article()->classify($article->id));
        }

        if(isset($Crm->ArticleLink))
        {
            $returnObjets[] = $W->Link($W->Icon($Crm->translate('Up-sell') , Func_Icons::ACTIONS_VIEW_LIST_DETAILS), $Crm->Controller()->Article()->linkArticle($article->id, 'I'));
            $returnObjets[] = $W->Link($W->Icon($Crm->translate('Cross-sell') , Func_Icons::ACTIONS_VIEW_LIST_DETAILS), $Crm->Controller()->Article()->linkArticle($article->id, 'C'));
            $returnObjets[] = $W->Link($W->Icon($Crm->translate('Related product') , Func_Icons::ACTIONS_VIEW_LIST_DETAILS), $Crm->Controller()->Article()->linkArticle($article->id, 'A'));
        }

        if ($Access->deleteArticle($article))
        {
            $returnObjets[] = $W->Link($W->Icon($Crm->translate('Delete article'), Func_Icons::ACTIONS_EDIT_DELETE), $Crm->Controller()->Article()->delete($article->id))->setConfirmationMessage($Crm->translate('Do you really want to delete the article?'));
        }

        if ($catalogItem = $article->getAccessibleCatalogItem())
        {
            /*@var $catalogItem crm_CatalogItem */
            $returnObjets[] = $W->Link($W->Icon($Crm->translate('View online'), Func_Icons::ACTIONS_GO_HOME), $catalogItem->getRewritenUrl());
        }
        return $returnObjets;
    }

    /*AJAX*/
    public function getToggleLink($type, $article_from, $article_to, $id)
    {
        bab_functionality::get('Icons');
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $set = $Crm->ArticleLinkSet();
        $access = $Crm->Access();
        if (!$access->viewShopAdmin())
        {
            $message = $Crm->translate('Access denied to online shop administration');
            throw new crm_AccessException($message);
        }

        $set = $Crm->ArticleLinkSet();
        $articleLink = $set->get(
            $set->article_from->is($article_from)
            ->_AND_($set->article_to->is($article_to))
            ->_AND_($set->type->is($type))
        );

        if($articleLink){
            $title = crm_translate('Remove the link to this article.');
            $icon = Func_Icons::ACTIONS_LIST_REMOVE;
        }else{
            $icon = Func_Icons::ACTIONS_LIST_ADD;
            $title = crm_translate('Add a link to this article.');
        }
        $link = $W->Link(
            $W->Icon(
                '',
                $icon
            ),
            $Crm->Controller()->Article()->toggleLink($type, $article_from, $article_to),
            $id
        )->setTitle($title);

         return $link->setAjaxAction($Crm->Controller()->Article()->toggleLink($type, $article_from, $article_to), array('linked-list-'.$article_to, 'linked-article-'.$article_to, 'linked-article') );
    }

    public function toggleLink($type, $article_from, $article_to)
    {
        $Crm = $this->Crm();
        $set = $Crm->ArticleLinkSet();
        $access = $Crm->Access();

        if (!$access->viewShopAdmin())
        {
            throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
        }

        $set = $Crm->ArticleLinkSet();
        $articleLink = $set->get(
            $set->article_from->is($article_from)
            ->_AND_($set->article_to->is($article_to))
            ->_AND_($set->type->is($type))
        );
        if($articleLink){
            $articleLink->delete(
                $set->article_from->is($article_from)
                ->_AND_($set->article_to->is($article_to))
                ->_AND_($set->type->is($type))
            );
            echo '1';
        }else{
            $record = $set->newRecord();
            $record->article_from = $article_from;
            $record->article_to = $article_to;
            $record->type = $type;
            $record->save();
            echo '2';
        }

        die;
    }



    public function linkArticle($article, $linkType, $articles = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $access = $Crm->Access();

        if (!$access->viewShopAdmin())
        {
            $message = $Crm->translate('Access denied to online shop administration');
            throw new crm_AccessException($message);
        }

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-list');

        switch($linkType){
            case 'I':
                $title = crm_translate('Up-sell');
                break;
            case 'C':
                $title = crm_translate('Cross-sell');
                break;
            case 'A':
                $title = crm_translate('Related product');
                break;
        }

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList($articles), $title);
        $page->setTitle($title);


        $filter = isset($articles['filter']) ? $articles['filter'] : array();


        $set = $this->Crm()->ArticleSet();

        if (isset($Crm->ArticleType))
        {
            $Crm->includeArticleTypeSet();
            $set->articletype();
        }

        if (isset($set->mainpackaging))
        {
            $Crm->includeArticlePackagingSet();
            $Crm->includePackagingSet();
            $set->mainpackaging();
            $set->mainpackaging->packaging();
        }

        if (isset($set->vat))
        {
            $Crm->includeVatSet();
            $set->vat();
        }

        if (isset($Crm->ArticleAvailability))
        {
            $Crm->includeArticleAvailabilitySet();
            $set->articleavailability();
        }

        $table = $Crm->Ui()->ArticleLinkTableView();


        $table->addDefaultColumns($set);
        $table->addColumn(widget_TableModelViewColumn('__action__', '')->setSortable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));

        $criteria = $table->getFilterCriteria($filter, $set);

        if (!empty($filter['catalog']))
        {
            $catalogItemSet = $Crm->CatalogItemSet();
            $criteria = $criteria->_AND_($set->id->in($catalogItemSet->catalog->is($filter['catalog'])));
        }


        $articleSelection = $set->select($criteria->_AND_($set->id->notIn($article)));

        $table->setDataSource($articleSelection);

        $table->addClass('icon-left-16 icon-left icon-16x16');

        $filterPanel = $table->filterPanel($filter, 'articles');
        $page->addItem($filterPanel);

        $page->addContextItem(
            $W->Section(
                $title,
                $W->DelayedItem($Crm->Controller()->Article()->getLinkedArticle($linkType, $article))
                    ->setPlaceHolderItem($Crm->Controller()->Article(false)->getLinkedArticle($linkType, $article))
                    ->setUpdateMode(Widget_DelayedItem::UPDATE_MODE_NONE)->setSizePolicy(Widget_SizePolicy::MINIMUM)
            )->addClass(Func_Icons::ICON_LEFT_16)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
        );
        return $page;

    }

    /*AJAX*/
    public function getLinkedArticle($linkType, $article){
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $Access = $Crm->Access();

        if (!$Access->viewShopAdmin())
        {
            throw new crm_AccessException('Access denied');
        }

        $listLinked = $W->VBoxLayout('linked-article');

        $linkSet = $this->Crm()->ArticleLinkSet();
        $linkSet->article_to();
        $links = $linkSet->select($linkSet->article_from->is($article)->_AND_($linkSet->type->is($linkType)));
        $links->orderAsc($linkSet->article_to->name);

        foreach($links as $link){
            $listLinked->addItem(
                $W->HBoxItems(
                    $W->Label($link->article_to->reference . ' ' . $link->article_to->name)->setSizePolicy(Widget_SizePolicy::MAXIMUM),
                    $W->DelayedItem($Crm->Controller()->Article()->getToggleLink($linkType, $article, $link->article_to->id, 'linked-article-'.$link->article_to->id))
                        ->setPlaceHolderItem($Crm->Controller()->Article(false)->getToggleLink($linkType, $article, $link->article_to->id, 'linked-article-'.$link->article_to->id))
                        ->setUpdateMode(Widget_DelayedItem::UPDATE_MODE_NONE)->setSizePolicy(Widget_SizePolicy::MINIMUM)
                )->setSizePolicy(Widget_SizePolicy::MAXIMUM)
            );
        }

        return $listLinked;
    }

    /**
     *
     * @param int $article
     *
     * @return Widget_Action
     */
    public function edit($article = null)
    {
        $Crm = $this->Crm();
        $access = $Crm->Access();



        if (null === $article) {

            if (!$access->createArticle()) {
                $message = $Crm->translate('Access denied');
                throw new crm_AccessException($message);
            }

            crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit($article), $Crm->translate('Create article'));
        } else {

            $set = $Crm->ArticleSet();
            $articleRecord = $set->get($article);

            if (!$access->updateArticle($articleRecord))
            {
                $message = $Crm->translate('Access denied');
                throw new crm_AccessException($message);
            }

            crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit($article), $Crm->translate('Edit article'));
        }

        /* @var $Ui crm_Ui */
        $Ui = $Crm->Ui();
        $page = $Ui->Page();

        $page->addClass('crm-page-editor');
        $page->setTitle($Crm->translate('Edit article'));

        if (null !== $article)
        {
            $form = $Ui->ArticleEditor($articleRecord);

        } else {

            $form = $Ui->ArticleCreateEditor($article);
        }





        $page->addItem($form);

        $page->addContextItem($form->MainPhoto());
        $page->addContextItem($form->OthersPhotos(), $Crm->translate('Product image gallery'));

        return $page;
    }




    public function copy($article = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $access = $Crm->Access();

        if (!$access->createArticle()) {
            $message = $Crm->translate('Access denied');
            throw new crm_AccessException($message);
        }

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit($article), $Crm->translate('Copy article'));

        $set = $Crm->ArticleSet();
        $articleRecord = $set->get($article);

        if (!isset($articleRecord)) {
            throw new crm_AccessException('Missing article origin');
        }


        /* @var $Ui crm_Ui */
        $Ui = $Crm->Ui();
        $page = $Ui->Page();

        $page->addClass('crm-page-editor');
        $page->setTitle(sprintf($Crm->translate('Create a copy of "%s"'), $articleRecord->name));


        $page->addItem($W->Title(sprintf($Crm->translate('Create a copy of "%s"'), $articleRecord->name)));

        $form = $Ui->ArticleCreateEditor($articleRecord);

        $page->addItem($form);


        return $page;
    }



    protected function createNewArticle(crm_ArticleSet $set, Array $article)
    {
        $Crm = $this->Crm();

        if (!empty($article['from'])) {
            $origin = $set->get($article['from']);
            $articleRecord = $set->get($article['from']);

            if (!isset($origin)) {
                throw new crm_SaveException('Missing origin');
            }

            $articleRecord->id = null;
            $articleRecord->setFormInputValues($article);

            if (!$articleRecord->name) {
                $message = $Crm->translate('The article name is mandatory');
                throw new crm_SaveException($message);
            }

            $articleRecord->save();

            // copy linked elements
            $origin->copyLinkedObjects($articleRecord);

            return $articleRecord;

        }


        $articleRecord = $set->newRecord();

        $vatSet = $Crm->VatSet();
        $vat = $vatSet->get($vatSet->default->is(1));
        if (isset($vat))
        {
            $articleRecord->vat = $vat->id;
        }

        $articleRecord->setFormInputValues($article);
        $articleRecord->save();

        return $articleRecord;
    }


    /**
     * Save article record creation/copy/update
     *
     * @param array $article
     * @throws crm_SaveException
     * @throws crm_AccessException
     *
     * @return crm_Article
     */
    protected function saveRecord(crm_ArticleSet $set, Array $article)
    {
        $Crm = $this->Crm();
        $access = $Crm->Access();

        if (empty($article['id']) && !$access->createArticle()) {
            $message = $Crm->translate('Access denied');
            throw new crm_AccessException($message);
        }

        if (empty($article['id'])) {

            return $this->createNewArticle($set, $article);
        }

        $articleRecord = $set->get($article['id']);

        if (!$access->updateArticle($articleRecord)) {
            $message = $Crm->translate('Access denied');
            throw new crm_AccessException($message);
        }

        /*@var $articleRecord crm_Article */
        $articleRecord->setFormInputValues($article);


        $articleRecord->save();

        return $articleRecord;
    }

    /**
     * attach main photo and others photos to articles for article creation
     * @param crm_Article $articleRecord
     */
    protected function saveAttachments(crm_Article $articleRecord)
    {
        if ($articleRecord->id) {
            return;
        }

        $W = bab_Widgets();

        $res = $W->ImagePicker()->getTemporaryFiles('mainphoto');
        if (isset($res))
        {
            foreach($res as $filePickerItem)
            {
                /*@var $filePickerItem Widget_FilePickerItem */
                $articleRecord->importPhoto($filePickerItem->getFilePath());
            }
        }

        $othersphoto = $W->ImagePicker()->getTemporaryFiles('othersphoto');
        if (isset($othersphoto))
        {
            $articleRecord->importOthersPhotos($othersphoto);
        }
    }



    /**
     * Save article
     * @param array $article
     *
     * @return bool
     */
    public function save($article = null)
    {
        $Crm = $this->Crm();

        $articleRecord = $this->saveRecord($Crm->ArticleSet(), $article);

        if (isset($article['articlePackaging']))
        {
            $this->saveArticlePackagings($articleRecord, $article['articlePackaging']);
        }

        $this->saveAttachments($articleRecord);
        $Crm->CatalogSet()->updateRefCount();

        if (empty($article['id']))
        {
            // article creation
            crm_redirect($this->proxy()->edit($articleRecord->id));
        }

        return true;
    }


    /**
     *
     * @param crm_Article $article
     * @param array $articlePackaging
     */
    protected function saveArticlePackagings(crm_Article $article, array $articlePackaging)
    {
        $Crm = $this->Crm();
        $Crm->includeVatSet();
        $Access = $Crm->Access();
        $set = $Crm->ArticlePackagingSet();

        $vat = $article->vat();

        if (!isset($vat) && 0 < count($articlePackaging))
        {
            $message = $Crm->translate('The VAT rate is mandatory');
            throw new crm_SaveException($message);
        }

        if (isset($articlePackaging['main']) && $articlePackaging['main'] > 0)
        {
            $main = $articlePackaging['main'];
            unset($articlePackaging['main']);
        } else {
            $main = key($articlePackaging);
        }


        $addonname = $Crm->getAddonName();

        $registry = bab_getRegistryInstance();
        $registry->changeDirectory("/$addonname/configuration/");

        $excltaxpriority = $registry->getValue('excltaxpriority', false);



        foreach($articlePackaging as $id_artpack => $arr)
        {
            // receive a 2 digits value, save a 3 digit value if necessary
            // unit_cost key : incl tax
            // unit_cost_ti key : incl tax


            if (false === $excltaxpriority)
            {
                $arr['unit_cost'] = ($set->unit_cost->input($arr['unit_cost_ti']) / (1 + ($vat->value / 100) ));
                $arr['unit_cost'] = round($arr['unit_cost'], 3);
            }


            if ($id_artpack > 0)
            {
                $articlePackaging = $set->get($id_artpack);


                if (empty($arr['packaging']) || empty($arr['unit_cost']))
                {
                    $set->delete($set->id->is($id_artpack));

                } else {

                    $articlePackaging->setValues($arr);
                    $articlePackaging->article = $article->id;
                    $articlePackaging->main = $main == $id_artpack ? '1' : '0';
                    $articlePackaging->save();
                }

            } else if (!empty($arr['packaging']) && !empty($arr['unit_cost'])) {

                $articlePackaging = $set->newRecord();
                $articlePackaging->setValues($arr);
                $articlePackaging->article = $article->id;
                $articlePackaging->main = $main <= 0 ? '1' : '0';
                $articlePackaging->save();
            }
        }

    }


    public function removeArticlePackaging($articlePackaging = null)
    {
        $Crm = $this->Crm();
        $Access = $Crm->Access();
        $set = $Crm->ArticlePackagingSet();
        $set->article();

        $articlePackaging = $set->get($articlePackaging);

        if (null === $articlePackaging)
        {
            throw new crm_AccessException('articlePackaging not found');
        }


        if (!$Access->updateArticle($articlePackaging->article))
        {
            throw new crm_AccessException('Access denied to article modification');
        }

        $set->delete($set->id->is($articlePackaging->id));

        return true;
    }




    /**
     * Display the form to select the article catalogs (create catalog Items)
     *
     * @param int		$article		The article id
     * @return Widget_Action
     */
    public function classify($article = null)
    {

        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->classify($article), $Crm->translate('Classify article'));

        $articleSet = $Crm->ArticleSet();
        $article = $articleSet->get($article);
        if (!isset($article)) {
            throw new crm_AccessException('Trying to access an article with a wrong (unknown) id.');
        }

        if (!$Crm->Access()->updateArticle($article)) {
            $message = $Crm->translate('Access denied');
            throw new crm_AccessException($message);
        }

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->setTitle($Crm->translate('Classify article'));
        $page->addItem($W->Title($Crm->translate('Edit article categories'), 1));



        $form = $Ui->ArticleClassifyEditor($article);

        $page->addItem($form);




        return $page;
    }



    /**
     * Saves the article classification into catalogs
     *
     * @param array	$articleClassify	array
     * @return Widget_Action
     */
    public function saveClassification($articleClassify = null)
    {
        $Crm = $this->Crm();

        $articleSet = $Crm->ArticleSet();
        $article = $articleSet->get($articleClassify['article']);

        if (!isset($article)) {
            throw new crm_AccessException('Trying to access an article with a wrong (unknown) id.');
        }

        if (!$Crm->Access()->updateArticle($article)) {
            $message = $Crm->translate('Access denied');
            throw new crm_AccessException($message);
        }

        // modify catalogItems

        $new = $articleClassify['catalogItem'];

        if (empty($new))
        {
            $message = $Crm->translate('The article must be in one category at least');
            throw new crm_SaveException($message);
        }


        $set = $Crm->CatalogItemSet();
        $res = $set->select($set->article->is($article->id));

        foreach ($res as $catalogItem)
        {
            if (!isset($new[$catalogItem->catalog]))
            {
                $set->delete($set->id->is($catalogItem->id));
            } else {
                unset($new[$catalogItem->catalog]);
            }
        }

        foreach ($new as $id_catalog => $dummy)
        {
            $catalogItem = $set->newRecord();
            $catalogItem->article = $article->id;
            $catalogItem->catalog = $id_catalog;
            $catalogItem->save();
        }

        $catalogSet = $Crm->CatalogSet();
        $refcountModified = $catalogSet->updateRefCount();


        // refcount has been modified, il the tree in sitemap use only populated categories, the sitemap need to be refreshed

        if ($refcountModified && !$catalogSet->displayEmpty())
        {
            bab_siteMap::clearAll();
        }

        return true;
    }





    /**
     * Download article attachment
     * @param int $article
     * @param string $filename
     * @param string $relativePath
     * @return Widget_Action
     */
    public function downloadAttachment($article = null, $filename = null, $relativePath = null)
    {
        $Crm = $this->Crm();
        $access = $Crm->Access();

        $article = $this->Crm()->ArticleSet()->get($article);
        if (!isset($article)) {
            return $this->Crm()->translate('The specified article does not seem to exist.');
        }

        if (!$access->readArticle($article))
        {
            $message = $Crm->translate('Access denied');
            throw new crm_AccessException($message);
        }

        $folder = $article->uploadPath();
        if (isset($relativePath)) {
            $relativePath = new bab_Path($relativePath);
            $folder->push($relativePath);
        }


        $file = bab_Widgets()->FilePicker()->getByName($folder, $filename);
        if (!isset($file)) {
            return $this->Crm()->translate('The file you requested does not seem to exist.');
        }
        $file->download();
    }



    /**
     * Download article attachements as zip archive
     * @param 	int	$article
     * @return Widget_Action
     */
    public function downloadAttachments($article = null)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
        $Crm = $this->Crm();
        $access = $Crm->Access();

        /* @var $article crm_Article */
        $article = $this->Crm()->ArticleSet()->get($article);
        if (!isset($article)) {
            return $this->Crm()->translate('The specified article does not seem to exist.');
        }


        if (!$access->readArticle($article))
        {
            $message = $Crm->translate('Access denied');
            throw new crm_AccessException($message);
        }

        $attachments = $article->attachments();

        if (null === $attachments) {
            return $this->Crm()->translate('The specified article have no attachments.');
        }

        $attachments->rewind();


        if (!$attachments->valid()) {
            return $this->Crm()->translate('The specified article have no attachments.');
        }

        /* @var $zip Func_Archive_Zip */
        $zip = bab_functionality::get('Archive/Zip');

        $path = new bab_Path(bab_getAddonInfosInstance('LibCrm')->getUploadPath());
        $path->push(session_id());
        $path->createDir();
        $path->push($article->getFilename().'.zip');

        $zip->open($path->toString());
        $this->addToZip($zip, $attachments);
        $zip->close();

        bab_downloadFile($path, null, true, false);

        $path->pop();
        $path->deleteDir();
        die();
    }


    private function addToZip(Func_Archive_Zip $zip, Widget_FilePickerIterator $attachments, $relativePath = '')
    {
        foreach($attachments as $file) {
            /* @var $file Widget_FilePickerItem */

            if ($file->isDir())
            {
                $path = $file->getFilePath();
                $this->addToZip($zip, new Widget_FilePickerIterator($path), $relativePath.$file->toString().'/');

            } else {
                $zip->addFile($file->getFilePath()->toString(), $relativePath.$file->toString());
            }
        }
    }




    public function delete($article)
    {
        $Crm = $this->Crm();
        $access = $Crm->Access();

        $set = $this->Crm()->ArticleSet();
        $articleRecord = $set->get($article);

        if (!$articleRecord)
        {
            $message = $Crm->translate('This article does not exists');
            throw new crm_Exception($message);
        }

        if (!$access->deleteArticle($articleRecord))
        {
            $message = $Crm->translate('Access denied');
            throw new crm_AccessException($message);
        }



        $set->delete($set->id->is($article));

        crm_redirect($this->proxy()->displayList());
    }




    /**
     * Disable or enable a product by ajax
     * @param int $article
     *
     */
    public function disable_enable_button($article)
    {
        $Crm = $this->Crm();
        $access = $Crm->Access();
        $W = bab_Widgets();

        $set = $Crm->ArticleSet();
        $articleRecord = $set->get($article);

        if (!$articleRecord)
        {
            $message = $Crm->translate('This article does not exists');
            throw new crm_Exception($message);
        }

        if (!$access->viewArticle($articleRecord))
        {
            $message = $Crm->translate('Access denied');
            throw new crm_AccessException($message);
        }

        bab_functionality::includefile('Icons');


        if ($articleRecord->disabled)
        {
            $text = $Crm->translate('Enable');
            // $icon = Func_Icons::ACTIONS_DIALOG_OK;
        } else {
            $text = $Crm->translate('Disable');
            // $icon = Func_Icons::ACTIONS_DIALOG_CANCEL;
        }


        $link = $W->Link(
            $text,
            $this->proxy()->disable_enable($articleRecord->id)
        );

        $link->setAjaxAction(
            $this->proxy()->disable_enable($articleRecord->id),
            $link
        )->setToggleClass('disabled', 'widget-table-row');

        return $link;
    }



    /**
     * Disable or enable a product by ajax
     * @param int $article
     */
    public function disable_enable($article)
    {
        $Crm = $this->Crm();
        $access = $Crm->Access();

        $set = $Crm->ArticleSet();
        $articleRecord = $set->get($article);

        if (!$articleRecord)
        {
            $message = $Crm->translate('This article does not exists');
            throw new crm_Exception($message);
        }

        if (!$access->viewArticle($articleRecord))
        {
            $message = $Crm->translate('Access denied');
            throw new crm_AccessException($message);
        }


        if ($articleRecord->disabled)
        {
            $articleRecord->disabled = 0;
            $articleRecord->save();
        } else {
            $articleRecord->disabled = 1;
            $articleRecord->save();
        }


        die();
    }



    /**
     * Export articles images in a zip archive
     *
     */
    public function exportPhotos()
    {
        $Crm = $this->Crm();
        $access = $Crm->Access();

        if (!$access->exportArticlesPhotos())
        {
            $message = $Crm->translate('Access denied');
            throw new crm_AccessException($message);
        }

        $filecount = 0;
        $addon = bab_getAddonInfosInstance($Crm->getAddonName());

        $zipfilename = new bab_Path($addon->getUploadPath(), 'tmp');
        $zipfilename->createDir();
        $zipfilename->push(session_id().'.zip');

        $zip = bab_functionality::get('Archive/Zip');
        $zip->open($zipfilename->tostring());

        $set = $Crm->ArticleSet();
        $res = $set->select();
        foreach($res as $article)
        {
            if ($file = $article->getPhotoFile())
            {
                $ext = (string) strrchr($file->getFileName(), '.');
                $zip->addFile($file->getFilePath()->tostring(), $article->reference.$ext);
                $filecount++;
            }

            if ($otherphotos = $article->getOthersPhotosIterator())
            {

                $i = 0;
                $used_names = array();
                foreach($otherphotos as $file)
                {
                    $firstdot = mb_strpos($file->toString(), '.');
                    if (false === $firstdot || $firstdot <= 1)
                    {
                        $name = 'im'.$i;
                    } else {
                        $name = mb_substr($file->toString(), 0, $firstdot);
                    }

                    while (isset($used_names[$name])) {
                        $i++;
                        $name = 'im'.$i;
                    }

                    $used_names[$name] = 1;

                    $ext = (string) strrchr($file->toString(), '.');
                    $zip->addFile($file->getFilePath()->tostring(), $article->reference.'.'.$name.$ext);
                    $filecount++;
                    $i++;
                }
            }
        }

        $zip->close();


        if (0 == $filecount)
        {
            @unlink($zipfilename->tostring());
            $message = $Crm->translate('No photos to download');
            throw new crm_AccessException($message);
        }

        bab_downloadFile($zipfilename, 'photos-'.date('d-m-Y').'.zip', false, false);

        unlink($zipfilename->tostring());
    }


    public function importArticles() {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->importArticles(), $Crm->translate('Import articles'));


        if (!$Crm->Access()->importArticles()) {
            $message = $Crm->translate('Access denied');
            throw new crm_AccessException($message);
        }

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->setTitle($Crm->translate('Import articles'));
        $page->addItem($W->Title($Crm->translate('Import articles'), 1));



        $form = $Ui->ArticleImportEditor();
        if ($form->getIterator())
        {
            /* Create a new record in table Import (crm_Import extends crm_TraceableRecord) */
            $importRecord = $Crm->ImportSet()->newRecord();
            $importRecord->objecttype = crm_Import::TYPE_ARTICLE;
            $importRecord->save();

            $frame = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'))->addClass('crm-editor');
            $page->addItem($frame);

            /* $progressAction : object Widget_Action */
            $progressAction = $this->proxy()->importArticleProgress($importRecord->id);
            $form->setActionParameters($progressAction); /* Add all parameters required to import a csv file on $progressAction */

            $nextButton = $W->Button()->addItem($W->Label($Crm->translate('Finish')));

            /* Create a progress bar : of importContactsProgress() to notes (historic of import) */
            $progressBar = $W->ProgressBar()
            ->setProgress(0)
            ->setTitle($Crm->translate('Import articles ...'))
            ->setProgressAction($progressAction, $nextButton)
            ->setCompletedAction($this->proxy()->displayList(), $nextButton)
            ;

            $frame->addItem($progressBar);
            $frame->addItem($nextButton);

        } else {
            $page->addItem($form);
        }

        return $page;
    }



    public function importArticleProgress($import = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();
        $progressBar = $W->ProgressBar();
        $set = $Crm->ArticleSet();
        $set->mainpackaging(); // joined to update the main packaging if possible


        if (!$Crm->Access()->importArticles()) {
            $progressBar->updateProgress(100, $Crm->translate('Access denied'));
            exit;
        }


        $importRecord = $Crm->ImportSet()->get($import);


        if (!$importRecord) {

            $progressBar->updateProgress(100, $Crm->translate('Error : missing import record'));
            exit;
        }


        $iterator = $Ui->ArticleImportEditor()->getIterator();

        if (!$iterator) {
            $progressBar->updateProgress(100, $Crm->translate('Error : missing data source'));
            exit;
        }

        foreach($iterator as $row) {
            /* @var $row Widget_CsvRow */
            /* Create a record in ArticleSet */
            $importRecord->importRow($set, $row); /* See function importRow() in import.class.php of LibCrm */

            /* Update the percentage of the progress bar */
            $progressBar->updateProgress($iterator->getProgress(), $Crm->translate('Import articles ...').$iterator->getProgress().'%');
        }



        $progressBar->updateProgress(100, $Crm->translate('Import articles ... done'));
        $importRecord->save();

        exit;
    }


    /**
     * photo upload form
     * @throws crm_AccessException
     */
    public function importPhotos()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->importPhotos(), $Crm->translate('Import articles photos'));


        if (!$Crm->Access()->importArticles()) {
            $message = $Crm->translate('Access denied');
            throw new crm_AccessException($message);
        }

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->setTitle($Crm->translate('Import articles photos'));
        $page->addItem($W->Title($Crm->translate('Import articles photos'), 1));



        $form = $Ui->ArticleImportPhotosEditor();
        $page->addItem($form);

        return $page;
    }



    /**
     * import uploaded photos and display report
     */
    public function importPhotosEnd($photos = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->importPhotos(), $Crm->translate('Photos import report'));


        if (!$Crm->Access()->importArticles()) {
            $message = $Crm->translate('Access denied');
            throw new crm_AccessException($message);
        }

        $page = $Ui->Page();

        $page->setTitle($Crm->translate('Photos import report'));
        $page->addItem($W->Title($Crm->translate('Photos import report'), 1));


        $page->addItem($result = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(.8,'em')));

        $result->addClass('crm-detailed-info-frame');

        $set = $Crm->ArticleSet();

        $good = 0;
        $bad = 0;


        // browse photos names

        foreach($_FILES['photos']['name']['files'] as $pos => $name)
        {
            $tmp_file = new bab_Path($_FILES['photos']['tmp_name']['files'][$pos]);

            if ($error = $_FILES['photos']['error']['files'][$pos])
            {
                $result->addItem($W->Label(sprintf($Crm->translate('The file %s has been ignored because of an upload error : %s'), $name, crm_fileUploadError($error))));
                $bad++;
                continue;
            }

            $extension = mb_substr($name, mb_strrpos($name, '.'));

            if ('.zip' === $extension)
            {
                $this->importZipPhotos($tmp_file, $set, $result, $good, $bad);
                continue;
            }


            if ($this->importOnePhoto($tmp_file, $set, $name, $result))
            {
                $good++;
            } else {
                $bad++;
            }

        }


        $result->addItem($W->Label(sprintf($Crm->translate('%d photos imported with %d errors'), $good, $bad)));


        $page->addItem($W->Link($Crm->translate('Continue'), $this->proxy()->displayList())->addClass('crm-dialog-button'));

        return $page;
    }

    /**
     * Import multiple photos from a zip archive
     *
     * @param	bab_Path		$tmp_file	Full path to temporary uploaded zip file
     * @param 	crm_ArticleSet 	$set
     * @param 	Widget_Frame 	$result
     * @param 	int 			&$good
     * @param 	int 			&$bad
     */
    protected function importZipPhotos(bab_Path $tmp_file, crm_ArticleSet $set, Widget_Frame $result, &$good, &$bad)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $addon = bab_getAddonInfosInstance($Crm->getAddonName());

        $path = new bab_Path($addon->getUploadPath(), 'tmp', session_id());
        $path->createDir();

        $zipf = clone $path;
        $zipf->push('photos.zip');

        if (!move_uploaded_file($tmp_file->tostring(), $zipf->tostring()))
        {
            $result->addItem($W->Label($Crm->translate('Failed to move uploaded file')));
            $bad++;
            return;
        }

        // extract all in a subfolder

        $subfolder = clone $path;
        $subfolder->push('extract');

        $zip = bab_functionality::get('Archive/Zip');
        $zip->open($zipf->tostring());

        $zip->extractTo($subfolder->tostring());

        // import files from subfolder

        foreach($subfolder as $photopath)
        {
            /*@var $photopath bab_path */
            if ($this->importOnePhoto($photopath, $set, $photopath->getBasename(), $result))
            {
                $good++;
            } else {
                $bad++;
            }
        }

        $path->deleteDir();
    }


    /**
     * Import one photo from import batch
     *
     * @param	bab_Path		$tmp_file		Full path to temporary file (if uploaded file, will be moved, if regular file, will be copied)
     * @param	crm_ArticleSet	$set
     * @param	string 			$name			Filename
     * @param	Widget_Frame	$result			Result frame for outputs
     *
     * @return bool
     */
    protected function importOnePhoto(bab_Path $tmp_file, crm_ArticleSet $set, $name, Widget_Frame $result)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $encname = str_replace('..', '/DOT/', $name);
        $dotcount = mb_substr_count($encname,'.');

        if ($dotcount === 0)
        {
            $result->addItem($W->Label(sprintf($Crm->translate('The file %s has been ignored because of the missing extension .png, .jpg or .zip'), $name)));
        }

        $reference = str_replace('/DOT/', '.', mb_substr($encname, 0, mb_strpos($encname, '.')));
        $article = $set->get($set->reference->is($reference));

        if (!isset($article))
        {
            $result->addItem($W->Label(sprintf($Crm->translate('The file %s has been ignored because the reference %s has not been found in articles'), $name, $reference)));
            return false;
        }

        if (1 === $dotcount)
        {
            // replace main image
            if ($article->importPhoto($tmp_file, true, str_replace('..', '.', $name)))
            {
                return true;
            }
        }


        $op_filename = str_replace('/DOT/', '.', mb_substr($encname, 1 + mb_strpos($encname, '.')));

        $imagePicker = $W->ImagePicker();
        $imagePicker->setFolder($article->getOthersPhotosUploadPath());
        if ($imagePicker->importFile($tmp_file, bab_charset::getIso(), $op_filename))
        {
            return true;
        }
    }




    /**
     * Does nothing and returns to the previous page.
     *
     *
     * @return Widget_Action
     */
    public function cancel()
    {
        crm_redirect(crm_BreadCrumbs::getPosition(-2));
    }

}
