<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * This controller manages actions that can be performed on emails.
 */
class crm_CtrlMail extends crm_Controller implements crm_ShopAdminCtrl
{

    /**
     *
     * @param int $email		The email id
     * @return Widget_Action
     */
    public function display($email)
    {
        $Crm = $this->Crm();
        $access = $Crm->Access();
        $W = bab_Widgets();
        $Crm->Ui()->includeEmail();

        $emailSet = $Crm->EmailSet();
        $email = $emailSet->get($email);

        if (!$access->readEmail($email)) {
            throw new crm_AccessException($Crm->translate('Access denied'));
        }

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->addItem($W->Title($Crm->translate('Email'), 1));

        $page->addItem(new crm_HistoryEmail($email, crm_HistoryEmail::MODE_FULL));

        $page->addContextItem(crm_emailLinkedObjects($email));


        $actionsFrame = $page->ActionsFrame();
        $actionsFrame->addItem($W->Link($W->Icon($Crm->translate('Send a copy this email'), Func_Icons::ACTIONS_EDIT_COPY), $this->proxy()->copy($email->id)));

        $page->addContextItem($actionsFrame);

        $page->displayHtml();
    }


    private function verify(crm_Page $page)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        if (!crm_Email::emailServerIsConfigured()) {
            $page->addItem(
                $W->Frame()->setLayout(
                    $W->VBoxItems(
                        $W->Title($Crm->translate('It seems that email is not configured / activated on your server.'), 2),
                        $W->Label($Crm->translate('Please, contact your system administrator.'))
                    )->setVerticalSpacing(0.5, 'em')
                )->addClass('crm-error')
            );

            return false;
        }

        return true;
    }


    /**
     * Displays an email editor.
     *
     * @param string $recipients
     * @param string $subject
     * @param string $body
     * @param string $references  A string containing a comma separated string of identifiers like Contact:123 or Organization:654
     * @param string $attachments  If not set (null), the attached files will be empty (the attached filepicker upload directory is deleted).
     *
     * @return Widget_Action
     */
    public function edit($recipients = null, $subject = null, $body = null, $references = null, $attachments = null, $signature = true, $bccRecipients= null)
    {
        $Crm = $this->Crm();
        $access = $Crm->Access();
        $W = bab_Widgets();

        if (!$access->createEmail()) {
            throw new crm_AccessException($Crm->translate('Access denied'));
        }

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit($recipients, $subject, $body, $references, $attachments, $signature, $bccRecipients), $Crm->translate('Send an email'));

        $Crm->includeEmailSet();
        $Crm->Ui()->includeEmail();

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->setTitle($Crm->translate('Send an email'));

        if (!$this->verify($page)) {
            return $page;
        }

        $emailEditor = $Crm->Ui()->EmailEditor($signature);

        $filePicker = $emailEditor->getFilePicker();
        $filePickerFolder = $filePicker->getFolder();
        if ($filePickerFolder->isDir()) {
            $filePickerFolder->deleteDir();
        }
        if (isset($attachments)) {
            if (is_string($attachments)) {
                $attachments = new bab_Path($attachments);
            }
            $filePicker->importPath($attachments, 'UTF-8');
        }
        $emailEditor->setSaveAction($this->proxy()->send(), $Crm->translate('Send'));
        $emailEditor->setCancelAction($this->proxy()->cancel());

        $page->addItem($emailEditor);

        $emailEditor->setValue('recipients', $recipients);
        $emailEditor->setValue('bccRecipients', $bccRecipients);
        $emailEditor->setValue('subject', $subject);
        $emailEditor->setValue('body', $body);

        $emailEditor->setHiddenValue('references', $references);


        $actionsFrame = $page->ActionsFrame();

        $actionsFrame->addItem(
            $W->Link(
                $Crm->translate('Edit email signatures'),
                $Crm->Controller()->EmailSignature()->displayList()
            )
        );

        $page->addContextItem($actionsFrame);

        return $page;
    }



    /**
     *
     * @param int $mail
     * @return unknown_type
     */
    public function copy($email = null)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();
        $Crm->Ui()->includeEmail();
        $access = $Crm->Access();

        $emailSet = $Crm->EmailSet();
        $email = $emailSet->request($email);

        if (!$access->copyEmail($email)) {
        	throw new crm_AccessException($Crm->translate('Access denied'));
        }

        $Crm->Ui()->includeEmail();

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->addItem($W->Title($Crm->translate('Send an email'), 1));

        if (!$this->verify($page)) {
            return $page;
        }

        $emailEditor = $Crm->Ui()->EmailEditor();

        $filepicker = $emailEditor->getFilePicker();
        $filepickerFolder = $filepicker->getFolder();
        if ($filepickerFolder->isDir()) {
            $filepickerFolder->deleteDir();
        }
        $filepickerFolder->createDir();

        $attachmentsPath = $email->uploadPath();

        $filepicker->importPath($attachmentsPath, 'UTF-8');
        $emailEditor->setSaveAction($this->proxy()->send(), $Crm->translate('Send'));
        $emailEditor->setCancelAction($this->proxy()->cancel());

        $page->addItem($emailEditor);

        $emailEditor->setValues($email->getValues());

        $emailEditor->setHiddenValue('references', implode(',', $email->getReferences()));

        return $page;
    }


    /**
     * retry to send a saved email
     */
    public function retry($email)
    {
        $Crm = $this->Crm();
        $access = $Crm->Access();
        if (!$access->createEmail()) {
            throw new crm_AccessException($Crm->translate('Access denied'));
        }

        $Spooler = @bab_functionality::get('Mailspooler');
        /*@var $Spooler Func_Mailspooler */

        if (!$Spooler) {
            return 'Missing mail spooler';
        }

        $Crm = $this->Crm();

        $emailSet = $Crm->EmailSet();
        $email = $emailSet->get($email);

        if (!$email || !$email->hash) {
            return 'Mail not found';
        }

        $mailer = $Spooler->getMail($email->hash);

        if (!$mailer->send()) {
            return $mailer->ErrorInfo();
        }

        $email->status = 'Sent';
        if (method_exists($mailer, 'getMessageId')) {
            // ovidentia > 7.8.95
            $email->message_id = $mailer->getMessageId();
        }

        $email->save();

        return true;
    }



    /**
     * Tries to send the email.
     *
     * @param string $recipients
     * @param string $ccRecipients
     * @param string $bccRecipients
     * @param string $signature
     * @param string $bccMyself
     * @param string $subject
     * @param string $body
     * @param string $references references contains a comma separated string of identifiers like Contact:123 or Organization:654
     *
     * @return Widget_Action
     */
    public function send($recipients = null, $ccRecipients = null, $bccRecipients = null, $signature = null, $bccMyself = null, $subject = null, $body = null, $references = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $access = $Crm->Access();

        $emailSet = $Crm->EmailSet();
        $Crm->includeEmailSignatureSet();

        if (!$access->createEmail()) {
        	throw new crm_AccessException($Crm->translate('Access denied'));
        }


        if ($bccMyself) {
            $userId = bab_getUserId();
            $userEmail = bab_getUserEmail($userId);
            if (!empty($userEmail)) {
                $bccRecipients = $userEmail . ',' . $bccRecipients;
            }
        }

        /* @var $email crm_Email */
        $email = $emailSet->newRecord();

        $email->recipients = trim($recipients, ' ,;');
        $email->ccRecipients = trim($ccRecipients, ' ,;');
        $email->bccRecipients = trim($bccRecipients, ' ,;');
        $email->subject = $subject;
        $email->body = $body;
        /* Signature */
        if (is_numeric($signature)) {
            $emailSignatureSet = $Crm->EmailSignatureSet();
            $filters = new ORM_TrueCriterion();
            $filters = $filters->_AND_($emailSignatureSet->id->is($signature));
            $emailSignatures = $emailSignatureSet->select($filters);
            foreach($emailSignatures as $signatureTmp) {
                $email->body = $email->body.'<br />'.$signatureTmp->signature;
            }
        }


        $email->save();

        if ($attachedFiles = $W->FilePicker()->getTemporaryFiles('attachments[]')) {
            foreach($attachedFiles as $file) {
                $email->attachFile($file);
            }
            $W->FilePicker()->cleanup();
        }

        if (isset($references)) {
            // references contains a comma separated string of identifiers like Contact:123 or Organization:654
            $referencedElements = explode(',', $references);
            foreach ($referencedElements as $element) {
                $subject = $Crm->getRecordByRef($element);
                if (isset($subject)) {
                    $email->linkTo($subject, 'referencedBy');
                }
            }
        }

        $email->setSiteSkin();
        $status = $email->send();

        if ($status) {
            crm_redirect(crm_BreadCrumbs::last(), $Crm->translate('Your email has been sent.'));
        } else {
            $error = $Crm->translate('An error happened while trying to send your email.') . "\n"
                . $Crm->translate('Technical error message returned by the system:') . "\n"
                . implode("\n", $email->statusMessages);
            crm_redirect(crm_BreadCrumbs::last(), $error);
        }
    }






    /**
     * Downloads the selected attached file.
     *
     * @param int		$email		The email id
     * @param string	$filename
     * @return Widget_Action
     */
    public function downloadAttachment($email = null, $filename = null, $inline = 1)
    {
        $Crm = $this->Crm();
        $access = $Crm->Access();

        $email = $Crm->EmailSet()->get($email);

        if (!isset($email)) {
            throw new crm_AccessException($Crm->translate('The specified email does not seem to exist.'));
        }

        if (!$access->readEmail($email))
        {
            throw new crm_AccessException($Crm->translate('Access denied'));
        }

        $file = bab_Widgets()->FilePicker()->getByName($email->uploadPath(), $filename);
        if (!isset($file)) {
            throw new crm_AccessException($Crm->translate('The file you requested does not seem to exist.'));
        }
        $file->download($inline == 1);
    }





//     /**
//      * Displays the list of email signatures for the current user.
//      *
//      * @throws crm_AccessException
//      * @return crm_Page
//      */
//     public function displaySignatureList()
//     {
//         $W = bab_Widgets();
//         $Crm = $this->Crm();
//         $access = $Crm->Access();

//         if (!$access->createEmail()) {
//             throw new crm_AccessException($Crm->translate('Access denied'));
//         }

//         crm_BreadCrumbs::setCurrentPosition($this->proxy()->displaySignatureList(), $Crm->translate('Signature management'));


//         $page = $W->VBoxLayout();
//         $page->setVerticalSpacing(1, 'em');

//         $page->addItem($W->Title($Crm->translate('Signature management'), 1));

//         $emailSignatureSet = $Crm->EmailSignatureSet();
//         $emailSignatures = $emailSignatureSet->select($emailSignatureSet->userid->is($access->currentUser()));
//         $emailSignatures->orderAsc($emailSignatureSet->name);


//         $toolbar = new crm_Toolbar();
//         $toolbar->addButton($Crm->translate('Add signature'), Func_Icons::ACTIONS_LIST_ADD, $Crm->Controller()->Email()->editSignature());
//         $page->addItem($toolbar);


//         $tableView = $W->TableView();
//         $tableView->addClass('crm-list', Func_Icons::ICON_LEFT_16);

//         $page->addItem($tableView);

//         $tableView->addSection('body', null, 'widget-table-body');
//         $tableView->setCurrentSection('body');

//         $row = 0;
//         foreach ($emailSignatures as $emailSignature) {
//             $tableView->addRowClass($row, 'crm-list-element');
//             $items = $element['items'];

//             $tableView->addItem(
//                 $W->Link(
//                     $emailSignature->name,
//                     $Crm->Controller()->Email()->editSignature($emailSignature->id)
//                 ),
//                 $row, 0
//             );
//             $tableView->addItem(
//                 $W->Html(
//                     $emailSignature->signature
//                 ),
//                 $row, 1
//             );
//             $tableView->addItem(
//                 $W->Link(
//                     $Crm->translate('Delete signature'),
//                     $Crm->Controller()->Email()->deleteSignature($emailSignature->id)
//                 )->setConfirmationMessage($Crm->translate('Are you sure you want to delete this signature?'))
//                 ->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE),
//                 $row, 2
//             );
//             $row++;

//         }

//         $tableView->addColumnClass(0, 'widget-col-10em');

//         $page = $Crm->Ui()->Page()->addItem($page);
//         $page->addClass('crm-page-editor');

//         return $page;
//     }





//     /**
//      * Deletes the specified email signature.
//      *
//      * @param string $emailSignature       The email signature id.
//      * @throws crm_AccessException
//      */
//     public function deleteSignature($emailSignature = null)
//     {
//         $Crm = $this->Crm();
//         $access = $Crm->Access();

//         $emailSignatureSet = $Crm->EmailSignatureSet();
//         $emailSignature = $emailSignatureSet->get($emailSignature);
//         if (!$emailSignature || $emailSignature->userid != $access->currentUser()) {
//             throw new crm_AccessException($Crm->translate('Access denied'));
//         }

//         $emailSignatureSet->delete($emailSignatureSet->id->is($emailSignature->id));

//         crm_redirect(crm_BreadCrumbs::last(), $Crm->translate(('The signature has been deleted.')));
//     }






//     /**
//      *
//      * @param string $emailSignature       The email signature id.
//      * @throws crm_AccessException
//      * @return crm_Page
//      */
//     public function editSignature($emailSignature = null)
//     {
//         $W = bab_Widgets();
//         $Crm = $this->Crm();
//         $access = $Crm->Access();

//         if (!$access->createEmail()) {
//             throw new crm_AccessException($Crm->translate('Access denied'));
//         }

//         $Crm->includeStatusSet();

//         $page = $Crm->Ui()->Page();
//         $page->addClass('crm-page-editor');

//         $page->addItem($W->Title($Crm->translate('Edit signature'), 1));


//         $form = new crm_Editor($Crm);
//         $form->setName('signature');
//         $form->setHiddenValue('tg', bab_rp('tg'));

//         $emailSignatureSet = $Crm->EmailSignatureSet();

//         if (isset($emailSignature)) {
//            $emailSignature = $emailSignatureSet->get($emailSignature);
//            if (!$emailSignature || $emailSignature->userid != $access->currentUser()) {
//                throw new crm_AccessException($Crm->translate('Access denied'));
//            }
//            $form->setHiddenValue('signature[id]', $emailSignature->id);
//         } else {
//             $emailSignature = $emailSignatureSet->newRecord();
//         }

//         $form->addItem(
//             $W->LineEdit()
//                ->addClass('widget-100pc')
//                ->setName('name')
//                ->setValue($emailSignature->name)
//         );
//         $form->addItem(
//             $W->SimpleHtmlEdit()
//                ->addClass('widget-100pc')
//                ->setName('signature')
//                ->setValue($emailSignature->signature)
//         );

//         $form->setCancelAction($Crm->Controller()->Email()->cancel());
//         $form->setSaveAction($Crm->Controller()->Email()->saveSignature(), $Crm->translate('Save the signature'));

//         $page->addItem($form);
//         $page->addContextItem($W->Frame());

//         return  $page;
//     }




//     /**
//      *
//      * @param array $signature
//      * @throws crm_AccessException
//      * @throws crm_SaveException
//      */
//     public function saveSignature($signature = null)
//     {
//         $Crm = $this->Crm();
//         $access = $Crm->Access();

//         if (!$access->createEmail()) {
//             throw new crm_AccessException($Crm->translate('Access denied'));
//         }

//         $emailSignatureSet = $Crm->EmailSignatureSet();

//         if (isset($signature['id'])) {
//             $record = $emailSignatureSet->get($signature['id']);
//             if (!isset($record)) {
//                 throw new crm_SaveException($Crm->translate('Trying to access a signature with a wrong (unknown) id.'));
//             }
//         } else {
//             $record = $emailSignatureSet->newRecord();
//             $record->userid = $access->currentUser();
//         }

//         $record->setValues($signature);

//         $record->save();

//         crm_redirect(crm_BreadCrumbs::last(), $Crm->translate(('The signature has been saved.')));
//     }





    public function cancel()
    {
        crm_redirect(crm_BreadCrumbs::last());
    }
}
