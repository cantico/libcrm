<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

require_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';


class crm_ShoppingCartPaymentEvent extends bab_event
{
	
	/**
	 * @var crm_Page
	 */
	public $page;
	
	/**
	 * @var crm_ShoppingCart
	 */
	public $shoppingCart;
}

class crm_ShoppingCartDisplayBeforePayment extends crm_ShoppingCartPaymentEvent { }
class crm_ShoppingCartDisplayAfterPayment extends crm_ShoppingCartPaymentEvent { }




/**
 * This controller manages actions that can be performed on the user shopping cart.
 *
 * @see crm_CtrlShoppingCartAdm for back-office
 *
 */
class crm_CtrlShoppingCart extends crm_Controller
{
	/**
	 * Public visualisation of a saved shopping cart (wishlist)
	 * Display the shopping cart as a list of products
	 *
	 * @param	string	$uuid		shopping cart UUID
	 * @return unknown_type
	 */
	public function display($uuid = null)
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$Access = $Crm->Access();
		$W = bab_Widgets();
		
		
		if (empty($uuid))
		{
			throw new crm_AccessException('Missing parameter uuid');
		}
		
		$set = $Crm->ShoppingCartSet();
		$cartRecord = $set->get($set->uuid->is($uuid));
		/*@var $cartRecord crm_ShoppingCart */
		
		if (!$cartRecord || !$Access->viewShoppingCart($cartRecord))
		{
			throw new crm_AccessException($Crm->translate('Error, access denied to this shopping cart'));
		}

		
		$page = $Ui->Page();
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->display(), $Crm->translate('Wishlist'));
		
		// $page->setTitle($Crm->translate('Wishlist'));
		
		
		
		
		$flow = $W->FlowLayout()->addClass('crm-shoppingcart-result');
		$page->addItem($flow);
		
		$flow->addItem($Ui->ShoppingCartHeaderFrame($cartRecord));

		$flow = $W->FlowLayout()->addClass('crm-shoppingcart-result');
		$page->addItem($flow);
		

		$res = $cartRecord->getItems();
	
	
		foreach($res as $shoppingCartItem)
		{
			/*@var $shoppingCartItem crm_ShoppingCartItem */
			$catalogItem = $shoppingCartItem->catalogitem;
			$display = $Ui->CatalogItemDisplay($catalogItem);
			$flow->addItem($display->getShopListFrame());
		}

		
		return $page;
	}
	
	


	/**
	 * Edit shopping cart
	 * User view of his shopping cart or one of his saved shopping cart
	 *
	 * @param	int	$cart 		id of shopping cart to process, only used for mutiple shopping cart management
	 * @param	int	$step		step in the payment process
	 *
	 * @return Widget_Action
	 */
	public function edit($cart = null, $step = '0')
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$Ui->includeAddress();
		$set = $Crm->ShoppingCartSet();

		if (null === $cart)
		{
			$cartRecord = $set->getMyCart();
			$cart = $cartRecord->id;
		} else {

			$cartRecord = $set->get($cart);
		}

		/*@var $cartRecord crm_ShoppingCart */


		$Crm->ShopLog('Shopping cart visualisation');

		$access = $Crm->Access();
		$W = bab_Widgets();

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit($cart), $Crm->translate('Shopping cart'));
		bab_siteMap::setPosition($Crm->classPrefix.'ShoppingCart');

		if (!$access->editShoppingCart($cartRecord))
		{
			throw new crm_AccessException($Crm->translate('Error, access denied to this shopping cart'));
		}

		if (crm_ShoppingCart::ARCHIVED === (int) $cartRecord->status)
		{
			throw new crm_AccessException($Crm->translate('Error, this shopping cart is not modifiable'));
		}


		$page = $Ui->Page();
		// $page->setTitle($Crm->translate('Shopping cart'));
		
		if ($cartRecord->isEmpty())
		{
			$page->addItem($W->Label($Crm->translate('Your shopping cart is empty')));
			return $page;
		}

		$page->addToolbar($Ui->ShoppingCartToolbar($cartRecord));

		$page->addItem($scFrame = $W->Frame()->addClass('crm-shoppingprocess-frame'));

		if ($steps = $Ui->ShoppingCartSteps($cartRecord, (int) $step))
		{
			$scFrame->addItem($steps);
		}



		$this->shoppingCartFrame($scFrame, $cartRecord);


		$scFrame->addItem(
			$W->Frame()->addClass('crm-shoppingcart-loading')
				->addClass(Func_Icons::ICON_LEFT_16)
				->addItem($W->Icon($Crm->translate('Loading...'), Func_Icons::STATUS_CONTENT_LOADING))
		);
		
		$contact = $cartRecord->getContact();

		if ('' !== $cartRecord->delivery && $contact && $step >= 2)
		{
			$scFrame->addItem($this->bottomAddressInfos($cartRecord, $contact));
		}

		$page->addItem($steps->getButtons());


		if (isset($_SERVER['SERVER_ADDR']) && ('127.0.0.1' === $_SERVER['SERVER_ADDR'] || '::1' === $_SERVER['SERVER_ADDR']))
		{
			$page->addError($Crm->translate('Warning, payments will not work with localhost'));
		}

		if (!$cartRecord->isEmpty())
		{
			$items = $cartRecord->getItems();
			$articles = array();
			foreach($items as $item){
				$articles[] = $item->catalogitem->article->id;
			}
			
			if(isset($Crm->ArticleLink)) {
				$Ui->includeCatalogItem();
				if ($linked = crm_getLinkedArticlesFrame($this->Crm(), $articles, 'C'))
				{
					$page->addItem($linked);
				}
			}
		}

		return $page;
	}
	
	
	
	
	/**
	 * 
	 * @param unknown_type $cartRecord
	 * @param unknown_type $contact
	 */
	protected function bottomAddressInfos(crm_ShoppingCart $cartRecord, $contact)
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = bab_Widgets();
	
		$addressesBox = $W->HBoxItems();
		$addressesBox->setHorizontalSpacing(6, 'em');
	
	
	
		$billingAddress = $contact->getMainAddress();
		$deliveryAddress = $cartRecord->getDeliveryAddress();
	
		$addressesBox->addItem(
			$W->VBoxItems(
				$W->Title($Crm->translate('Billing address'), 2),
				$W->Label($contact->getParentSet()->title->output($contact->title) . ' ' . $contact->getFullName())
				->addClass('crm-strong'),
				new crm_AddressFrame($billingAddress)
			)
		);
		
		if ($billingAddress->id == $deliveryAddress->id) {
		    
			$addressesBox->addItem(
				$W->VBoxItems(
					$W->Title($Crm->translate('Delivery address'), 2),
					$W->Label($Crm->translate('Same as billing address'))
				)
			);
			
		} else if (isset($Crm->CollectStore) && $cartRecord->getFkPk('collectstore') > 0) {
		    
		    $collectStore = $cartRecord->collectstore();
		    
		    $openninghours = null;
		    if ($collectStore->openinghours) {
		        $openninghours = $W->Section($Crm->translate('Opening hours'), $W->RichText($collectStore->openinghours), 3)->setFoldable(true, false);
		    }
		    
		    $addressesBox->addItem(
	           $W->VBoxItems(
	               $W->Title($Crm->translate('Pick-up address'), 2),
	               $W->Label($collectStore->name)->addClass('crm-strong'),
	               $Ui->AddressFrame($collectStore->address()),
	               $openninghours
	            )
	        );
		    
		} else {
	
	
	
			$deliveryAddress = $cartRecord->getDeliveryAddress();
			if (empty($deliveryAddress->recipient))
			{
				$label = $W->Label($contact->getParentSet()->title->output($contact->title) . ' ' . $contact->getFullName())->addClass('crm-strong');
			} else {
				$label = null;
			}
	
	
	
			$addressesBox->addItem(
				$W->VBoxItems(
					$W->Title($Crm->translate('Delivery address'), 2),
					$label,
			        $Ui->AddressFrame($deliveryAddress)
				)
			);
	
	
		}
	
		return $addressesBox;
	
	}
	
	


	/**
	 * add content to shopping cart frame
	 * @param Widget_Frame $scFrame
	 * @param crm_ShoppingCart $cartRecord
	 */
	protected function shoppingCartFrame($scFrame, $cartRecord)
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		
	

		$scFrame->addItem($Ui->ShoppingCartItemTableView($cartRecord));
		
		$couponSet = $Crm->CouponSet();
		if ($couponSet->containItems()) {
		    $scFrame->addItem($Ui->ShoppingCartCouponEditor());
		}

		if ($postPaid = $this->postPaid())
		{
			$scFrame->addItem($postPaid);
		}
		
		if ($marketing = $Ui->ShoppingCartMarketing($cartRecord))
		{
			$scFrame->addItem($marketing);
		}
	}


	public function shoppingCartCoupon($coupon = null)
	{
		$Crm = $this->Crm();

		$Access = $Crm->Access();
		$cartSet = $Crm->ShoppingCartSet();
		$cartRecord = $cartSet->getMyCart();

		if (!in_array((int) $cartRecord->status, $cartRecord->getOngoingStatus())) {
			throw new crm_SaveException($Crm->translate('Your shopping cart is already confirmed, please finish your order before filing a new one'));
		}

		$set = $Crm->CouponSet();
		
		if (isset($Crm->ArticlePackaging)) {
    		$set->articlepackaging();
    		$set->articlepackaging->packaging();
    		$set->articlepackaging->article();
    		$set->articlepackaging->article->joinHasOneRelations();
		}
		
		
		if ('' === $coupon['code']) {
		    throw new crm_SaveException($Crm->translate('This coupon is not valid.'));
		}
		

		$couponRecord = $set->get(
			$set->code->is($coupon['code'])
			->_AND_($set->start_date->lessThanOrEqual(date('Y-m-d')))
			->_AND_($set->end_date->greaterThanOrEqual(date('Y-m-d')))
		);

		if(!$couponRecord) {
			throw new crm_SaveException($Crm->translate('This coupon is not valid.'));
		}

        
		
		$email = $cartRecord->getEmail();
		
		if (isset($email)) {
		    // to use this check, make sure the contact is linked before coupon input
		    $couponUsages = $couponRecord->selectUsages($email);
		    if ($couponUsages->count() > 0) {
		        throw new crm_SaveException($Crm->translate('This coupon has already been used.'));
		    }
		}
		
		
		if ($couponRecord->maxuse) {
		    $couponUsages = $couponRecord->selectUsages();
			if($couponUsages->count() >= $couponRecord->maxuse) {
				throw new crm_SaveException($Crm->translate('This coupon is not valid.'));
			}
		}
		
		
		if ($cartRecord->hasCoupon($couponRecord))
		{
			throw new crm_SaveException($Crm->translate('This coupon is associated to the shopping cart'));
		}
		
		
		if ($cartRecord->getCouponRows()) {
		    throw new crm_SaveException($Crm->translate('Only one coupon at a time is possible'));
		}
		

		if (isset($Crm->ArticlePackaging) && $couponRecord->articlepackaging->article->id)
		{
			$articlePackaging = $couponRecord->articlepackaging;
			$catalogItem = $couponRecord->articlepackaging->article->getCatalogItem();
			$catalogItem->article = $couponRecord->articlepackaging->article;

			if (!$Access->addCatalogItemToShoppingCart($catalogItem, $articlePackaging))
			{
				throw new crm_SaveException($Crm->translate('The product associated to this coupon is not available.'));
			}


			$couponRecord->addArticleTo($cartRecord);
		}

		$linkSet = $Crm->CouponUsageSet();

		$newLink = $linkSet->newRecord();
		$newLink->coupon = $couponRecord->id;
		$newLink->shoppingcart = $cartRecord->id;
		$newLink->email = $cartRecord->email;
		$newLink->save();
		return true;
	}





	public function removeShoppingCartCoupon($couponUage = null)
	{
		$Crm = $this->Crm();
		$cartSet = $Crm->ShoppingCartSet();
		$cartRecord = $cartSet->getMyCart();
		$cart = $cartRecord->id;

		if (!in_array((int) $cartRecord->status, $cartRecord->getOngoingStatus())) {
		    throw new crm_SaveException($Crm->translate('Your shopping cart is not modifiable'));
		}
		


		$linkSet = $Crm->CouponUsageSet();
		$linkSet->delete($linkSet->id->is($couponUage)->_AND_($linkSet->shoppingcart->is($cart)));

		crm_redirect(crm_BreadCrumbs::last());
	}



	public function recordDisplay()
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$cartSet = $Crm->ShoppingCartSet();
		$cartRecord = $cartSet->getMyCart();

		if (!$cartRecord)
		{
			throw new crm_AccessException($Crm->translate('The shopping cart does not exists'));
		}



		$CItemSet = $this->Crm()->CatalogItemSet();
		$this->Crm()->includeArticleSet();
		$CItemSet->join('article');

		return $Ui->ShoppingCartRecordDialogEditor();
	}

	public function recordShoppingCart()
	{
		$Crm = $this->Crm();
		
		if (!$Crm->isContactLogged())
		{
			crm_redirect($Crm->Controller()->MyContact()->loginCreate(2), $Crm->translate('You must be logged in to save your shopping cart'));
		}
		
		
		$Ui = $Crm->Ui();
		$set = $Crm->ShoppingCartSet();


		$cartRecord = $set->getMyCart();

		/*@var $cartRecord crm_ShoppingCart */

		if (!$cartRecord)
		{
			throw new crm_AccessException($Crm->translate('The shopping cart does not exists'));
		}


		$access = $Crm->Access();
		if(!$access->canRecordShoppingCart())
		{
			throw new crm_AccessException($Crm->translate("You can't record your shopping cart"));
		}
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->recordShoppingCart(), $Crm->translate('Shopping cart record'));


		$page = $Crm->Ui()->Page();
		$page->setTitle($Crm->translate('Shopping cart save'));

		$page->addItem($Ui->ShoppingCartRecordEditor());

		return $page;
	}


	public function recordShoppingCartEnd($shoppingcart = null)
	{
		require_once $GLOBALS['babInstallPath'].'utilit/uuid.php';
		
		$Crm = $this->Crm();
		$ShoppingCartset = $Crm->ShoppingCartSet();
		$ShoppingCartItemset = $Crm->ShoppingCartItemSet();
		$cartRecord = $ShoppingCartset->getMyCart();

		if (!$cartRecord)
		{
			throw new crm_AccessException($Crm->translate('The shopping cart does not exists'));
		}


		$access = $Crm->Access();
		if(!$access->canRecordShoppingCart())
		{
			throw new crm_AccessException($Crm->translate("You can't record your shopping cart"));
		}
		
		
		if (empty($shoppingcart['name']))	
		{
			throw new crm_SaveException($Crm->translate("The name must not be empty"));
		}
		
		$contact = $Crm->getMyContact();
		if (null === $contact)
		{
			throw new crm_AccessException($Crm->translate("You must be mogged in as a customer"));
		}
		

		$cart = $cartRecord->id;

		$cartRecord->id = null;
		$cartRecord->cookie = '';
		$cartRecord->session = '';
		$cartRecord->expireOn = '';
		$cartRecord->name = $shoppingcart['name'];
		$cartRecord->contact = $contact->id;
		$cartRecord->user = $contact->user;
		$cartRecord->uuid = bab_uuid();
		
		$cartRecord->save();

		$items = $ShoppingCartItemset->select($ShoppingCartItemset->cart->is($cart));
		foreach($items as $item){
			$item->id = null;
			$item->cart = $cartRecord->id;
			$item->save();
		}
		
		
		if (!empty($shoppingcart['recipients']) && !empty($shoppingcart['message']))
		{
			$email = $Crm->Notify()->shoppingCart_WishList($cartRecord, $shoppingcart['recipients'], $shoppingcart['message']);
			$email->send();
		}

		crm_redirect($this->proxy()->edit(), $Crm->translate('Your shopping cart has been successfuly saved.'));
	}



	protected function postPaid()
	{
		$Crm = $this->Crm();
		$W = bab_Widgets();
		$Ui = $Crm->Ui();

		$addonname = $Crm->getAddonName();
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory("/$addonname/configuration/");
		$shipping_cost_limit = $registry->getValue('shipping_cost_limit', 0);

		if ($shipping_cost_limit > 0)
		{
			return $W->Label(sprintf($Crm->translate('Post-paid %s tax incl without shipping'), $Crm->numberFormat($shipping_cost_limit, 2).' '.$Ui->Euro()));
		}

		return null;
	}

	
	/**
	 * Get a valid billing address or null if address in invalid
	 * @param crm_ShoppingCart $cartRecord
	 * @throws crm_AccessException
	 * @return crm_Address | null
	 */
	protected function getBillingAddress(crm_ShoppingCart $cartRecord)
	{
		$Crm = $this->Crm();
		$contact = $cartRecord->getContact();
		if (null == $contact)
		{
			throw new crm_AccessException($Crm->translate('Access denied, you are not logged in as a customer'));
		}
	
		$billingaddress = $contact->getBillingAddress();
	
		if (null == $billingaddress || $billingaddress->isEmpty() || !$billingaddress->street || !$billingaddress->city)
		{
			return null;
		}
	
		return $billingaddress;
	}
	







	public function addressEditor($address = null, $reloadItem = null)
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = bab_Widgets();

		$addressForm = $W->Form();
		$addressForm->setName('address');
		$addressForm->setLayout(
			$W->VBoxLayout()->setVerticalSpacing(2, 'em')
		);

		$addressForm->addItem(
			$Ui->AddressEditor(true, true, false)
		);

		$addressForm->addItem(
			$W->SubmitButton()
				->setAjaxAction($Crm->Controller()->MyContact()->saveDeliveryAddress(), $reloadItem)
				->setLabel($Crm->translate('Save this address'))
		);
	}

	
	
	/**
	 * Ajax load of delivery address list and collect stores list
	 * @return Widget_Layout
	 */
	public function deliveryInformation()
	{
	    $Crm = $this->Crm();
	    $Ui = $Crm->Ui();
	    
	    return $Ui->ShoppingCartDeliverySelect();
	}


	
	public function selectCollectStore($collectstore = null)
	{
	    $Crm = $this->Crm();
	    $set = $Crm->ShoppingCartSet();
	    $cart = $set->getMyCart();
	    
	    $cart->collectstore = $collectstore;
	    $cart->save();
	    
	    return true;
	}



	/**
	 * Displays a page to allow the customer to specify the delivery address for the shopping cart.
	 *
	 *
	 */
	public function setDeliveryAddress()
	{
		
		
		$Crm = $this->Crm();
		$Crm->ShopLog('Set delivery address page');
		$Ui = $Crm->Ui();
		$set = $Crm->ShoppingCartSet();

		$cartRecord = $set->getMyCart();
		/*@var $cartRecord crm_ShoppingCart */
		$cart = $cartRecord->id;
		
		
		$deliveryaddress = $cartRecord->getDeliveryAddress($cartRecord);
		
		$shippingamount = null;
		if (null !== $deliveryaddress)
		{
			$shippingamount = $cartRecord->computeShippingCost(
				$deliveryaddress->postalCode,
				$deliveryaddress->getCountry()->id
			);
		} 
		

		$access = $Crm->Access();
		$W = bab_Widgets();

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit($cart), $Crm->translate('Set delivery address'));
		

		if (!$access->editShoppingCart($cartRecord))
		{
			throw new crm_AccessException($Crm->translate('Error, access denied to this shopping cart'));
		}

		if (crm_ShoppingCart::ARCHIVED === (int) $cartRecord->status)
		{
			throw new crm_AccessException($Crm->translate('Error, this shopping cart is not modifiable'));
		}
		
		
		
		$page = $Crm->Ui()->Page();
		
		$frame = $W->Frame()->addClass('crm-shoppingprocess-frame');
		$page->addItem($frame);
		
		$steps = $Ui->ShoppingCartSteps($cartRecord, 2);
		
		$frame->addItem($steps);
		$frame->addItem($layout = $W->VBoxLayout()->setVerticalSpacing(3,'em'));
		
		
		// if there is no addresses, display a form to create the addresses
		
		$billingaddress = $this->getBillingAddress($cartRecord);
		
		if (null === $billingaddress)
		{
			// display the addresses creation form
			
			$page->setTitle($Crm->translate('Billing parameters'));
			$frame->addItem($form = $W->Form(null, $W->VBoxLayout()->setVerticalSpacing(2,'em')));
			$form
				->setHiddenValue('tg', bab_rp('tg'))
				->setName('contact')
				->addItem($W->Title($Crm->translate('Set your billing address'), 3))
				->addItem($Ui->MandatoryAddressEditor()->setName('address'))
				->addItem(
					$W->SubmitButton()
						->setLabel($Crm->translate('Continue to delivery options'))
						->setAction($this->proxy()->saveBillingAddress())
						->setSuccessAction($this->proxy()->setDeliveryAddress())
						->setFailedAction($this->proxy()->setDeliveryAddress())
				);
			
			$contact = $cartRecord->getContact();
			
			$address = array();
			if ($addressRecord = $contact->address()) {
    			$address = $addressRecord->getValues();
    			if (empty($address['country']))
    			{
    				unset($address['country']);
    			}
			}
			
			$form->setValues(array('contact' => array('address' => $address)));
			return $page;
		} 


		
		$page->setTitle($Crm->translate('Set delivery address'));

		if ($postPaid = $this->postPaid()) {
			$layout->addItem($postPaid);
		}
		
		$deliveryLayout = $W->DelayedItem($this->proxy()->deliveryInformation())->setPlaceHolderItem($this->deliveryInformation());
		$deliveryLayout->setUpdateMode(Widget_DelayedItem::UPDATE_MODE_NONE);
		
		// $deliveryLayout = $W->DelayedItem($this->proxy()->deliveryInformation());
		// $deliveryLayout->setUpdateMode(Widget_DelayedItem::UPDATE_MODE_ON_PAGE_LOAD);
		
		
		$layout->addItem($deliveryLayout);
		
		
		
		$buttons = $steps->getButtonsContainer();

		$saveShipping = $W->Link(
			$W->Icon($Crm->translate('Confirm shipping information'), Func_Icons::ACTIONS_DIALOG_OK),
			$this->proxy()->saveShipping()
		)->addClass('crm-dialog-button');

		$frame->addItem($buttons);

		
		if (null === $shippingamount)
		{
			$page->addItem(
				$W->Label(
					$Crm->translate('This shop does not make delivery to your address, you must change your delivery address in order to continue the purchase process')
				)->addClass('crm-alert')
			);

		} else {

			$buttons->addItem($saveShipping);
		}

		return $page;
	}
	
	
	/**
	 * 
	 */
	public function saveBillingAddress($contact = null)
	{
		$Crm = $this->Crm();
		
		$set = $Crm->AddressSet();
		$billingAddress = $set->newRecord();
		$billingAddress->setValues($contact['address']);
		$billingAddress->save();
		
		$contactSet = $Crm->ContactSet();
    	$userId = bab_getUserId();
    	$contact = $contactSet->get($contactSet->user->is($userId));
		
		$contact->address = $billingAddress->id;
		$contact->save();
		
		return true;
	}


	
	/**
	 * display terms of sales in an external page (do not require access to a shopping cart)
	 * @param int $popup
	 */
	public function displayShopTerms($popup = 0)
	{
		$Crm = $this->Crm();
		$Crm->ShopLog('Display shop terms page');
		$Ui = $Crm->Ui();
		$W = bab_Widgets();
		
		
		$page = $Ui->Page();
		if (1 == $popup)
		{
			$page->setEmbedded(false);
		}
		
		$addonname = $Crm->getAddonName();
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory("/$addonname/configuration/");

		$page->addItem($W->RichText($registry->getValue('terms')));
		
		return $page;
	}
	



	/**
	 * display terms of sales in an editor, with shopping cart steps management
	 * accept conditions before continue to the confirmMyCart method
	 */
	public function confirmShopTerms()
	{
		$Crm = $this->Crm();
		$Crm->ShopLog('Confirm shop terms page');
		$Ui = $Crm->Ui();
		$W = bab_Widgets();
		
		$set = $Crm->ShoppingCartSet();
		$cartRecord = $set->getMyCart();

		$editor = $Crm->Ui()->shopTermsEditor($this->Crm());

		$page = $this->Crm()->Ui()->Page();
		
		$page->addItem($scFrame = $W->Frame()->addClass('crm-shoppingprocess-frame'));
		$steps = $Ui->ShoppingCartSteps($cartRecord, 3);
		$scFrame->addItem($steps);
		
		$scFrame->addItem($editor);

		return $page;
	}







	/**
	 * save shopping cart, shopping car items quantity
	 * @param	array	$item
	 * @return Widget_Action
	 */
	public function save($item = null)
	{
		return true;
	}






	/**
	 * save shipping amount to shopping cart
	 */
	public function saveShipping()
	{
		$Crm = $this->Crm();
		$Crm->ShopLog('Save shipping method and delivery address choices');
		$cartSet = $Crm->ShoppingCartSet();
		$cartRecord = $cartSet->getMyCart();

		$cartRecord->updateShippingCost();

		crm_redirect($this->proxy()->edit(null, 3)); // goto step 3 payment
	}


	/**
	 * Delete current shopping cart
	 * @return Widget_Action
	 */
	public function delete()
	{
		$Crm = $this->Crm();
		
		$Crm->ShopLog('Delete the shopping cart');
		$Crm->ShoppingCartSet()->deleteMyCart();
		crm_redirect('?');
	}

	/**
	 * Confirm the current edited shopping cart
	 * @param	array	shop terms editor values
	 * @return Widget_Action
	 */
	public function confirmMyCart($terms = null)
	{
		$Crm = $this->Crm();

		$mycontact = $Crm->getMyContact();

		if (!$mycontact)
		{
			throw new crm_AccessException($Crm->translate('You are not logged in as a regular customer'));
		}

		$cartSet = $this->Crm()->ShoppingCartSet();
		$cart = $cartSet->getMyCart();

		if (!$cart) {
			throw new crm_AccessException($Crm->translate('Access denied, no shopping cart'));
		}

		if (crm_ShoppingCart::EDIT !== (int) $cart->status) {
			throw new crm_AccessException($Crm->translate('Access denied, wrong shopping cart status'));
		}

		$items = $cart->getItems();
		if (0 === $items->count()) {
			throw new crm_AccessException($Crm->translate('Access denied, no items in shopping cart'));
		}


		if (!$terms['accept'])
		{
			throw new crm_SaveException($Crm->translate('The shop terms must be accepted to proceed to payment'));
		}



		if ($cart->contact != $mycontact->id)
		{
			// panier cree par un utilisateur annonyme sur ce poste
			$cart->contact = $mycontact->id;
		}

		if ($cart->user != $GLOBALS['BAB_SESS_USERID'])
		{
			// panier cree par un utilisateur annonyme sur ce poste
			$cart->user = $GLOBALS['BAB_SESS_USERID'];
		}

		$cart->cookie = ''; // casser le lien avec le cookie pour eviter de se retrouver avec un panier confirme en tant qu'annonyme
		$cart->status = crm_ShoppingCart::CONFIRMED;
		$cart->save();
		
		$Crm->ShopLog('Shopping cart has been set to the confirmed status');

		return true;
	}



	/**
	 * Get frame content for dialog used to set options when a catalog item is added to shopping cart
	 * @param int $catalogitem
	 */
	public function catalogItemOptionsDialog($catalogitem = null, $articlepackaging = null, $quantity = null)
	{
		$Crm = $this->Crm();
		
		$Crm->ShopLog(__FUNCTION__, true);
		
		$Ui = $Crm->Ui();
		$cartSet = $Crm->ShoppingCartSet();
		$cartRecord = $cartSet->getMyCart();
		$access = $Crm->Access();

		if (!$cartRecord)
		{
			die($Crm->translate('The shopping cart does not exists'));
		}



		$CItemSet = $Crm->CatalogItemSet();

		$CItemSet->article();

		if(isset($Crm->ArticleAvailability)) {
			$CItemSet->article->articleavailability();
		}

		$catalogItemRecord = $CItemSet->get($catalogitem);
		/*@var $catalogItem crm_CatalogItem */

		if (!$catalogItemRecord)
		{
			die($Crm->translate('Error, this article does not exists'));
		}

		// get article packaging

		$apSet = $Crm->ArticlePackagingSet();
		$apSet->packaging();

		$articlePackagingRecord = $apSet->get($apSet->article->is($catalogItemRecord->article->id)->_AND_($apSet->id->is($articlepackaging)));



		if (null === $articlePackagingRecord || !$articlePackagingRecord->id)
		{
			die($Crm->translate('Error, this article packaging does not exists'));
		}

		if (!$access->addCatalogItemToShoppingCart($catalogItemRecord, $articlePackagingRecord))
		{
			die($Crm->translate('You are not allowed to add this item in the shopping cart'));
		}

        
		if (empty($quantity))
		{
			$quantity = null;
		}
		
		$min = $articlePackagingRecord->packaging->getMinimalQuantity();
		if ($min > 0 && (!isset($quantity) || $min > $quantity)) {
		    $quantity = $min;
		}

		return $Ui->catalogItemOptionsDialogEditor($catalogItemRecord, $articlePackagingRecord, $quantity);
	}





	/**
	 * Add a crm_ShoppingCartItem
	 *
	 * @param	int		$catalogitem		id of a crm_CatalogItem
	 * @param  int		$articlepackaging	id of a crm_ArticlePackaging
	 * @param	int		$quantity
	 */
	public function addCatalogItem($catalogitem = null, $articlepackaging = null, $quantity = null)
	{
		$Crm = $this->Crm();
		
		$Crm->ShopLog(__FUNCTION__, true);

		$cartSet = $this->Crm()->ShoppingCartSet();
		$CItemSet = $this->Crm()->CatalogItemSet();
		$this->Crm()->includeArticleSet();
		$CItemSet->article();
		if(isset($Crm->ArticleAvailability)){
			$CItemSet->article->articleavailability();
		}

		crm_BreadCrumbs::setCurrentPosition($this->proxy()->addCatalogItem($catalogitem, $articlepackaging, $quantity), $Crm->translate('Add to shopping cart'));

		$cart = $cartSet->getMyCart();

		if (!($cart instanceOf crm_ShoppingCart) || empty($cart->id)) {
			throw new crm_SaveException($Crm->translate('Error, no shopping cart found'));
		}

		$catalogItemRecord = $CItemSet->get($catalogitem);
		/*@var $catalogItemRecord crm_CatalogItem */


		if (!$catalogitem || !$catalogItemRecord)
		{
			throw new crm_SaveException($Crm->translate('Error, this article does not exists'));
		}

		$apSet = $Crm->ArticlePackagingSet();
		$apSet->packaging();
		$apSet->article();

		$articlePackagingRecord = $apSet->get($apSet->article->id->is($catalogItemRecord->article->id)->_AND_($apSet->id->is($articlepackaging)));
		/*@var $articlePackagingRecord crm_ArticlePackaging */
		if (!$articlepackaging || !$articlePackagingRecord)
		{
			throw new crm_SaveException($Crm->translate('Error, this article packaging does not exists'));
		}


		$access = $this->Crm()->Access();
		if (!$access->addCatalogItemToShoppingCart($catalogItemRecord, $articlePackagingRecord))
		{
			throw new crm_SaveException($Crm->translate('You are not allowed to add this item in the shopping cart'));
		}


		if (crm_ShoppingCart::EDIT !== (int) $cart->status)
		{
			throw new crm_SaveException($Crm->translate('Your shopping cart is already confirmed, please finish your order before filing a new one'));
		}
		
		$min = $articlePackagingRecord->packaging->getMinimalQuantity();
		if ($quantity < $min) {
		    throw new crm_SaveException(sprintf($Crm->translate('The minimal order quantity for this packaging is %d'), $min));
		}

		$cart->addCatalogItem($catalogItemRecord, $articlePackagingRecord, $quantity);



		return true;
	}




	/**
	 * add catalog item and end shopping, goto shopping cart
	 * @param int $item
	 * @param int $quantity
	 */
	public function addCatalogItemEnd($catalogitem = null, $articlepackaging = null, $quantity = null)
	{
		return $this->addCatalogItem($catalogitem, $articlepackaging, $quantity);
	}




	/**
	 * Remove shopping cart item from shopping cart
	 * @param int $item		ID shopping cart item
	 * @return Widget_Action
	 */
	public function remove($item = null)
	{
		$Crm = $this->Crm();
		
		$Crm->ShopLog(sprintf('remove %d from shopping cart', $item));
		
		$cartSet = $this->Crm()->ShoppingCartSet();
		$SCItemSet = $this->Crm()->ShoppingCartItemSet()->join('cart');

		$cart = $cartSet->getMyCart();
		$SCItemSet->delete(
			$SCItemSet->id->is($item)
			->_AND_($SCItemSet->cart->is($cart->id))
			->_AND_($SCItemSet->cart->status->is(crm_ShoppingCart::EDIT))
		);

		if ($cart->delivery)
		{
			$cart->updateShippingCost();
		}

		crm_redirect(crm_BreadCrumbs::last());
	}






	/**
	 * Payment by credit card
	 */
	public function creditCard()
	{
		$Crm = $this->Crm();
		$Ui = $this->Crm()->Ui();
		$cartSet = $this->Crm()->ShoppingCartSet();

		// verify articles amount before payment

		$cartRecord = $cartSet->getMyCart();
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->creditCard(), $Crm->translate('Payment by credit card'));
		
		if (0 === (int) (100 * $cartRecord->getTotalTI())) {
			throw new crm_SaveException($Crm->translate('This shopping cart is not valid because of the empty total amount'));
		}
		
		if (0 === (int) $cartRecord->shipping_status) {
		    throw new crm_SaveException($Crm->translate('The shipping to this address is not possible or the total weight of the shopping cart is too large'));
		}


		if (crm_ShoppingCart::CONFIRMED !== (int) $cartRecord->status)
		{
			$Crm->ShopLog('Display shop terms');
			
			// display confirmation screen
			return $this->confirmShopTerms();
		}

		$Crm->ShopLog('Payment page');

		foreach ($cartRecord->getItems() as $cartItem)
		{
			/* @var $cartItem crm_ShoppingCartItem */
			
			$_real = $cartItem->getRealUnitCostDF();
			
			if (null === $_real)
			{
				// the item in shopping cart has no attachment  in catalog
				continue;
			}

			$real_unit_cost = (int) round($_real * 100); // centimes
			$cart_unit_cost = (int) round($cartItem->getUnitCostDF() * 100); // centimes
			
			$gap = abs($real_unit_cost - $cart_unit_cost); // centimes

			if ($gap > 1)
			{
				// TODO verify the VAT rate
				// TODO display a confirmation dialog to update the shopping cart prices (method updatePriceFromArticlePackaging)

				
				$message = sprintf($this->Crm()->translate('The price of the product %s has been modified since the insertion in shopping cart, please remove the product and add it with the new price'), $cartItem->catalogitem->article->getName());
				
				$Crm->ShopLog($message);

				$page = $Ui->Page();
				$page->addError($message);

				return $page;
			}

		}


		// default method use payment system

		try {
			$page = $this->Crm()->Payment()->displayPaymentPage();
		} catch (Exception $e)
		{
			$Crm->ShopLog($e->getMessage());
			
			$page = $Ui->Page()->addError($e->getMessage());
			return $page;
		}
		
		
		$event = new crm_ShoppingCartDisplayBeforePayment; // event can be used to add tracking codes
		$event->page = $page;
		$event->shoppingCart = $cartRecord;
		bab_fireEvent($event);
		
		
		return $page;
	}
	
	
	
	/**
	 * Get back from the payment gateway to this page
	 * 
	 */
	public function paymentUserReturn($shoppingcart = null, $canceled = 0)
	{
		$Crm = $this->Crm();
		$Ui = $this->Crm()->Ui();
		$W = bab_Widgets();
		$Access = $Crm->Access();
		
		bab_siteMap::setPosition($Crm->classPrefix.'PaymentUserReturn');
		
		$Crm->ShopLog('Payment confirmation page');
		
		$page = $Ui->Page();
		
		$head = bab_getInstance('babHead');
		$head->setTitle($Crm->translate('Payment confirmation'));
		
		$frame = $W->Frame(null, $W->VBoxItems()->setVerticalSpacing(2,'em'))
			->addClass('crm-payment-user-return')
			->addClass('crm-dialog');
		
		
		$set = $Crm->ShoppingCartSet();
		$set->contact();
		$set->contact->address();
		$set->contact->address->country();
		if (null === $shoppingcart)
		{
			$cartRecord = null;
		} else {
			$cartRecord = $set->get($shoppingcart);
		}
		
		if ($canceled) {
		    $message = $Crm->translate('Payment canceled');
		} else {

    		if (null === $cartRecord || !$Access->editShoppingCart($cartRecord))
    		{
    			// we get on this page from a link?
    			
    			$message = $Crm->translate('Payment confirmation page');
    			
    			
    			
    		} else {
    			
    			$message = $Crm->translate('Thank you for your order. You will receive an email containing all your order details.');
    			$this->googleAnalytics($cartRecord);
    			
    			$event = new crm_ShoppingCartDisplayAfterPayment; // event can be used to add tracking codes
    			$event->page = $page;
    			$event->shoppingCart = $cartRecord;
    			bab_fireEvent($event);
    		}
		}
		
		$frame->addItem($W->Label($message));
		
		$frame->addItem($W->Link($Crm->translate('Get back to homepage'), '?')->addClass('crm-dialog-button'));
		
		
		$page->addItem($frame);
		
		// close shop trace
		// $stSet = $Crm->ShopTraceSet();
		// $stSet->endTrace();
		
		return $page;
	}
	
	
	/**
	 * add the shopping cart to google analytics if possible
	 * 
	 * @todo move this code to the googleanalytics module using the crm_ShoppingCartDisplayAfterPayment event
	 * 
	 * @param	crm_ShoppingCart $cartRecord
	 */ 
	protected function googleAnalytics(crm_ShoppingCart $cartRecord)
	{
		
		$ga = @bab_functionality::get('GoogleAnalytics');
		/*@var $ga Func_GoogleAnalytics */
		
		if (false === $ga)
		{
			return;
		}
		
		$Crm = $this->Crm();
		
		
		$ga->_addTrans(
				$cartRecord->id,  											// transaction ID - required
				$Crm->getAddonName(),  										// affiliation or store name
				number_format($cartRecord->getTotalTI(), 2, '.', ''),       // total - required
				number_format($cartRecord->getVAT(), 2, '.', ''),           // tax
				number_format($cartRecord->getFreight(), 2, '.', ''),       // shipping
				$cartRecord->contact->address->city,     					// city
				'',     													// state or province
				$cartRecord->contact->address->country->getName()           // country
		);
		
		
		$items = $cartRecord->getItems();
		
		foreach($items as $scItem)
		{
			/*@var $scItem crm_ShoppingCartItem */
			
			$ga->_addItem(
				$cartRecord->id,  											// transaction ID - required
				$scItem->catalogitem->article->reference,           		// SKU/code - required
				$scItem->name,        										// product name
				@(string) $scItem->articlepackaging->packaging()->name,   	// category or variation
				number_format($scItem->getUnitCostTI(), 2, '.', ''),   		// unit price - required
				(string) round($scItem->quantity)        					// quantity - required
			);
		}
		
		$ga->_trackTrans();
		
	}





	/**
	 * return only values allowed to be modified with the ajax request
	 * @see crm_CtrlShoppingCart::ajaxUpdate()
	 * @param array $values
	 * @return array
	 */
	protected function sanitizeValues(crm_ShoppingCartItem $record, Array $values)
	{
		if (!isset($values['quantity']))
		{
			return array();
		}
		
		$quantity = abs($values['quantity']);
		
		if (!$record->canOrderWithDecimals()) {
		    $quantity = (int) $quantity;
		}
		
		$min = $record->articlepackaging->packaging->getMinimalQuantity();
		
		if ($quantity < $min) {
		    $quantity = $min;
		}

		return array('quantity' => $quantity);
	}



	/**
	 * Update shopping cart item by ajax
	 * @return Widget_Action
	 */
	public function ajaxUpdate($cart = null, $item = null)
	{
		
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();

		$set = $Crm->ShoppingCartSet();
		$cartRecord = $set->get($cart);

		/* @var $cartRecord crm_ShoppingCart */

		$access = $Crm->Access();

		if (!$access->editShoppingCart($cartRecord))
		{
			die(crm_json_encode('access denied'));
		}

		$Crm->includeCatalogItemSet();
		$Crm->includeArticleSet();
		$Crm->includeShoppingCartSet();


		$itemSet = $Crm->ShoppingCartItemSet();
		$itemSet->catalogitem();
		$itemSet->catalogitem->article();
		$itemSet->articlepackaging();
		$itemSet->articlepackaging->packaging();
		$itemSet->articlepackaging->article();
		$itemSet->cart();

		$JSON = '{item:{';
		$JSON_items = array();

		foreach($item as $id => $values) {
			$record = $itemSet->get($itemSet->id->is($id)->_AND_($itemSet->cart->is($cart)));
			/* @var $record crm_ShoppingCartItem */

			$values = $this->sanitizeValues($record, $values);

			$record->setValues($values);
			$record->save();

			$record->updatePriceFromArticlePackaging();

			$JSON_items[] = $id.': {
				unit_cost:	\''.$Crm->numberFormat($record->unit_cost).'\',
				vat:		\''.$Crm->numberFormat($record->vat).'\',
				total_df:	\''.$Crm->numberFormat($record->getTotalDF()).'\',
				total_ti:	\''.$Crm->numberFormat($record->getTotalTI()).'\'
			}';
		}

		$cartRecord->updateShippingCost();

		$JSON .= implode(',', $JSON_items).'}';


		if ($extra = $Ui->ShoppingCartExtraRows($cartRecord))
		{
			$list = array();
			foreach($extra as $name => $arr)
			{
				if (null === $arr['value'])
				{
					$list[] = "$name: null";
				} else {
					$list[] = "$name: '".$Crm->numberFormat($arr['value'])."'";
				}
			}
			$JSON .= ", extra: {\n".implode(',' , $list).'}';
		}

		if ($coupons = $Ui->CouponRows($cartRecord))
		{
			$list = array();
			foreach($coupons as $id_coupon => $arr)
			{
				if (null === $arr['value'])
				{
					$list[] = "$id_coupon: [null, '".bab_toHtml($arr['message'], BAB_HTML_JS)."']";
				} else {
					$list[] = "$id_coupon: '".$Crm->numberFormat($arr['value'])."'";
				}
			}
			$JSON .= ", coupon: {\n".implode(',' , $list).'}';
		}
		
		
		if ($marketing = $Ui->ShoppingCartMarketing($cartRecord))
		{
			// update a widget with marketing information, no changes on shopping cart amount, displayed under the shopping cart
			$W = bab_Widgets();
			$JSON .= ", marketing:". crm_json_encode($marketing->display($W->HtmlCanvas()));
		}


		$JSON .= '}';

		echo $JSON;

		die();
	}

	
	
	/**
	 * Display terms of sales in an HTML page
	 */
	public function termsOfSale()
	{
		$Crm = $this->Crm();
		$Ui = $Crm->Ui();
		$W = bab_Widgets();
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->termsOfSale(), $Crm->translate('Terms of sale'));
		
		bab_siteMap::setPosition($Crm->classPrefix.'TermsOfSale');
		
		$page = $Ui->Page();
		$page->setTitle($Crm->translate('Terms of sale'));
		
		$addonname = $Crm->getAddonName();
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory("/$addonname/configuration/");
		
		$terms = (string) $registry->getValue('terms');
		
		$page->addItem($W->RichText($terms)->setRenderingOptions(BAB_HTML_ALL)->addClass('crm-terms-of-sale'));
		
		return $page;
	}
	
	
	
	/**
	 * Save gift comment in my current shopping cart
	 * @param unknown_type $gift_comment
	 */
	public function saveGiftComment($gift_comment = null)
	{
		$Crm = $this->Crm();
		$set = $Crm->ShoppingCartSet();
		$cart = $set->getMyCart(false);
		
		if (null !== $cart)
		{
			$cart->gift_comment = bab_convertToDatabaseEncoding($gift_comment);
			$cart->save();
		}
		
		return true;
	}
	
	
	
	/**
	 * Add a wish list to shopping cart
	 */
	public function addWishList($uuid = null)
	{
		$Crm = $this->Crm();
		$Access = $Crm->Access();
		$set = $Crm->ShoppingCartSet();
		
		if (empty($uuid))
		{
			throw new crm_AccessException('missing parameter uuid');
		}
		
		$shoppingcart = $set->getMyCart(true);
		$wishlist = $set->get($set->uuid->is($uuid));
		
		/*@var $wishlist crm_ShoppingCart */
		
		if (!$wishlist)
		{
			throw new crm_AccessException('Wishlist not found');
		}
		
		if (crm_ShoppingCart::EDIT !== (int) $shoppingcart->status)
		{
			throw new crm_SaveException($Crm->translate('Your shopping cart is already confirmed, please finish your order before filing a new one'));
		}
		
		
		$items = $wishlist->getItems();
		
		foreach($items as $scItem)
		{
			/*@var $scItem crm_ShoppingCartItem */
			
			$catalogItem = $scItem->catalogitem;
			$articlePackaging = $scItem->articlepackaging;
			
			if (!$Access->addCatalogItemToShoppingCart($catalogItem, $articlePackaging))
			{
				continue;
			}

			$shoppingcart->addCatalogItem($catalogItem, $articlePackaging, $scItem->quantity);
		}
		
		return true;
	}
	



	/**
	 * Does nothing and returns to the previous page.
	 *
	 * @param array	$message
	 * @return Widget_Action
	 */
	public function cancel()
	{
		crm_redirect(crm_BreadCrumbs::last());
	}


}