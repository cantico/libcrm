<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * This controller manages actions that can be performed on administration tables.
 */
class crm_CtrlAdmin extends crm_Controller
{



    /**
     *
     * array(
     * 		'Category name 1' => array(
     *
     * 			'item id 1' => array(
     * 				'label' => 			'Translated label',
     *				'description' => 	'Translated description',
     *				'icon' =>			icon class,
     *				'action' => 		Widget_Action
     *			),
     *			...
     * 		),
     * 		...
     * )
     *
     * @return array
     */
    protected function getManageableItems()
    {
        $Crm = $this->Crm();
        $Crm->includeEntrySet();

        $crmConfigurationItems = array();

        $standardListsItems = array();


        $crmConfigurationItems = array(

            'roles' => array(
                'label' => 			$Crm->translate('Roles'),
                'description' => 	$Crm->translate('Manage the roles of collaborators'),
                'icon' =>			Func_Icons::APPS_PREFERENCES_AUTHENTICATION,
                'action' => 		$Crm->Controller()->Admin()->displayRoleList()
            ),

            'sectors' => array(
                'label' => 			$Crm->translate('Sectors'),
                'description' => 	$Crm->translate('Sectors will help you automatically organize elements of your CRM'),
                'icon' =>			Func_Icons::APPS_STATISTICS,
                'action' => 		$Crm->Controller()->Admin()->displaySectorList()
            ),

            'accessRights' => array(
                'label' => 			$Crm->translate('Access rights'),
                'description' => 	$Crm->translate('Manage the access rights of collaborators'),
                'icon' =>			Func_Icons::ACTIONS_DIALOG_OK,
                'action' => 		$Crm->Controller()->Admin()->displayRightList()
            ),

            'mainOrganization' => array(
                'label' => 			$Crm->translate('Main organization'),
                'description' => 	$Crm->translate('Select the main organization for the CRM'),
                'icon' =>			Func_Icons::APPS_GROUPS,
                'action' => 		$Crm->Controller()->Admin()->editMainOrganization()
            ),

        );




        if (isset($Crm->ContactDirectoryMap)) {
            $crmConfigurationItems['ovidentiaDirectory'] = array(
                'label' => 			$Crm->translate('Link with Ovidentia directory'),
                'description' => 	$Crm->translate('Configure the updated fields'),
                'icon' =>			Func_Icons::APPS_CONTACTS,
                'action' => 		$Crm->Controller()->Admin()->editContactDirectory()
            );
        }

        if (isset($Crm->MailBox)) {
            $crmConfigurationItems['mailBox'] = array(
                'label' => 			$Crm->translate('List of mailboxes'),
                'description' => 	$Crm->translate('Configure the list of mailboxes used for downloading messages associated with those of CRM'),
                'icon' =>			Func_Icons::APPS_MAIL,
                'action' => 		$Crm->Controller()->MailBox()->displayList()
            );
        }



        $standardListsItems = array(

            'dealOrigins' => array(
                'label' => 			$Crm->translate(crm_Entry::DEAL_ORIGINS),
                'description' => 	$Crm->translate('Deal origins'),
                'icon' =>			Func_Icons::ACTIONS_VIEW_LIST_DETAILS,
                'action' => 		$Crm->Controller()->Admin()->displayEntryList(crm_Entry::DEAL_ORIGINS)
            ),

            'addressTypes' => array(
                'label' => 			$Crm->translate(crm_Entry::ADDRESS_TYPES),
                'description' => 	$Crm->translate('Address types'),
                'icon' =>			Func_Icons::ACTIONS_VIEW_LIST_DETAILS,
                'action' => 		$Crm->Controller()->Admin()->displayEntryList(crm_Entry::ADDRESS_TYPES)
            ),

            'contactPositions' => array(
                'label' => 			$Crm->translate(crm_Entry::CONTACT_POSITIONS),
                'description' => 	$Crm->translate('Contact positions'),
                'icon' =>			Func_Icons::ACTIONS_VIEW_LIST_DETAILS,
                'action' => 		$Crm->Controller()->Admin()->displayEntryList(crm_Entry::CONTACT_POSITIONS)
            ),

            'tags' => array(
                'label' => 			$Crm->translate('Tags'),
                'description' => 	$Crm->translate('Manage the tags that can be used to quickly organize your data'),
                'icon' =>			Func_Icons::ACTIONS_ARTICLE_TOPIC_NEW,
                'action' => 		$Crm->Controller()->Tag()->displayList()
            ),

            'classifications' => array(
                'label' => 			$Crm->translate('Classifications'),
                'description' => 	$Crm->translate('Deal, contact and organization classifications'),
                'icon' =>			Func_Icons::ACTIONS_VIEW_LIST_TREE,
                'action' => 		$Crm->Controller()->Admin()->displayClassifications()
            ),

            'status' => array(
                'label' => 			$Crm->translate('Status'),
                'description' => 	$Crm->translate('Deal status'),
                'icon' =>			Func_Icons::ACTIONS_VIEW_LIST_TREE,
                'action' => 		$Crm->Controller()->Admin()->displayStatus()
            ),

        );



        $manageableItems = array(
            'Configuration' => $crmConfigurationItems,
            'Standard lists' => $standardListsItems
        );


        return $manageableItems;
    }





    /**
     *
     * @return Widget_Action
     */
    public function administer()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        $Access = $Crm->Access();

        if (!$Access->administer()) {
            throw new crm_AccessException($Crm->translate('Access denied'));
        }

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->administer(), $Crm->translate('CRM configuration'));

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor crm-page-configuration');

        $page->addItemMenu('administration', $Crm->translate('Administration'), $this->proxy()->administer());
        $page->setCurrentItemMenu('administration');

        $configurationFrame = $W->Frame()
            ->setLayout($W->VBoxLayout()->setVerticalSpacing(1, 'em'))
            ->addClass(Func_Icons::ICON_LEFT_48)
            ->addClass('crm-configuration-frame');

        $configurationFrame->addItem($W->Title($Crm->translate('CRM configuration'), 1));

        $manageableItems = $this->getManageableItems();

        foreach ($manageableItems as $categoryName => $categoryItems) {
            $categorySection = $W->Section(
                $Crm->translate($categoryName),
                $W->FlowItems()
                    ->setHorizontalSpacing(2, 'em')
                    ->setVerticalSpacing(2, 'em')
                    ->setVerticalAlign('top')
            )
            ->setFoldable(true);

            foreach ($categoryItems as $item) {
                $button = $W->Link(
                    $W->FlowItems(
                        $W->Icon('', $item['icon'])->setSizePolicy('widget-5em'),
                        $W->VboxItems(
                            $W->Label($item['label']),
                            $W->Label($item['description'])
                                ->addClass('widget-small')
                        )->setSizePolicy('widget-75pc')
                    )->setVerticalAlign('top'),
                    $item['action']
                )->setSizePolicy('widget-33pc')
                ->addClass('widget-100pc');
                if(isset($item['confirm']) && $item['confirm']){
                    $button->setConfirmationMessage($item['confirm']);
                }
                $categorySection->addItem($button);
            }
            $configurationFrame->addItem($categorySection);
        }


        $page->addItem($configurationFrame);

        return $page;
    }



    /**
     * @return Widget_Action
     */
    public function displayRoleList()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $Crm->Ui()->includeRole();


        crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayRoleList(), $Crm->translate('Roles'));

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->addItemMenu('list', $Crm->translate('Roles'), $this->proxy()->displayRoleList());
        $page->setCurrentItemMenu('list');

        $page->addItem($W->Title($Crm->translate('Roles'), 1));

        $roleSet = $Crm->RoleSet();

        $roleSelection = $roleSet->select();

        $roleSelection
            ->orderAsc($roleSet->name);

        $availableColumns = array(
            widget_TableModelViewColumn('name', $Crm->translate('Name')),
            widget_TableModelViewColumn('_actions_', '')
                ->addClass('widget-column-thin widget-column-center')
        );

        $table = new crm_RoleTableView();
        $table->setDataSource($roleSelection);
        $table->setAvailableColumns($availableColumns);

        $table->addClass('icon-16x16 icon-left icon-left-16');

        $actionsFrame = $page->ActionsFrame();
        $page->addContextItem($actionsFrame);

        $roleEditor = new crm_Editor($Crm);
        $roleEditor->setName('role');
        $roleEditor->setSelfPageHiddenFields();
        $roleEditor->setSaveAction($Crm->Controller()->Admin()->saveRole());
        $roleEditor->addItem($roleEditor->labelledField($Crm->translate('Name'), $W->LineEdit()->setSize(35), 'name'));
        $roleEditor->addItem($roleEditor->labelledField($Crm->translate('Description'), $W->TextEdit()->setColumns(33), 'description'));

        $addRole = $W->Accordions()->addClass('crm-action-accordion');
        $addRole->addPanel($Crm->translate('Add a role'), $roleEditor);

        $actionsFrame->addItem($addRole);

        $page->addItem($table);

        return $page;
    }

    /**
     *
     * @return Widget_Action
     */
    public function editRole($role)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $Crm->Ui()->includeBase();

        $roleSet = $Crm->RoleSet();

        $role = $roleSet->get($role);

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->addItem($W->Title($Crm->translate('Edit role'), 1));

        $roleEditor = new crm_Editor($Crm);
        $roleEditor->setName('role');
        $roleEditor->setHiddenValue('tg', bab_rp('tg'));
        if (isset($role->id)) {
            $roleEditor->setHiddenValue('role[id]', $role->id);
        }
        $roleEditor->setSaveAction($Crm->Controller()->Admin()->saveRole());
        $roleEditor->setCancelAction($Crm->Controller()->Admin()->cancel());
        $roleEditor->addItem(
            $roleEditor->labelledField(
                $Crm->translate('Name'),
                $W->LineEdit()->setSize(60)->setValue($role->name),
                'name'
            )
        );
        $roleEditor->addItem(
            $roleEditor->labelledField(
                $Crm->translate('Description'),
                $W->TextEdit()->setColumns(58)->setValue($role->description),
                'description'
            )
        );

        $page->addItem($roleEditor);

        return $page;
    }


    /**
     *
     * @param int	$role
     * @return Widget_Action
     */
    public function deleteRole($role = null)
    {
        $Crm = $this->Crm();
        $roleSet = $Crm->RoleSet();

        $roleSet->delete($roleSet->id->is($role));
        crm_redirect(crm_BreadCrumbs::last(), $Crm->translate('The role has been deleted.'));
    }


    /**
     *
     * @param array	$role ('name')
     * @return Widget_Action
     */
    public function saveRole($role = null)
    {
        $Crm = $this->Crm();

        $roleSet = $Crm->RoleSet();

        if (isset($role['id'])) {
            $newRole = $roleSet->get($role['id']);
        } else {
            $newRole = $roleSet->newRecord();
        }

        $newRole->name = $role['name'];
        $newRole->description = $role['description'];
        $newRole->save();

        crm_redirect(crm_BreadCrumbs::last());
    }







    /**
     * @return Widget_Action
     */
    public function displayClassifications()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $Crm->Ui()->includeBase();
        $Crm->Ui()->includeClassification();


        crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayClassifications(), $Crm->translate('Classifications'));

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->addItem($W->Title($Crm->translate('Edit codifications'), 1));

        $editor = crm_classificationEditor($Crm);
        $page->addItem($editor);

        $page->addContextItem($W->Title('', 5));

        return $page;
    }


    /**
     * @return Widget_Action
     */
    public function addClassification($parentClassification)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $Crm->Ui()->includeBase();
        $Crm->Ui()->includeClassification();


        $classificationSet = $Crm->ClassificationSet();



        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $classificationEditor = new crm_Editor($Crm);
        $classificationEditor->setName('classification');
        $classificationEditor->setHiddenValue('tg', bab_rp('tg'));

        $parentClassification = $classificationSet->get($parentClassification);
        if (isset($parentClassification->id)) {
            $classificationEditor->setHiddenValue('classification[id_parent]', $parentClassification->id);
        }
        $classificationEditor->setSaveAction($Crm->Controller()->Admin()->saveClassification());
        $classificationEditor->setCancelAction($Crm->Controller()->Admin()->cancel());
        $classificationEditor->addItem($classificationEditor->labelledField($Crm->translate('Text'), $W->LineEdit()->setSize(60), 'name'));

        $page->addItem($classificationEditor);

        return $page;
    }


    /**
     * @return Widget_Action
     */
    public function editClassification($classification)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $Crm->Ui()->includeBase();

        $Crm->Ui()->includeClassification();

        $classificationSet = $Crm->ClassificationSet();

        $classification = $classificationSet->get($classification);

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');


        $classificationEditor = new crm_Editor($Crm);
        $classificationEditor->setName('classification');
        $classificationEditor->setHiddenValue('tg', bab_rp('tg'));
        if (isset($classification->id)) {
            $classificationEditor->setHiddenValue('classification[id]', $classification->id);
        }
        $classificationEditor->setSaveAction($Crm->Controller()->Admin()->saveClassification());
        $classificationEditor->setCancelAction($Crm->Controller()->Admin()->cancel());
        $classificationEditor->addItem($classificationEditor->labelledField($Crm->translate('Text'), $W->LineEdit()->setSize(60)->setValue($classification->name), 'name'));

        $page->addItem($classificationEditor);

        return $page;
    }


    /**
     *
     * @param array	$entry ('category, 'text')
     * @return Widget_Action
     */
    public function saveClassification($classification = null)
    {
        $Crm = $this->Crm();

        $classificationSet = $Crm->ClassificationSet();

        if (isset($classification['id'])) {
            $c = $classificationSet->get($classification['id']);
            $c->name = $classification['name'];
            $c->save();
        } else if (isset($classification['id_parent'])) {
            $c = $classificationSet->newRecord();
            $c->name = $classification['name'];
            $parent = $classificationSet->get($classification['id_parent']);
            $parent->appendChild($c);
        }


        crm_redirect(crm_BreadCrumbs::last());
    }


    /**
     * Delete the specified classification and all its descendants.
     *
     * @param int	$classification
     * @return Widget_Action
     */
    public function proposeDeleteClassification($classification)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $Crm->Ui()->includeDeal();
        $Crm->Ui()->includeContact();
        $Crm->Ui()->includeOrganization();

        $Crm->includeClassificationSet();
        $Crm->includeAddressSet();

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-detailed-info-frame');

        $classificationSet = $Crm->ClassificationSet();

        $classification = $classificationSet->get($classification);


        $allClassificationIds = array($classification->id => $classification->id);

        $descendants = $classification->getDescendants();
        foreach ($descendants as $descendant) {
            $allClassificationIds[$descendant->id] = $descendant->id;
        }

        $dealClassificationSet = $Crm->DealClassificationSet();
        $dealClassificationSet->join('deal');

        $dealClassifications = $dealClassificationSet->select($dealClassificationSet->classification->in($allClassificationIds));
        $deals = array();

        $nbDeals = 0;
        $dealsBox =  $W->VBoxLayout()
            ->setCanvasOptions(Widget_Item::Options()->minWidth(20, 'em'))
            ->setVerticalSpacing(2, 'em');
        $dealsBox->addItem($dealsTitle = $W->Title('', 3));
        foreach ($dealClassifications as $dealClassification) {
            if (!array_key_exists($dealClassification->deal->id, $deals)) {
                $nbDeals++;
                $deals[$dealClassification->deal->id] = $dealClassification->deal->id;
                $dealsBox->addItem($Crm->Ui()->DealCardFrame($dealClassification->deal()));
            }
        }
        $dealsTitle->setText($nbDeals . ' ' . $Crm->translate('deals'));



        $contactClassificationSet = $Crm->ContactClassificationSet();
        $contactClassificationSet->join('contact');

        $contactClassifications = $contactClassificationSet->select($contactClassificationSet->classification->in($allClassificationIds));
        $contacts = array();

        $nbContacts = 0;
        $contactsBox =  $W->VBoxLayout()
                ->setCanvasOptions(Widget_Item::Options()->minWidth(20, 'em'))
                ->setVerticalSpacing(2, 'em');
        $contactsBox->addItem($contactsTitle = $W->Title('', 3));
        foreach ($contactClassifications as $contactClassification) {
            if (!array_key_exists($contactClassification->contact->id, $contacts)) {
                $nbContacts++;
                $contacts[$contactClassification->contact->id] = $contactClassification->contact->id;
                $contactsBox->addItem($Crm->Ui()->ContactCardFrame($contactClassification->contact(), crm_ContactCardFrame::SHOW_ALL & ~(crm_ContactCardFrame::SHOW_ADDRESS | crm_ContactCardFrame::SHOW_EMAIL | crm_ContactCardFrame::SHOW_PHONE)));
            }
        }
        $contactsTitle->setText($nbContacts . ' ' . $Crm->translate('contacts'));


        $organizationClassificationSet = $Crm->OrganizationClassificationSet();
        $organizationClassificationSet->join('organization');

        $organizationClassifications = $organizationClassificationSet->select($organizationClassificationSet->classification->in($allClassificationIds));
        $organizations = array();

        $nbOrganizations = 0;
        $organizationsBox =  $W->VBoxLayout()
                ->setCanvasOptions(Widget_Item::Options()->minWidth(20, 'em'))
                ->setVerticalSpacing(2, 'em');
        $organizationsBox->addItem($organizationsTitle = $W->Title('', 3));
        foreach ($organizationClassifications as $organizationClassification) {
            if (!array_key_exists($organizationClassification->organization->id, $organizations)) {
                $nbOrganizations++;
                $organizations[$organizationClassification->organization->id] = $organizationClassification->organization->id;
                $organizationsBox->addItem($Crm->Ui()->OrganizationCardFrame($organizationClassification->organization()));
            }
        }
        $organizationsTitle->setText($nbOrganizations . ' ' . $Crm->translate('organizations'));

        $page->addItem($W->Title(sprintf($Crm->translate('Delete classification: %s'), $classification->name), 1));

        $confirmForm = new crm_Editor($Crm);
        $confirmForm->setCancelAction($this->proxy()->Cancel());
        $confirmForm->setSaveAction($this->proxy()->deleteClassification(), $Crm->translate('Proceed with deletion'));
        $confirmForm->setHiddenValue('tg', bab_rp('tg'));
        $confirmForm->setHiddenValue('classification', $classification->id);
        $confirmForm->addClass('icon-left-48 icon-left icon-48x48');
        $frame = $W->VBoxItems(
            $confirmForm
        )->setVerticalSpacing(4, 'em');

        if ($nbDeals + $nbContacts + $nbOrganizations > 0) {
            $page->addItem($W->Title(sprintf($Crm->translate('%d elements are associated with this classification'), $nbDeals + $nbContacts + $nbOrganizations), 2));
            $confirmForm->addItem($W->Icon($Crm->translate('Deleting this classification will definitely remove it from all associated elements.'), Func_Icons::STATUS_DIALOG_WARNING));

            $elementsBox = $W->HBoxItems()->setHorizontalSpacing(4, 'em');
            if ($nbDeals > 0) {
                $elementsBox->addItem($dealsBox);
            }
            if ($nbContacts > 0) {
                $elementsBox->addItem($contactsBox);
            }
            if ($nbOrganizations > 0) {
                $elementsBox->addItem($organizationsBox);
            }
            $frame->addItem($elementsBox);
        } else {
            $page->addItem($W->Title($Crm->translate('This classification is not associated with any element'), 2));
        }
        $page->addItem($frame);

        return $page;
    }

    /**
     * Delete the specified classification and all its descendants.
     *
     * @param int	$classification
     * @return Widget_Action
     */
    public function deleteClassification($classification = null)
    {
        if (isset($classification)) {

            $Crm = $this->Crm();

            $dealClassificationSet = $Crm->DealClassificationSet();
            $organizationClassificationSet = $Crm->OrganizationClassificationSet();
            $contactClassificationSet = $Crm->ContactClassificationSet();

            // Delete links between deals and classification
            $dealClassificationSet->delete($dealClassificationSet->classification->is($classification));
            // Delete links between organizations and classification
            $organizationClassificationSet->delete($organizationClassificationSet->classification->is($classification));
            // Delete links between contacts and classification
            $contactClassificationSet->delete($contactClassificationSet->classification->is($classification));

            $classificationSet = $Crm->ClassificationSet();
            $classificationSet->delete($classificationSet->id->is($classification));
        }

        crm_redirect(crm_BreadCrumbs::last());
    }



    /**
     * @return Widget_Action
     */
    public function displayStatus()
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $Crm->Ui()->includeBase();
        $Crm->Ui()->includeStatus();



        crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayStatus(), $Crm->translate('Status'));

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->addItem($W->Title($Crm->translate('Edit status'), 1));

        $page->addItem(crm_statusEditor($Crm));

        $page->addContextItem($W->Title('', 5));

        return $page;
    }


    /**
     * @return Widget_Action
     */
    public function addStatus($parentStatus)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $Crm->Ui()->includeBase();
//		$Crm->Ui()->includeStatus();

        $statusSet = $Crm->StatusSet();



        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');


        $statusEditor = new crm_Editor($Crm);
        $statusEditor->setName('status');
        $statusEditor->setHiddenValue('tg', bab_rp('tg'));

        $parentStatus = $statusSet->get($parentStatus);
        if (isset($parentStatus->id)) {
            $statusEditor->setHiddenValue('status[id_parent]', $parentStatus->id);
        }
        $statusEditor->setSaveAction($Crm->Controller()->Admin()->saveStatus());
        $statusEditor->setCancelAction($Crm->Controller()->Admin()->cancel());
        $statusEditor->addItem($statusEditor->labelledField($Crm->translate('Text'), $W->LineEdit()->setSize(60), 'name'));

        $page->addItem($statusEditor);

        return $page;
    }




    /**
     * @return Widget_Action
     */
    public function editStatus($status)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $Crm->Ui()->includeBase();
        $Crm->Ui()->includeStatus();

        //		require_once dirname(__FILE__) . '/admin.ui.php';

        $statusSet = $Crm->StatusSet();

        $status = $statusSet->get($status);

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->addItem($W->Title($Crm->translate('Edit status'), 1));

        //require_once dirname(__FILE__) . '/task.ui.php';
        $statusEditor = new crm_Editor($Crm);
        $statusEditor->setName('status');
        $statusEditor->setHiddenValue('tg', bab_rp('tg'));
        if (isset($status->id)) {
            $statusEditor->setHiddenValue('status[id]', $status->id);
        }
        $statusEditor->setSaveAction($Crm->Controller()->Admin()->saveStatus());
        $statusEditor->setCancelAction($Crm->Controller()->Admin()->cancel());
        $statusEditor->addItem(
            $statusEditor->labelledField(
                $Crm->translate('Text'),
                $W->LineEdit()->setSize(60)->setValue($status->name),
                'name'
            )
        );

        $radioMenu = $W->RadioMenu()
            ->width(100)
            ->menuWidth(680)
            ->setValue($status->icon)
            ->addClass('icon-top-24 icon-top icon-24x24 widgets-radiomenu-multiple-items');



        bab_functionality::includefile('Icons');
        $iconReflector = new ReflectionClass('Func_Icons');
        $constants = array_flip($iconReflector->getConstants());
        bab_Sort::natcasesort($constants);
        foreach($constants as $key => $constantName) {
            if (substr($constantName, 0, 5) === 'ICON_') {
                continue;
            }
            $radioMenu->addOption($key, $W->Icon(''/* $constantName */, eval('return Func_Icons::'.$constantName.';')));
        }


        $statusEditor->addItem(
            $statusEditor->labelledField(
                $Crm->translate('Associated icon'),
                $radioMenu,
                'icon'
            )
        );

        $statusEditor->addItem(
            $statusEditor->labelledField(
                $Crm->translate('Associated color'),
                $W->ColorPicker()
                            ->setColorSpace('YUV')
                            ->setRules('V', 'U', 'Y')
                            ->setSliderValue(0.65)
                            ->setValue($status->color),
                'color'
            )
        );

        $page->addItem($statusEditor);

        return $page;
    }



    /**
     *
     * @param array	$entry ('category, 'text')
     * @return Widget_Action
     */
    public function saveStatus($status = null)
    {
        $Crm = $this->Crm();

        $statusSet = $Crm->StatusSet();

        if (isset($status['id'])) {
            $c = $statusSet->get($status['id']);
            $c->setFormInputValues($status);
            $c->save();
        } else if (isset($status['id_parent'])) {
            $c = $statusSet->newRecord();
            $c->setFormInputValues($status);
            $parent = $statusSet->get($status['id_parent']);
            $parent->appendChild($c);
        }


        crm_redirect(crm_BreadCrumbs::last());
    }


    /**
     * Delete the specified status and all its descendants.
     *
     * @param int	$status
     * @return Widget_Action
     */
    public function proposeDeleteStatus($status)
    {
        $Crm = $this->Crm();

        $Crm->Ui()->includeDeal();

        $Crm->includeStatusSet();
        $Crm->includeAddressSet();

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-detailed-info-frame');

        $status = crm_Status($status);


        $allStatusIds = array($status->id => $status->id);

        $descendants = $status->getDescendants();
        foreach ($descendants as $descendant) {
            $allStatusIds[$descendant->id] = $descendant->id;
        }



/*
        $dealStatusSet = $Crm->StatusSet();
//		$dealStatusSet->join('deal');

        $dealStatus = $dealStatusSet->select($dealStatusSet->status->in($allStatusIds));
        $deals = array();

/*
        $nbDeals = 0;
        $dealsBox =  $W->VBoxLayout()
            ->setCanvasOptions(Widget_Item::Options()->minWidth(20, 'em'))
            ->setVerticalSpacing(2, 'em');
        $dealsBox->addItem($dealsTitle = $W->Title('', 3));
        foreach ($dealStatus as $dealStatus) {
            if (!array_key_exists($dealStatus->deal->id, $deals)) {
                $nbDeals++;
                $deals[$dealStatus->deal->id] = $dealStatus->deal->id;
                $dealsBox->addItem(crm_dealShortPreview($dealStatus->deal()));
            }
        }
        $dealsTitle->setText($nbDeals . ' ' . $Crm->translate('deals'));

        $page->addItem($W->Title(sprintf($Crm->translate('Delete status: %s'), $status->name), 1));

        $confirmForm = new crm_Editor($Crm);
        $confirmForm->setCancelAction($this->proxy()->Cancel());
        $confirmForm->setSaveAction($this->proxy()->deleteStatus(), $Crm->translate('Proceed with deletion'));
        $confirmForm->setHiddenValue('tg', bab_rp('tg'));
        $confirmForm->setHiddenValue('status', $status->id);
        $confirmForm->addClass('icon-left-48 icon-left icon-48x48');
        $frame = $W->VBoxItems(
            $confirmForm
        )->setVerticalSpacing(4, 'em');

        if ($nbDeals > 0) {
            $page->addItem($W->Title(sprintf($Crm->translate('%d elements are associated with this status'), $nbDeals), 2));
            $confirmForm->addItem($W->Icon($Crm->translate('Deleting this status will definitely remove it from all associated elements.'), Func_Icons::STATUS_DIALOG_WARNING));

            $elementsBox = $W->HBoxItems()->setHorizontalSpacing(4, 'em');
            if ($nbDeals > 0) {
                $elementsBox->addItem($dealsBox);
            }
            $frame->addItem($elementsBox);
        } else {
            $page->addItem($W->Title($Crm->translate('This status is not associated with any element'), 2));
        }
        $page->addItem($frame);
*/
        return $page;
    }



    /**
     * Delete the specified status and all its descendants.
     *
     * @param int	$status
     * @return Widget_Action
     */
    public function deleteStatus($status = null)
    {
        if (isset($status)) {

            $Crm = $this->Crm();

            $statusSet = $Crm->StatusSet();
            $statusSet->delete($statusSet->id->is($status));
        }

        crm_redirect(crm_BreadCrumbs::last());
    }






    /**
     * @return Widget_Action
     */
    public function displayEmblemList($emblems = null)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $Crm->Ui()->includeBase();

        $Crm->Ui()->includeEmblem();


        crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayEmblemList($emblems), $Crm->translate('Emblems'));

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-list');

        $page->addItemMenu('list', $Crm->translate('Emblems'), $this->proxy()->displayEmblemList($emblems));
        $page->setCurrentItemMenu('list');


        $filter = isset($emblems['filter']) ? $emblems['filter'] : array();


        $emblemSet = $Crm->EmblemSet();

        if (isset($emblems['filter']['label'])) {
            $emblemSelection = $emblemSet->select(
                    $emblemSet->label->contains($emblems['filter']['label'])
                    ->_OR_($emblemSet->description->contains($emblems['filter']['label']))
            );
        } else {
            $emblemSelection = $emblemSet->select();
        }

        $emblemSelection
            ->orderAsc($emblemSet->label);

        $table = new crm_EmblemTableView();

        $availableColumns = array(
            widget_TableModelViewColumn('icon', ' ')
                ->setExportable(false)
                ->setSortable(false)
                ->addClass('widget-column-thin widget-column-center '. Func_Icons::ICON_TOP_32),
            widget_TableModelViewColumn($emblemSet->label, $Crm->translate('Label')),
            widget_TableModelViewColumn('__actions__', ' ')
                ->addClass('widget-column-thin widget-column-center')

//			widget_TableModelViewColumn('tags', $Crm->translate('Tags')),
        );

        $table->setDataSource($emblemSelection);
        $table->setAvailableColumns($availableColumns);
        $table->allowColumnSelection();

//		$table->addClass(Func_Icons::ICON_LEFT_16);

        $filterPanel = $table->filterPanel('emblems', $filter);

        $actionsFrame = $page->ActionsFrame();
        $page->addContextItem($actionsFrame);



        $emblemEditor = new crm_Editor($Crm);
        $emblemEditor->setName('emblem');
        $emblemEditor->setSelfPageHiddenFields();
        $emblemEditor->setSaveAction($Crm->Controller()->Admin()->saveEmblem());

        $radioMenu = $W->RadioMenu()
            ->width(100)
            ->menuWidth(680)
            ->addClass(Func_Icons::ICON_TOP_24 . ' widgets-radiomenu-multiple-items');



        bab_functionality::includefile('Icons');
        $iconReflector = new ReflectionClass('Func_Icons');
        $constants = array_flip($iconReflector->getConstants());
        bab_Sort::natcasesort($constants);
        foreach($constants as $key => $constantName) {
            if (substr($constantName, 0, 5) === 'ICON_') {
                continue;
            }
            $radioMenu->addOption($key, $W->Icon(''/* $constantName */, eval('return Func_Icons::'.$constantName.';')));
        }


        $emblemEditor->addItem(
            $emblemEditor->labelledField(
                $Crm->translate('Icon'),
                $radioMenu,
                'icon'
            )
        );
        $emblemEditor->addItem(
            $emblemEditor->labelledField(
                $Crm->translate('Label'),
                $W->LineEdit()->setSize(35),
                'label'
            )
        );
        $emblemEditor->addItem(
            $emblemEditor->labelledField(
                $Crm->translate('Description'),
                $W->TextEdit(),
                'description'
            )
        );

//		$addEmblem = $W->Accordions()->addClass('crm-action-accordion');
//		$addEmblem->addPanel($Crm->translate('Add a emblem'), $emblemEditor);

        $actionsFrame->addItem(
            $W->VboxItems(
                $W->Link(
                    $W->Icon($Crm->translate('Add an emblem'), Func_Icons::ACTIONS_LIST_ADD),
                    $Crm->Controller()->Admin()->displayEmblemList()
                )
                ->addClass('widget-instant-button'),
                $emblemEditor
                    ->setTitle($Crm->translate('Add an emblem'))
                    ->addClass('widget-instant-form')
            )->addClass('widget-instant-container')
        );

//		$actionsFrame->addItem($addEmblem);

        $page->addItem($filterPanel);

        return $page;
    }





    /**
     *
     * @param array	$emblem ('text')
     * @return Widget_Action
     */
    public function saveEmblem($emblem = null)
    {
        $Crm = $this->Crm();

        $emblemSet = $Crm->EmblemSet();

        if (isset($emblem['id'])) {

            // This is an update

            $editedEmblem = $emblemSet->get($emblem['id']);

            $sameEmblem = $emblemSet->get($emblemSet->label->is($emblem['label']));

            if (isset($sameEmblem) && $sameEmblem->id != $editedEmblem->id) {
                // When we update with an existing emblem, we use this existing emblem
                // to replace the one being edited
                $emblemSet->replace($editedEmblem->id, $sameEmblem->id);

                $emblemSet->delete($emblemSet->id->is($editedEmblem->id));
                crm_redirect(crm_BreadCrumbs::last(), $Crm->translate('The emblem has been replaced.'));
            }

        } else {

            // This is a creation
            $editedEmblem = $emblemSet->newRecord();

        }

        $editedEmblem->icon = $emblem['icon'];
        $editedEmblem->label = $emblem['label'];
        $editedEmblem->description = $emblem['description'];

        $editedEmblem->save();
        crm_redirect(crm_BreadCrumbs::last(), $Crm->translate('The emblem has been saved.'));
    }






    /**
     * @return Widget_Action
     */
    public function deleteEmblem($emblem)
    {
        $Crm = $this->Crm();

        $emblemSet = $Crm->EmblemSet();
        $emblemSet->delete($emblemSet->id->is($emblem));

        crm_redirect(crm_BreadCrumbs::last());
    }





    /**
     * Displays the administrator's interface for specifying
     * the users access rights according to their roles.
     *
     * @return Widget_Action
     */
    public function displayRightList()
    {
        /* @var $Crm Func_Crm */
        $Crm = $this->Crm();
        $W = crm_widgets();

        $Crm->includeRoleAccessSet();

        $Crm->Ui()->includeBase();

        $page = $Crm->Ui()->Page();

        $page->addClass('crm-page-editor');

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayRightList(), $Crm->translate('Access rights'));

        $page->addItem($W->Title($Crm->translate('Access rights'), 1));

        $rightsBox = $W->VBoxLayout();

        $editor = new crm_Editor($Crm, null, $rightsBox);
        $editor->setName('rights');
        $editor->setHiddenValue('tg', bab_rp('tg'));
        $editor->setSaveAction($Crm->Controller()->Admin()->saveRights());
        $editor->setCancelAction($Crm->Controller()->Admin()->cancel());

        $page->addItem($editor);

        $rights = array(
            'global' => array(
                'manage' => $Crm->translate('Manage')
            ),
            'contact' => array(
                'create' => $Crm->translate('Create'),
                'read' => $Crm->translate('View'),
                'update' => $Crm->translate('Edit'),
                'delete' => $Crm->translate('Delete')
            ),
            'organization' => array(
                'create' => $Crm->translate('Create'),
                'read' => $Crm->translate('View'),
                'update' => $Crm->translate('Edit'),
                'delete' => $Crm->translate('Delete')
            ),
            'deal' => array(
                'create' => $Crm->translate('Create'),
                'read' => $Crm->translate('View'),
                'update' => $Crm->translate('Edit'),
                'delete' => $Crm->translate('Delete')
            )
        );

        foreach ($rights as $elementType => $elementRights) {
            $grid = $W->GridLayout(count($rights) + 1);
            $grid->setSpacing(6, 'px');

            $grid->addItem($W->Label(''), 0, 0);
            $i = 1;
            foreach ($elementRights as $rightText) {
                $grid->addItem($W->Label($rightText), 0, $i++);
            }

            $roleSet = $Crm->RoleSet();

            $roles = $roleSet->select();
            $row = 1;
            foreach ($roles as $role) {
                $col = 0;
                $grid->addItem($W->Label($role->name), $row, $col++);
                foreach ($elementRights as $right => $rightText) {
                    if ($elementType === 'Administration') {
                        $grid->addItem($W->NamedContainer()->addItem(
                            $W->Select()->setName($role->id)
                                ->addOption(crm_RoleAccess::ACCESS_ALWAYS, $Crm->translate('Always'))
                                ->addOption(crm_RoleAccess::ACCESS_NEVER, $Crm->translate('Never'))
                                ->addOption(crm_RoleAccess::ACCESS_ORGANIZATION, $Crm->translate('Own organization'))
                            )->setName($right),
                            $row, $col++
                        );
                    } else {
                        $grid->addItem($W->NamedContainer()->addItem(
                            $W->Select()->setName($role->id)
                                ->addOption(crm_RoleAccess::ACCESS_ALWAYS, $Crm->translate('Always'))
                                ->addOption(crm_RoleAccess::ACCESS_NEVER, $Crm->translate('Never'))
                                ->addOption(crm_RoleAccess::ACCESS_OWN, $Crm->translate('Own'))
                                ->addOption(crm_RoleAccess::ACCESS_ORGANIZATION, $Crm->translate('Own organization'))
                                ->addOption(crm_RoleAccess::ACCESS_SECTOR, $Crm->translate('Own sector'))
                            )->setName($right),
                            $row, $col++
                        );
                    }
                }
                $row++;
            }

            $rightsBox->addItem(
                $W->Section(
                    $Crm->translate('_right_element_type_' . $elementType),
                    $grid
                )
                ->setName(strtolower($elementType))
                ->setFoldable(true)
            );
        }

        $roleAccessSet = $Crm->RoleAccessSet();
        $roleAccesses = $roleAccessSet->select();
        foreach	($roleAccesses as $roleAccess) {
            list($elementType, $action) = explode(':', $roleAccess->action);
            $editor->setValue(array('rights', $elementType, $action, $roleAccess->role), $roleAccess->criterion);
        }

        $page->displayHtml();

    }



    public function saveRights($rights = null)
    {
        $Crm = $this->Crm();
        $roleAccessSet = $Crm->RoleAccessSet();
        $roleAccessSet->delete();

        foreach ($rights as $elementType => $elementTypeRights) {
            foreach ($elementTypeRights as $action => $elementTypeActionRights) {
                foreach ($elementTypeActionRights as $roleId => $condition) {
                    /* @var $roleAccess crm_RoleAccess */
                    $roleAccess = $roleAccessSet->newRecord();
                    $roleAccess->role = $roleId;
                    $roleAccess->action = $elementType . ':' . $action;
                    $roleAccess->criterion = $condition;
                    $roleAccess->save();
                }
            }
        }
        crm_redirect(crm_BreadCrumbs::last(), $Crm->translate('Access rights have been updated.'));
    }




    /**
     * @return Widget_Action
     */
    public function displayEntryList($category, $texts = null)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $Crm->Ui()->includeBase();
        $Crm->Ui()->includeEntry();

//		require_once dirname(__FILE__) . '/admin.ui.php';

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayEntryList($category, $texts), $Crm->translate($category));

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-list');

        $page->addItemMenu('list', $Crm->translate($category), $this->proxy()->displayEntryList($category));
        $page->setCurrentItemMenu('list');


        $filter = isset($texts['filter']) ? $texts['filter'] : array();


        $entrySet = $Crm->EntrySet();

        $entrySelection = $entrySet->select(
            $entrySet->category->is($category)
                ->_AND_($entrySet->text->matchAllWords(isset($filter['text']) ? $filter['text'] : ''))
        );



        $entrySelection
            ->orderAsc($entrySet->checked)
            ->orderDesc($entrySet->modifiedOn);

        $visibleColumns = array(
            'text' => $Crm->translate('Text'),
            'modifiedOn' => $Crm->translate('Last modification'),
            '__actions__' => ''
        );


        $table = new crm_EntryTableView();
        $table->setDataSource($entrySelection);
        $table->setVisibleColumns($visibleColumns);


        $table->addClass(Func_Icons::ICON_LEFT_16);

//		$toolbar = new crm_Toolbar();
//		$toolbar->addButton($Crm->translate('Add text'), Func_Icons::ACTIONS_USER_GROUP_NEW, $this->proxy()->editEntry('Deal origins'));

//		$page->addItem($toolbar);

//		$filterPanel = crm_genericTableModelViewFilterPanel($table, $filter, 'texts');
        $filterPanel = $table->filterPanel('texts', $filter);

        $actionsFrame = $page->ActionsFrame();
        $page->addContextItem($actionsFrame);

        $entryEditor = new crm_Editor($Crm);
        $entryEditor->setName('entry');
        $entryEditor->setHiddenValue('entry[category]', $category);
        $entryEditor->setSelfPageHiddenFields();
        $entryEditor->setSaveAction($Crm->Controller()->Admin()->saveEntry());
        $entryEditor->addItem($entryEditor->labelledField($Crm->translate('Text'), $W->LineEdit()->setSize(35), 'text'));
        $entryEditor->addItem($entryEditor->labelledField($Crm->translate('Alias'), $W->LineEdit()->setSize(35), 'description'));

        $addTask = $W->Accordions()->addClass('crm-action-accordion');
        $addTask->addPanel($Crm->translate('Add an entry'), $entryEditor);

        $actionsFrame->addItem($addTask);

        $page->addItem($filterPanel);

        return $page;
    }


    /**
     *
     * @return Widget_Action
     */
    public function editEntry($entry)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $Crm->Ui()->includeBase();
        $Crm->Ui()->includeEntry();

//		require_once dirname(__FILE__) . '/admin.ui.php';

        $entrySet = $Crm->EntrySet();

        $entry = $entrySet->get($entry);

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');


        $entryEditor = new crm_Editor($Crm);
        $entryEditor->setName('entry');
        $entryEditor->setHiddenValue('tg', bab_rp('tg'));
        if (isset($entry->id)) {
            $entryEditor->setHiddenValue('entry[id]', $entry->id);
        }
        $entryEditor->setHiddenValue('entry[category]', $entry->category);
        $entryEditor->setSaveAction($Crm->Controller()->Admin()->saveEntry());
        $entryEditor->setCancelAction($Crm->Controller()->Admin()->cancel());
        $entryEditor->addItem($entryEditor->labelledField($Crm->translate('Text'), $W->LineEdit()->setSize(60)->setValue($entry->text), 'text'));
        $entryEditor->addItem($entryEditor->labelledField($Crm->translate('Alias'), $W->LineEdit()->setSize(60)->setValue($entry->description), 'description'));
        $entryEditor->addItem($entryEditor->labelledField($Crm->translate('Checked'), $W->CheckBox()->setValue($entry->checked), 'checked'));

        $page->addItem($entryEditor);

        return $page;
    }



    /**
     *
     * @param array	$entry ('category, 'text')
     * @return Widget_Action
     */
    public function saveEntry($entry = null)
    {
        $Crm = $this->Crm();

        $entrySet = $Crm->EntrySet();

        if (isset($entry['id'])) {
            $newEntry = $entrySet->get($entry['id']);
        } else {
            $newEntry = $entrySet->newRecord();
        }
        $oldText = $newEntry->text;

        $newEntry->category = $entry['category'];
        $newEntry->text = $entry['text'];
        $newEntry->description = $entry['description'];
        if (isset($entry['checked'])) {
            $newEntry->checked = $entry['checked'];
        }
        $newEntry->save();

        if (isset($entry['id']) && ($oldText !== $newEntry->text)) {

            switch ($entry['category']) {

                case crm_Entry::DEAL_ORIGINS:
                    $dealSet = $Crm->DealSet();
                    $deals = $dealSet->select($dealSet->origin->is($oldText));
                    foreach ($deals as $deal) {
                        $deal->origin = $newEntry->text;
                        $deal->save();
                    }
                    break;

                case crm_Entry::CONTACT_POSITIONS:
                    $contactSet = $Crm->ContactSet();
                    $contacts = $contactSet->select($contactSet->position->is($oldText));
                    foreach ($contacts as $contact) {
                        $contact->position = $newEntry->text;
                        $contact->save();
                    }
                    break;
            }
        }

        crm_redirect(crm_BreadCrumbs::last());
    }


    /**
     * @return Widget_Action
     */
    public function validateEntry($entry)
    {
        $Crm = $this->Crm();

        $entrySet = $Crm->EntrySet();
        $entry = $entrySet->get($entrySet->id->is($entry));
        $entry->checked = true;
        $entry->save();

        crm_redirect(crm_BreadCrumbs::last());
    }


    /**
     * @return Widget_Action
     */
    public function deleteEntry($entry)
    {
        $Crm = $this->Crm();

        $entrySet = $Crm->EntrySet();
        $entrySet->delete($entrySet->id->is($entry));

        crm_redirect(crm_BreadCrumbs::last());
    }




    /**
     * Displays a page to select and modify the base organization for the CRM.
     *
     * @return Widget_Action
     */
    public function editMainOrganization()
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();
        $Crm->Ui()->includeBase();
        $Crm->Ui()->includeOrganization();
        $Crm->includeOrganizationSet();

        $organization = $Crm->getMainOrganization();

        if (isset($organization)) {
            $organizationSet = $Crm->OrganizationSet();
            $organization = $organizationSet->get($organization);
        }

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->editMainOrganization(), $Crm->translate('Main organization'));

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->addItemMenu('organization', $Crm->translate('Main organization'), $this->proxy()->editMainOrganization());
        $page->setCurrentItemMenu('organization');

        $page->addItem($W->Title($Crm->translate('Main organization'), 1));


        $baseOrgEditor = new crm_Editor($Crm);
        $baseOrgEditor->colon();
        $baseOrgEditor->addItem(
            $baseOrgEditor->labelledField(
                $Crm->translate('Main organization'),
                $Crm->Ui()->SuggestOrganization()
                    ->setSize(40)
                    ->setMinChars(0),
                'organizationName'
            )
        );
        $baseOrgEditor->setHiddenValue('tg', bab_rp('tg'));
        $baseOrgEditor->setSaveAction($Crm->Controller()->Admin()->saveMainOrganization());
        $baseOrgEditor->setCancelAction($Crm->Controller()->Admin()->cancel());

        if (isset($organization)) {
            $baseOrgEditor->setValue(array('organizationName'), $organization->name);
            $orgChart = crm_orgChart($Crm, $organization->id);

            $page->addItem($orgChart->addClass('crm-simple-orgchart'));
        } else {
            $page->addItem(
                $W->Label($Crm->translate('No main organization defined yet.'))
                    ->addClass('crm-emptyframe-placeholder')
            );
        }


        $page->addContextItem($baseOrgEditor);


        return $page;
    }



    public function editContactDirectory()
    {
        $Crm = $this->Crm();
        $Ui = $Crm->Ui();

        $Crm->Ui()->includeOrganization();
        $Crm->includeOrganizationSet();

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->editContactDirectory(), $Crm->translate('Configure directory fields'));

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');


        $editor = $Ui->ContactDirectoryEditor();

        // load values


        $set = $Crm->ContactDirectoryMapSet();
        $res = $set->select();


        if (0 === $res->count())
        {

            $values = array(
                'contact' => array(
                    'Contact.firstname' 			=> 'givenname',
                    'Contact.lastname'				=> 'sn',
                    'Contact.phone'					=> 'btel',
                    'Contact.mobile'				=> 'mobile',
                    'Contact.fax'					=> 'bfax',
                    'Contact.email'					=> 'email',
                    'Contact.address.street'		=> 'bstreetaddress',
                    'Contact.address.postalCode'	=> 'bpostalcode',
                    'Contact.address.city'			=> 'bcity',
                    'Contact.address.country'		=> 'bcountry'
                ),
                'user' => array(
                    'givenname'			=> 'Contact.firstname',
                    'sn'				=> 'Contact.lastname',
                    'btel'				=> 'Contact.phone',
                    'mobile'			=> 'Contact.mobile',
                    'bfax'				=> 'Contact.fax',
                    'email'				=> 'Contact.email',
                    'bstreetaddress'	=> 'Contact.address.street',
                    'bpostalcode'		=> 'Contact.address.postalCode',
                    'bcity'				=> 'Contact.address.city',
                    'bcountry'			=> 'Contact.address.country'
                )
            );

        } else {

            $values = array();
        }


        foreach($res as $record)
        {

            switch($record->type)
            {
                case 'contact':
                    $formfield = $record->contactfield;
                    $selectfield = $record->userfield;
                    break;
                case 'user':
                    $formfield = $record->userfield;
                    $selectfield = $record->contactfield;
                    break;
            }

            $values[$record->type][$formfield] = $selectfield;
        }



        $editor->setValues(array('contactdirectory' => $values));


        $page->addItem($editor);


        return $page;
    }



    public function saveContactDirectory($contactdirectory = null)
    {
        $Crm = $this->Crm();
        $set = $Crm->ContactDirectoryMapSet();

        $set->delete();

        foreach($contactdirectory as $type => $arr)
        {
            foreach($arr as $formfield => $selectfield)
            {
                if (empty($selectfield))
                {
                    continue;
                }

                switch($type)
                {
                    case 'contact':
                        $contactfield = $formfield;
                        $userfield = $selectfield;
                        break;
                    case 'user':
                        $contactfield = $selectfield;
                        $userfield = $formfield;
                        break;

                    default:
                        throw new crm_SaveException('wrong type');
                }

                $record = $set->newRecord();
                $record->type = $type;
                $record->contactfield = $contactfield;
                $record->userfield = $userfield;
                $record->save();
            }
        }

        return true;
    }





    /**
     *
     * @return widget_Action
     */
    public function saveMainOrganization($organizationName = null)
    {
        if (!bab_isUserAdministrator()) {
            throw new crm_Exception('Access denied');
        }
        $Crm = $this->Crm();
        if (isset($organizationName) && $organizationName !== '') {
            $organizationSet = $Crm->OrganizationSet();
            $organization = $organizationSet->get($organizationSet->name->is($organizationName));
            if ($organization) {
                $Crm->setMainOrganization($organization->id);
            }
        }
        crm_redirect(crm_BreadCrumbs::last());
    }





    /**
     *
     * @return Widget_Action
     */
    public function displaySectorList($sectors = null)
    {
        $W = bab_Widgets();
        $Crm = $this->Crm();

        crm_BreadCrumbs::setCurrentPosition($this->proxy()->displaySectorList($sectors), $Crm->translate('Sectors'));

        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $toolbar = new crm_Toolbar();
        $page->addToolbar($toolbar);

        $page->addItem($W->Title($Crm->translate('Sectors'), 1));

        $page->addContextItem($W->Label(''));
        $newSectorForm = $W->Form()
                ->setLayout(
                    $W->FlowItems(
                        $W->LineEdit()->setName('sector'),
                        $W->SubmitButton()
                            ->setLabel( $Crm->translate('Create'))
                            ->setAction($this->proxy()->createSector())
                    )->setHorizontalSpacing(1, 'em')
                );
        $newSectorForm->setHiddenValue('tg', bab_rp('tg'));


        $toolbar->addInstantForm($newSectorForm, $Crm->translate('New sector'), Func_Icons::ACTIONS_FOLDER_NEW, $this->proxy()->createSector());


        $sectorsBox = $W->VBoxItems();

        $sectorSet = $Crm->SectorSet();
        $sectors = $sectorSet->select();
        $sectors->orderAsc($sectorSet->name);
        foreach ($sectors as $sector) {
            $sectorBox = $W->Section($sector->name,
                $W->VBoxItems()
                ->setVerticalSpacing(0.5, 'em')
            )->setFoldable(true);

            $sectorBox->addContextMenu('popup')
                ->addItem(
                    $W->Link($W->Icon($Crm->translate('Delete this sector'), Func_Icons::ACTIONS_EDIT_DELETE), $this->proxy()->deleteSector($sector->id))
                            ->setConfirmationMessage($Crm->translate('Are you sure you want to delete this sector?'))
                )
                ->addItem(
                    $W->Link($W->Icon($Crm->translate('Edit this sector'), Func_Icons::ACTIONS_DOCUMENT_EDIT), $this->proxy()->editSectorRules($sector->id))
                );

            $rules = json_decode($sector->rules, true);

            if (is_array($rules)) {
                foreach ($rules as $rule) {
                    $sectorBox->addItem(
                        $W->HBoxItems(
                            $W->Label($Crm->translate($rule['property'])),
                            $W->Label($Crm->translate($rule['condition'])),
                            $W->Label($rule['value'])
                        )->setHorizontalSpacing(1, 'em')
                    );
                }
            }




            $sectorsBox->addItem($sectorBox);

        }
        $page->addItem($sectorsBox);

        $page->displayHtml();
    }

    public function createSector($sector = null)
    {
        $Crm = $this->Crm();

        $sectorSet = $Crm->SectorSet();
        $newSector = $sectorSet->newRecord();
        $newSector->name = $sector;
        $newSector->save();

        crm_redirect(crm_BreadCrumbs::last(), $Crm->translate('The sector has been created.'));
    }




    public function deleteSector($sector)
    {
        $Crm = $this->Crm();
        $sectorSet = $Crm->SectorSet();
        $sector = $sectorSet->get($sector);
        $sectorSet->delete($sectorSet->id->is($sector->id));
        crm_redirect(crm_BreadCrumbs::last(), sprintf($Crm->translate('The sector \'%s\' has been deleted.'), $sector->name));
    }





    public function editSectorRules($sector)
    {
        $Crm = $this->Crm();
        $W = bab_Widgets();

        $sectorSet = $Crm->SectorSet();
        $sector = $sectorSet->get($sector);


        $Crm->Ui()->includeBase();
//		$Crm->Ui()->includeSector();


        $page = $Crm->Ui()->Page();
        $page->addClass('crm-page-editor');

        $page->addItem($W->Title($sector->name, 1));


        $ruleEditor = new crm_Editor($Crm, null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $ruleEditor
            ->setName('rules');

        $rules = json_decode($sector->rules, true);
        $nbRules = count($rules);

        for ($i = 0; $i < $nbRules + 1; $i++) {
            $ruleEditor->addItem(
                $W->Frame()
                ->setName('' . $i)
                ->setLayout(
                    $W->HBoxItems(
                        $ruleEditor->labelledField(
                            $Crm->translate('Property'),
                            $W->Select()
                                ->addOption('', '')
                                ->addOption('name', $Crm->translate('name'))
                                ->addOption('address/postalCode', $Crm->translate('address/postalCode'))
                                ->addOption('address/country/code', $Crm->translate('address/country/code'))
                                ->addOption('classifications', $Crm->translate('Classifications')),
                                'property'),
                        $ruleEditor->labelledField(
                            $Crm->translate('Condition'),
                            $W->Select()
                                ->addOption('', '')
                                ->addOption('contains', $Crm->translate('contains'))
                                ->addOption('startsWith', $Crm->translate('startsWith')),
                            'condition'),
                        $ruleEditor->labelledField($Crm->translate('Value'), $W->LineEdit(), 'value')
                    )->setHorizontalSpacing(1, 'em')
                )
            );
        }
        $ruleEditor
            ->setSaveAction($Crm->Controller()->Admin()->saveSectorRule())
            ->setCancelAction($Crm->Controller()->Admin()->cancel());
        $ruleEditor
            ->setHiddenValue('tg', bab_rp('tg'))
            ->setHiddenValue('sector', $sector->id);

        if (is_array($rules)) {
            $ruleEditor->setValues($rules, array('rules'));
        }

        $page->addItem($ruleEditor);

        return $page;
    }





    /**
     *
     * @param $sector
     * @param $rules
     */
    public function saveSectorRule($sector = null, $rules = null)
    {
        $Crm = $this->Crm();

        $sectorSet = $Crm->SectorSet();
        $sector = $sectorSet->get($sector);

        $jsonRules = array();
        foreach ($rules as $rule) {
            if ($rule['value'] !== '') {
                $jsonRules[] = $rule;
            }
        }
        $jsonRules = crm_json_encode($jsonRules);

        $sector->rules = $jsonRules;

        $sector->save();

        crm_redirect(crm_BreadCrumbs::last());
    }



    /**
     * Does nothing and return to the previous page.
     *
     * @return Widget_Action
     */
    public function cancel()
    {
        crm_redirect(crm_BreadCrumbs::last());
    }



    /**
     * Displays help on this page.
     *
     * @return Widget_Action
     */
    public function help()
    {

    }
}
