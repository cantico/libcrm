<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */






/**
 * This controller manages actions that can be performed on campaigns.
 */
class crm_CtrlCampaign extends crm_Controller
{

	/**
	 * Save the specified campaign
	 *
	 * @param int	$campaign
	 * @return Widget_Action
	 */
	public function save($campaign = null)
	{

		$W = bab_Widgets();

		$set = $this->Crm()->CampaignSet();
		if ($campaign) {
			$record = $set->get($campaign);
		} else {
			$record = $set->newRecord();
		}

		$values = $_POST + $_GET;

		$values['period_begin'] = $W->DatePicker()->getISODate($values['period_begin']);
		$values['period_end'] 	= $W->DatePicker()->getISODate($values['period_end']);

		$record->setValues($values);
		$record->save();

		crm_redirect($this->proxy()->displayList());
	}






	public function saveRecipientList($campaign = null, $recipient = null)
	{
		$this->saveRecipient($campaign, $recipient, 'list');
	}

	public function saveRecipientNext($campaign = null, $recipient = null)
	{
		$this->saveRecipient($campaign, $recipient, 'recipient');
	}


	/**
	 * Edit or add a contact into a campaign
	 *
	 * @param 	int		$campaign
	 * @param	int		$recipient
	 * @param	string	$next
	 *
	 * @return Widget_Action
	 */
	private function saveRecipient($campaign = null, $recipient = null, $next = null)
	{
		$CRecord = $this->Crm()->CampaignSet()->get($campaign);
		$C = $CRecord->getFromType();

		$set = $C->CampaignRecipientSet();

		$recipientRecord = $set->get($recipient);


		if ($C->haveOrganizationField() && bab_pp('organisationName')) {
			$OrgSet = $this->Crm()->OrganizationSet();

			if ($OrgRecord = $OrgSet->get($OrgSet->name->is(bab_pp('organisationName')))) {
				$recipientRecord->organization = $OrgRecord->id;
			}
		}

		if ($C->haveContactField() && bab_pp('contactName')) {
			if ($ContactRecord = $this->Crm()->ContactSet()->get($this->Crm()->ContactSet()->name->is(bab_pp('contactName')))) {
				$recipientRecord->contact = $ContactRecord->id;
			}
		}


		$C->saveRecipient($campaign, $recipientRecord);

		$nextTaskRecipient = $C->nextTaskRecipient($campaign);
		if (null === $nextTaskRecipient) {
			$next = 'list';
		}


		if ('list' === $next) {
			crm_redirect($this->proxy()->displayPopulate($campaign));
		}

		if ('recipient' === $next) {
			crm_redirect($this->proxy()->displayEditRecipient($campaign, $nextTaskRecipient));
		}

	}




	/**
	 * Export
	 * @param int	$campaign
	 * @return Widget_Action
	 */
	public function exportRecipients($campaign = null)
	{
		$C = $this->Crm()->CampaignSet()->get($campaign)->getFromType();
		$C->exportRecipients($campaign);
	}







	/**
	 * Deletes the specified campaign
	 *
	 * @param int	$campaign
	 * @return Widget_Action
	 */
	public function delete($campaign = null)
	{

		$set = $this->Crm()->CampaignSet();
		$key = $set->getPrimaryKey();

		$set->delete($set->$key->is($campaign));
		crm_redirect($this->proxy()->displayList());
	}

	/**
	 * remove recipient from campaign
	 *
	 * @return	int	$campaign
	 * @return	Widget_Action
	 */
	public function deleteRecipient($campaign = null, $recipient = null)
	{
		$C = $this->Crm()->CampaignSet()->get($campaign)->getFromType();

		$C->deleteRecipient($recipient);

		crm_redirect($this->proxy()->displayPopulate($campaign));
	}




	/**
	 * Does nothing and return to the previous page.
	 *
	 * @param array	$message
	 * @return Widget_Action
	 */
	public function cancel()
	{
		crm_redirect(crm_BreadCrumbs::last());
	}

}