<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * This controller manages actions that can be performed on tags.
 */
class crm_CtrlPackaging extends crm_Controller implements crm_ShopAdminCtrl
{
	
	/**
	 * @return Widget_Action
	 */
	public function displayList($packaging = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin())
		{
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->displayList($packaging), $Crm->translate('Packaging list'));
		
		$page = $Crm->Ui()->Page();
		$page->addClass('crm-page-list');
		
		
		$page->setTitle($Crm->translate('Packaging configuration'));
		
		
		$filter = isset($packaging['filter']) ? $packaging['filter'] : array();
		
		$set = $Crm->PackagingSet();

		$table = $Crm->Ui()->PackagingTableView();
		
		$table->addColumn(widget_TableModelViewColumn('_edit_', '')->setSortable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));
		$table->addColumn(widget_TableModelViewColumn($set->name, $Crm->translate('Name')));
		
		
		$criteria = $table->getFilterCriteria($filter);
		$table->setDataSource($set->select($criteria));
		
		$table->addClass(Func_Icons::ICON_LEFT_16);
		
		$filterPanel = $table->filterPanel($filter, 'packaging');
		$page->addItem($filterPanel);
		
		$toolbar = new crm_Toolbar();
		$toolbar->addButton($Crm->translate('Add a packaging unit'), Func_Icons::ACTIONS_LIST_ADD, $this->proxy()->edit());
		
		$page->addToolbar($toolbar);
		
		return $page;
	}
	
	/**
	 * @return Widget_Action
	 */
	public function display()
	{
		// Not used
	}
	
	/**
	 * @return Widget_Action
	 */
	public function edit($packaging = null)
	{
		$W = bab_Widgets();
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		crm_BreadCrumbs::setCurrentPosition($this->proxy()->edit($packaging), $Crm->translate('Edit packaging unit'));
		
		/* @var $Ui crm_Ui */
		$Ui = $Crm->Ui();
		$page = $Ui->Page();
		
		$page->addClass('crm-page-editor');
		$page->setTitle($Crm->translate('Edit packaging'));
		
		if (null !== $packaging)
		{
			$set = $Crm->PackagingSet();
			$packaging = $set->get($packaging);
		
		}
		
		
		
		$form = $Ui->PackagingEditor($packaging);
		
		$page->addItem($form);
		
		if ($packaging instanceof crm_Packaging)
		{
			$actionsFrame = $page->ActionsFrame();
			$page->addContextItem($actionsFrame);
			
			$actionsFrame->addItem($W->Link($W->Icon($Crm->translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE), $this->proxy()->delete($packaging->id)));
		}
		
		return $page;
	}
	
	/**
	 * @return Widget_Action
	 */
	public function save($packaging = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		$set = $Crm->PackagingSet();
		
		if (empty($packaging['id']))
		{
			$record = $set->newRecord();
		} else {
			$record = $set->get($packaging['id']);
			if (null == $record)
			{
				throw new crm_AccessException($Crm->translate('This packaging unit does not exists'));
			}
		}
		
		$record->setValues($packaging);
		
		$record->save();
		
		return true;
	}
	
	
	
	public function delete($packaging = null)
	{
		$Crm = $this->Crm();
		$access = $Crm->Access();
		
		if (!$access->viewShopAdmin()) {
			throw new crm_AccessException($Crm->translate('Access denied to online shop administration'));
		}
		
		if (!$packaging)
		{
			throw new crm_AccessException($Crm->translate('Access denied'));
		}
		
		$set = $Crm->PackagingSet();
		$set->delete($set->id->is($packaging));
		
		crm_redirect($this->proxy()->displayList());
	}
}
