<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



class crm_MyContactNotify extends crm_NotifyMessage
{
	protected $contact;
	
	public function __construct(Func_Crm $Crm, crm_Contact $contact)
	{
		$this->contact = $contact;
		parent::__construct($Crm);
	}
	
	

	
}



class crm_MyContact_createAccount extends crm_MyContactNotify
{
	
	/**
	 * @var string
	 */
	protected $sitename;
	
	/**
	 * 
	 * @var string
	 */
	protected $password;
	
	
	public function __construct(Func_Crm $Crm, crm_Contact $contact, $password)
	{
		parent::__construct($Crm, $contact);
		global $babUrl;

		$this->password = $password;

		$this->addContactRecipient($contact);
		$this->sitename = $_SERVER['HTTP_HOST'];
		$this->setSubject(sprintf($Crm->translate('Registration on site %s'), $this->sitename));
		
		$this->setBody($this->prepareBody($password));
	}
	
	
	/**
	 * Build body text
	 * @param unknown_type $password
	 */
	protected function prepareBody($password)
	{
		$Crm = $this->Crm();
		$contact = $this->contact;
		
		$line1 = sprintf($Crm->translate('Your registration on site %s has been recorded.'), $this->sitename);
		if ($Crm->loginByEmail)
		{
			$line2 = sprintf($Crm->translate('Login ID / Email: %s'), $contact->email);
		} else {
			$line2 = sprintf($Crm->translate('Login ID: %s'), bab_getUserNickname($contact->user));
		}
		
		$body = bab_toHtml($line1, BAB_HTML_ALL);
		$body .= bab_toHtml($line2, BAB_HTML_ALL);
		
		if ($Crm->sendPassword) {
		    $line3 = sprintf($Crm->translate('Password: %s'), $password);
		    $body .= bab_toHtml($line3, BAB_HTML_ALL);
		}
		
		return $body;
	}
	
	
	
	/**
	 * Save the email to database
	 * obfuscate the password into save email
	 * 
	 * @param crm_Email $email
	 */
	protected function save(crm_Email $email)
	{
		$hidden_password = str_repeat('*' , mb_strlen($this->password));
		$email->body = $this->prepareBody($hidden_password);
		parent::save($email);
		$email->body = $this->prepareBody($this->password); // restore correct body for sending
	}

}




class crm_MyContact_newPassword extends crm_MyContact_createAccount
{



	public function __construct(Func_Crm $Crm, crm_Contact $contact, $password)
	{
		parent::__construct($Crm, $contact, $password);
		global $babUrl;

		$this->setSubject(sprintf($Crm->translate('New password for %s'), $this->sitename));
	}


	/**
	 * Build body text
	 * @param unknown_type $password
	 */
	protected function prepareBody($password)
	{
		$Crm = $this->Crm();
		$contact = $this->contact;

		$line1 = sprintf($Crm->translate('Your password to access site %s has been modified.'), $this->sitename);
		if ($Crm->loginByEmail)
		{
			$line2 = sprintf($Crm->translate('Login ID / Email: %s'), $contact->email);
		} else {
			$line2 = sprintf($Crm->translate('Login ID: %s'), bab_getUserNickname($contact->user));
		}
		$line3 = sprintf($Crm->translate('Password: %s'), $password);


		$body = bab_toHtml($line1, BAB_HTML_ALL);
		$body .= bab_toHtml($line2, BAB_HTML_ALL);
		$body .= bab_toHtml($line3, BAB_HTML_ALL);

		return $body;
	}


	/**
	 * (non-PHPdoc)
	 * @see crm_NotifyMessage::getEmail()
	 */
	public function getEmail()
	{
		$email = parent::getEmail();
		
		if (isset($email)) {
			$email->fromaddress = $GLOBALS['babAdminEmail'];
			$email->fromname = $GLOBALS['babAdminName'];
		}

		return $email;
	}
}

