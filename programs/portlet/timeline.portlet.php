<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */








class crm_PortletDefinition_Timeline extends crm_PortletDefinition implements portlet_PortletDefinitionInterface
{

	/**
	 * (non-PHPdoc)
	 * @see portlet_PortletDefinitionInterface::getName()
	 * 
	 * @return string
	 */
	public function getName()
	{
		$Crm = $this->Crm();
		return $Crm->translate('Timeline');
	}


	public function getDescription()
	{
		$Crm = $this->Crm();
		return $Crm->translate('Timeline in CRM ' . $Crm->getAddonName());
	}

	/**
	* @return array
	*/
	public function getPreferenceFields()
	{
		$Crm = $this->Crm();
		
		$W = bab_Widgets();		

		$modesList = array(
			array('value' => 'user', 'label' => $Crm->translate('My tasks')),
			array('value' => 'all', 'label' => $Crm->translate('All tasks'))
		);
		
		$prefs = array(
			array('label' => $Crm->translate('Mode'), 'type' => 'list', 'name' => 'mode', 'options' => $modesList)
		);
		
		
		return $prefs;
	}
	
}




class crm_PortletUi_Timeline extends crm_PortletUi implements portlet_PortletInterface
{

	protected $delayedItem = null;
	
	/**
	 * @param Func_Crm $crm
	 */
	public function __construct(Func_Crm $crm)
	{
		$W = bab_Widgets();
		parent::__construct($crm);
		
		$this->setInheritedItem($W->Frame());
	}

	
	/**
	 * @param Widget_Canvas	$canvas
	 * @ignore
	 */
	public function display(Widget_Canvas $canvas)
	{
		$taskCtrl = $this->Crm()->Controller()->Task(false);
				
		if (method_exists($taskCtrl, 'timeline')) {
			if (empty($this->options['mode'])) {
				$this->options['mode'] = 'user';
			}
			$this->setInheritedItem($taskCtrl->timeline($this->options['mode']));
		}

		return parent::display($canvas);
	}
		
}