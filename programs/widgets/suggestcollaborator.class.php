<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/suggestcontact.class.php';




/**
 * Constructs a crm_SuggestCollaborator.
 *
 * @param string		$id			The item unique id.
 * @return crm_SuggestCollaborator
 */
function crm_SuggestCollaborator($id = null)
{
    return new crm_SuggestCollaborator($id);
}


/**
 * A crm_SuggestCollaborator
 */
class crm_SuggestCollaborator extends crm_SuggestContact
{

    /**
     * Set the widget used for specifying the organization
     *
     * @return 	crm_SuggestCollaborator
     */
    public function setRelatedOrganizations($orgs)
    {
        $this->relatedOrganizations = $orgs;

        return $this;
    }



    /**
     * Send suggestions
     */
    public function suggest()
    {
        $Crm = $this->Crm();
        if (false !== $keyword = $this->getSearchKeyword()) {
            $contactSet = $Crm->ContactSet();

            $criteria = $contactSet->any(
                $contactSet->lastname->startsWith($keyword),
                $contactSet->firstname->startsWith($keyword),
                $contactSet->firstname->concat(' ', $contactSet->lastname)->startsWith($keyword)
            );

            $criteria = $criteria->_AND_(
                $contactSet->isCollaborator()
            );

            if (isset($this->criteria)) {
                $criteria = $criteria->_AND_($this->criteria);
            }

            $contacts = $contactSet->select($criteria);
            $contacts->orderAsc($contactSet->lastname);


            $i = 0;
            foreach ($contacts as $contact) {
                $i++;
                if ($i > Widget_SuggestLineEdit::MAX) {
                    break;
                }

                $organization = $contact->getMainOrganization();
                $organizationName = $organization ? $organization->name : '';

                $this->addSuggestion(
                    $contact->id,
                    $contact->getFullName(),
                    $organizationName,
                    $organizationName
                );
            }

            $this->sendSuggestions();
        }
    }


    /**
     * {@inheritDoc}
     * @see Widget_SuggestLineEdit::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->suggest();
        return parent::display($canvas);
    }
}

