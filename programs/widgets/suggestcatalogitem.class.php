<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

$W = bab_Widgets();
$W->includePhpClass('Widget_SuggestLineEdit');






/**
 * Constructs a crm_SuggestCatalogItem.
 * Suggest the reference of a catalog item
 *
 * @param string		$id			The item unique id.
 * @return crm_SuggestCatalogItem
 */
function crm_SuggestCatalogItem($id = null)
{
    return new crm_SuggestCatalogItem($id);
}


/**
 * A crm_SuggestCatalogItem
 */
class crm_SuggestCatalogItem extends Widget_SuggestLineEdit implements Widget_Displayable_Interface, crm_Object_Interface
{

    private $crm = null;


    /**
     * Get Crm object
     * @return Func_Crm
     */
    public function Crm()
    {
        return $this->crm;
    }

    /**
     * Forces the Func_Crm object to which this object is 'linked'.
     *
     * @param Func_Crm	$crm
     * @return crm_SuggestCatalogItem
     */
    public function setCrm(Func_Crm $crm = null)
    {
        $this->crm = $crm;
        return $this;
    }



    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'crm-suggestcatalogitem';
        return $classes;
    }





    /**
     * Send suggestions
     */
    public function suggest()
    {
        $this->Crm()->includeArticleSet();
        $this->Crm()->includeCatalogSet();
        $set = $this->Crm()->CatalogItemSet();
        $set->join('article');
        $set->join('catalog');

        if (false !== $keyword = $this->getSearchKeyword()) {


            $criteria = $set->getAccessibleItems()->_AND_(
                $set->article->name->startsWith($keyword)->_OR_($set->article->reference->startsWith($keyword))
            );


            $catalogItems = $set->select($criteria);

            $i = 0;
            foreach ($catalogItems as $item) {
                /* @var $item crm_CatalogItem */

                $i++;
                if ($i > Widget_SuggestLineEdit::MAX) {
                    break;
                }

                // the suggest field use reference as value
                parent::addSuggestion(
                    $item->id,
                    $item->article->getReference(),
                    $item->article->getName()
                );
            }

            parent::sendSuggestions();
        }
    }




    public function display(Widget_Canvas $canvas) {

        $this->suggest();
        return parent::display($canvas);
    }

}

