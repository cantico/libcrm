<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

$W = bab_Widgets();
$W->includePhpClass('Widget_InputWidget');




/**
 * A crm_SuggestOrganization
 */
class crm_SuggestOrganization extends crm_UiObject implements Widget_AssociableLabel_Interface
{
    private $excludedBranch = array();

    protected $excludedOrganizationIds = null;

    private $organizationSet = null;

    private $criteria = null;


    /**
     * @param Func_Crm  $Crm
     * @param string    $id
     */
    public function __construct(Func_Crm $Crm, $id = null)
    {
        parent::__construct($Crm);
        // We simulate inheritance from Widget_VBoxLayout.
        $W = bab_Widgets();
        $this->setInheritedItem($W->SuggestLineEdit($id));
        $this->addClass('crm-suggestorganization');
    }


    /**
     * (non-PHPdoc)
     * @see Widget_AssociableLabel_Interface::setAssociatedLabel()
     */
    public function setAssociatedLabel(Widget_Label $widget = null)
    {
        $this->item->setAssociatedLabel($widget);
    }


    /**
     * Excludes the specified organization and all its descendants from the returned results.
     *
     * @return crm_SuggestOrganization
     */
    public function excludeDescendantsOf($organizationId)
    {
        $this->excludedBranch[$organizationId] = $organizationId;
        $this->excludedOrganizationIds = null;

        $this->setSuggestAction($this->Crm()->Controller()->Organization()->suggestParent($organizationId), 'search');
        return $this;
    }


    /**
     * Initializes the list of excluded organizations.
     *
     * @return void
     */
    protected function init()
    {
        if (isset($this->excludedOrganizationIds)) {
            return;
        }

        $excluded = array();
        foreach ($this->excludedBranch as $parentId) {
            $excluded[$parentId] = $parentId;
            $org = $this->Crm()->OrganizationSet()->get($parentId);
            $org->getDescendants($excluded);
        }
        $this->excludedOrganizationIds = $excluded;
    }



    /**
     *
     * @return crm_OrganizationSet
     */
    public function getOrganizationSet()
    {
        if (!isset($this->organizationSet)) {
            $this->organizationSet = $this->Crm()->OrganizationSet();
        }
        return $this->organizationSet;
    }


    /**
     * Specifies criteria that will be applied to suggested organizations.
     *
     * @return crm_SuggestContact
     */
    public function setCriteria(ORM_Criteria $criteria = null)
    {
        $this->criteria = $criteria;
        return $this;
    }


    /**
     * Send suggestions
     */
    public function suggest()
    {
        $this->init();

        if (false !== $keyword = $this->getSearchKeyword()) {

            $organizationSet = $this->getOrganizationSet();
            $organizationSet->address();

            // First display organizations with a name *starting* with the keyword
            $criteria = $organizationSet->name->startsWith($keyword)
                ->_AND_($organizationSet->id->notIn($this->excludedOrganizationIds));

            $suggestedIds = $this->addSuggestions($organizationSet, $criteria);
            $nbSuggestions = count($suggestedIds);
            if ($nbSuggestions < Widget_SuggestLineEdit::MAX) {
                // Then display organizations with a name *containing* the keyword
                $criteria = $organizationSet->name->contains($keyword)
                    ->_AND_($organizationSet->id->notIn($this->excludedOrganizationIds))
                    ->_AND_($organizationSet->id->notIn($suggestedIds));

                $this->addSuggestions($organizationSet, $criteria, $nbSuggestions);
            }

            $this->sendSuggestions();
        }
    }


    /**
     * @param crm_OrganizationSet   $organizationSet
     * @param ORM_Criterion         $criteria
     * @param int[]
     */
    private function addSuggestions($organizationSet, $criteria, $nbSuggestions = 0)
    {
        if (isset($this->criteria)) {
            $criteria = $criteria->_AND_($this->criteria);
        }

        $organizations = $organizationSet->select($criteria);

        $organizations->orderAsc($organizationSet->name);

        $suggestedIds = array();
        foreach ($organizations as $organization) {
            $nbSuggestions++;
            if ($nbSuggestions > Widget_SuggestLineEdit::MAX) {
                break;
            }

            $suggestedIds[$organization->id] = $organization->id;

            $this->addSuggestion(
                $organization->id,
                $organization->getName(),
                $organization->getSuggestInfos(),
                $organization->getSuggestInfos()
            );
        }

        return $suggestedIds;
     }




    /**
     * {@inheritDoc}
     * @see crm_UiObject::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->suggest();
        return $this->item->display($canvas);
    }


    /**
     * Set the value of the organization name from the id of this organization
     * If the organization id does not exist it does nothing
     *
     * @param   int     $id             The organization id
     * @return  crm_SuggestOrganization
     */
    public function setIdValue($id)
    {
        $organizationSet = $this->getOrganizationSet();
        $organization = $organizationSet->get($id);
        if ($organization) {
            $this->setValue($organization);
        } else {
            $this->setValue('');
        }

        return $this->item->setIdValue($id);
    }


    /**
     * Sets the value.
     *
     * @param mixed $value
     */
    public function setValue($value)
    {
        if ($value instanceof crm_Organization) {
            $this->item->setValue($value->name);
            $this->item->setIdValue($value->id);
        } elseif ($value == 0) {
            $this->item->setValue('');
        } else {
            $this->item->setValue($value);
        }
        return $this;
    }
}
