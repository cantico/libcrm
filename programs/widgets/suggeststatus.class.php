<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

$W = bab_Widgets();
$W->includePhpClass('Widget_SuggestLineEdit');






/**
 * Constructs a crm_SuggestStatus.
 *
 * @param string		$id			The item unique id.
 * @return crm_SuggestStatus
 */
function crm_SuggestStatus($id = null)
{
	return new crm_SuggestStatus($id);
}


/**
 * A crm_SuggestStatus
 */
class crm_SuggestStatus extends Widget_SuggestLineEdit implements Widget_Displayable_Interface
{
	private $crm = null;

	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'crm-Suggestdealstatus';
		return $classes;
	}


	/**
	 * Forces the Func_Crm object to which this object is 'linked'.
	 *
	 * @param Func_Crm	$crm
	 * @return crm_SuggestCatalogItem
	 */
	public function setCrm(Func_Crm $crm = null)
	{
		$this->crm = $crm;
		return $this;
	}


	/**
	 * Send suggestions
	 */
	public function suggest() {


		if (false !== $keyword = $this->getSearchKeyword()) {

			$Crm = $this->crm;

			$statusSet = $Crm->StatusSet();

			$statuses = $statusSet->select(
				$statusSet->name->contains($keyword)
			);

			$i = 0;
			foreach ($statuses as $status) {
				/* @var $status betom_Status */
				if (trim($status->name) === '') {
					continue;
				}

				$i++;
				if ($i > Widget_SuggestLineEdit::MAX) {
					break;
				}

				parent::addSuggestion(
					$status->id,
					$status->name,
					'',
					''
				);
			}

			parent::sendSuggestions();
		}
	}




	public function display(Widget_Canvas $canvas) {

		$this->suggest();
		return parent::display($canvas);
	}

}