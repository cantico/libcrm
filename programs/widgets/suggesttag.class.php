<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

$W = bab_Widgets();
$W->includePhpClass('Widget_SuggestLineEdit');






/**
 * Constructs a crm_SuggestTag.
 *
 * @param string		$id			The item unique id.
 * @return crm_SuggestTag
 */
function crm_SuggestTag($id = null)
{
	return new crm_SuggestTag($id);
}


/**
 * A crm_SuggestTag
 */
class crm_SuggestTag extends Widget_SuggestLineEdit implements Widget_Displayable_Interface
{

	private $category = null;


	/**
	 * @param Func_Crm $crm		The category to select from.
	 * @param string $id			The item unique id.
	 * @return Widget_LineEdit
	 */
	public function __construct($crm, $id = null)
	{
		$this->crm = $crm;
		parent::__construct($id);
		$this->setMinChars(0);
	}

	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'crm-suggesttag';
		return $classes;
	}





	/**
	 * Send suggestions
	 */
	public function suggest()
	{
		$Crm = $this->crm;

		$notChecked = $Crm->translate('Not checked');
		$checked = '';

		if (false !== $keyword = $this->getSearchKeyword()) {

			$tagSet = $Crm->TagSet();

			$entries = $tagSet->select(
				$tagSet->label->contains($keyword)
				->_OR_($tagSet->description->contains($keyword))
			);
			$entries->orderAsc($tagSet->label);

			$i = 0;
			foreach ($entries as $tag) {
				/* @var $label crm_Tag */

				$i++;
				if ($i > Widget_SuggestLineEdit::MAX) {
					break;
				}


				parent::addSuggestion(
					$tag->id,
					$tag->label,
					$tag->checked ? $checked : $notChecked,
					''
				);
			}

			parent::sendSuggestions();
		}
	}




	public function display(Widget_Canvas $canvas)
	{
		$this->suggest();
		return parent::display($canvas);
	}

}