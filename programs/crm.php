<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/define.php';


/**
 * Provides extensible functionalities to manage a CRM application.
 */
class Func_Crm extends bab_Functionality
{
    // 	const CLASS_PREFIX = 'crm_';
    // 	const CLASS_PREFIX_LENGTH = 4;

    public $addonPrefix;
    public $classPrefix;
    public $addonName;
    public $controllerTg;

    /**
     * If true, a clipboard interface is displayed on pages to allow copying and pasting of CRM elements.
     * @var bool
     */
    protected $hasClipboardManagement = false;

    /**
     * If true, the login forms, contact forms, registration forms must replace the nickname field by a email field
     * @var bool
     */
    public $loginByEmail = false;


    /**
     * Send the password in the registration email
     * @var bool
     */
    public $sendPassword = false;


    /**
     * Specify if the current CRM is an online shop
     * if set to true, the CRM will work with a back-office and a front office
     * a registration form and a shopping cart will be accessible
     *
     * @see crm_Controller::applySkin()
     *
     * @var bool
     */
    public $onlineShop = false;


    /**
     * Specify if the current CRM shop use delivery address
     * if set to true, the CRM will not include delivery field
     *
     * @var bool
     */
    public $noDelivery = false;


    /**
     * Use rewriting if possible
     * this varable is used for catalogItem
     * @var bool
     */
    public $rewriting = false;


    /**
     * List of disabled notifications
     * class names from tho notify folder
     * use this property to disable notification without create the Notifiy object in your addon
     */
    public $disabledNotifications = array();


    /**
     * @see __isset()
     * @var array
     */
    private $implementedObjects = null;



    /**
     * Current translation language or null for current user language.
     * @var string|null
     */
    protected $language = null;


    public function __construct()
    {
        $this->addonPrefix = 'crm';
        $this->addonName = 'LibCrm';

        $this->classPrefix = $this->addonPrefix . '_';
        $this->controllerTg = 'addon/' . $this->addonName . '/main';
    }


    /**
     * @return string
     *
     */
    public function getDescription()
    {
        return 'Provides a library of CRM objects.';
    }


    /**
     *
     * @return string
     */
    public function getSitemapRootTitle()
    {
    	$Crm = $this->Crm();

    	if ($this->onlineShop)
    	{
    		return $Crm->translate('Online shop');
    	}
    	return $Crm->translate('Crm');
    }


    /**
     * Get action to go to online shop home page
     * Default is site home page
     *
     * @return Widget_Action
     */
    public function getShopHomeAction() {
        return Widget_Action::fromUrl('?');
    }


    /**
     * Register myself as a functionality.
     *
     */
    public static function register()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
        $functionalities = new bab_functionalities();

        $addon = bab_getAddonInfosInstance('LibCrm');

        if (method_exists($addon, 'registerFunctionality')) {
            // 8.1.98
            $addon->registerFunctionality('Crm', 'crm.php');
        } else {
            $functionalities->registerClass(__CLASS__, $addon->getPhpPath().'crm.php');
        }
    }


    /**
     * Check validity of addons used in CRM
     * should be used before using a Crm object in a way it can forbid access to addon upgrade (ex OVML)
     * @return bool
     */
    public function isAddonsValid()
    {
    	static $v = null;

    	if (null === $v)
    	{
	    	$addon = bab_getAddonInfosInstance('LibCrm');
	    	if (!$addon->isInstalled() || !$addon->isValid())
	    	{
	    		$v = false;
	    		return $v;
	    	}

	    	$addon = bab_getAddonInfosInstance($this->getAddonName());
	    	if (!$addon->isInstalled() || !$addon->isValid() || $addon->getIniVersion() != $addon->getDbVersion())
	    	{
	    		$v = false;
	    		return $v;
	    	}

	    	$v = true;
    	}

    	return $v;
    }


    /**
     * Get the addon name of the current CRM
     * @return string
     */
	public function getAddonName()
	{
		// TODO get addon name with a better method
		list(,$addonname) = explode('/', $this->controllerTg);

		return $addonname;
	}


	public function getAddon()
	{
	    return bab_getAddonInfosInstance($this->getAddonName());
	}

    /**
     * Synchronize sql tables for all classes found in Crm object
     * using methods  'includeXxxxxClassName'
     * sychronize if the two correspond methods are met and the set classname match the prefix from parameter
     * do not synchronize if the set method has a VueSet suffix, in this case the table si juste a readonly vue
     *
     * @param	string	$prefix		tables and classes prefix
     * @return bab_synchronizeSql
     */
    public function synchronizeSql($prefix)
    {
        if (!$prefix) {
            return null;
        }

        require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
        $mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
        $sql = 'SET FOREIGN_KEY_CHECKS=0;
            ';

        foreach (get_class_methods($this) as $method) {

            if (substr($method, 0, 7) === 'include' && substr($method, -3) === 'Set') {
                $incl = $method;
                $classNameMethod = substr($method, strlen('include')) . 'ClassName';


                $classname = $this->$classNameMethod();

                if ($prefix === substr($classname, 0, strlen($prefix))
                    && 'Set' === substr($classname, -3)
                    && 'ViewSet' !== substr($classname, -7)
                ) {
                    if (method_exists($this, $incl)) {
                        $this->$incl();

                        $call = substr($classname, strlen($prefix));

                        if (class_exists($classname)) {

                            /* @var $set crm_RecordSet */
                            $set = $this->$call();
                            if (method_exists($set, 'useLang')) {
                                // This is necessary if the recordSet constructor uses a setLang().
                                // We need to revert to multilang fields before synchronizing.
                                $set->useLang(false);
                            }
                            $sql .= $mysqlbackend->setToSql($set) . "\n";
                        }
                    }
                }
            }
        }

        require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
        $synchronize = new bab_synchronizeSql();

        $synchronize->fromSqlString($sql);

        return $synchronize;
    }



    public function getSynchronizeSql($prefix)
    {
    	if (!$prefix) {
    		return null;
    	}

    	require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
    	$mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
    	$sql = '';

    	foreach (get_class_methods($this) as $method) {

    		if (substr($method, 0, strlen('include')) === 'include' && substr($method, -strlen('Set')) === 'Set') {
    			$incl = $method;
    			$classNameMethod = substr($method, strlen('include')) . 'ClassName';

    			$classname = $this->$classNameMethod();

    			if ($prefix === substr($classname, 0, strlen($prefix))
    					&& 'Set' === substr($classname, -3)
    					&& 'ViewSet' !== substr($classname, -6)
    			) {
    				if (method_exists($this, $incl)) {
    					$this->$incl();
    					$call = substr($classname, strlen($prefix));

    					if (class_exists($classname) && method_exists($this, $call)) {
    						$set = $this->$call();
    						$sql .= $mysqlbackend->setToSql($set) . "\n";
    					}
    				}
    			}
    		}
    	}

    	return $sql;
    }



    /**
     * Includes TracableSet class definition.
     * @deprecated
     */
    public function includeTracableSet()
    {
        require_once FUNC_CRM_SET_PATH . 'base.class.php';
    }

    /**
     * Includes TracableSet class definition.
     */
    public function includeTraceableSet()
    {
        require_once FUNC_CRM_SET_PATH . 'base.class.php';
    }



    /**
     * Includes LinkSet class definition.
     */
    public function includeLinkSet()
    {
        require_once FUNC_CRM_SET_PATH . 'link.class.php';
    }

    public function LinkClassName()
    {
        return 'crm_Link';
    }

    public function LinkSetClassName()
    {
        return $this->LinkClassName() . 'Set';
    }

    /**
     * @return crm_LinkSet
     */
    public function LinkSet()
    {
        $this->includeLinkSet();
        $className = $this->LinkSetClassName();
        $set = new $className($this);
        return $set;
    }



    /**
     * Includes LogSet class definition.
     */
    public function includeLogSet()
    {
        require_once FUNC_CRM_SET_PATH . 'log.class.php';
    }

    public function LogClassName()
    {
        return 'crm_Log';
    }
    public function LogSetClassName()
    {
        return $this->LogClassName() . 'Set';
    }

    /**
     * @return crm_LogSet
     */
    public function LogSet()
    {
        $this->includeLogSet();
        $className = $this->LogSetClassName();
        $set = new $className($this);
        return $set;
    }



    /**
     * Includes EntrySet class definition.
     */
    public function includeEntrySet()
    {
        require_once FUNC_CRM_SET_PATH . 'entry.class.php';
    }
    public function EntryClassName()
    {
        return  $this->classPrefix . 'Entry';
    }
    public function EntrySetClassName()
    {
        return $this->EntryClassName() . 'Set';
    }

    /**
     * @return crm_EntrySet
     */
    public function EntrySet()
    {
        $this->includeEntrySet();
        $className = $this->EntrySetClassName();
        return new $className($this);
    }




    /**
     * Includes ContactSet class definition.
     */
    public function includeContactSet()
    {
        require_once FUNC_CRM_SET_PATH . 'contact.class.php';
    }

    public function ContactClassName()
    {
        return $this->classPrefix . 'Contact';
    }
    public function ContactSetClassName()
    {
        return $this->ContactClassName() . 'Set';
    }

    /**
     * @return crm_ContactSet
     */
    public function ContactSet()
    {
        $this->includeContactSet();
        $className = $this->ContactSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes ContactClassificationSet class definition.
     */
    public function includeContactClassificationSet()
    {
        require_once FUNC_CRM_SET_PATH . 'contactclassification.class.php';
    }

    public function ContactClassificationClassName()
    {
        return $this->classPrefix . 'ContactClassification';
    }
    public function ContactClassificationSetClassName()
    {
        return $this->ContactClassificationClassName() . 'Set';
    }

    /**
     * @return crm_ContactClassificationSet
     */
    public function ContactClassificationSet()
    {
        $this->includeContactClassificationSet();
        $className = $this->ContactClassificationSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes OrganizationSet class definition.
     */
    public function includeOrganizationSet()
    {
        require_once FUNC_CRM_SET_PATH . 'organization.class.php';
    }

    public function OrganizationClassName()
    {
        return $this->classPrefix . 'Organization';
    }
    public function OrganizationSetClassName()
    {
        return $this->OrganizationClassName() . 'Set';
    }

    /**
     * @return crm_OrganizationSet
     */
    public function OrganizationSet()
    {
        $this->includeOrganizationSet();
        $className = $this->OrganizationSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes OrganizationClassificationSet class definition.
     */
    public function includeOrganizationClassificationSet()
    {
        require_once FUNC_CRM_SET_PATH . 'organizationclassification.class.php';
    }

    public function OrganizationClassificationClassName()
    {
        return $this->classPrefix . 'OrganizationClassification';
    }
    public function OrganizationClassificationSetClassName()
    {
        return $this->OrganizationClassificationClassName() . 'Set';
    }

    /**
     * @return crm_OrganizationClassificationSet
     */
    public function OrganizationClassificationSet()
    {
        $this->includeOrganizationClassificationSet();
        $className = $this->OrganizationClassificationSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes ContactOrganizationSet class definition.
     */
    public function includeContactOrganizationSet()
    {
        require_once FUNC_CRM_SET_PATH . 'contactorganization.class.php';
    }

    public function ContactOrganizationClassName()
    {
        return $this->classPrefix . 'ContactOrganization';
    }
    public function ContactOrganizationSetClassName()
    {
        return $this->ContactOrganizationClassName() . 'Set';
    }

    /**
     * @return crm_ContactOrganizationSet
     */
    public function ContactOrganizationSet()
    {
        $this->includeContactOrganizationSet();
        $className = $this->ContactOrganizationSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes ClassificationSet class definition.
     */
    public function includeClassificationSet()
    {
        require_once FUNC_CRM_SET_PATH . 'classification.class.php';
    }
    public function ClassificationClassName()
    {
        return $this->classPrefix . 'Classification';
    }
    public function ClassificationSetClassName()
    {
        return $this->ClassificationClassName() . 'Set';
    }

    /**
     * @return crm_ClassificationSet
     */
    public function ClassificationSet()
    {
        $this->includeClassificationSet();
        $className = $this->ClassificationSetClassName();
        $set = new $className($this);
        return $set;
    }



    /**
     * Includes ContactAcquaintanceSet class definition.
     */
    public function includeContactAcquaintanceSet()
    {
        require_once FUNC_CRM_SET_PATH . 'contactacquaintance.class.php';
    }
    public function ContactAcquaintanceClassName()
    {
        return $this->classPrefix . 'ContactAcquaintance';
    }
    public function ContactAcquaintanceSetClassName()
    {
        return $this->ContactAcquaintanceClassName() . 'Set';
    }

    /**
     * @return crm_ContactAcquaintanceSet
     */
    public function ContactAcquaintanceSet()
    {
        $this->includeContactAcquaintanceSet();
        $className = $this->ContactAcquaintanceSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes OrganizationTypeSet class definition.
     */
    public function includeOrganizationTypeSet()
    {
        require_once FUNC_CRM_SET_PATH . 'organizationtype.class.php';
    }

    public function OrganizationTypeClassName()
    {
        return $this->classPrefix . 'OrganizationType';
    }
    public function OrganizationTypeSetClassName()
    {
        return $this->OrganizationTypeClassName() . 'Set';
    }

    /**
     * @return crm_OrganizationTypeSet
     */
    public function OrganizationTypeSet()
    {
        $this->includeOrganizationTypeSet();
        $className = $this->OrganizationTypeSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes AddressSet class definition.
     */
    public function includeAddressSet()
    {
        require_once FUNC_CRM_SET_PATH . 'address.class.php';
    }

    public function AddressClassName()
    {
        return $this->classPrefix . 'Address';
    }
    public function AddressSetClassName()
    {
        return $this->AddressClassName() . 'Set';
    }
    /**
     * @return crm_AddressSet
     */
    public function AddressSet()
    {
        $this->includeAddressSet();
        $className = $this->AddressSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes CountrySet class definition.
     */
    public function includeCountrySet()
    {
        require_once FUNC_CRM_SET_PATH . 'country.class.php';
    }

    public function CountryClassName()
    {
        return $this->classPrefix . 'Country';
    }
    public function CountrySetClassName()
    {
        return $this->CountryClassName() . 'Set';
    }
    /**
     * @return crm_CountrySet
     */
    public function CountrySet()
    {
        $this->includeCountrySet();
        $className = $this->CountrySetClassName();
        $set = new $className($this);
        return $set;
    }






    /**
     * Includes CurrencySet class definition.
     */
    public function includeCurrencySet()
    {
        require_once FUNC_CRM_SET_PATH . 'currency.class.php';
    }

    public function CurrencyClassName()
    {
        return $this->classPrefix . 'Currency';
    }
    public function CurrencySetClassName()
    {
        return $this->CurrencyClassName() . 'Set';
    }
    /**
     * @return crm_CurrencySet
     */
    public function CurrencySet()
    {
        $this->includeCurrencySet();
        $className = $this->CurrencySetClassName();
        $set = new $className($this);
        return $set;
    }








    /**
     * Includes NoteSet class definition.
     */
    public function includeNoteSet()
    {
        require_once FUNC_CRM_SET_PATH . 'note.class.php';
    }

    public function NoteClassName()
    {
        return $this->classPrefix . 'Note';
    }
    public function NoteSetClassName()
    {
        return $this->NoteClassName() . 'Set';
    }
    /**
     * @return crm_NoteSet
     */
    public function NoteSet()
    {
        $this->includeNoteSet();
        $className = $this->NoteSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes CommentSet class definition.
     */
    public function includeCommentSet()
    {
    	require_once FUNC_CRM_SET_PATH . 'comment.class.php';
    }

    public function CommentClassName()
    {
    	return $this->classPrefix . 'Comment';
    }
    public function CommentSetClassName()
    {
    	return $this->CommentClassName() . 'Set';
    }
    /**
     * @return crm_CommentSet
     */
    public function CommentSet()
    {
    	$this->includeCommentSet();
    	$className = $this->CommentSetClassName();
    	$set = new $className($this);
    	return $set;
    }




    /**
     * Includes EmailSet class definition.
     */
    public function includeEmailSet()
    {
        require_once FUNC_CRM_SET_PATH . 'email.class.php';
    }

    public function EmailClassName()
    {
        return $this->classPrefix . 'Email';
    }
    public function EmailSetClassName()
    {
        return $this->EmailClassName() . 'Set';
    }
    /**
     * @return crm_EmailSet
     */
    public function EmailSet()
    {
        $this->includeEmailSet();
        $className = $this->EmailSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes TagSet class definition.
     */
    public function includeTagSet()
    {
        require_once FUNC_CRM_SET_PATH . 'tag.class.php';
    }

    public function TagClassName()
    {
        return $this->classPrefix . 'Tag';
    }
    public function TagSetClassName()
    {
        return $this->TagClassName() . 'Set';
    }
    /**
     * @return crm_TagSet
     */
    public function TagSet()
    {
        $this->includeTagSet();
        $className = $this->TagSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes EmblemSet class definition.
     */
    public function includeEmblemSet()
    {
        require_once FUNC_CRM_SET_PATH . 'emblem.class.php';
    }

    public function EmblemClassName()
    {
        return $this->classPrefix . 'Emblem';
    }
    public function EmblemSetClassName()
    {
        return $this->EmblemClassName() . 'Set';
    }
    /**
     * @return crm_EmblemSet
     */
    public function EmblemSet()
    {
        $this->includeEmblemSet();
        $className = $this->EmblemSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes TaskSet class definition.
     */
    public function includeTaskSet()
    {
        require_once FUNC_CRM_SET_PATH . 'task.class.php';
    }

    public function TaskClassName()
    {
        return $this->classPrefix . 'Task';
    }
    public function TaskSetClassName()
    {
        return $this->TaskClassName() . 'Set';
    }
    /**
     * @return crm_TaskSet
     */
    public function TaskSet()
    {
        $this->includeTaskSet();
        $className = $this->TaskSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes TeamSet class definition.
     */
    public function includeTeamSet()
    {
        require_once FUNC_CRM_SET_PATH . 'team.class.php';
    }

    public function TeamClassName()
    {
        return $this->classPrefix . 'Team';
    }
    public function TeamSetClassName()
    {
        return $this->TeamClassName() . 'Set';
    }
    /**
     * @return crm_TeamSet
     */
    public function TeamSet()
    {
        $this->includeTeamSet();
        $className = $this->TeamSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes TaskCategorySet class definition.
     */
    public function includeTaskCategorySet()
    {
        require_once FUNC_CRM_SET_PATH . 'taskcategory.class.php';
    }

    public function TaskCategoryClassName()
    {
        return $this->classPrefix . 'TaskCategory';
    }
    public function TaskCategorySetClassName()
    {
        return $this->TaskCategoryClassName() . 'Set';
    }
    /**
     * @return crm_TaskCategorySet
     */
    public function TaskCategorySet()
    {
        $this->includeTaskCategorySet();
        $className = $this->TaskCategorySetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes CatalogSet class definition.
     */
    public function includeCatalogSet()
    {
        require_once FUNC_CRM_SET_PATH . 'catalog.class.php';
    }

    public function CatalogClassName()
    {
        return $this->classPrefix . 'Catalog';
    }
    public function CatalogSetClassName()
    {
        return $this->CatalogClassName() . 'Set';
    }
    /**
     * @return crm_CatalogSet
     */
    public function CatalogSet()
    {
        $this->includeCatalogSet();
        $className = $this->CatalogSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes CatalogItemSet class definition.
     */
    public function includeCatalogItemSet()
    {
        require_once FUNC_CRM_SET_PATH . 'catalogitem.class.php';
    }

    public function CatalogItemClassName()
    {
        return $this->classPrefix . 'CatalogItem';
    }
    public function CatalogItemSetClassName()
    {
        return $this->CatalogItemClassName() . 'Set';
    }
    /**
     * @return crm_CatalogItemSet
     */
    public function CatalogItemSet()
    {
        $this->includeCatalogItemSet();
        $className = $this->CatalogItemSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes ArticleSet class definition.
     */
    public function includeArticleSet()
    {
        require_once FUNC_CRM_SET_PATH . 'article.class.php';
    }

    public function ArticleClassName()
    {
        return $this->classPrefix . 'Article';
    }
    public function ArticleSetClassName()
    {
        return $this->ArticleClassName() . 'Set';
    }
    /**
     * @return crm_ArticleSet
     */
    public function ArticleSet()
    {
        $this->includeArticleSet();
        $className = $this->ArticleSetClassName();
        $set = new $className($this);
        return $set;
    }






    /**
     * Includes ArticleTypeSet class definition.
     */
    public function includeArticleTypeSet()
    {
    	require_once FUNC_CRM_SET_PATH . 'articletype.class.php';
    }
    public function ArticleTypeClassName()
    {
    	return $this->classPrefix . 'ArticleType';
    }
    public function ArticleTypeSetClassName()
    {
    	return $this->ArticleTypeClassName() . 'Set';
    }
    /**
     * @return crm_ArticleTypeSet
     */
    public function ArticleTypeSet()
    {
    	$this->includeArticleTypeSet();
    	$className = $this->ArticleTypeSetClassName();
    	$set = new $className($this);
    	return $set;
    }




    /**
     * Includes ArticleAvailabilitySet class definition.
     */
    public function includeArticleAvailabilitySet()
    {
    	require_once FUNC_CRM_SET_PATH . 'articleavailability.class.php';
    }
    public function ArticleAvailabilityClassName()
    {
    	return $this->classPrefix . 'ArticleAvailability';
    }
    public function ArticleAvailabilitySetClassName()
    {
    	return $this->ArticleAvailabilityClassName() . 'Set';
    }
    /**
     * @return crm_ArticleAvailabilitySet
     */
    public function ArticleAvailabilitySet()
    {
    	$this->includeArticleAvailabilitySet();
    	$className = $this->ArticleAvailabilitySetClassName();
    	$set = new $className($this);
    	return $set;
    }



	public function includeArticleLinkSet()
	{
		require_once FUNC_CRM_SET_PATH.'articlelink.class.php';
	}
	public function ArticleLinkClassName()
	{
		return $this->classPrefix . 'ArticleLink';
	}
	public function ArticleLinkSetClassName()
	{
		return $this->ArticleLinkClassName() . 'Set';
	}
	/**
	 * @return crm_ArticleLinkSet
	 */
	public function ArticleLinkSet()
	{
		$this->includeArticleLinkSet();
		$className = $this->ArticleLinkSetClassName();
		$set = new $className($this);
		return $set;
	}






    /**
     * Includes ArticlePackagingSet class definition.
     */
    public function includeArticlePackagingSet()
    {
    	require_once FUNC_CRM_SET_PATH . 'articlepackaging.class.php';
    }
    public function ArticlePackagingClassName()
    {
    	return $this->classPrefix . 'ArticlePackaging';
    }
    public function ArticlePackagingSetClassName()
    {
    	return $this->ArticlePackagingClassName() . 'Set';
    }
    /**
     * @return crm_ArticlePackagingSet
     */
    public function ArticlePackagingSet()
    {
    	$this->includeArticlePackagingSet();
    	$className = $this->ArticlePackagingSetClassName();
    	$set = new $className($this);
    	return $set;
    }






    /**
     * Includes ArticlePrivateSellSet class definition.
     */
    public function includeArticlePrivateSellSet()
    {
    	require_once FUNC_CRM_SET_PATH . 'articleprivatesell.class.php';
    }
    public function ArticlePrivateSellClassName()
    {
    	return $this->classPrefix . 'ArticlePrivateSell';
    }
    public function ArticlePrivateSellSetClassName()
    {
    	return $this->ArticlePrivateSellClassName() . 'Set';
    }
    /**
     * @return crm_ArticlePrivateSellSet
     */
    public function ArticlePrivateSellSet()
    {
    	$this->includeArticlePrivateSellSet();
    	$className = $this->ArticlePrivateSellSetClassName();
    	$set = new $className($this);
    	return $set;
    }






    /**
     * Includes PrivateSellSet class definition.
     */
    public function includePrivateSellSet()
    {
    	require_once FUNC_CRM_SET_PATH . 'privatesell.class.php';
    }
    public function PrivateSellClassName()
    {
    	return $this->classPrefix . 'PrivateSell';
    }
    public function PrivateSellSetClassName()
    {
    	return $this->PrivateSellClassName() . 'Set';
    }
    /**
     * @return crm_PrivateSellSet
     */
    public function PrivateSellSet()
    {
    	$this->includePrivateSellSet();
    	$className = $this->PrivateSellSetClassName();
    	$set = new $className($this);
    	return $set;
    }







    /**
     * Includes PackagingSet class definition. (packaging of articles)
     */
    public function includePackagingSet()
    {
    	require_once FUNC_CRM_SET_PATH . 'packaging.class.php';
    }
    public function PackagingClassName()
    {
    	return $this->classPrefix . 'Packaging';
    }
    public function PackagingSetClassName()
    {
    	return $this->PackagingClassName() . 'Set';
    }
    /**
     * @return crm_PackagingSet
     */
    public function PackagingSet()
    {
    	$this->includePackagingSet();
    	$className = $this->PackagingSetClassName();
    	$set = new $className($this);
    	return $set;
    }






    /**
     * Includes CustomFieldSet class definition.
     */
    public function includeCustomFieldSet()
    {
        require_once FUNC_CRM_SET_PATH . 'customfield.class.php';
    }
    public function CustomFieldClassName()
    {
        return $this->classPrefix . 'CustomField';
    }
    public function CustomFieldSetClassName()
    {
        return $this->CustomFieldClassName() . 'Set';
    }
    /**
     * @return crm_CustomFieldSet
     */
    public function CustomFieldSet()
    {
        $this->includeCustomFieldSet();
        $className = $this->CustomFieldSetClassName();
        $set = new $className($this);
        return $set;
    }






    /**
     * Includes CustomSectionSet class definition.
     */
    public function includeCustomSectionSet()
    {
        require_once FUNC_CRM_SET_PATH . 'customsection.class.php';
    }
    public function CustomSectionClassName()
    {
        return $this->classPrefix . 'CustomSection';
    }
    public function CustomSectionSetClassName()
    {
        return $this->CustomSectionClassName() . 'Set';
    }
    /**
     * @return crm_CustomSectionSet
     */
    public function CustomSectionSet()
    {
        $this->includeCustomSectionSet();
        $className = $this->CustomSectionSetClassName();
        $set = new $className($this);
        return $set;
    }






    /**
     * Includes ShoppingCart class definition.
     */
    public function includeShoppingCartSet()
    {
        require_once FUNC_CRM_SET_PATH . 'shoppingcart.class.php';
    }

    public function ShoppingCartClassName()
    {
        return $this->classPrefix . 'ShoppingCart';
    }
    public function ShoppingCartSetClassName()
    {
        return $this->ShoppingCartClassName() . 'Set';
    }
    /**
     * @return crm_ShoppingCartSet
     */
    public function ShoppingCartSet()
    {
        $this->includeShoppingCartSet();
        $className = $this->ShoppingCartSetClassName();
        $set = new $className($this);
        return $set;
    }



    /**
     * Includes ShoppingCartItem class definition.
     */
    public function includeShoppingCartItemSet()
    {
        require_once FUNC_CRM_SET_PATH . 'shoppingcartitem.class.php';
    }

    public function ShoppingCartItemClassName()
    {
        return $this->classPrefix . 'ShoppingCartItem';
    }
    public function ShoppingCartItemSetClassName()
    {
        return $this->ShoppingCartItemClassName() . 'Set';
    }
    /**
     * @return crm_ShoppingCartItemSet
     */
    public function ShoppingCartItemSet()
    {
        $this->includeShoppingCartItemSet();
        $className = $this->ShoppingCartItemSetClassName();
        $set = new $className($this);
        return $set;
    }


    public function includeShoppingCartItemSourceInterface()
    {
    	require_once FUNC_CRM_SET_PATH . 'shoppingcartitemsource.class.php';
    }





    /**
     *
     */
    public function includeShippingScaleSet()
    {
    	require_once FUNC_CRM_SET_PATH . 'shippingscale.class.php';
    }

    public function ShippingScaleClassName()
    {
    	return $this->classPrefix . 'ShippingScale';
    }
    public function ShippingScaleSetClassName()
    {
    	return $this->ShippingScaleClassName() . 'Set';
    }
    /**
     * @return crm_ShippingScaleSet
     */
    public function ShippingScaleSet()
    {
    	$this->includeShippingScaleSet();
    	$className = $this->ShippingScaleSetClassName();
    	$set = new $className($this);
    	return $set;
    }




    /**
     * Includes Discount class definition.
     */
    public function includeDiscountSet()
    {
        require_once FUNC_CRM_SET_PATH . 'discount.class.php';
    }

    public function DiscountClassName()
    {
        return $this->classPrefix . 'Discount';
    }
    public function DiscountSetClassName()
    {
        return $this->DiscountClassName() . 'Set';
    }
    /**
     * @return crm_DiscountSet
     */
    public function DiscountSet()
    {
        $this->includeDiscountSet();
        $className = $this->DiscountSetClassName();
        $set = new $className($this);
        return $set;
    }





    /**
     * Includes Coupon class definition.
     */
    public function includeCouponSet()
    {
        require_once FUNC_CRM_SET_PATH . 'coupon.class.php';
    }

    public function CouponClassName()
    {
        return $this->classPrefix . 'Coupon';
    }
    public function CouponSetClassName()
    {
        return $this->CouponClassName() . 'Set';
    }
    /**
     * @return crm_CouponSet
     */
    public function CouponSet()
    {
        $this->includeCouponSet();
        $className = $this->CouponSetClassName();
        $set = new $className($this);
        return $set;
    }


    /**
     * Includes CouponUsage class definition.
     */
    public function includeCouponUsageSet()
    {
        require_once FUNC_CRM_SET_PATH . 'couponusage.class.php';
    }

    public function CouponUsageClassName()
    {
        return $this->classPrefix . 'CouponUsage';
    }
    public function CouponUsageSetClassName()
    {
        return $this->CouponUsageClassName() . 'Set';
    }
    /**
     * @return crm_CouponUsageSet
     */
    public function CouponUsageSet()
    {
        $this->includeCouponUsageSet();
        $className = $this->CouponUsageSetClassName();
        $set = new $className($this);
        return $set;
    }











    /**
     * Includes PaymentError class definition.
     */
    public function includePaymentErrorSet()
    {
    	require_once FUNC_CRM_SET_PATH . 'paymenterror.class.php';
    }

    public function PaymentErrorClassName()
    {
    	return $this->classPrefix . 'PaymentError';
    }
    public function PaymentErrorSetClassName()
    {
    	return $this->PaymentErrorClassName() . 'Set';
    }
    /**
     * @return crm_PaymentErrorSet
     */
    public function PaymentErrorSet()
    {
    	$this->includePaymentErrorSet();
    	$className = $this->PaymentErrorSetClassName();
    	$set = new $className($this);
    	return $set;
    }






    /**
     * Includes MailBox class definition.
     */
    public function includeMailBoxSet()
    {
    	require_once FUNC_CRM_SET_PATH . 'mailbox.class.php';
    }

    public function MailBoxClassName()
    {
    	return $this->classPrefix . 'MailBox';
    }
    public function MailBoxSetClassName()
    {
    	return $this->MailBoxClassName() . 'Set';
    }
    /**
     * @return crm_MailBoxSet
     */
    public function MailBoxSet()
    {
    	$this->includeMailBoxSet();
    	$className = $this->MailBoxSetClassName();
    	$set = new $className($this);
    	return $set;
    }




    /**
     * Includes DealSet class definition.
     */
    public function includeDealSet()
    {
        require_once FUNC_CRM_SET_PATH . 'deal.class.php';
    }

    public function DealClassName()
    {
        return $this->classPrefix . 'Deal';
    }
    public function DealSetClassName()
    {
        return $this->DealClassName() . 'Set';
    }

    /**
     * @return crm_DealSet
     */
    public function DealSet()
    {
        $this->includeDealSet();
        $className = $this->DealSetClassName();
        $set = new $className($this);
        return $set;
    }



    /**
     * Includes DealClassificationSet class definition.
     */
    public function includeDealClassificationSet()
    {
        require_once FUNC_CRM_SET_PATH . 'dealclassification.class.php';
    }

    public function DealClassificationClassName()
    {
        return $this->classPrefix . 'DealClassification';
    }
    public function DealClassificationSetClassName()
    {
        return $this->DealClassificationClassName() . 'Set';
    }

    /**
     * @return crm_DealClassificationSet
     */
    public function DealClassificationSet()
    {
        $this->includeDealClassificationSet();
        $className = $this->DealClassificationSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes StatusSet class definition.
     */
    public function includeStatusSet()
    {
        require_once FUNC_CRM_SET_PATH . 'status.class.php';
    }
    public function StatusClassName()
    {
        return $this->classPrefix . 'Status';
    }
    public function StatusSetClassName()
    {
        return $this->StatusClassName() . 'Set';
    }

    /**
     * @return crm_StatusSet
     */
    public function StatusSet()
    {
        $this->includeStatusSet();
        $className = $this->StatusSetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes DealStatusHistorySet class definition.
     */
    public function includeDealStatusHistorySet()
    {
        require_once FUNC_CRM_SET_PATH . 'dealstatushistory.class.php';
    }
    public function DealStatusHistoryClassName()
    {
        return $this->classPrefix . 'DealStatusHistory';
    }
    public function DealStatusHistorySetClassName()
    {
        return $this->DealStatusHistoryClassName() . 'Set';
    }

    /**
     * @return crm_DealStatusHistorySet
     */
    public function DealStatusHistorySet()
    {
        $this->includeDealStatusHistorySet();
        $className = $this->DealStatusHistorySetClassName();
        $set = new $className($this);
        return $set;
    }




    /**
     * Includes ImportSet class definition.
     */
    public function includeImportSet()
    {
        require_once FUNC_CRM_SET_PATH . 'import.class.php';
    }

    public function ImportClassName()
    {
        return $this->classPrefix . 'Import';
    }
    public function ImportSetClassName()
    {
        return $this->ImportClassName() . 'Set';
    }
    /**
     * @return crm_ImportSet
     */
    public function ImportSet()
    {
        $this->includeImportSet();
        $className = $this->ImportSetClassName();
        $set = new $className($this);
        return $set;
    }





    /**
     * Includes BankAccountSet class definition.
     */
    public function includeBankAccountSet()
    {
    	require_once FUNC_CRM_SET_PATH . 'bankaccount.class.php';
    }

    public function BankAccountClassName()
    {
    	return $this->classPrefix . 'BankAccount';
    }
    public function BankAccountSetClassName()
    {
    	return $this->BankAccountClassName() . 'Set';
    }

    /**
     * @return crm_BankAccountSet
     */
    public function BankAccountSet()
    {
    	$this->includeBankAccountSet();
    	$className = $this->BankAccountSetClassName();
    	$set = new $className($this);
    	return $set;
    }






    /**
     * Includes OrderSet class definition.
     */
    public function includeOrderSet()
    {
        require_once FUNC_CRM_SET_PATH . 'order.class.php';
    }

    public function OrderClassName()
    {
        return $this->classPrefix . 'Order';
    }
    public function OrderSetClassName()
    {
        return $this->OrderClassName() . 'Set';
    }

    /**
     * @return crm_OrderSet
     */
    public function OrderSet()
    {
        $this->includeOrderSet();
        $className = $this->OrderSetClassName();
        $set = new $className($this);
        return $set;
    }





    /**
     * Includes OrderItemSet class definition.
     * @see crm_Order
     */
    public function includeOrderItemSet()
    {
        require_once FUNC_CRM_SET_PATH . 'orderitem.class.php';
    }

    public function OrderItemClassName()
    {
        return $this->classPrefix . 'OrderItem';
    }
    public function OrderItemSetClassName()
    {
        return $this->OrderItemClassName() . 'Set';
    }

    /**
     * @return crm_OrderItemSet
     */
    public function OrderItemSet()
    {
        $this->includeOrderItemSet();
        $className = $this->OrderItemSetClassName();
        $set = new $className($this);
        return $set;
    }







    /**
     * Includes OrderTaxSet class definition.
     * @see crm_Order
     */
    public function includeOrderTaxSet()
    {
        require_once FUNC_CRM_SET_PATH . 'ordertax.class.php';
    }

    public function OrderTaxClassName()
    {
        return $this->classPrefix . 'OrderTax';
    }
    public function OrderTaxSetClassName()
    {
        return $this->OrderTaxClassName() . 'Set';
    }

    /**
     * @return crm_OrderTaxSet
     */
    public function OrderTaxSet()
    {
        $this->includeOrderTaxSet();
        $className = $this->OrderTaxSetClassName();
        $set = new $className($this);
        return $set;
    }





    /**
     * Includes SectorSet class definition.
     */
    public function includeSectorSet()
    {
        require_once FUNC_CRM_SET_PATH . 'sector.class.php';
    }
    public function SectorClassName()
    {
        return $this->classPrefix . 'Sector';
    }
    public function SectorSetClassName()
    {
        return $this->SectorClassName() . 'Set';
    }
    /**
     * @return crm_SectorSet
     */
    public function SectorSet()
    {
        $this->includeSectorSet();
        $className = $this->SectorSetClassName();
        $set = new $className($this);
        return $set;
    }





    /**
     * Includes EmailSignatureSet class definition.
     */
    public function includeEmailSignatureSet()
    {
        require_once FUNC_CRM_SET_PATH . 'emailsignature.class.php';
    }
    public function EmailSignatureClassName()
    {
        return $this->classPrefix . 'EmailSignature';
    }
    public function EmailSignatureSetClassName()
    {
        return $this->EmailSignatureClassName() . 'Set';
    }
    /**
     * @return crm_EmailSignatureSet
     */
    public function EmailSignatureSet()
    {
        $this->includeEmailSignatureSet();
        $className = $this->EmailSignatureSetClassName();
        $set = new $className($this);
        return $set;
    }





    /**
     * Includes RoleSet class definition.
     */
    public function includeRoleSet()
    {
        require_once FUNC_CRM_SET_PATH . 'role.class.php';
    }
    public function RoleClassName()
    {
        return $this->classPrefix . 'Role';
    }
    public function RoleSetClassName()
    {
        return $this->RoleClassName() . 'Set';
    }
    /**
     * @return crm_RoleSet
     */
    public function RoleSet()
    {
        $this->includeRoleSet();
        $className = $this->RoleSetClassName();
        $set = new $className($this);
        return $set;
    }



    /**
     * Includes ContactRoleSet class definition.
     */
    public function includeContactRoleSet()
    {
        require_once FUNC_CRM_SET_PATH . 'contactrole.class.php';
    }
    public function ContactRoleClassName()
    {
        return $this->classPrefix . 'ContactRole';
    }
    public function ContactRoleSetClassName()
    {
        return $this->ContactRoleClassName() . 'Set';
    }
    /**
     * @return crm_ContactRoleSet
     */
    public function ContactRoleSet()
    {
        $this->includeContactRoleSet();
        $className = $this->ContactRoleSetClassName();
        $set = new $className($this);
        return $set;
    }



    /**
     * Includes ContactSectorSet class definition.
     */
    public function includeContactSectorSet()
    {
        require_once FUNC_CRM_SET_PATH . 'contactsector.class.php';
    }

    public function ContactSectorClassName()
    {
        return $this->classPrefix . 'ContactSector';
    }
    public function ContactSectorSetClassName()
    {
        return $this->ContactSectorClassName() . 'Set';
    }
    /**
     * @return crm_ContactSectorSet
     */
    public function ContactSectorSet()
    {
        $this->includeContactSectorSet();
        $className = $this->ContactSectorSetClassName();
        $set = new $className($this);
        return $set;
    }


    /**
     * Includes OrganizationSectorSet class definition.
     */
    public function includeOrganizationSectorSet()
    {
        require_once FUNC_CRM_SET_PATH . 'organizationsector.class.php';
    }

    public function OrganizationSectorClassName()
    {
        return $this->classPrefix . 'OrganizationSector';
    }
    public function OrganizationSectorSetClassName()
    {
        return $this->OrganizationSectorClassName() . 'Set';
    }
    /**
     * @return crm_OrganizationSectorSet
     */
    public function OrganizationSectorSet()
    {
        $this->includeOrganizationSectorSet();
        $className = $this->OrganizationSectorSetClassName();
        $set = new $className($this);
        return $set;
    }


    /**
     * Includes RoleAccessSet class definition.
     */
    public function includeRoleAccessSet()
    {
        require_once FUNC_CRM_SET_PATH . 'roleaccess.class.php';
    }

    public function RoleAccessClassName()
    {
        return $this->classPrefix . 'RoleAccess';
    }
    public function RoleAccessSetClassName()
    {
        return $this->RoleAccessClassName() . 'Set';
    }
    /**
     * @return crm_RoleAccessSet
     */
    public function RoleAccessSet()
    {
        $this->includeRoleAccessSet();
        $className = $this->RoleAccessSetClassName();
        $set = new $className($this);
        return $set;
    }





    /**
     * Includes PasswordTokenSet class definition.
     */
    public function includePasswordTokenSet()
    {
    	require_once FUNC_CRM_SET_PATH . 'passwordtoken.class.php';
    }

    public function PasswordTokenClassName()
    {
    	return $this->classPrefix . 'PasswordToken';
    }
    public function PasswordTokenSetClassName()
    {
    	return $this->PasswordTokenClassName() . 'Set';
    }
    /**
     * @return crm_PasswordTokenSet
     */
    public function PasswordTokenSet()
    {
    	$this->includePasswordTokenSet();
    	$className = $this->PasswordTokenSetClassName();
    	$set = new $className($this);
    	return $set;
    }







    /**
     * Includes SearchCatalogSet class definition.
     */
    public function includeSearchCatalogSet()
    {
    	require_once FUNC_CRM_SET_PATH . 'searchcatalog.class.php';
    }

    public function SearchCatalogClassName()
    {
    	return $this->classPrefix . 'SearchCatalog';
    }
    public function SearchCatalogSetClassName()
    {
    	return $this->SearchCatalogClassName() . 'Set';
    }
    /**
     * @return crm_SearchCatalogSet
     */
    public function SearchCatalogSet()
    {
    	$this->includeSearchCatalogSet();
    	$className = $this->SearchCatalogSetClassName();
    	$set = new $className($this);
    	return $set;
    }





    /**
     * Includes SearchHistorySet class definition.
     */
    public function includeSearchHistorySet()
    {
    	require_once FUNC_CRM_SET_PATH . 'searchhistory.class.php';
    }

    public function SearchHistoryClassName()
    {
    	return $this->classPrefix . 'SearchHistory';
    }
    public function SearchHistorySetClassName()
    {
    	return $this->SearchHistoryClassName() . 'Set';
    }
    /**
     * @return crm_SearchHistorySet
     */
    public function SearchHistorySet()
    {
    	$this->includeSearchHistorySet();
    	$className = $this->SearchHistorySetClassName();
    	$set = new $className($this);
    	return $set;
    }





    /**
     * Includes VatSet class definition.
     */
    public function includeVatSet()
    {
    	require_once FUNC_CRM_SET_PATH . 'vat.class.php';
    }
    public function VatClassName()
    {
    	return $this->classPrefix . 'Vat';
    }
    public function VatSetClassName()
    {
    	return $this->VatClassName() . 'Set';
    }
    /**
     * @return crm_VatSet
     */
    public function VatSet()
    {
    	$this->includeVatSet();
    	$className = $this->VatSetClassName();
    	$set = new $className($this);
    	return $set;
    }






    /**
     * Includes ContactDirectoryMapSet class definition.
     */
    public function includeContactDirectoryMapSet()
    {
    	require_once FUNC_CRM_SET_PATH . 'contactdirectorymap.class.php';
    }
    public function ContactDirectoryMapClassName()
    {
    	return $this->classPrefix . 'ContactDirectoryMap';
    }
    public function ContactDirectoryMapSetClassName()
    {
    	return $this->ContactDirectoryMapClassName() . 'Set';
    }
    /**
     * @return crm_ContactDirectoryMapSet
     */
    public function ContactDirectoryMapSet()
    {
    	$this->includeContactDirectoryMapSet();
    	$className = $this->ContactDirectoryMapSetClassName();
    	$set = new $className($this);
    	return $set;
    }








    /**
     * Includes CollectStoreSet class definition.
     */
    public function includeCollectStoreSet()
    {
        require_once FUNC_CRM_SET_PATH . 'collectstore.class.php';
    }

    public function CollectStoreClassName()
    {
        return $this->classPrefix . 'CollectStore';
    }
    public function CollectStoreSetClassName()
    {
        return $this->CollectStoreClassName() . 'Set';
    }
    /**
     * @return crm_CollectStoreSet
     */
    public function CollectStoreSet()
    {
        $this->includeCollectStoreSet();
        $className = $this->CollectStoreSetClassName();
        $set = new $className($this);
        return $set;
    }







    public function includeShopTraceSet()
    {
    	require_once FUNC_CRM_SET_PATH . 'shoptrace.class.php';
    }

    public function ShopTraceClassName()
    {
    	return $this->classPrefix . 'ShopTrace';
    }

    public function ShopTraceSetClassName()
    {
    	return $this->ShopTraceClassName() . 'Set';
    }

    /**
     * @return crm_ShopTraceSet
     */
    public function ShopTraceSet()
    {
    	$this->includeShopTraceSet();
    	$className = $this->ShopTraceSetClassName();
    	$set = new $className($this);
    	return $set;
    }



    /**
     * Add a message to shop trace
     * @param string $message
     *
     * @return crm_ShopTrace | null
     */
    public function ShopLog($message, $createtrace = false)
    {
    	if (isset($this->ShopTrace))
    	{
    		$set = $this->ShopTraceSet();
    		return $set->log($message, $createtrace);
    	}

    	return null;
    }








    /**
     * Returns the crm_Record corresponding to the specified
     * reference $ref.
     *
     * @param string 	$ref	A reference string (e.g. Contact:12)
     * @return crm_Record	or null if no corresponding record is found.
     */
    public function getRecordByRef($ref)
    {
        $refParts = explode(':', $ref);
        if (count($refParts) !== 2) {
            return null;
        }
        list($classname, $id) = $refParts;
        $classSet = $classname . 'Set';
        $set = $this->$classSet();
        if (isset($set)) {
            return $set->get($id);
        }
        return null;
    }

    /**
     * Returns the reference corresponding to the specified
     * crm_Record $record (e.g. Contact:12 or Deal:125)
     *
     * @param crm_Record	$record
     * @return string
     */
    public function getRecordRef(crm_Record $record)
    {
        $fullClassName = get_class($record);
        list(, $className) = explode('_', $fullClassName);
        return $className . ':' . $record->id;
    }


    /**
     * Specifies the language to use for translation.
     *
     * If $language is null, the language is reset to
     * the current logged user language.
     *
     * @param string|null $language
     */
    function setTranslateLanguage($language)
    {
        $this->language = $language;
    }


    /**
     * Translates the string.
     *
     * @param string $str
     * @return string
     */
    public function translate($str)
    {
        if (!isset($this->language)) {
            $translation = bab_translate($str, 'LibCrm');
        } else {
            $translation = bab_translate($str, 'LibCrm', $this->language);
        }

        return $translation;
    }


    /**
     * Translates all the string in an array and returns a new array.
     *
     * @param array $arr
     * @return array
     */
    public function translateArray($arr)
    {
        $newarr = $arr;

        foreach ($newarr as &$str) {
            $str = $this->translate($str);
        }
        return $newarr;
    }



    /**
     * Returns a link for writting an email to the specified email address.
     *
     * @param string $addr
     * @param string $subject
     * @param string $body
     *
     * @return string
     */
    public function mailTo($addr, $subject = null, $body = null)
    {
        $mailTo = 'mailto:' . $addr;
        $parameters = array();
        if (isset($subject)) {
            $parameters[] = 'subject=' . $subject;
        }
        if (isset($body)) {
            $parameters[] = 'body=' . $body;
        }
        if (!empty($parameters)) {
            $mailTo .= '?' . implode('&', $parameters);
        }

        return $mailTo;
    }



    /**
     * Format a number for display
     *
     * @param	float | string	$number		Numeric value with decimal
     * @return string
     */
    public function numberFormat($number, $decimals = 2)
    {
        if (is_null($number)) {
            return '#,##';
        }

        $number = number_format($number, $decimals, ',', ' ');
        return str_replace(' ', $this->Ui()->Nbsp(), $number);
    }


    /**
     * Format a number with an optional unit.
     *
     * If the value is >= 1000 the value is shortened and the corresponding prexif (k or M) is used.
     *
     * @param number $number
     * @param string $unitSymbol    (For example $, m2, Wh)
     * @param int $decimals
     * @return string|mixed
     */
    public function shortFormatWithUnit($number, $unitSymbol = '', $decimals = 2)
    {
        if (is_null($number)) {
            return '#,##';
        }

        $prefix = '';
        if ($number >= 1000000) {
            $number /= 1000000;
            $prefix = 'M';
        } elseif ($number >= 1000) {
            $number /= 1000;
            $prefix = 'k';
        }

        $number = number_format($number, $decimals, ',', ' ');
        return str_replace(' ', $this->Ui()->Nbsp(), $number . ' ' . $prefix . $unitSymbol);
    }


    /**
     * Reformat a phone number in the specified format.
     *
     * @since 1.0.47
     *
     * @param string    $phone      The phone number to be formatted
     * @param int       $format     The format the phone number should be formatted into
     *
     * @return string               The formatted phone number
     */
    public function phoneNumberFormat($phone, $format = null)
    {
        $PhoneNumber = bab_Functionality::get('PhoneNumber');
        if ($PhoneNumber === false) {
            return $phone;
        }

        if (!isset($format)) {
            $format = bab_registry::get('/' . $this->addonName . '/numberFormat', $PhoneNumber->getDefaultFormat());
        }
        $phoneNumberUtil = $PhoneNumber->PhoneNumberUtil();

        try {
            $phoneNumber = $phoneNumberUtil->parse($phone, 'FR');
            $phone = $phoneNumberUtil->format($phoneNumber, $format);
        } catch (\libphonenumber\NumberParseException $e) {
        }

        return $phone;
    }



    /**
     * Export Set data as CSV.
     *
     * The function sends headers end does not return (exit).
     *
     * @param   ORM_RecordSet       $set
     * @param   Array               $export     columns to export
     * @param   string              $title      title for filename
     * @param   string              $encoding   format of the encoding of the export (utf-8 or iso-8859-15)
     *
     */
    public function exportSet(ORM_RecordSet $set, Array $export, $title, $encoding='utf-8')
    {
        require_once FUNC_CRM_PHP_PATH . '/export.php';

        crm_csvExport::exportSet($set, $export, $title, $encoding);
    }



    /**
     * Includes Controller class definition.
     */
    public function includeController()
    {
        require_once FUNC_CRM_PHP_PATH . '/controller.class.php';
    }


    /**
     * Includes RecordController class definition.
     */
    public function includeRecordController()
    {
        require_once FUNC_CRM_PHP_PATH . '/ctrl/record.ctrl.php';
    }


    /**
     * Instanciates the controller.
     *
     * @return crm_Controller
     */
    public function Controller()
    {
        $this->includeController();
        return bab_getInstance($this->classPrefix.'Controller')->setCrm($this);
    }


    /**
     * Instanciates a controller class.
     *
     * @return crm_Controller
     */
    public function ControllerProxy($className, $proxy = true)
    {
        $this->includeController();

        if ($proxy) {
            return crm_Controller::getProxyInstance($this, $className);
        }

        return new $className($this);
    }



    /**
     * Include class crm_Ui
     *
     */
    protected function includeUi()
    {
        require_once FUNC_CRM_UI_PATH . '/ui.class.php';
    }


    /**
     * The crm_Ui object propose an access to all ui files and ui objects (widgets)
     *
     * @return crm_Ui
     */
    public function Ui()
    {
        $this->includeUi();
        return bab_getInstance($this->classPrefix.'Ui')->setCrm($this);
    }


    /**
     * Include class crm_Notify
     *
     */
    protected function includeNotify()
    {
    	require_once FUNC_CRM_NOTIFY_PATH . '/notify.class.php';
    }


    /**
     * The crm_Notify object propose an access to all notifications
     *
     * @return crm_Notify
     */
    public function Notify()
    {
    	$this->includeNotify();
    	$notify = bab_getInstance('crm_Notify')->setCrm($this);
    	/*@var $notify crm_Notify */
    	$notify->includeBase();
    	return $notify;
    }



    protected function includeAccess()
    {
        require_once FUNC_CRM_PHP_PATH . '/access.class.php';
    }



    /**
     * The crm_Access object propose methods to test access rights
     * @return crm_Access
     */
    public function Access()
    {
        $this->includeAccess();
        $access = bab_getInstance($this->classPrefix.'Access')->setCrm($this);
        return $access;
    }






    protected function includeEvent()
    {
        require_once FUNC_CRM_PHP_PATH . '/event.class.php';
    }



    /**
     * The crm_Event object propose methods to add to event listener
     * @return crm_Event
     */
    public function Event()
    {
        $this->includeEvent();
        $event = bab_getInstance('crm_Event')->setCrm($this);
        return $event;
    }



    /**
     * Include class crm_Portlet
     *
     */
    protected function includePortlet()
    {
        require_once dirname(__FILE__).'/define.php';
    	require_once FUNC_CRM_PORTLET_PATH . '/portlet.class.php';
    }


    /**
     * The crm_Portlet object propose an access to all portlets
     *
     * @return crm_Portlet
     */
    public function Portlet()
    {
    	$this->includePortlet();
    	$portlet = bab_getInstance('crm_Portlet')->setCrm($this);
    	$portlet->includeBase();
    	return $portlet;
    }




    protected function includePayment()
    {
    	require_once FUNC_CRM_PHP_PATH . '/payment.class.php';
    }



    /**
     * The crm_Payment object propose methods to use payment functionality.
     *
     * @return crm_Payment
     */
    public function Payment()
    {
    	$this->includePayment();
    	return new crm_Payment($this);
    }


    /**
     *
     */
    public function includeFileAttachment()
    {
        require_once FUNC_CRM_PHP_PATH . '/fileattachment.class.php';
    }

    /**
     * @return crm_FileAttachment
     */
    public function FileAttachment(crm_Record $record, $pathname)
    {
        $this->includeFileAttachment();
        return new crm_FileAttachment($this, $record, $pathname);
    }




    /**
     * Get upload path
     * if the method return null, no upload functionality
     *
     * @return bab_Path
     */
    public function uploadPath()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
		return new bab_Path(bab_getAddonInfosInstance($this->getAddonName())->getUploadPath());
    }


    /**
     * get the contact of the logged user
     * @return crm_Contact
     */
    public function getMyContact()
    {
    	if (!bab_isUserLogged()) {
    		return null;
    	}

    	$set = $this->ContactSet();
    	$set->address();
    	if ($this->onlineShop && !$this->noDelivery)
    	{
    		$set->deliveryaddress();
    	}
    	$userId = bab_getUserId();
    	$contact = $set->get($set->user->is($userId));


    	return $contact;
    }


    /**
     * get the main organization of the logged in contact
     * @return crm_Organization
     */
    public function getMyOrganization()
    {
    	if (!bab_isUserLogged()) {
    		return null;
    	}

    	$set = $this->ContactOrganizationSet();
    	$set->contact();
    	$set->organization();

    	$co = $set->get(
    		$set->main->is(1)
    		->_AND_($set->contact->user->is($GLOBALS['BAB_SESS_USERID']))
    	);

    	if (null === $co)
    	{
    		return null;
    	}

    	return $co->organization;
    }


    /**
     * test if the logged user is a contact
     * @param	int		$id_user
     *
     * @return bool
     */
    public function isContactLogged()
    {
        if (!bab_isUserLogged()) {
            return false;
        }

        $set = $this->ContactSet();
        $contact = $set->get($set->user->is($GLOBALS['BAB_SESS_USERID']));

        $set->__destruct();

        return null !== $contact;
    }




    public function __call($name, $arguments)
    {
        switch (true) {


            case substr($name, -strlen('SetClassName')) === 'SetClassName':
                $setName = substr($name, 0, strlen($name) - strlen('ClassName'));
                return $this->classPrefix . $setName;

            case substr($name, -strlen('ClassName')) === 'ClassName':
                $recordName = substr($name, 0, strlen($name) - strlen('ClassName'));
                return $this->classPrefix . $recordName;

            case substr($name, 0, strlen('include')) === 'include' && substr($name, -strlen('Set')) === 'Set':
                $fileNameBase = strtolower(substr(substr($name, 0, strlen($name) - strlen('Set')), strlen('include')));
                require_once FUNC_CRM_SET_PATH . $fileNameBase . '.class.php';
                return;

            case substr($name, -strlen('Set')) === 'Set':
                $includeMethod = 'include' . $name;
                $this->$includeMethod();
                $setClassNameMethod = $name . 'ClassName';
                $className = $this->$setClassNameMethod();
                $set = new $className($this);
                return $set;

                //case method_exists($this, $name . 'Set'):
            default:
                $setName = $name . 'Set';
            $recordClassNameMethod = $name . 'ClassName';
            $recordClassName = $this->$recordClassNameMethod();
            if (isset($arguments[0])) {
                if ($arguments[0] instanceof $recordClassName) {
                    return $arguments[0];
                }
                $set = $this->$setName();
                return $set->get($arguments[0]);
            }
            return null;

            echo $name;
            $r = method_exists($this, $name . 'Set');
            var_dump($r);
            die;
        }
    }


    /**
     * Test if this CRM implementation handles the specified object class.
     *
     * The default is to consider that an object Xxxx implemented if the includeXxxxSet() method is
     * declared public on this CRM.
     *
     * @param	string	$objectClassName		CRM object name (eg. 'Contact' or 'CatalogItem')
     * @return bool
     */
    public function __isset($objectClassName)
    {
        if (null === $this->implementedObjects) {

        	$this->implementedObjects = array();

            $className = get_class($this);
            $rClass = new ReflectionClass($className);

            foreach ($rClass->getMethods(ReflectionMethod::IS_PUBLIC) as $m) {

                // We consider object Xxxx implemented if the includeXxxxSet() method is
                // declared public on this.

                if ($m->getDeclaringClass()->name !== $className) {
                    // The method is declared on an ancestor class.
                    continue;
                }

                if (substr($m->name, 0, 7) === 'include' && substr($m->name, -3) === 'Set') {
                    $this->implementedObjects[substr($m->name, 7, -3)] = 1;
                }
            }
        }

        return isset($this->implementedObjects[$objectClassName]);
    }




    /**
     * Create an empty set class and record class from a record name
     */
    public function createEmptySet($recordname)
    {
    	if (class_exists($this->classPrefix.$recordname))
    	{
    		return;
    	}

    	$set = 'class '.$this->classPrefix.$recordname.'Set extends crm_'.$recordname.'Set {}';
    	$record = 'class '.$this->classPrefix.$recordname.' extends crm_'.$recordname.' {}';

    	eval($set);
    	eval($record);
    }





    /**
     * Returns the CRM main organization.
     * This organization has a special role and represents the
     * organization running the CMS.
     *
     * @return string	The id of the main organization or null if not defined.
     */
    public function getMainOrganization()
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/' . $this->addonPrefix);
        $organizationId = $registry->getValue('mainOrganization', null);

        return $organizationId;
    }




    /**
     * Sets the CRM main organization.
     * This organization has a special role and represents the
     * organization running the CMS.
     *
     * @param int	$organizationId		The id of the main organization.
     */
    public function setMainOrganization($organizationId)
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/' . $this->addonPrefix);
        $registry->setKeyValue('mainOrganization', $organizationId);
    }




    /**
     * Adds a crm record in the specified user clipboard.
     *
     * @param crm_Record|string $crmRecord The crm record (or its reference)
     * 								   	   to add to the user clipboard.
     * @param int               $user      The user id (current user if not
     * 									   specified or null).
     *
     * @return Func_Crm
     */
    public function addToUserClipboard($crmRecord, $user = null)
    {
        if ($crmRecord instanceof crm_Record) {
            $ref = $crmRecord->getRef();
        } else {
            $ref = $crmRecord;
        }
        if (!isset($user)) {
            $user = $this->Access()->currentUser();
        }

        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/' . $this->addonPrefix . '/clipboards');

        $recordReferences = $registry->getValue($user, array());
        if (!isset($recordReferences[$ref])) {
            $recordReferences[$ref] = true;
            $registry->setKeyValue($user, $recordReferences);
        }

        return $this;
    }




    /**
     * Removes a crm record from the specified user clipboard.
     *
     * @param crm_Record|string $crmRecord The crm record (or its reference)
     * 									   to remove from the user clipboard.
     * @param int               $user      The user id (current user if not
     *                                     specified or null).
     *
     * @return Func_Crm
     */
    public function removeFromUserClipboard($crmRecord, $user = null)
    {
        if ($crmRecord instanceof crm_Record) {
            $ref = $crmRecord->getRef();
        } else {
            $ref = $crmRecord;
        }
        if (!isset($user)) {
            $user = $this->Access()->currentUser();
        }

        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/' . $this->addonPrefix . '/clipboards');

        $recordReferences = $registry->getValue($user, array());
        if (isset($recordReferences[$ref])) {
            unset($recordReferences[$ref]);
            $registry->setKeyValue($user, $recordReferences);
        }

        return $this;
    }



    /**
     * Method to override to define if an instance of crm
     * uses clipboard management.
     *
     * @return boolean If the Crm manages a clipboard.
     */
    public function hasClipboardManagement()
    {
        return $this->hasClipboardManagement;
    }

    /**
     * Returns the content specified user's clipboard.
     *
     * @param int               $user      The user id (current user if not
     *                                     specified or null).
     *
     * @return array
     */
    public function getUserClipboardContent($user = null)
    {
        if (!isset($user)) {
            $user = $this->Access()->currentUser();
        }
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/' . $this->addonPrefix . '/clipboards');
        $recordReferences = $registry->getValue($user, array());

        return $recordReferences;
    }





    /**
     * Set skin for online shop back-office
     */
    public function applyShopAdminTheme()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/skinincl.php';
        bab_skin::applyOnCurrentPage('theme_shopadmin', 'altblue.crushed.css');
    }
}

