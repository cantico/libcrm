<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/utilit.php';
require_once dirname(__FILE__).'/ui/base.ui.php';

bab_functionality::includefile('PortletBackend');





/**
 * Crm Portlet backend
 */
class Func_PortletBackend_Crm extends Func_PortletBackend
{
	public function getDescription()
	{
		return crm_translate('CRM');
	}


	public function select($category = null)
	{
		require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

		$func = new bab_functionalities();
		$crm_list = $func->getChildren('Crm');

		$return_list = array();

		foreach ($crm_list as $crm_pathname) {
			$Crm = @bab_functionality::get('Crm/'.$crm_pathname);
			/*@var $Crm Func_Crm */
			if (false !== $Crm && $portlet = $Crm->Portlet()) {
				$return_list += $portlet->select($category);
			}
		}

		return $return_list;
	}


    /**
     * get a list of categories supported by the backend
     * @return Array
     */
    public function getCategories()
    {
        return array(
            'onlineshop_stats' => crm_translate('Online shop statistics'),
            'crm_stats' => crm_translate('CRM statistics')
        );
    }


	/**
	 * Get portlet definition instance
	 * @param	string	$portletId			portlet definition ID
	 *
	 * @return portlet_PortletDefinitionInterface
	 */
	public function getPortletDefinition($portletId)
	{
		// find the Crm coresponding to portlet definition ID


		list($crm_pathname, $portletConstructorMethod) = explode('/', $portletId);
		$Crm = @bab_functionality::get('Crm/'.$crm_pathname);

		if (false === $Crm)
		{
			bab_debug('No Crm found for portletId '.$portletId);
			return null;
		}

		$Portlet = $Crm->Portlet();

		if (method_exists($Portlet, $portletConstructorMethod)) {
			return $Portlet->$portletConstructorMethod();
		}

		return null;
	}



	/**
	 * (non-PHPdoc)
	 * @see Func_PortletBackend::getConfigurationActions()
	 */
	public function getConfigurationActions()
	{
		return array();
	}
}
