<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * 
 * @property ORM_StringField 	$contactfield
 * @property ORM_StringField 	$userfield
 * @property ORM_EnumField		$type
 *
 */
class crm_ContactDirectoryMapSet extends crm_RecordSet
{
	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);
	
		$this->setPrimaryKey('id');
	
		$this->addFields(
				ORM_StringField('contactfield'),
				ORM_StringField('userfield'),
				ORM_EnumField('type', self::getFormType())
		);
	}
	
	
	public static function getFormType()
	{
		return array(
			'contact' => crm_translate('Contact'),
			'user' => crm_translate('User directory entry')	
		);
	}
	
}


/**
 *
 * @property string 	$contactfield
 * @property string 	$userfield
 * @property string		$type
 *
 */
class crm_ContactDirectoryMap extends crm_Record
{
	
}