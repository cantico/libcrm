<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * A class used to store tags.
 *
 * @property    ORM_StringField    $label
 * @property    ORM_StringField    $description
 * @property    ORM_BoolField      $checked
 *
 * @method crm_Tag                  get(mixed $criteria)
 * @method crm_Tag                  request(mixed $criteria)
 * @method crm_Tag[]|\ORM_Iterator  select(\ORM_Criteria $criteria)
 * @method crm_Tag                  newRecord()
 *
 * @method Func_Crm Crm()
 */
class crm_TagSet extends crm_TraceableRecordSet
{
    /**
     * @param Func_Crm $Crm
     */
    public function __construct(Func_Crm $Crm = null)
    {
        parent::__construct($Crm);

        $Crm = $this->Crm();

        $this->setDescription('Tag');

        $this->addFields(
            ORM_StringField('label')
                ->setDescription('Label'),
            ORM_StringField('description')
                ->setDescription('Description'),
            ORM_BoolField('checked')
                ->setDescription('Checked')
                ->setOutputOptions($Crm->translate('No'), $Crm->translate('Yes'))
        );
    }


    /**
     * Returns an iterator of tags associated to the specified object,
     * optionally filtered on the specified link type.
     *
     * @return ORM_Iterator
     */
    public function selectFor(crm_Record $object)
    {
        return parent::selectLinkedTo($object, 'hasTag');
    }


    /**
     * Replaces all links to $oldTagId by links to $newTagId.
     *
     * @return crm_tagSet
     */
    public function replaceLinks($oldTagId, $newTagId, $linkType = 'hasTag')
    {
        $linkSet = $this->Crm()->LinkSet();

        $links = $linkSet->select(
            $linkSet->targetClass->is($this->Crm()->TagClassName())
               ->_AND_($linkSet->targetId->is($oldTagId))
               ->_AND_($linkSet->type->is($linkType))
        );

        foreach ($links as $link) {
            $link->targetId = $newTagId;
            $link->save();
        }

        return $this;
    }

    /**
     * @return ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }


    /**
     * @return ORM_Criteria
     */
    public function isUpdatable()
    {
        if ($this->Crm()->Access()->administer()) {
            return $this->all();
        }
        return $this->none();
    }

    /**
     * @return ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }


    /**
     * {@inheritDoc}
     * @see crm_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return $this->Crm()->Access()->administer();
    }
}



/**
 * @property    string     $label
 * @property    string     $description
 * @property    bool       $checked
 *
 * @method Func_Crm Crm()
 */
class crm_Tag extends crm_TraceableRecord
{
    /**
     * {@inheritDoc}
     * @see ORM_Record::getRecordTitle()
     */
    public function getRecordTitle()
    {
        return $this->label;
    }

    /**
     * {@inheritDoc}
     * @see crm_TraceableRecord::linkTo()
     */
    public function linkTo(crm_Record $object, $linkType = 'hasTag')
    {
        return parent::linkTo($object, $linkType);
    }


    /**
     * {@inheritDoc}
     * @see crm_TraceableRecord::unlinkFrom()
     */
    public function unlinkFrom(crm_Record $object, $linkType = 'hasTag')
    {
        return parent::unlinkFrom($object, $linkType);
    }


    /**
     * Replaces all links this tag by links to the one specified.
     *
     * @param crm_Tag|int	$tag	A tag object or id.
     *
     * @return crm_Tag
     */
    public function replaceBy($tag)
    {
        if ($tag instanceof crm_Tag) {
            $tag = $tag->id;
        }
        $this->getParentSet()->replaceLinks($this->id, $tag);
        return $this;
    }
}
