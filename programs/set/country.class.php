<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * A crm_Country
 *
 * @property ORM_PkField		$id
 * @property ORM_StringField	$code
 * @property ORM_StringField	$name_en
 * @property ORM_StringField	$name_fr
 */
class crm_CountrySet extends crm_RecordSet
{
	public function __construct($crm = null)
	{
		parent::__construct($crm);

		$this->setDescription('Country');
		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('code', 2)
					->setDescription('ISO code'),
			ORM_StringField('name_en', 255)
					->setDescription('Name (english)'),
			ORM_StringField('name_fr', 255)
					->setDescription('Name (french)')
		);
	}


	/**
	 *
	 *
	 * @return string
	 */
	public function getNameColumn()
	{

		$colname = 'name_en';
		if ('fr' === $GLOBALS['babLanguage']) {
			$colname = 'name_fr';
		}

		return $colname;
	}
	
	
	public function getNameField()
	{
		$name = $this->getNameColumn();
		return $this->$name;
	}


	/**
	 * Get countries as array
	 *
	 *
	 */
	public function getOptions()
	{
		//require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
		//bab_debug_print_backtrace();

		$colname = $this->getNameColumn();

		$options = array(0 => '');
		$iterator = $this->select()->orderAsc($this->$colname);

		foreach($iterator as $country) {
			$options[$country->id] = $country->$colname;
		}

		return $options;
	}


	/**
	 * populate database with geoNames data
	 *
	 * @return bool
	 */
	public function populateFromGeoNames()
	{
		$gn = @bab_functionality::get('GeoNames');
		if (!$gn) {
			return false;
		}

		$iterator = $gn->getCountryOrmSet()->select();

		foreach($iterator as $country) {
			$record = $this->newRecord();
			$record->geoNamesMap($country);
			$record->save();
		}

		return true;
	}
	
	
	/**
	 * Guess country from browser language
	 * @param	$country_code	Default country code to use if nothing found
	 * @return crm_Country | null
	 */
	public function guessCountryFromBrowser($country_code = null)
	{
		$langs = array();
		
		if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) 
		{
	  		preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);
	  		if (count($lang_parse[1]))
	  		{
		    	$langs = array_combine($lang_parse[1], $lang_parse[4]);
		    	foreach ($langs as $lang => $val)
		    	{
		      		if ($val === '') $langs[$lang] = 1;
		    	}
		    	arsort($langs, SORT_NUMERIC);
	  		}
	  	}

	  	// try to get a 2 char country code
	  	
	  	foreach($langs as $code => $dummy)
	  	{
	  		if (2 === strlen($code))
	  		{
	  			$country_code = mb_strtoupper($code);
	  			break;
	  		}
	  		  		
	  		if (false !== mb_strpos($code, '-'))
	  		{
	  			
	  			list($language, $country_code) = explode('-', $code);
	  			$country_code = mb_strtoupper($country_code);
	  			break;
	  		}
	  	}
	  	
	  	
	  	
	  	if (null !== $country_code)
	  	{
	  		return $this->get($this->code->is($country_code));
	  	}
	  	
	  	
	  	
	  	return null;
	}


}

/**
 * A crm_Country
 *
 * @property int				$id
 * @property string				$code
 * @property string				$name_fr
 * @property string				$name_en
 */
class crm_Country extends crm_Record
{
	/**
	 * overload default record title
	 * 
	 * (non-PHPdoc)
	 * @see ORM_Record::getRecordTitle()
	 */
	public function getRecordTitle()
	{
		return $this->getName();
	}
	
	
	/**
	 * Name of country according to language
	 *
	 * @return string
	 */
	public function getName()
	{
		$colname = 'name_en';

		if ('fr' === bab_getLanguage()) {
			$colname = 'name_fr';
		}

		return $this->$colname;
	}

	/**
	 * Map a geonames country into a CRM contry record
	 *
	 * @param	ORM_Record	$geoNameCountry
	 */
	public function geoNamesMap(ORM_Record $geoNameCountry)
	{
		$this->code = $geoNameCountry->iso;
		$this->name_en = $geoNameCountry->country_en;
		$this->name_fr = $geoNameCountry->country_fr;
	}
}
