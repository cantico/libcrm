<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */






/**
 * A crm_ContactSector associates a contact with a sector in the Crm.
 *
 * @property	crm_ContactSet		$contact
 * @property	crm_SectorSet		$sector
 */
class crm_ContactSectorSet extends crm_RecordSet
{

	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();

		$this->setPrimaryKey('id');


		$this->hasOne('contact'		, $Crm->ContactSetClassName());
		$this->hasOne('sector'		, $Crm->SectorSetClassName());
	}


}


/**
 * A crm_ContactSector associates a contact with a sector in the Crm.
 *
 * @property	crm_Contact			$contact
 * @property	crm_Sector			$sector
 */
class crm_ContactSector extends crm_Record
{
}
