<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * A crm_Comment is a text comment that can be associated to any record.
 *
 * @property ORM_StringField	$summary
 * @property ORM_IntField		$rating
 * @property ORM_BoolField		$private
 * @property ORM_BoolField		$confirmed
 * @property ORM_IntField		$confirmedBy
 * @property ORM_DatetimeField	$confirmedOn
 * @property crm_ArticleSet		$article
 */
class crm_CommentSet extends crm_TraceableRecordSet
{

	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();

		$this->setDescription('Comment');
		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_TextField('summary')
					->setDescription('Summary'),
			ORM_IntField('rating')
					->setDescription('rating of object linked to comment 1-5'),
			ORM_BoolField('private')
					->setDescription('Private comment'),
			ORM_BoolField('confirmed')
					->setDescription('approbation of comment'),
			ORM_IntField('confimedBy')
					->setDescription('approbator user id'),
			ORM_DateTimeField('confirmedOn')
		);
		
		
		$this->hasOne('article', $this->Crm()->ArticleSetClassName());

	}

	/**
	 * @return crm_CommentSet
	 */
	public function joinArticleForDisplay()
	{
		$this->article();
		
		if (isset($this->article->articletype))
		{
			$this->article->articletype();
		}
		
		if (isset($this->article->articleavailability))
		{
			$this->article->articleavailability();
		}
		
		if (isset($this->article->packaging))
		{
			$this->article->packaging();
		}
		
		return $this;
	}
}


/**
 * A crm_Comment is a text comment with a rating that can be associated to any record.
 *
 * @property 	string		$summary
 * @property 	int			$rating
 * @property 	int			$private
 * @property 	int			$confirmed
 * @property 	int			$confirmedBy
 * @property 	string		$confirmedOn
 * @property 	crm_Article	$article
 * @method 		crm_Article	article()
 */
class crm_Comment extends crm_TraceableRecord
{

	/**
	 *
	 */
	public function linkTo(crm_Record $source, $linkType = 'hasComment')
	{
		parent::linkTo($source, $linkType);
	}


	/**
	 * @return bool
	 */
	public function updateTargetAverage()
	{
		$this->Crm()->includeArticleSet();
		
		if ($article = $this->article())
		{

			$res = $article->selectPublicComments();
			$total = 0;
			$sum = 0;
			foreach($res as $comment)
			{
				if ($comment->rating)
				{
					$total++;
					$sum += $comment->rating;
				}
			}
			
			$article->rating_average = $total > 0 ? ($sum / $total) : 0;
			$article->rating_count = $total;
			$article->save();
			
			return true;
		}
		
		return false;
	}

}
