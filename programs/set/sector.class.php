<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * A sector is a set of rules that help automatically
 * classify crm elements if they match those rules.
 *
 * @property ORM_StringField	$name
 * @property ORM_StringField	$description
 * @property ORM_TextField		$rules
 */
class crm_SectorSet extends crm_RecordSet
{
	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();

		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('name')
					->setDescription('Name'),
			ORM_StringField('description')
					->setDescription('Description'),
			ORM_TextField('rules')
					->setDescription('Rules')
		);
	}

	/**
	 * Selects sectors matching the specified record.
	 *
	 * @return array <crm_Sector> array(<sector_id> => crm_Sector)
	 */
	public function selectFor(crm_Record $record)
	{
		$sectors = $this->select();

		$matchingSectors = array();

		foreach ($sectors as $sector) {
			$recordSet = $record->getParentSet();
			$criteria = $sector->computeCriteria($recordSet)->_AND_($recordSet->id->is($record->id));
			if ($recordSet->select($criteria)->count() > 0) {
				$matchingSectors[$sector->id] = $sector;
			}
		}

		return $matchingSectors;
	}
}



/**
 * @property string		$name
 * @property string		$description
 * @property string		$rules
 */
class crm_Sector extends crm_Record
{

	/**
	 * Returns the rules defining the sector as an indexed
	 * array.
	 *
	 * @return array ['property' , 'condition', 'value']
	 */
	public function getRules()
	{
		return json_decode($this->rules, true);
	}

	/**
	 * Returns the ORM criteria corresponding to the sector's rules
	 * when applied to a specific crm_RecordSet.
	 *
	 * @return ORM_Criteria
	 */
	public function computeCriteria(crm_RecordSet $set)
	{
		$rules = $this->getRules();

		//
		$criteria = new ORM_TrueCriterion();
		foreach ($rules as $rule) {
			$property = $rule['property'];
			$condition = $rule['condition'];
			$value = $rule['value'];
			$values = explode(',', $value);

			try {
				$field = $set->getFieldByPath($property);
			} catch (ORM_Exception $e) {
				continue;
			}

			$value = array_pop($values);
			if ($field instanceof ORM_ManyRelation) {
				/* @var $relationSet ORM_RecordSet */
				$relationSet = $field->getSet();

				$externalFieldName = $field->getForeignFieldName();
				$externalField = $relationSet->$externalFieldName;
				$internalField = null;

				$allFields = $relationSet->getFields();
				foreach ($allFields as $fieldName => $f) {
					if (($f instanceof ORM_FkField) && ($f->getForeignSetName() === get_class($set))) {
						$internalFieldName = $fieldName;
						break;
					}
				}
				$subCriteria = $set->id->in($externalField->is($value)->_AND_($relationSet->deleted->is(false), $internalFieldName));
			} elseif (method_exists($field, $condition)) {
				$subCriteria = $field->$condition($value);
				foreach ($values as $value) {
					$subCriteria = $subCriteria->_OR_($field->$condition($value));
				}
			} else {
			    continue;
			}
			$criteria = $criteria->_AND_($subCriteria);
		}

		return $criteria;
	}
}
