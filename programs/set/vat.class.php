<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


/**
 * VAT rates.
 * 
 * @property ORM_StringField		$name
 * @property ORM_TextField			$description
 * @property ORM_DecimalField		$value
 * @property ORM_BoolField			$default
 */
class crm_VatSet extends crm_TraceableRecordSet
{
	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);
	
		$this->setDescription('VAT rate');
		$this->setPrimaryKey('id');
	
		$this->addFields(
				ORM_StringField('name'),
				ORM_TextField('description'),
				ORM_DecimalField('value', 2)
					->setDescription('VAT rate in percentage'),
				ORM_BoolField('default')
					->setDescription('Default VAT used for a new product')
		);
	}
	


	/**
	 * Returns the vat rates as an options array usable with widget_Select::setOptions()
	 * or widget_Select::addOptions().
	 *  
	 * @see Widget_Select
	 * @return array[string]
	 */
	public function getOptions()
	{
		$Crm = $this->Crm();
		$return = array(0 => '');
		
		$res = $this->select();
		$res->orderAsc($this->name);
		foreach($res as $vat)
		{
			$return[$vat->id] = $Crm->numberFormat($vat->value); // label is used by javascript, crm.jquery.js
		}
		
		return $return;
	}
}



/**
 * VAT rates.
 *
 * @property string			$name
 * @property string			$description
 * @property float			$value
 * @property bool			$default
 */
class crm_Vat extends crm_TraceableRecord
{
	/**
	 * (non-PHPdoc)
	 * @see ORM_Record::getRecordTitle()
	 * 
	 * @return string
	 */
	public function getRecordTitle()
	{
		$Crm = $this->Crm();
		return $Crm->numberFormat($this->value).'%';
	}
}