<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__).'/catalog.class.php';


/**
 * A crm_Catalog is a category of articles (products) populated with a serach result.
 *
 * @property ORM_StringField	$name
 * @property ORM_TextField		$description
 * @property ORM_StringField	$page_title
 * @property ORM_TextField		$page_description
 * @property ORM_TextField		$page_keywords
 * @property ORM_StringField	$image_alt
 * @property ORM_StringField	$search_url
 * @property crm_CatalogSet		$parentcatalog
 */
class crm_SearchCatalogSet extends crm_TraceableRecordSet
{

	public function __construct($crm = null)
	{
		parent::__construct($crm);

		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('name')
					->setDescription('Name'),
			ORM_TextField('description')
					->setDescription('Description'),

			ORM_EnumField('catalogitem_displaymode', crm_CatalogSet::getDisplayModes()),
				
			ORM_IntField('sortkey'),
				
			ORM_StringField('page_title')
					->setDescription('Page title used for referencing'),
			ORM_TextField('page_description')
					->setDescription('Page description used for referencing'),
			ORM_TextField('page_keywords')
					->setDescription('Page keywords used for referencing'),
			ORM_StringField('image_alt')
					->setDescription('Alt text for the main article image'),
				
			ORM_StringField('search_url')
					->setDescription('Url of a search result to use for SearchCatalog content')
		);
		
		$this->hasOne('parentcatalog', $this->Crm()->CatalogSetClassName());
	}


}


/**
 * 
 *
 * @property string			$name
 * @property string			$description
 * @property string			$page_title
 * @property string			$page_description
 * @property string			$page_keywords
 * @property string			$image_alt
 * @property string			$search_url
 * @property crm_Catalog	$parentcatalog
 */
class crm_SearchCatalog extends crm_TraceableRecord
{

	/**
	 * @return bab_Path
	 */
	public function getPhotoFolder()
	{
		$ul = $this->uploadPath();
		$ul->push('photo');
		$ul->createDir();
		return $ul;
	}
	
	/**
	 * get photo path or null if no photo
	 * @return bab_Path
	 */
	public function getPhotoPath()
	{
		$uploadpath = $this->getPhotoFolder();

		if (!isset($uploadpath)) {
			return null;
		}

		$W = bab_Widgets();

		$uploaded = $W->ImagePicker()->getFolderFiles($uploadpath);

		if (!isset($uploaded)) {
			return null;
		}

		foreach($uploaded as $file) {
			return $file->getFilePath();
		}

		return null;
	}
	
	
	/**
	 * Get search parameters as an array
	 * @return array
	 */
	public function getSearchParameters()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
		
		$search_url = new bab_url($this->search_url);
		
		return $search_url->search;
	}


	
	/**
	 * List of visible articles
	 * @return ORM_Iterator
	 */
	public function getVisibleArticles($sortcol = null, $sortmethod = null)
	{
		$this->Crm()->includeArticleSet();
		$set = $this->Crm()->CatalogItemSet();
		$set->catalog();
		$set->article();
		$set->article->joinHasOneRelations();
		$set->article->addUnitCostField();
		$res = $set->select($set->getSearchCriteria($this->getSearchParameters()));
		
		if (null !== $sortcol && null !== $sortmethod)
		{
			$res->$sortmethod($set->article->$sortcol);
		}
		
		return $res;
	}
	
	
	
	/**
	 * Test if there are no articles
	 * @return boolean
	 */
	public function isEmpty()
	{
		
		$res = $this->getVisibleArticles();
		if($res->count() > 0)
		{
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * Get an array of catalogs from root to current catalog
	 * @return array
	 */
	public function getAncestors()
	{
		$arr = array();
		$catalog = $this;
		
		do {
			array_unshift($arr, $catalog);
			$catalog = $catalog->parentcatalog();
		} while ($catalog && $catalog->id);
		
		return $arr;
	}
	
	/**
	 * Get textual representation of a catalog with ancestors
	 * @return string
	 */
	public function asText()
	{
		$text = '';
		$arr = $this->getAncestors();
		foreach($arr as $catalog)
		{
			$text .= $catalog->name.' > ';
		}
		
		return rtrim($text, '> ');
	}
	
	
	
	
	/**
	 * @return string
	 */
	public function getPageTitle()
	{
		if ($this->page_title)
		{
			return $this->page_title;
		}
	
		return $this->name;
	}
	
	/**
	 * @return string
	 */
	public function getPageDescription()
	{
		if ($this->page_description)
		{
			return $this->page_description;
		}
	
		return bab_abbr($this->description, BAB_ABBR_FULL_WORDS, 400);
	}
	
	/**
	 * @return string
	 */
	public function getPageKeywords()
	{
		return $this->page_keywords;
	}
	
	/**
	 * @return string
	 */
	public function getRewritenUrl()
	{
		$Crm = $this->Crm();
		
		if (!$Crm->rewriting)
		{
			return $Crm->Controller()->CatalogItem()->searchcatalog($this->id)->url();
		}
		$nodeId = $Crm->classPrefix.'SearchCatalog'.$this->id;
		return bab_siteMap::rewrittenUrl($nodeId);
	}
	
	
	/**
	 * Set sortkey recursively
	 * @param 	int 	$sortkey
	 * @return int	last sortkey
	 */
	public function setSortKey($sortkey)
	{
		$this->sortkey = $sortkey;
		$this->save();
		return $sortkey;
	}
	
	
	public function getSortKey()
	{
		return $this->sortkey;
	}
}