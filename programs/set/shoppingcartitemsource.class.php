<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * Represent an object used to create a shopping cart item
 *
 */
interface crm_ShoppingCartItemSourceInterface 
{
	/**
	 * Unit cost displayed on product page
	 * the cost is tax excluded without reduction
	 * @return float
	 */
	public function getRawUnitCostDF();
	
	/**
	 * Get reduction amount for this product
	 * positive value
	 * @return float
	 */
	public function getReduction();
	
	/**
	 * get reduction in percentage on the product
	 * @return float | null
	 */
	public function getReductionPercentage();
	
	
	/**
	 * Unit cost displayed on product page
	 * the cost is tax excluded with reduction, this cost will be duplicated in shopping cart Item
	 * @return float
	 */
	public function getUnitCostDF();
	
	
	/**
	 * Unit cost without reduction displayed on product page, this is the unavailable price if a reduction exists on product
	 * the cost is without reduction and tax included
	 * @return float
	 */
	public function getRawUnitCostTI();
	
	
	/**
	 * Unit cost displayed on product page, the real product cost for customer
	 * the cost is with reduction and tax included
	 * @return float
	 */
	public function getUnitCostTI();
	
	/**
	 * default quantity to use when the product is added to shopping cart
	 * @return float
	 */
	public function getDefaultQuantity();
	
	/**
	 * Return VAT rate
	 * @example 0.196
	 * @return float
	 */
	public function getVatRate();
	
	/**
	 * Get raw VAT amount for this product, computed on price without reduction
	 * return float
	 */
	public function getRawVat();
	
	/**
	 * Get VAT amount for this product
	 * return float
	 */
	public function getVat();
	
	
	/**
	 * Get packaging name
	 * @return string
	 */
	public function getPackagingName();
}