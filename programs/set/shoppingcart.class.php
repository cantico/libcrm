<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/order.class.php';


/**
 * A crm_ShoppingCart is a shopping cart.
 * @see crm_ShoppingCartItemSet
 *
 * @property ORM_StringField	$name
 * @property ORM_IntField		$user
 * @property ORM_StringField	$cookie
 * @property ORM_StringField	$session
 * @property ORM_DatetimeField  $createdOn
 * @property ORM_DatetimeField	$expireOn
 * @property ORM_EnumField		$status
 * @property ORM_StringField	$delivery
 * @property ORM_BoolField      $shipping_status
 * @property ORM_DecimalField	$shipping_amount
 * @property ORM_TextField		$gift_comment
 * @property ORM_TextField		$shipped_comment
 * @property crm_ContactSet		$contact
 * @property crm_OrderSet		$paymentorder
 * @property crm_OrderSet		$accountingdocument
 */
class crm_ShoppingCartSet extends crm_RecordSet
{

    public function __construct(Func_Crm $Crm = null)
    {
        parent::__construct($Crm);

        $Crm = $this->Crm();

        $this->setDescription('Shopping cart');

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name')
                    ->setDescription('name, if shopping cart is saved by a customer as a wishlist'),
            ORM_IntField('user')
                    ->setDescription('Owner id user'),
            ORM_StringField('cookie')
                    ->setDescription('cookie uuid'),
            ORM_StringField('session')
                    ->setDescription('Owner session id'),
            ORM_DatetimeField('createdOn')
                    ->setDescription('Creation date'),
            ORM_DatetimeField('expireOn')
                    ->setDescription('Expiration date'),
            ORM_EnumField('status', $this->getStatus()),
            ORM_StringField('delivery')
                    ->setDescription('delivery method, empty if no delivery method selected'),
            ORM_BoolField('shipping_status')
                    ->setDescription('Last shipping amount update validity'),
            ORM_DecimalField('shipping_amount', 4)
                    ->setDescription('total shipping amount for shopping cart'),
            ORM_TextField('gift_comment')
                    ->setDescription('A comment set by customer, this message will be joined to the package for the final recipient'),
            ORM_TextField('shipped_comment')
                    ->setDescription('A comment set by manager to provide informations to the customer about the package associated to the shopping cart'),
            ORM_StringField('paymentToken')
                    ->setDescription('payment token of the last payment attempt'), /** @see crm_PaymentErrorSet for the others attempts */

            ORM_StringField('uuid')
        );


        // optional attachement to contact
        $this->hasOne('contact', $Crm->ContactSetClassName());

        // optional attachement to order invoice after payement
        $this->hasOne('paymentorder', $Crm->OrderSetClassName());

        // optional attachement to order after confirmation if this is not a payment
        $this->hasOne('accountingdocument', $Crm->OrderSetClassName());

        if (isset($Crm->CollectStore)) {
            // a warehouse address if there is no delivery (click and collect)
            $this->hasOne('collectstore', $Crm->CollectStoreSetClassName());
        }
    }

    /**
     * Remove expired shopping carts
     * shopping cart will be deleted if the expireOn date is lower than current date or if the only link is
     * the session id and the session id already expired
     *
     *
     */
    public function removeExpired()
    {
        // delete from expiration date
        $criteria = $this->expireOn->is('0000-00-00 00:00:00')->_NOT()->_AND_($this->expireOn->lessThan(date('Y-m-d H:i:s')));

        $ongoing = array();
        foreach(bab_getActiveSessions() as $arr) {
            $ongoing[$arr['session_id']] = 1;
        }

        // select the shopping cart only attached to sessions

        $I = $this->select(
            $this->session->isNot('')
            ->_AND_($this->cookie->is(''))
            ->_AND_($this->user->is(0))
            ->_AND_($this->contact->is(0))
        );

        $expired = array();

        foreach($I as $cartRecord) {

            // remove carts linked only by session ID

            if (!isset($ongoing[$cartRecord->session])) {

                // the session is expired
                $expired[] = $cartRecord->session;

            }
        }

        if ($expired) {
            $criteria = $criteria->_OR_($this->session->in($expired));
        }


        foreach($this->select($criteria) as $cartRecord) {
            /*@var $cartRecord crm_ShoppingCart */
            $cartRecord->emptyCart();
        }

        $this->delete($criteria);
    }


    /**
     * Remove empty ongoing shopping carts
     * items add and removed afterwards create empty shopping carts
     *
     *
     */
    public function removeEmpty()
    {
        $Crm = $this->Crm();
        $res = $this->select($this->status->in(array(crm_ShoppingCart::EDIT, crm_ShoppingCart::CONFIRMED)));

        foreach($res as $shoppingCart)
        {
            /*@var $shoppingCart crm_ShoppingCart */
            $items = $shoppingCart->getItems();

            if (0 === $items->count())
            {
                $shoppingCart->emptyCart();
                $shoppingCart->delete();
            }
        }
    }


    /**
     * Get shopping cart cookie of user
     * @return string
     */
    public function getMyCookie()
    {
        if (isset($_COOKIE[$this->getCookieName()]))
        {
            return $_COOKIE[$this->getCookieName()];
        }

        return null;
    }



    /**
     * Criteria used to select shopping cart for user
     *
     * 1 - by cookie
     * 2 - by session
     * 3 - by user id
     *
     *
     *
     * @return ORM_Criteria
     */
    protected function myCartCriteria()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/userincl.php';

        // by cookie or by session or by user

        $criteria = $this->session->is(session_id());

        if (bab_isUserLogged())
        {
            // lors de la confirmation de la commande, le cookie est supprime de la table mais existe toujours chez l'utilisateur
            // on est dans le cas de figure ou le panier est selectionne par le champ user
            // dans ce cas le lien est casse si session_id a change (creation du panier -> deconnection -> reconnection)
            // un nouveau panier est cree alors que le panier en cours n'a pas pu etre paye
            // ce critere a ete ajoute pour eviter ce probleme

            $criteria = $criteria->_OR_($this->user->is($GLOBALS['BAB_SESS_USERID'])->_AND_($this->cookie->is('')));
        }


        if ($cookie = $this->getMyCookie())
        {
            $criteria = $criteria->_OR_($this->cookie->is($cookie)->_AND_($this->user->in(array(0, $GLOBALS['BAB_SESS_USERID']))));
        }

        $criteria = $criteria->_AND_($this->status->in(array(crm_ShoppingCart::EDIT, crm_ShoppingCart::CONFIRMED)));
        $criteria = $criteria->_AND_($this->name->is(''));

        return $criteria;
    }


    /**
     * Get shopping cart timeout in seconds
     * used if the shopping cart is linked to a cookie
     * if the method return null, the cookie is not created
     *
     * @return int
     */
    protected function getTimeout()
    {
        $day = 3600 * 24;

        return (3 * $day);
    }




    /**
     * Get one shopping cart, if no cart create a session shopping cart
     * if create is set to false and no shopping cart found, the method return null
     *
     * @param	bool	$create
     *
     * @return crm_ShoppingCart
     */
    public function getMyCart($create = true)
    {
        $Crm = $this->Crm();

        // dans le cas ou il y a plus de 1 panier valide
        // exemple l'utilisateur viens de se connecter avec un panier associe a son cookie alors qu'il a deja un panier associe a son compte utilisateur
        // dans ce cas il faut prioriser le panier associe au cookie


        $this->addFields(
            $this->cookie->strcmp($this->getMyCookie())->abs()->setName('sortkey')
        );


        try {
            $res = $this->select($this->myCartCriteria());
            $res->orderAsc($this->sortkey);

            if (0 !== $res->count()) {
                foreach($res as $cart) {
                    /*@var $cart crm_ShoppingCart */

                    $save = false;

                    // definir le timeout correct si cela n'est pas encore fait
                    // les paniers sont crees avec un petit timeout pour traiter le probleme des bots

                    $duration = round((bab_mktime($cart->expireOn) - time()) / 3600); // heures

                    if ($duration <= 1)
                    {
                        // ce panier expire dans une heure

                        if ($timeout = $this->getTimeout()) {
                            $cart->expireOn = date('Y-m-d H:i:s', time() + $timeout);
                            $save = true;
                        }
                    }

                    // fix contact if necessary

                    $myContact = $Crm->getMyContact();
                    if (isset($myContact) && $myContact->id !== $cart->getFkPk('contact')) {
                        // create multiple accounts on same shopping cart
                        if ($cart->contact instanceof crm_Contact) {
                            $cart->contact = $myContact;
                        } else {
                            $cart->contact = $myContact->id;
                        }

                        $save = true;

                    }

                    if ($save) {
                        $cart->save();
                    }

                    return $cart;
                }
            }
        }
        catch(ORM_BackEndSelectException $e)
        {
            // erreur SQL
            bab_debug(bab_toHtml($e->getMessage()));
            return null;
        }


        if (!$create) {
            return null;
        }

        require_once $GLOBALS['babInstallPath'].'utilit/uuid.php';

        $cart = $this->newRecord();

        $cart->user = $GLOBALS['BAB_SESS_USERID'];
        $cart->cookie = $this->cookieOnCreate();
        $cart->session = session_id();
        $cart->contact = $this->contactOnCreate();
        $cart->createdOn = date('Y-m-d H:i:s');
        $cart->uuid = bab_uuid();

        if ($timeout = $this->getTimeout()) {
            // ce panier peut etre cree par un bot sans gestion des cookie
            // il expire dans une heure, si on reutilise se panier dans l'heure, date d'expiration relle sera enregistree
            $cart->expireOn = date('Y-m-d H:i:s', time() + 3600);
        }

        $cart->save();

        return $cart;
    }


    /**
     * get cookie name used to store shopping cart identifier
     * @return string
     */
    protected function getCookieName()
    {
        global $babUrl;
        return sprintf("%s%u", $this->Crm()->classPrefix, crc32($babUrl));
    }


    /**
     * Set cookie on shopping cart creation
     * the cookie is set with the timeout from the getTimeout method, if the getTimeout() method return null, no cookie is set for the shopping cart
     * and the cart is attached only to session
     * The string returned by this method will be used in a cookie, it should not be guessable to protect access to others shopping carts
     *
     * @see crm_ShoppingCartSet::getTimeout()
     *
     * @return string
     */
    protected function cookieOnCreate()
    {
        $timeout = $this->getTimeout();

        if (null === $timeout)
        {
            return '';
        }

        require_once $GLOBALS['babInstallPath'].'utilit/uuid.php';
        $uuid = bab_uuid();

        setcookie($this->getCookieName(), $uuid, time() + $timeout, '/');

        return $uuid;
    }

    /**
     * Set contact on shopping cart create
     * @return int
     */
    protected function contactOnCreate()
    {
        if (!$GLOBALS['BAB_SESS_LOGGED'])
        {
            // throw new Exception('shopping cart creation for unregistered user is forbidden');
            return 0;
        }

        $set = $this->Crm()->ContactSet();
        $contact = $set->get($set->user->is($GLOBALS['BAB_SESS_USERID']));

        if (!$contact)
        {
            return 0;
        }

        return $contact->id;
    }


    /**
     * Delete user shopping cart
     * @return bool
     */
    public function deleteMyCart()
    {
        return $this->delete($this->myCartCriteria());
    }



    /**
     * Get possibles status for shopping cart
     *
     * @return Array
     */
    public function getStatus()
    {
        $Crm = $this->Crm();

        return array(
            crm_ShoppingCart::EDIT 		=> $Crm->translate('Ongoing'),
            crm_ShoppingCart::CONFIRMED => $Crm->translate('Confirmed for payment'),
            crm_ShoppingCart::ARCHIVED	=> $Crm->translate('Payement made'), // si on est dans le cas d'une prise de commmande sans paiement, il faut utiliser se statut pour les commande confirmees et surcharger la methode
            crm_ShoppingCart::DELIVERY	=> $Crm->translate('Shipment preparation'),
            crm_ShoppingCart::SHIPPED	=> $Crm->noDelivery?$Crm->translate('Order ready'):$Crm->translate('Package sent')
        );
    }


    /**
     * Delete shopping carts and shopping cart items associated
     * @param	ORM_Criteria	$criteria
     * @return	bool
     */
    public function delete(ORM_Criteria $criteria = null)
    {
        $Crm = $this->Crm();

        $res = $this->select($criteria);
        foreach($res as $cart) {

            if ($cart->cookie)
            {
                setcookie($this->getCookieName(), false, time() - 3600);
            }

            $cart->emptyCart();

            // cleanup shopping trace if possible

            if (isset($Crm->ShopTrace))
            {
                $stSet = $Crm->ShopTraceSet();
                $res = $stSet->select($stSet->cart->is($cart->id));

                foreach($res as $shoptrace)
                {
                    $shoptrace->cart = null;
                    $shoptrace->save();
                }
            }
        }

        return parent::delete($criteria);
    }
}


/**
 * A shopping cart
 *
 * a shopping cart can be linked to a session, a user id, a contact
 *
 *
 * @see crm_ShoppingCartItem
 *
 * @property string		    $name
 * @property int			$user
 * @property string			$cookie
 * @property string			$session
 * @property string			$createdOn
 * @property string			$expireOn
 * @property int			$status
 * @property string			$delivery
 * @property int            $shipping_status
 * @property float			$shipping_amount
 * @property string			$gift_comment
 * @property string			$shipped_comment
 * @property crm_Contact	$contact
 * @property crm_Order		$paymentorder
 * @property crm_Order		$accountingdocument
 * @property crm_CollectStore   $collectstore
 */
class crm_ShoppingCart extends crm_Record
{
    const EDIT		= 0;	// in modification by customer
    const CONFIRMED	= 1;	// confirmed for payment
    const ARCHIVED	= 2;	// archived for customer, an order has been created
    const DELIVERY	= 3;	// delivery in process
    const SHIPPED	= 4;	// package sent



    /**
     * get list of status where the shopping cart is edited by customer, no payment associated and no order or invoice created
     * @return array
     */
    public static function getOngoingStatus()
    {
        return array(self::EDIT, self::CONFIRMED);
    }

    /**
     * get list of status where the shopping cart have a payment associated
     * @return array:
     */
    public static function getStatusWithPayment()
    {
        return array(self::ARCHIVED, self::DELIVERY, self::SHIPPED);
    }

    /**
     * Get the list of status where the shop managers team have a task
     * @return multitype:
     */
    public static function getManagersStatus()
    {
        return array(self::ARCHIVED, self::DELIVERY);
    }


    /**
     * Get associated order if exists
     * @return crm_Order
     */
    public function getOrder()
    {

        if (($this->paymentorder instanceof crm_Order) && $this->paymentorder->id)
        {
            return $this->paymentorder;
        }

        if (($this->accountingdocument instanceof crm_Order) && $this->accountingdocument->id)
        {
            return $this->accountingdocument;
        }

        return null;
    }


    /**
     * Get title depending of status
     * @return string
     */
    public function getTitle()
    {
        $Crm = $this->Crm();

        if (in_array($this->status, self::getOngoingStatus()))
        {
            return sprintf($Crm->translate('Shopping cart #%d'), $this->id);
        }

        return sprintf($Crm->translate('Order #%d'), $this->id);
    }


    /**
     * Remove items in shoping cart
     *
     */
    public function emptyCart()
    {
        $Crm = $this->Crm();

        // remove items

        $set = $Crm->ShoppingCartItemSet();
        $criteria = $set->cart->is($this->id);

        $set->delete($criteria);

        // remove coupons

        if (isset($Crm->CouponUsage))
        {
            $set = $Crm->CouponUsageSet();
            $set->delete($set->shoppingcart->is($this->id));
        }

        // remove failed payments attempts

        if (isset($Crm->PaymentError))
        {
            $set = $Crm->PaymentErrorSet();
            $set->delete($set->cart->is($this->id));
        }
    }

    /**
     * Test if the shopping cart is empty
     * @return boolean
     */
    public function isEmpty()
    {
        $set = $this->Crm()->ShoppingCartItemSet();
        $res = $set->select($set->cart->is($this->id));

        return ($res->count() === 0);
    }


    /**
     * Test if the shopping cart contain an article or catalogitem
     * @param crm_Article | crm_CatalogItem $product
     * @return bool
     */
    public function contains(crm_Record $product)
    {
        if ($product instanceof crm_CatalogItem)
        {
            $article = $product->article;
            if (!($article instanceof crm_Article))
            {
                throw new crm_Exception('Missing join on article');
            }

        } elseif ($product instanceof crm_Article) {
            $article = $product;
        } else {
            throw new crm_Exception('wrong object');
        }


        $Crm = $this->Crm();
        $set = $Crm->ShoppingCartItemSet();
        $set->catalogitem();

        $item = $set->get(
            $set->cart->is($this->id)
            ->_AND_($set->catalogitem->article->is($article->id))
        );

        if (null === $item)
        {
            return false;
        }

        return true;
    }


    /**
     * Add an available and accessibles catalogItem
     * this method create the item without access test
     *
     * @param		crm_CatalogItem			$catalogItemRecord
     * @param		crm_ArticlePackaging	$articlePackagingRecord
     * @param		int						$quantity
     *
     * @return crm_ShoppingCartItem
     */
    public function addCatalogItem(crm_CatalogItem $catalogItemRecord, crm_ArticlePackaging $articlePackagingRecord, $quantity)
    {
        $SCItemSet = $this->Crm()->ShoppingCartItemSet();


        $res = $SCItemSet->select(
                $SCItemSet->catalogitem->is($catalogItemRecord->id)
                ->_AND_($SCItemSet->articlepackaging->is($articlePackagingRecord->id))
                ->_AND_($SCItemSet->cart->is($this->id))
        );


        if (0 === $res->count())
        {
            $article = $catalogItemRecord->article;

            $record = $SCItemSet->newRecord();

            $record->name 				= $article->name;
            $record->subtitle			= $article->subtitle;
            $record->description 		= $article->getTextDescription();
            $record->quantity 			= ($quantity === null ? $articlePackagingRecord->getDefaultQuantity() : $quantity);
            $record->cart 				= $this->id;
            $record->catalogitem 		= $catalogItemRecord->id;
            $record->articlepackaging	= $articlePackagingRecord->id;
            $record->packaging			= $articlePackagingRecord->packaging->name;
            $record->unit_cost 			= $articlePackagingRecord->getUnitCostDF(); 			// stored with 4 digits
            $record->reduction			= $articlePackagingRecord->getReductionPercentage();	// stored with 2 digits
            $record->vat 				= ($articlePackagingRecord->getVatRate() * 100);

        } else {
            foreach($res as $record) {
            }
            $record->quantity		= $record->quantity + $quantity;
        }

        $record->save();


        // if delivery method allready set, update the shipping price

        if ($this->delivery)
        {
            $this->updateShippingCost();
        }

        return $record;
    }




    /**
     * Get shopping cart items with joined catlogItem and joined articles
     * @return ORM_Iterator
     */
    public function getItems()
    {
        $set = $this->Crm()->ShoppingCartItemSet();
        $set->joinHasOneRelations();

        $set->catalogitem->article();
        $set->catalogitem->article->joinHasOneRelations();
        $set->articlepackaging->joinHasOneRelations();

        return $set->select($set->cart->is($this->id));
    }



    public function getCount()
    {
        $n = 0;
        $res = $this->getItems();
        foreach($res as $shoppincartItem)
        {
            $n += $shoppincartItem->quantity;
        }

        return $n;
    }



    /**
     * Get organization of shopping cart owner to use for access rights verification
     * (main organization)
     *
     * @return crm_Organization
     */
    public function getOrganization()
    {
        $Crm = $this->Crm();

        if (!isset($Crm->Organization))
        {
            return null;
        }


        $this->Crm()->includeOrganizationSet();
        $this->Crm()->includeContactSet();

        $coSet = $this->Crm()->ContactOrganizationSet()->join('organization')->join('contact');

        if ($this->contact) {

            if ($this->contact instanceOf crm_Contact) {
                $criteria = $coSet->contact->id->is($this->contact->id);

            } else {
                $criteria = $coSet->contact->id->is($this->contact);
            }

        } else {
            $criteria = $coSet->contact->user->is($this->user);
        }
        $criteria = $criteria->_AND_($coSet->main->is(1));

        $res = $coSet->select($criteria);

        foreach($res as $co) {
            return $co->organization;
        }

        return null;
    }



    /**
     * Get contact for shopping cart
     * @return crm_Contact | null
     */
    public function getContact()
    {
        if ($this->contact instanceOf crm_Contact)
        {
            return $this->contact;
        }

        if (0 != $this->contact)
        {
            $this->Crm()->includeContactSet();
            return $this->contact();
        }

        if (!bab_isUserLogged()) {
            return null;
        }
        
        // search contact by userid

        $contactSet = $this->Crm()->ContactSet();
        $contactSet->address();
        if(!$this->Crm()->noDelivery){
            $contactSet->deliveryaddress();
        }
        $contact = $contactSet->get($contactSet->user->is($GLOBALS['BAB_SESS_USERID']));

        return $contact;
    }
    
    
    public function getEmail()
    {
        $contact = $this->getContact();
        if (!isset($contact)) {
            return null;
        }
        return $contact->email;
    }


    /**
     * Get delivery address
     * click and collect store address or contact delivery address
     * @return crm_Address
     */
    public function getDeliveryAddress()
    {
        $Crm = $this->Crm();
        $Crm->includeAddressSet();

        if (isset($Crm->CollectStore) && $this->getFkPk('collectstore') > 0) {
            $Crm->includeCollectStoreSet();
            $collectStore = $this->collectstore();

            if (!isset($collectStore)) {
                return null;
            }

            return $collectStore->address();
        }


        $contact = $this->getContact();
        if (null == $contact) {
            throw new crm_AccessException($Crm->translate('Access denied, you are not logged in as a customer'));
        }

        $deliveryaddress = $contact->getDeliveryAddress();

        if (null == $deliveryaddress || $deliveryaddress->isEmpty()) {
            return null;
        }

        return $deliveryaddress;
    }



    /**
     * Total weight of shopping cart
     * @return float
     */
    public function getWeight()
    {
        $total = 0;
        foreach($this->getItems() as $item) {
            /*@var $item crm_ShoppingCartItem */
            $total += ($item->quantity * $item->articlepackaging->weight);
        }

        return $total;
    }


    /**
     * transport freight computed from cart content
     * shippment method must be selected allready, this amount is tax included
     * @return float
     */
    public function getFreight()
    {
        return (float) $this->shipping_amount;
    }

    /**
     * Test if at leat one of the item in shopping cart is shippable
     */
    public function isShippable()
    {
        $items = $this->getItems();
        foreach($items as $item)
        {
            if ($item->isShippable())
            {
                return true;
            }
        }
        return false;
    }


    /**
     * Update shipping amount with default shipping scale
     * @return bool
     */
    public function updateShippingCost()
    {
        $Crm = $this->Crm();

        $contact = $this->getContact();
        if (null == $contact) {
            $this->shipping_amount = 0.0;
            $this->shipping_status = false;
            $this->delivery = $Crm->translate('No contact');
            $this->save();
            return false;
        }

        if (!$this->isShippable()) {
            $this->shipping_amount = 0.0;
            $this->shipping_status = true;
            $this->delivery = $Crm->translate('Not shippable');
            return $this->save();
        }

        $deliveryaddress = $contact->getDeliveryAddress();

        if (null == $deliveryaddress || $deliveryaddress->isEmpty()) {
            $this->shipping_amount = 0.0;
            $this->shipping_status = false;
            $this->delivery = $Crm->translate('No delivery address');
            $this->save();
            return false;
        }

        $shippingamount = $this->computeShippingCost($deliveryaddress->postalCode, $deliveryaddress->getCountry()->id);
        if (null === $shippingamount) {
            $this->shipping_amount = 0.0;
            $this->shipping_status = false;
            $this->delivery = $Crm->translate('Incompatible with shipping scale');
            $this->save();
            return false;
        }


        $this->shipping_amount = $shippingamount;
        $this->shipping_status = true;
        $this->delivery = $Crm->translate('Default shipping scale');
        return $this->save();
    }



    /**
     * verifier le franco de port
     * @return bool
     */
    protected function ShippingCostLimit($amount)
    {
        $Crm = $this->Crm();
        $addonname = $Crm->getAddonName();

        $registry = bab_getRegistryInstance();
        $registry->changeDirectory("/$addonname/configuration/");
        $shipping_cost_limit = $registry->getValue('shipping_cost_limit', 0);

        if ($shipping_cost_limit != 0 && $shipping_cost_limit <= $amount)
        {
            return true;
        }

        return false;
    }




    /**
     * get department from postal code
     * @param string $postalCode
     * @return string
     */
    protected function getDepartment($postalCode)
    {
        if (mb_strlen($postalCode) < 2)
        {
            return '';
        } else {
            return mb_substr($postalCode,0,2);
        }
    }



    /**
     * Compute the shipping cost for a delivery address
     * according to shopping cart content
     *
     * @param	string	$postalCode
     * @param	int		$country
     *
     * @return Number | null
     */
    public function computeShippingCost($postalCode, $country)
    {
        $Crm = $this->Crm();

        if (isset($Crm->CollectStore) && $this->getFkPk('collectstore') > 0) {
            return 0;
        }


        $amount = $this->getRawTotalTI();

        if ($this->ShippingCostLimit($amount))
        {
            return 0;
        }


        $weight = $this->getWeight();

        $shippingScalSet = $Crm->ShippingScaleSet();
        return $shippingScalSet->getAmount($this->getDepartment($postalCode), $country, $weight);
    }





    /**
     * Get total all VAT amount of products
     * @return float
     */
    public function getVAT()
    {
        $total = 0;
        foreach($this->getItems() as $item) {
            /*@var $item crm_ShoppingCartItem */
            $total += ($item->getTotalDF() * ($item->vat / 100));
        }

        return $total;
    }



    /**
     * @return float
     */
    public function getItemSumDF()
    {
        $total = 0;
        foreach($this->getItems() as $item) {
            $total += $item->getTotalDF();
        }

        return $total;
    }

    /**
     * @return float
     */
    public function getItemSumTI()
    {
        static $total = null;
        // sans cette static, il se produit 1 segmentation fault dans certains cas lors de l'appel de $this->getRawTotalTI()
        // car $this->getDiscountTotal() appelle a nouveau getItemSumTI() dans le module advshop

        if (null === $total)
        {
            $total = 0;
            foreach($this->getItems() as $item) {
                $total += $item->getTotalTI();
            }
        }

        return $total;
    }




    /**
     * Get total taxes included with reductions, without shipping cost
     * @return float
     */
    public function getRawTotalTI()
    {
        $total = $this->getItemSumTI();

        $total += $this->getAmountDiscountTotal($total);
        $total += $this->getShippingDiscountTotal($total);

        return $total;
    }


    /**
     * Get total taxes excluded with reductions, without shipping cost
     * @return float
     */
    public function getRawTotalDF()
    {
        $total = $this->getItemSumDF();
        $total += $this->getAmountDiscountTotal(); // do not set the max value because disounts are base on total tax included
        $total += $this->getShippingDiscountTotal();

        return $total;
    }
    
    
    /**
     * Test if coupon code is in shopping cart
     * @return boolean
     */
    public function isCouponCodeUsed($code)
    {
        $Crm = $this->Crm();
        $set = $Crm->CouponUsageSet();
        $set->coupon();
        $res = $set->select(
            $set->shoppingcart->is($this->id)
            ->_AND_($set->coupon->code->is($code))
        );
        
        return ($res->count() > 0);
    }
    
    
    /**
     * @return crm_CouponUsage[]
     */
    public function selectCouponUsages($shipping = null)
    {
        $Crm = $this->Crm();
        $set = $Crm->CouponUsageSet();
        $set->coupon();
        if (isset($Crm->ArticlePackaging)) {
            $set->coupon->articlepackaging();
            $set->coupon->articlepackaging->article();
        }
        $set->coupon->orderitem();
        
        $criteria = $set->shoppingcart->is($this->id);
        
        if (isset($shipping)) {
            $criteria = $criteria->_AND_($set->coupon->shipping->is($shipping));
        }
        
        return $set->select($criteria);
    }
    


    /**
     * List of coupons
     * each discount can have keys 'title' and 'value' (float)
     *
     * @param bool $shipping   Filter on shipping  TRUE: discount on shipping only
     *                                             FALSE: disount on amount only
     *                                             null: all discounts
     * @param float $max_discount_value
     *
     * @return array
     */
    public function getComputedCouponRows($shipping, $max_coupon_value)
    {
        $Crm = $this->Crm();

        if (!isset($Crm->Coupon))
        {
            return array();
        }

        $coupons = $this->selectCouponUsages($shipping);

        //$max_coupon_value = $this->getItemSumTI();
        //$max_coupon_value += $this->getDiscountTotal();
        //$max_coupon_value += $this->getFreight();

        $max = new stdClass();
        $max->total = $max_coupon_value;
        $max->shipping = $this->getFreight();

        $return = array();
        foreach ($coupons as $coupon) {

            /*@var $coupon crm_CouponUsage */

            try {
                $amount = $coupon->coupon->getAmount($this, $max);
                $message = null;

                $coupon->coupon->applyAmount($max, $amount);

            } catch(crm_DiscountBaseException $e)
            {
                $amount = null;
                $message = $e->getMessage();
            }


            $return[] = array(
                    'id' 		=> $coupon->id, // coupon usage id
                    'ref' 		=> $coupon->coupon->code,
                    'title' 	=> $coupon->coupon->description,
                    'value' 	=> $amount,
                    'message'	=> $message,
                    'type'      => 'coupon'
            );
        }

        return $return;
    }


    /**
     * List of coupons
     * each discount can have keys 'title' and 'value' (float)
     *
     * @return array
     */
    public function getCouponRows()
    {
        return $this->getFilteredDiscountRows('coupon');
    }


    /**
     * Test if a coupon allready associated to shopping cart
     * @return bool
     */
    public function hasCoupon(crm_Coupon $coupon)
    {
        $Crm = $this->Crm();

        $set = $Crm->CouponUsageSet();
        $result = $set->get($set->coupon->is($coupon->id)->_AND_($set->shoppingcart->is($this->id)));

        return (null !== $result);
    }


    /**
     * List of automatic discounts applied to the shopping cart
     * each discount can have keys 'title' and 'value' (float)
     *
     * @param bool $shipping   Filter on shipping  TRUE: discount on shipping only
     *                                             FALSE: disount on amount only
     *                                             null: all discounts
     * @param float $max_discount_value
     *
     * @return array
     */
    public function getComputedDiscountRows($shipping, $max_discount_value)
    {
        $Crm = $this->Crm();

        if (!isset($Crm->Discount))
        {
            return array();
        }

        $now = date('Y-m-d');

        $set = $Crm->DiscountSet();


        $criteria = $set->start_date->lessThanOrEqual($now)->_OR_($set->start_date->is('0000-00-00'))
            ->_AND_($set->end_date->greaterThanOrEqual($now)->_OR_($set->end_date->is('0000-00-00')));

        if (isset($shipping)) {
            $criteria = $criteria->_AND_($set->shipping->is($shipping));
        }

        $discounts = $set->select($criteria);

        //$max_discount_value = $this->getItemSumTI();

        $max = new stdClass();
        $max->total = $max_discount_value;
        $max->shipping = $this->getFreight();


        $return = array();
        foreach ($discounts as $discount) {

            try {
                $amount = $discount->getAmount($this, $max);
                $message = null;

                $discount->applyAmount($max, $amount);

            } catch(crm_DiscountBaseException $e)
            {
                $amount = null;
                $message = $e->getMessage();
            }


            $return[] = array(
                    'id' 		=> $discount->id,
                    'ref' 		=> '',
                    'title' 	=> $discount->description,
                    'value' 	=> $amount,
                    'message'	=> $message,
                    'type'      => 'discount'
            );
        }

        return $return;
    }



    /**
     * List of automatic discounts applied to the shopping cart
     * each discount can have keys 'title' and 'value' (float)
     *
     * @return array
     */
    public function getDiscountRows()
    {
        return $this->getFilteredDiscountRows('discount');
    }



    /**
     * Get amount produced by the list automatic discounts applied to the shopping cart
     *
     * @deprectated this method get all discounts in one shot but we need all discounts/coupons on amount before all discounts/coupons on shipping
     *              the method should continue to work but will get the list of all discounts and filter by type afterward
     *
     * @return float
     */
    public function getDiscountTotal()
    {
        $total = 0;
        foreach($this->getDiscountRows() as $discount)
        {
            if (null === $discount['value'])
            {
                continue;
            }

            $total += $discount['value'];
        }
        return $total;
    }



    /**
     * Get amount produced by the list of coupons in shopping cart
     *
     * @deprectated this method get all coupons in one shot but we need all discounts/coupons on amount before all discounts/coupons on shipping
     *              the method should continue to work but will get the list of all discounts and filter by type afterward
     *
     * return float
     */
    public function getCouponTotal()
    {
        $total = 0;
        foreach($this->getCouponRows() as $coupon){
            $total += $coupon['value'];
        }

        return $total;
    }


    /**
     * Get amount produced by the list of automatic discounts on amount + the list of coupons on amount
     *
     * @return float negative value
     */
    public function getAmountDiscountTotal($max = null)
    {
        $total = 0;
        if (!isset($max)) {
            $max = $this->getItemSumTI();
        }

        foreach($this->getComputedDiscountRows(false, $max) as $discount)
        {
            if (null === $discount['value']) {
                continue;
            }

            $max += $discount['value'];
            $total += $discount['value'];
        }

        foreach($this->getComputedCouponRows(false, $max) as $discount)
        {

            if (null === $discount['value']) {
                continue;
            }

            $max += $discount['value'];
            $total += $discount['value'];
        }

        return $total;
    }

    /**
     * Get amount produced by the list of automatic discounts on shipping + the list of coupons on shipping
     * @param float $max set the max value to prevent calculation by the getAmountDiscountTotal() method
     * @return float negative value
     */
    public function getShippingDiscountTotal($max = null)
    {
        $total = 0;

        if (!isset($max)) {
           $max = $this->getItemSumTI();
           $max += $this->getAmountDiscountTotal($max);
        }

        foreach($this->getComputedDiscountRows(true, $max) as $discount)
        {
            if (null === $discount['value']) {
                continue;
            }

            $max += $discount['value'];
            $total += $discount['value'];
        }

        foreach($this->getComputedCouponRows(true, $max) as $discount)
        {
            if (null === $discount['value']) {
                continue;
            }

            $max += $discount['value'];
            $total += $discount['value'];
        }

        return $total;
    }



    /**
     * Process all discounts in order and get all discounts rows
     * @return array
     */
    public function getDiscountAndCouponRows()
    {
        $rows = array();
        $max = $this->getItemSumTI();

        foreach($this->getComputedDiscountRows(false, $max) as $discount)
        {
            $rows[] = $discount;
            $max += $discount['value'];
        }

        foreach($this->getComputedCouponRows(false, $max) as $discount)
        {
            $rows[] = $discount;
            $max += $discount['value'];
        }

        return $rows;
    }




    /**
     * Return discounts by type based on the full computed discount list
     * @return array
     */
    protected function getFilteredDiscountRows($type)
    {
        $computedRows = $this->getDiscountAndCouponRows();
        $newList = array();

        foreach ($computedRows as $discount) {
            if ($type === $discount['type']) {
                $newList[] = $discount;
            }
        }

        return $newList;
    }




    /**
     * Get total taxes and freight included
     * with 2 digits precision
     *
     * @return float
     */
    public function getTotalTI()
    {
        $total = $this->getRawTotalTI();
        $total += $this->getFreight();

        return round($total, 2);
    }


    /**
     * Get total taxes excluded and freight included
     * with 2 digits precision
     *
     * @return float
     */
    public function getTotalDF()
    {
        $total = $this->getRawTotalDF();
        $total += $this->getFreight();

        return round($total, 2);
    }

    
    /**
     * Mark attached coupons as consumed
     */
    public function consumeCoupons()
    {
        $usages = $this->selectCouponUsages();
        foreach ($usages as $couponUsage) {
            $couponUsage->consumed = 1;
            $couponUsage->save();
        }
    }
    


    /**
     * Create order on confirmed shopping cart
     * this set status of shopping cart to archived
     *
     * @param	string	$autorization_id		Autorization id of the payement
     * @param	string	$transaction_id			Transaction id to link order creation to payement gateway transaction
     *
     * @throws crm_OrderDuplicateException, ErrorException
     *
     *
     * return crm_Order | false
     */
    public function createOrder($autorization_id, $transaction_id, $type = crm_Order::INVOICE)
    {
        $contact = $this->getContact();

        if (!$contact)
        {
            throw new ErrorException('Mailed to create the order from shopping cart, the contact is missing in shopping cart');
        }

        $Crm = $this->Crm();

        $Crm->includeAddressSet();
        $set = $Crm->OrderSet();
        $set->join('billingaddress');
        $set->join('deliveryaddress');
        $order = $set->newRecord();
        /*@var $order crm_Order */

        $order->autorization_id 	= $autorization_id;
        $order->transaction_id 		= $transaction_id;
        $order->type 				= $type;
        $order->recipienttype 		= crm_Order::CUSTOMER;
        $order->setBillingFromContact($contact); // TODO if billing address linked to shopping cart exists, use it in priority
        $order->setDeliveryFromShoppingCart($this);
        $order->request_date 		= date('Y-m-d');
        $order->contact				= $contact->id;
        $order->shoppingcart		= $this->id;
        $order->delivery			= $this->delivery;
        $order->shipped_comment		= $this->shipped_comment;
        $order->gift_comment		= $this->gift_comment;

        if (isset($Crm->Currency)) {
            // optionnal link to currency
            $order->currency 		= 1;
        }

        if (isset($Crm->CollectStore)) {
            $order->collectstore = $this->collectstore;
        }

        $order->save();
        $this->consumeCoupons();

        $oiSet = $this->Crm()->OrderItemSet();
        $otSet = $this->Crm()->OrderTaxSet();

        // copy shopping cart items to order


        $i =0;
        foreach ($this->getItems() as $cartItem)
        {
            /* @var $cartItem crm_ShoppingCartItem */

            $cartItem->createOrderItem($oiSet, $order, $i);
            $i++;

            $cartItem->catalogitem->article->sales_count += $cartItem->quantity;
            $cartItem->catalogitem->article->save();
        }






        foreach ($this->getCouponRows() as $reference => $coupon)
        {
            if (null === $coupon['value'])
            {
                continue;
            }

            $tax = $otSet->newRecord();

            $tax->reference = $reference;
            $tax->name = $coupon['title'];
            $tax->amount = $coupon['value'];
            $tax->parentorder = $order->id;
            $tax->sortkey = 0;
            $tax->save();
        }


        // add discounts

        foreach ($this->getDiscountRows() as $reference => $discount)
        {
            if (null === $discount['value'])
            {
                continue;
            }

            $tax = $otSet->newRecord();

            $tax->reference = $discount['ref'];
            $tax->name = $discount['title'];
            $tax->amount = $discount['value'];
            $tax->parentorder = $order->id;
            $tax->sortkey = 0;
            $tax->save();
        }


        // VAT amount will be added dynamically with the $order->refreshTotal() method
        // (ligne de montant TVA dynamique en fonction des differents taux de tva sur chaque ligne produit)



        // add shipping cost

        $freight = $this->getFreight();

        $tax = $otSet->newRecord();

        $tax->reference = 'shipping';
        $tax->name = $this->Crm()->translate('Shipping cost incl tax');
        $tax->amount = $freight;
        $tax->parentorder = $order->id;
        $tax->sortkey = 2; // place le frais de port apres la TVA
        $tax->save();


        // close shopping cart

        if (!empty($autorization_id))
        {
            $this->paymentorder 		= $order->id;
        } else {
            $this->accountingdocument 	= $order->id;
        }
        $this->status 				= crm_ShoppingCart::ARCHIVED;
        $this->expireOn 			= '0000-00-00 00:00:00';
        $this->save();


        $order->refreshTotal();
        $order->save();


        // verifier que le montant calcule de la facture est egal au montant paye du panier

        $cartCentimes = (int) round($this->getTotalTI() * 100);
        $orderCentimes = (int) round(100 * (float) $order->total_ti);

        if ($cartCentimes !== $orderCentimes)
        {
            trigger_error(sprintf('Error in order creation, the shopping cart total is different from the invoice total %s != %s', $cartCentimes/100, $orderCentimes/100));
            return false;
        }


        return $order;
    }




    /**
     * @return ORM_Iterator
     */
    public function selectPaymentError()
    {
        $Crm = $this->Crm();
        $set = $Crm->PaymentErrorSet();

        $res = $set->select($set->cart->is($this->id));

        return $res;
    }
}
