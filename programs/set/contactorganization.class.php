<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * A crm_ContactOrganization is a contact in an organization
 * this is the set object definitition
 *
 * @property	ORM_PkField		    $id
 * @property	ORM_StringField		$position
 * @property	ORM_BoolField		$main
 * @property	crm_ContactSet		$contact
 * @property	crm_OrganizationSet	$organization
 */
class crm_ContactOrganizationSet extends crm_RecordSet
{
    /**
     * @param Func_Crm $Crm
     */
    public function __construct(Func_Crm $Crm = null)
    {
        parent::__construct($Crm);

        $Crm = $this->Crm();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('position')
                ->setDescription('Position in organization'),
            ORM_BoolField('main')
                ->setDescription('Is the main contact in organization'),
            ORM_DateField('history_from')
                ->setDescription('Association date beetween contact and organization'),
            ORM_DateField('history_to')
                ->setDescription('End date of association beetween contact and organization')
        );

        $this->hasOne('contact', $Crm->ContactSetClassName());
        $this->hasOne('organization', $Crm->OrganizationSetClassName());
    }


    /**
     * Matches contact-organization links active at the specified date or today
     * if no date is specified.
     *
     * @since 0.9.13
     * @param string|null $date      ISO formatted date. null = today
     *
     * @return ORM_Criteria
     */
    public function isActive($date = null)
    {
        if (!isset($date)) {
            $date = date('Y-m-d');
        }
        return $this->history_to->is('0000-00-00')->_OR_(
            $this->history_to->greaterThan($date)
        );
    }
}


/**
 * A crm_ContactOrganization is a contact in an organization
 * one contact can have multiple organizations
 * this object implement crm_ORganizationPerson as well as crm_Contact
 *
 * the crm_Organization::selectContacts() method return an iterator of crm_OrganizationPerson
 * witch are crm_ContactOrganization objects or crm_Contact objects in case of an implementation
 * of crm with one organization per contact
 *
 * @property	string				$position
 * @property	int					$main
 * @property	crm_Contact			$contact
 * @property	crm_Organization	$organization
 */
class crm_ContactOrganization extends crm_Record implements crm_OrganizationPerson
{
	/**
	 * Position in organization
	 * @return string
	 */
	public function getMainPosition() {

		return $this->position;
	}

	/**
	 * The main organization of organization person
	 * this method exists to respect the same interface as crm_Contact (crm_OrganizationPerson)
	 * @return crm_Organization
	 */
	public function getMainOrganization()
	{
		$this->Crm()->includeOrganizationSet();
		return $this->organization();
	}

	/**
	 * The contact of person, if the organization person is the contact,
	 * the method return $this (crm_Contact implement crm_OrganizationPerson)
	 *
	 * @return crm_Contact
	 */
	public function getContact()
	{
		$this->Crm()->includeContactSet();
		return $this->contact();
	}



	/**
	 * Update contact from user infos, called when a user directory is modified
	 * @see crm_Contact::onUserModified()
	 * @param	Array	$infos
	 */
	public function updateFromUserInfos(array $infos)
	{
		$Crm = $this->Crm();
		$mapSet = $Crm->ContactDirectoryMapSet();
		$res = $mapSet->select($mapSet->type->is('contact')->_AND_($mapSet->contactfield->startsWith('ContactOrganization.')));

		foreach($res as $record)
		{
			$contactfield = explode('.',$record->contactfield);
			$recordName = array_shift($contactfield);
			$value = $infos[$record->userfield]['value'];

			$fieldname = array_shift($contactfield);

			switch($fieldname)
			{
				case 'id':
				case 'contact':
				case 'organization':
					// never update these fields
					break;

				default:
					$this->$fieldname = $value;
				break;
			}
		}
	}
}
