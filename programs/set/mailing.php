<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/campaign.php';


/**
 * A mailing campaing
 * campaing recipients are stored with a sent date time
 *
 */
class crm_CampaignTypeMailing extends crm_CampaignType
{



	/**
	 * @return string
	 *
	 */
	public function getDescription()
	{
		return 'Mailing campaign';
	}



	/**
	 * Fields used in each recipient of the campaign
	 *
	 * @return crm_MailingRecipientSet
	 */
	public function CampaignRecipientSet()
	{
		return $this->Crm()->MailingRecipientSet();
	}


	/**
	 * list recipients of a mailing, display crm contact informations
	 *
	 * @param	int				$campaign			campaign ID
	 *
	 * @return Widget_Form
	 */
	public function getPopulateWidget($campaign, Widget_Action $editrecipient_action)
	{

//		require_once dirname(__FILE__) . '/mailing.ui.class.php';
		$set = $this->CampaignRecipientSet();
		$Crm = bab_functionality::get('Crm');
		$Crm->includeContactSet();
		$Crm->includeOrganizationSet();
		$set->join('contact');
		$set->contact->join('organization');
		$iterator = $set->select($set->campaign->is($campaign));

		$table = new crm_MailingRecipientTableView;
		$table->edit_action = $editrecipient_action;
		$table->setDataSource($iterator);

		return $table;
	}


	/**
	 * get a widget with additional fields for recipient
	 * each campaign type may overload this method
	 *
	 * @param	crm_CampaignRecipient | null	$recipient
	 * @return	Widget_Frame
	 */
	public function getRecipientFields($recipient)
	{

		$frame = parent::getRecipientFields($recipient);

		// one additional field to display : sentdate

		$W = bab_functionality::get('Widgets');

		$field = $this->CampaignRecipientSet()->sentdate;
		$label = $W->Label($field->getDescription());
		$datepart = $W->DatePicker()->setAssociatedLabel($label)->setName('date');
		$timepart = $W->TimePicker()->setName('time');

		if (isset($recipient)) {
			$value = explode(' ', $recipient->sentdate);
			$datepart->setValue($value[0]);
			$timepart->setValue($value[1]);
		}

		$sentdate = $W->Frame(null, $W->HBoxLayout())->setName('sentdate')
				->addItem($datepart)
				->addItem($timepart);

		$frame->additem(
			$W->VBoxItems(
				$label,
				$sentdate
			)
		);

		return $frame;
	}


	/**
	 * Save recipient to campaign
	 * @param	int						$campaign
	 * @param	crm_MailingRecipientSet	$recipient
	 */
	public function saveRecipient($campaign, crm_MailingRecipient $recipient)
	{


		if (null === $recipient) {
			// create recipient

			$recipient = $this->CampaignRecipientSet()->newRecord();

			$recipient->campaign = $campaign;
			$recipient->contact = $contact;
		}

		if ($sentdate = bab_pp('sentdate')) {

			$W = bab_functionality::get('Widgets');

			$datepart = $W->DatePicker()->getISODate($sentdate['date']);
			$timepart = $sentdate['time'].':00';



			if ($datepart) {
				$recipient->sentdate = $datepart.' '.$timepart;
			}
		}


		$recipient->save();

	}



	/**
	 * return true if the campaign recipient have a field named "contact"
	 * @return bool
	 */
	public function haveContactField()
	{
		return true;
	}






	/**
	 * required fileds to import recipient in the phoning campaign
	 * return list of fields to import to
	 * @return array
	 */
	public function getImportFields()
	{
		return array(

			'contact' 		=> $this->Crm()->translate('Contact UUID'),
			'sentdate'		=> $this->Crm()->translate('Sent date'),
			'uuid'			=> $this->Crm()->translate('Universally Unique IDentifier')
		);
	}


	public function exportRecipients($campaign)
	{
		$record = $this->Crm()->campaignSet()->get($campaign);

		$arr = array(

			'contact/lastname' 			=> $this->Crm()->translate('Lastname'),
			'contact/firstname'			=> $this->Crm()->translate('Firstname'),
			'contact/organization/name' => $this->Crm()->translate('Organization'),
			'contact/phone'				=> $this->Crm()->translate('Phone'),
			'contact/email'				=> $this->Crm()->translate('Email'),
			'contact/title'				=> $this->Crm()->translate('Title'),
			'contact/gender'			=> $this->Crm()->translate('Gender'),
			'sentdate'					=> $this->Crm()->translate('Sent date'),
			'contact/uuid' 				=> $this->Crm()->translate('Contact UUID'),
			'uuid' 						=> $this->Crm()->translate('Universally Unique IDentifier')

		);

		$this->Crm()->includeContactSet();
		$this->Crm()->includeOrganizationSet();

		$set = $this->CampaignRecipientSet()->join('contact');
		$set->contact->join('organization');

		$this->Crm()->exportSet($set, $arr, $record->name);

	}







	/**
	 * next recipient with a task on it or null if all is done
	 *
	 * @param	int		$campaign
	 *
	 * @return int
	 */
	public function nextTaskRecipient($campaign)
	{
		$set = $this->CampaignRecipientSet();
		$nextIterator = $set->select($set->campaign->is($campaign)->_AND_($set->sentdate->is('0000-00-00 00:00:00')));
		$nextIterator->orderDesc($set->modifiedOn);

		foreach($nextIterator as $recipient) {
			return (int) $recipient->id;
		}

		return null;
	}

}
