<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */



/**
 * Mailbox Set
 * This Set require the LibImap addon
 *
 *
 * @property	ORM_UserField		$user
 * @property	ORM_StringField		$host
 * @property	ORM_IntField		$port
 * @property	ORM_EnumField		$type
 * @property	ORM_StringField		$username
 * @property	ORM_StringField		$password
 * @property	ORM_DatetimeField	$lastemail
 *
 */
class crm_MailBoxSet extends crm_RecordSet
{

	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();

		$this->setDescription('Mailbox');
		$this->setPrimaryKey('id');


		$this->addFields(

				ORM_UserField('user')->setDescription('Associated portal user id'), // if 0 global mailbox for all users
				ORM_StringField('host'),
				ORM_IntField('port'),
		        ORM_EnumField('type', $this->getTypes()),
				ORM_StringField('username'),
				ORM_StringField('password'),
				ORM_DatetimeField('lastemail')->setDescription('Date of last fetched email in mailbox')

		);
	}


	public function getTypes()
	{
		$Crm = $this->Crm();

		return array(
			'/pop3'	 						=> $Crm->translate('Pop3'),
			'/imap'	 						=> $Crm->translate('Imap'),
			'/pop3/ssl'						=> $Crm->translate('Pop3 SSL (signed certificate)'),
			'/imap/ssl'						=> $Crm->translate('Imap SSL (signed certificate)'),
			'/pop3/ssl/novalidate-cert'		=> $Crm->translate('Pop3 SSL (unsigned certificate)'),
			'/imap/ssl/novalidate-cert'		=> $Crm->translate('Imap SSL (unsigned certificate)')
		);
	}


	/**
	 * import mail into email set from the mailboxes based on the references informations
	 * @return int
	 */
	public function importResponses()
	{
		$res = $this->select();
		$count = 0;
		foreach($res as $mailbox)
		{
			try {
				$count += $mailbox->importResponses();
			} catch(LibImap_Exception $e)
			{
				// ignore connexion errors
			}
		}

		return $count;
	}

}


/**
 *
 * @property	int				$user
 * @property	string			$host
 * @property	int				$port
 * @property	string			$type
 * @property	string			$username
 * @property	string			$password
 * @property 	string			$lastemail
 */
class crm_MailBox extends crm_Record
{
	/**
	 *
	 * @var Func_Imap
	 */
	private $imap;


	/**
	 * @throws LibImap_Exception
	 * @return Func_Imap | false
	 */
	public function getImap($mailboxName = 'INBOX')
	{
// 		if (null === $this->imap)
// 		{
			$this->imap = @bab_functionality::get('Imap', false);

			if (!$this->imap)
			{
				return $this->imap;
			}

			$path = '{'.$this->host.':'.$this->port.$this->type.'}' . $mailboxName;

			$this->imap->setMailBox($path, $this->username, $this->password);
			$this->imap->connect();
// 		}

		return $this->imap;
	}


	/**
	 * Get the default port from the mailbox type
	 * @return int
	 */
	public function getDefaultPort()
	{
		switch($this->type)
		{
			case '/pop3':						return 110;
			case '/pop3/ssl':					return 995;
			case '/imap/ssl':					return 993;
			case '/pop3/ssl/novalidate-cert':	return 995;
			case '/imap/ssl/novalidate-cert':	return 993;
			default:
			case '/imap': 						return 143;
		}
	}


	/**
	 * import mail into email set from the mailbox based on the references informations
	 * @return int number of respsones imported
	 */
	public function importResponses()
	{
		$imap = $this->getImap();

		if (!$imap)
		{
			bab_debug('Import email response failed : the imap functionality is not available');
			return 0;
		}


		if (!empty($this->lastemail) && $this->lastemail !== '0000-00-00 00:00:00')
		{
			$ts = bab_mktime($this->lastemail);
		} else {
			$ts = strToTime("-2 days");
		}

		$date = date("d M Y", $ts);
		$res = $imap->searchMails(sprintf('SINCE "%s"', $date));

		bab_debug(sprintf('%d email found in the last 2 days on mailbox %d',count($res), $this->id));

		$Crm = $this->Crm();
		$emailSet = $Crm->EmailSet();

		$count = 0;
		$new_lastemail = $this->lastemail;

		foreach($res as $mId)
		{
			$email = $imap->getMail($mId);

			if ($email->getDate() > $new_lastemail)
			{
				$new_lastemail = $email->getDate();
			}

			$messageid = $email->getMessageID();

			if (empty($messageid))
			{
				continue;
			}


			if (null !== $emailSet->get($emailSet->message_id->is($messageid)))
			{
				// this email exists in database, attention les message ID ne sont pas uniques, mais c'est le cas pour les mail expedie depuis ovidentia
				// http://cr.yp.to/immhf/thread.html

				continue;
			}


			$references = $email->getReferences();

			if (empty($references))
			{
				// not a response and not a forward
				continue;
			}

			$existing_references = $emailSet->select($emailSet->message_id->in($references));

			if (0 === $existing_references->count())
			{
				continue;
			}

			// one of the references exists in the CRM, the email can be created.

			$record = $emailSet->newRecord();
			/* @var $record crm_Email */

			require_once $GLOBALS['babInstallPath'].'utilit/calincl.php';

			$record->createdOn = $email->getDate();
			$record->createdBy = bab_getUserIdByEmailAndName($email->getFromAddress(), $email->getFromName());

			$record->modifiedOn = $record->createdOn;
			$record->modifiedBy = $record->createdBy;

			$record->fromaddress = $email->getFromAddress();
			$record->fromname = $email->getFromName();
			$record->recipients = implode(', ',array_keys($email->getTo()));
			$record->ccRecipients = implode(', ',array_keys($email->getCc()));
			$record->subject = $email->getSubject();
			$record->body = $email->getHtmlContent(); // strip embeded images
			$record->message_id = $messageid;
			$record->mailbox = $this->id;

			$record->save(true); // do not trace

			// save attachments
			foreach($email->getAttachments() as $attachement)
			{
				/*@var $attachement LibImap_MailAttachment */
				$record->importAttachment($attachement->filename, $attachement->data);
			}


			// link new email to the same references

			foreach($existing_references as $ref_email)
			{
				/*@var $ref_email crm_Email */
				$ref_email->copyLinksTo($record);
			}

			$count++;
		}


		if ($new_lastemail !== $this->lastemail)
		{
			$this->lastemail = $new_lastemail;
			$this->save();
		}

		return $count;
	}

}