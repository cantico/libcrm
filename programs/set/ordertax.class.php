<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * An order taxe set
 * storage for taxes, freights
 * will be displayed as additonal rows after the total duty-free and before the total taxes-included
 *
 * @property ORM_StringField		$reference
 * @property ORM_StringField		$name
 * @property ORM_TextField			$description
 * @property ORM_DecimalField		$amount
 * @property ORM_IntField			$sortkey
 * @property ORM_BoolField			$readonly
 *
 * @property crm_OrderSet			$parentorder		The parent order
 */
class crm_OrderTaxSet extends crm_RecordSet
{
	public function __construct(Func_Crm $Crm = null)
	{
		parent::__construct($Crm);

		$Crm = $this->Crm();

		$this->setDescription('Order tax row');
		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('reference')
					->setDescription('unique reference in order'),
			ORM_StringField('name')
					->setDescription('Name'),
			ORM_TextField('description')
					->setDescription('Description'),
			ORM_DecimalField('amount', 4)
					->setDescription('Amount'),
			ORM_IntField('sortkey')
					->setDescription('charge sort key in order'),
			ORM_BoolField('readonly')
					->setDescription('charge readonly, computed by software, name and description are modifiable, the charge is not deletable and the amount is readonly')
		);

		$this->hasOne('parentorder', $Crm->OrderSetClassName());
	}
}


/**
 * An order tax
 *
 * @property string		$reference
 * @property string		$name
 * @property string		$description
 * @property float			$amount
 * @property int			$sortkey
 * @property bool			$readonly
 *
 * @method 	 crm_Order		$parentorder
 */
class crm_OrderTax extends crm_Record
{

}