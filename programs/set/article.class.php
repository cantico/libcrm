<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * Article database
 *
 * @property ORM_StringField				$reference
 * @property ORM_StringField				$name
 * @property ORM_StringField				$subtitle
 * @property ORM_TextField					$shortdesc
 * @property ORM_TextField					$description
 * @property ORM_TextField					$properties
 * @property ORM_Decimalfield				$weight
 * @property ORM_BoolField					$disabled
 * @property ORM_BoolField					$homepage
 * @property ORM_DateField					$newproduct
 * @property ORM_DecimalField				$rating_average
 * @property ORM_IntField					$rating_count
 * @property ORM_StringField				$page_title
 * @property ORM_TextField					$page_description
 * @property ORM_TextField					$page_keywords
 * @property ORM_StringField				$image_alt
 *
 * @property crm_OrganizationSet			$supplier
 * @property crm_ArticleTypeSet				$articletype
 * @property crm_ArticleAvailabilitySet		$articleavailability
 * @property crm_ArticlePackagingSet 		$mainpackaging
 * @property crm_VatSet 					$vat
 *
 * @method crm_Article get()
 * @method crm_Article newRecord()
 * @method crm_Article[] select()
 */
class crm_ArticleSet extends crm_TraceableRecordSet
{



    public function __construct(Func_Crm $Crm = null)
    {
        parent::__construct($Crm);

        $Crm = $this->Crm();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('reference')->index(),
            ORM_StringField('name')
                    ->setDescription('Name of product'),
            ORM_StringField('subtitle')
                    ->setDescription('subtitle for product name'),
            ORM_TextField('shortdesc', false, new ORM_CustomProperties(array('maxlength')))
                ->setDescription('Short description to use in lists of results or in card frames, text box must be limited in length')
                ->setCustomPropertyValue('maxlength', 200),
            ORM_HtmlField('description')
                    ->setDescription('Generic description field in HTML'),
            ORM_TextField('properties')
                    ->setDescription('Generic properties field'),
            ORM_BoolField('disabled')
                    ->setDescription('Disable online availability'),
            ORM_BoolField('homepage')
                    ->setDescription('Display on home page'),
            ORM_DateField('newproduct')
                    ->setDescription('The article is a new product until the date'),
            ORM_DecimalField('rating_average', 2)
                    ->setDescription('Average rating of product'),
            ORM_IntField('rating_count')
                    ->setDescription('Number of ratings'),
            ORM_IntField('sales_count')
                    ->setDescription('Number of sales'),
            ORM_StringField('page_title')
                    ->setDescription('Page title used for referencing'),
            ORM_TextField('page_description')
                    ->setDescription('Page description used for referencing'),
            ORM_TextField('page_keywords')
                    ->setDescription('Page keywords used for referencing'),
            ORM_StringField('image_alt')
                    ->setDescription('Alt text for the main article image'),
            ORM_StringField('rewritename')
                ->setDescription('Rewrite name of product')
        );

        if (isset($Crm->Organization)) {
            $this->hasOne('supplier', $Crm->OrganizationSetClassName());
        }
        if (isset($Crm->ArticleType)) {
            $this->hasOne('articletype', $Crm->ArticleTypeSetClassName()); // not modifiable after creation
        }
        if (isset($Crm->ArticleAvailability)) {
            $this->hasOne('articleavailability', $Crm->ArticleAvailabilitySetClassName());
        }
        if (isset($Crm->Vat)) {
            $this->hasOne('vat', $Crm->VatSetClassName());
        }
        if (isset($Crm->ArticlePackaging)) {
            $this->hasOne('mainpackaging', $Crm->ArticlePackagingSetClassName());
        }
        foreach ($this->getCustomFields() as $customfield) {
            $this->addFields($customfield->getORMField());
        }
    }



    /**
     * Add a field named "unit_cost" to the crm_ArticleSet
     * @throws ErrorException
     */
    public function addUnitCostField()
    {
        if (!($this->mainpackaging instanceof crm_ArticlePackagingSet)) {
            throw new ErrorException('missing join on mainpackaging');
        }

        if (!($this->vat instanceof crm_VatSet)) {
            throw new ErrorException('missing join on vat');
        }

        if (isset($this->unit_cost)) {
            throw new ErrorException('there is allready a field named unit_cost into the crm_ArticleSet object');
        }

        $mainpackaging = $this->mainpackaging;
        $reduction = $mainpackaging->unit_cost->times($mainpackaging->reduction->divideBy(100));
        $vatval = $mainpackaging->unit_cost->minus($reduction)->times($this->vat->value->divideBy(100));

        $this->addFields(
            $mainpackaging->unit_cost->minus($reduction)->plus($vatval)->setName('unit_cost')
        );
    }




    private function uniqueRewriteName(crm_Article $record)
    {
        $id = (int) $record->id;
        $res = $this->select($this->rewritename->is($record->rewritename)->_AND_($this->id->is($id)->_NOT()));

        if ($res->count() > 0) {
            $record->rewritename .= '-2';
            return $this->uniqueRewriteName($record);
        }

        return true;
    }



    /**
     *
     * {@inheritDoc}
     * @see crm_TraceableRecordSet::save()
     */
    public function save(ORM_Record $record, $noTrace = false)
    {
        $Crm = $this->Crm();

        if (isset($record->rewritename) && '' !== $record->rewritename) {
            $this->uniqueRewriteName($record);
        }


        $savereturn = parent::save($record, $noTrace);

        if (!$noTrace) {
            // This fixes performance problems when multiple articles are
            // updated at once, eg.: crm_CatalogItem::getRewritenUrl().
            $catalogSet = $Crm->CatalogSet();

            $catalogSet->updateRefCount();
            if (!$catalogSet->displayEmpty()) {
                require_once $GLOBALS['babInstallPath'].'utilit/sitemap.php';
                bab_siteMap::clearAll();
            }
        }

        return $savereturn;
    }




    public function delete(ORM_Criteria $criteria = null, $definitive = false)
    {
        $Crm = $this->Crm();

        // remove links to catalogs

        $res = $this->select($criteria);

        foreach ($res as $article) {

            $cSet = $Crm->CatalogItemSet();
            $cSet->delete($cSet->article->is($article->id));
        }

        return parent::delete($criteria, $definitive);
    }




    /**
     * Import a CSV row
     *
     * @throw Widget_ImportException
     *
     * @param	crm_Import			$import
     * @param	Widget_CsvRow		$row
     *
     * @return bool | int
     */
    public function import(crm_Import $import, Widget_CsvRow $row, $notrace = false)
    {

        $Crm = $this->Crm();

        if (isset($row->reference) && '' !== (string) $row->reference) {

            // update row if reference exists


            $article = $this->get($this->reference->is($row->reference));

            if (!$article) {

                $article = $this->newRecord();
            }

            /* @var $article crm_Article */

        } else {

            $message = sprintf($Crm->translate('Missing mandatory reference at line %d'), $row->line());

            $exception = new Widget_ImportException($message);
            $exception->setCsvRow($row);

            throw $exception;

            return false;
        }


        $nb_prop = $article->import($row);
        if (0 < $nb_prop) {
            $article->save($notrace);
            $import->addArticle($article);

            $article->mainpackaging->article = $article->id;
            $article->mainpackaging->save();

            // import categories
            // The import category code is disabled because it can delete
            // all links to categories in existing database
            // This can be used to import new datas
            /*
            if (isset($row->_categories)) {

                $categories = array();

                $text = str_replace('\,', '\$COMMA', $row->_categories);
                $text = str_replace('\>', '\$SUP', $row->_categories);

                $_categories = explode(',', $text);
                foreach ($_categories as $category) {
                    $ancestors = array();
                    $_ancestors = explode('>', $category);
                    foreach ($_ancestors as $ancestor) {
                        $ancestor = str_replace('\$COMMA', ',', $ancestor);
                        $ancestor = str_replace('\$SUP', ',', $ancestor);
                        $ancestor = trim($ancestor);

                        $ancestors[] = $ancestor;
                    }

                    $categories[] = $ancestors;
                }

                $article->importCategoriesArray($categories);
            }
            */
        }

        return $article->id;
    }
}



/**
 * A crm_Article is article
 *
 * @property string					$reference
 * @property string					$name
 * @property string 				$subtitle
 * @property string					$description HTML
 * @property string					$properties HTML
 * @property float					$weight
 * @property int					$disabled
 * @property int					$homepage
 * @property string					$newproduct
 * @property float					$rating_average
 * @property int					$rating_count
 * @property string					$page_title
 * @property string					$page_description
 * @property string					$page_keywords
 * @property string					$image_alt
 *
 * @property crm_Organization			$supplier
 * @property crm_ArticleType			$articletype
 * @property crm_Packaging				$packaging
 * @property crm_ArticleAvailability	$articleavailability
 * @property crm_ArticlePackaging		$mainpackaging
 * @property crm_Vat					$vat
 * 
 * @method crm_Organization			    supplier()
 * @method crm_ArticleType			    articletype()
 * @method crm_Packaging				packaging()
 * @method crm_ArticleAvailability	    articleavailability()
 * @method crm_ArticlePackaging         mainpackaging()
 * @method crm_Vat                      vat()
 */
class crm_Article extends crm_TraceableRecord
{
    /**
     * Get article name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Get article reference
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }


    /**
     * Get short description (raw text)
     * @return string
     */
    public function getShortDesc()
    {
        if ('' === $this->shortdesc) {

            if ('UTF-8' !== bab_charset::getIso()) {

                // decode entitites in UTF-8 for better compatibility
                // the translit will convert unsuported characters like &rsquo;

                $text = bab_convertStringFromDatabase($this->description, 'UTF-8');
                $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
                $text = iconv('UTF-8', bab_charset::getIso().'//TRANSLIT', $text);
            } else {
                $text = bab_unhtmlentities($this->description);
            }

            return bab_abbr(
                strip_tags($text),
                BAB_ABBR_FULL_WORDS,
                $this->getParentSet()->shortdesc->getCustomPropertyValue('maxlength')
            );
        }

        return $this->shortdesc;
    }



    /**
     * The description can be an HTML string
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Description as plain text
     * @return string
     */
    public function getTextDescription()
    {
        return bab_unhtmlentities(strip_tags($this->description));
    }

    /**
     * Get Main supplier
     * @return crm_Organization
     */
    public function getMainSupplier()
    {
        return null;
    }

    /**
     * Get the name of the reference property
     * add reference support to articles with a test duplicate on save
     * return null if no reference column on table
     * @return string | null
     */
    public function getReferenceProperty()
    {
        return null;
    }

    /**
     * Create a new random reference
     *
     * @param	string	$prefix
     *
     * @return string
     */
    public function newReference($prefix = '')
    {
        $seed = abs(crc32(uniqid()));
        $seed = round($seed/1000);
        $seedStr = $prefix.sprintf('%07d', $seed);

        $set = $this->getParentSet();

        while ($set->get($set->reference->is($seedStr))) {
            $seed++;
            $seedStr = $prefix.sprintf('%07d', $seed);
        }

        return $seedStr;
    }



    /**
     * Get the list of technical properties fields of product
     * this array can be used to compare two products
     * @return array
     */
    public function getTechnicalProperties()
    {
        $return = array();
        $set = $this->getParentSet();
        $Crm = $this->Crm();

        if ($this->rating_count) {
            $return['rating_average'] = array(
                'name' 			=> $Crm->translate('Rating'),
                'description' 	=> $Crm->translate('Users rating average'),
                'value'			=> $this->rating_average.'/5'
            );
        }

        foreach ($set->getCustomFields() as $customfield) {
            /*@var $customfield crm_CustomField */
            $fieldname = $customfield->fieldname;
            $return[$fieldname] = array(
                'name' 			=> $customfield->name,
                'description' 	=> $customfield->description
            );
        }

        return $return;
    }




    /**
     * Get CatalogItem iterator for article
     * @return crm_CatalogItem[]
     */
    public function getCatalogs()
    {
        $this->Crm()->includeCatalogSet();
        $set = $this->Crm()->CatalogItemSet();
        $set->join('catalog');
        $set->join('article');

        return $set->select($set->article->is($this->id));
    }

    /**
     * Get accessible catalogItems
     * @return ORM_Iterator <crm_CatalogItem>
     */
    public function getAccessibleCatalogs($articlejoin = false)
    {
        $this->Crm()->includeCatalogSet();
        $set = $this->Crm()->CatalogItemSet();
        $set->join('catalog');
        $set->join('article');

        if ($articlejoin) {
            $set->article->joinHasOneRelations();
        }

        $criteria = $set->article->is($this->id)
            ->_AND_($set->article->disabled->is(0))
            ->_AND_($set->catalog->getAccessibleCatalogs());

        return $set->select($criteria);
    }

    /**
     * Get the first catalog item or null if article is not in a catalog
     * @return crm_CatalogItem
     */
    public function getCatalogItem()
    {
        $iterator = $this->getCatalogs();
        foreach ($iterator as $catalogItem) {
            return $catalogItem;
        }

        return null;
    }

    /**
     * Get the first accessible catalogItem
     * @return crm_CatalogItem
     */
    public function getAccessibleCatalogItem($articlejoin = false)
    {
        $iterator = $this->getAccessibleCatalogs($articlejoin);
        bab_debug($iterator->getSelectQuery());
        foreach ($iterator as $catalogItem) {
            return $catalogItem;
        }

        return null;
    }


    /**
     * privates sales linked to article
     * @return ORM_Iterator
     */
    public function getPrivateSales()
    {
        $Crm = $this->Crm();
        $set = $Crm->ArticlePrivateSellSet();
        $set->privatesell();

        $res = $set->select($set->article->is($this->id));
        $res->orderAsc($set->privatesell->startedOn);

        return $res;
    }


    /**
     * Get private sell tu use as container if product is disabled
     * @return crm_PrivateSell
     */
    public function getPrivateSell()
    {
        $res = $this->getPrivateSales();
        if (0 === $res->count()) {
            return null;
        }

        foreach ($res as $articlePrivateSell) {
            return $articlePrivateSell->privatesell;
        }
    }


    /**
     * Get the first accessible private sell
     * @return crm_PrivateSell
     */
    public function getAccessiblePrivateSell()
    {
        $Crm = $this->Crm();

        if (!isset($Crm->ArticlePrivateSellSet)) {
            return null;
        }


        $res = $this->getPrivateSales();
        if (0 === $res->count()) {
            return null;
        }



        foreach ($res as $articlePrivateSell) {
            if ($articlePrivateSell->privatesell->isAccessValid()) {
                return $articlePrivateSell->privatesell;
            }
        }


        return null;
    }



    /**
     * Get similar articles iterator
     *
     * @return ORM_Iterator
     */
    public function similarArticles()
    {
        if (!isset($this->name)) {
            return null;
        }

        if (5 > mb_strlen($this->name)) {
            return null;
        }

        $set = $this->getParentSet();
        return $set->select($set->name->contains($this->name)->_AND_($set->id->is($this->id)->_NOT()));
    }

    /**
     * Get a string usable for a filename
     * @return string
     */
    public function getFilename()
    {
        return trim(
            bab_abbr(
                preg_replace('/[^\w\s]/', '', bab_removeDiacritics($this->name)),
                BAB_ABBR_FULL_WORDS,
                60
            ),
            ' .'
        );
    }



    /**
     * get iterator with attachements
     * @param	array | bab_Path	$subfolders		relative directory of subfolder
     *                                              array of non encoded string or encoded bab_Path
     * @return Widget_FilePickerIterator	<Widget_FilePickerItem>
     */
    public function attachments($subfolder = null)
    {
        $up = $this->uploadPath();
        if (!isset($up)) {
            return null;
        }

        if (isset($subfolder) && $subfolder instanceof bab_Path) {
            $up->push($subfolder);
        }

        $filePicker = bab_Widgets()->FilePicker();
        $filePicker->setFolder($up);

        if (isset($subfolder) && is_array($subfolder)) {
            foreach ($subfolder as $s) {
                $filePicker->push($s);
            }
        }

        return $filePicker->getFolderFiles($filePicker->getFolder());
    }

    /**
     * Import a file in article attachements
     * @param bab_path           $sourceFile
     * @param string|bab_Path    [$relativePath]    Relative path in the article upload folder
     *                                              missing folders will be created
     * @param string             [$filename]        If not set the filename of sourcefile will be used
     * @param string		     [$source_encoding] If not set, the sourceFile should use the database charset for the filename encoding
     *
     */
    public function importFile(bab_Path $sourceFile, $relativePath = null, $filename = null, $source_encoding = null)
    {
        $up = clone $this->uploadPath();

        if (isset($relativePath)) {
            if (!($relativePath instanceof bab_Path)) {
                $relativePath = new bab_Path($relativePath);
            }

            $up->push($relativePath);
        }

        if (!isset($filename)) {
            $filename = $sourceFile->getBasename();
        }

        if (!isset($source_encoding)) {
            $source_encoding = bab_charset::getIso();
        }


        $W = bab_Widgets();
        $filePicker = $W->FilePicker();
        $filePicker->setFolder($up);

        return $filePicker->importFile($sourceFile, $source_encoding);
    }


    /**
     * Import a folder of files to attachments
     *
     * @param	bab_Path	$path
     * @param	string		$source_encoding
     *
     * @return 	int			Number of files imported
     */
    public function importFiles(bab_Path $path, $source_encoding)
    {
        $up = $this->uploadPath();
        if (!isset($up)) {
            return null;
        }

        $n = 0;

        $filePicker = bab_Widgets()->FilePicker();
        $filePicker->setFolder($up);

        $path = $path->toString();
        $d = dir($path);

        while (false !== ($entry = $d->read())) {
            if ('.' !== $entry && '..' !== $entry && is_file($path.'/'.$entry)) {
                $file = new bab_Path($path.'/'.$entry);
                if ($filePicker->importFile($file, $source_encoding)) {
                    $n++;
                }
            }
        }
        $d->close();

        return $n;
    }



    /**
     * Get the main photo upload path
     * @return bab_Path
     */
    public function getPhotoUploadPath()
    {
        $up = $this->uploadPath();
        if (!isset($up)) {
            return null;
        }

        $up->push('photo');

        return $up;
    }


    /**
     * Get the the others photos upload path
     * @return bab_Path
     */
    public function getOthersPhotosUploadPath()
    {
        $up = $this->uploadPath();
        if (!isset($up)) {
            return null;
        }

        $up->push('othersphotos');

        return $up;
    }

    /**
     * @return Widget_FilePickerItem
     */
    public function getPhotoFile()
    {
        $uploadpath = $this->getPhotoUploadPath();

        if (!isset($uploadpath)) {
            return null;
        }

        $W = bab_Widgets();

        $uploaded = $W->ImagePicker()->getFolderFiles($uploadpath);

        if (!isset($uploaded)) {
            return null;
        }

        foreach ($uploaded as $file) {
            return $file;
        }

        return null;
    }


    /**
     * Return the full path of the main photo file
     * this method return null if there is no photo to display
     *
     * @return bab_Path | null
     */
    public function getPhotoPath()
    {
        if ($file = $this->getPhotoFile()) {
            return $file->getFilePath();
        }

        return null;
    }



    /**
     * Main photo URL or null if no image
     * @return string
     */
    public function getPhotoUrl()
    {
        $photopath = $this->getPhotoPath();

        if (null !== $photopath && $T = @bab_functionality::get('Thumbnailer')) {
            /*@var $T Func_Thumbnailer */
            $T->setSourceFile($photopath);
            return $GLOBALS['babUrl'].$T->getThumbnail(400, 400);
        }

        return null;
    }




    /**
     * @return Widget_FilePickerIterator
     */
    public function getOthersPhotosIterator()
    {
        $uploadpath = $this->getOthersPhotosUploadPath();

        if (!isset($uploadpath)) {
            return null;
        }

        $W = bab_Widgets();

        $uploaded = $W->ImagePicker()->getFolderFiles($uploadpath);

        if (!isset($uploaded)) {
            return null;
        }

        return $uploaded;
    }


    /**
     * Move photo to photo upload path
     * this method is used to import a photo from a temporary directory of the filePicker widget or another file
     * warning, the default behavior remove the source file and existing article photo file
     *
     * @param	bab_Path 	$sourcefile
     * @param	bool		$temporary			default is true, if the sourcefile is a filePicker temporary file, if the sourcefile is an uploaded file
     * @param	string		$filename			Change the target filename, default is from the sourcefile
     * @return 	bool
     */
    public function importPhoto(bab_Path $sourcefile, $temporary = true, $filename = null)
    {
        $uploadpath = $this->getPhotoUploadPath();

        if (!isset($uploadpath)) {
            return false;
        }

        $uploadpath->createDir();


        if (null !== $photo = $this->getPhotoPath()) {
            unlink($photo->toString());
        }

        if (!$temporary) {
            $W = bab_Widgets();
            return $W->imagePicker()->setFolder($uploadpath)->importFile($sourcefile, bab_charset::getIso(), $filename);
        }


        $original = $sourcefile->toString();
        if (null === $filename) {
            $uploadpath->push(basename($original));
        } else {

            $W = bab_Widgets();
            $imagePicker = $W->imagePicker()->setFolder($uploadpath);
            $imagePicker->push($filename);
            $uploadpath = $imagePicker->getFolder();
        }

        if (is_uploaded_file($original)) {
            return move_uploaded_file($original, $uploadpath->toString());
        }

        return rename($original, $uploadpath->toString());
    }

    /**
     * Move temporary files to others photos upload path
     * @return bool
     */
    public function importOthersPhotos(Widget_FilePickerIterator $files)
    {
        $uploadpath = $this->getOthersPhotosUploadPath();

        if (!isset($uploadpath)) {
            return false;
        }

        $uploadpath->createDir();

        foreach ($files as $filePickerItem) {
            /*@var $filePickerItem Widget_FilePickerItem */

            $target = clone $uploadpath;
            $target->push($filePickerItem->getFileName());

            if (!rename($filePickerItem->getFilePath()->toString(), $target->toString())) {
                return false;
            }
        }

        return true;
    }




    /**
     * Select associated comments.
     * all private comments + public comments
     *
     * @return ORM_Iterator
     */
    public function selectAllComments()
    {
        $commentSet = $this->Crm()->CommentSet();
        return $commentSet->select($commentSet->article->is($this->id));
    }

    /**
     * Select associated public comments.
     * source of comments used for average rating of article
     *
     * @return ORM_Iterator
     */
    public function selectPublicComments()
    {
        $commentSet = $this->Crm()->CommentSet();
        return $commentSet->select(
            $commentSet->article->is($this->id)
            ->_AND_($commentSet->private->is(0))
            ->_AND_($commentSet->confirmed->is(1))
        );
    }



    /**
     * Select associated visibles comments.
     * my private comments + public comments
     *
     * @return ORM_Iterator
     */
    public function selectComments()
    {
        $Access = $this->Crm()->Access();
        $commentSet = $this->Crm()->CommentSet();

        $criteria = $commentSet->article->is($this->id);

        if (bab_isUserLogged()) {
            $criteria = $criteria->_AND_(
                $commentSet->private->is(0)->_OR_($commentSet->createdBy->is($Access->currentUser()))
            )->_AND_(
                $commentSet->confirmed->is(1)->_OR_($commentSet->createdBy->is($Access->currentUser()))
            );
        } else {
            $criteria = $criteria->_AND_($commentSet->private->is(0)->_AND_($commentSet->confirmed->is(1)));
        }

        return $commentSet->select($criteria);
    }


    /**
     *
     * @return crm_ArticlePackaging[]
     */
    public function selectPackagings()
    {
        $Crm = $this->Crm();

        $set = $Crm->ArticlePackagingSet();
        $set->packaging();
        $set->article();

        $res = $set->select($set->article->id->is($this->id));


        return $res;
    }


    /**
     * Get the main particle packaging or null if no packaging defined
     * @return crm_ArticlePackaging
     */
    public function getMainArticlePackaging()
    {
        $Crm = $this->Crm();

        $set = $Crm->ArticlePackagingSet();
        $set->packaging();
        $set->article();

        $articlePackaging = $set->get($set->article->is($this->id)->_AND_($set->main->is(1)));

        if (isset($articlePackaging)) {
            return $articlePackaging;
        }

        $articlePackaging = $set->get($set->article->is($this->id));

        if (isset($articlePackaging)) {
            // set first article packaging as main
            $articlePackaging->main = 1;
            $articlePackaging->save();
            return $articlePackaging;
        }

        return null;
    }


    /**
     * Format a weight
     * @param unknown_type $decimal
     * @return string
     */
    public static function weightFormat($decimal)
    {
        if (!preg_match('/[^\.0]/', $decimal)) {
            return '';
        }

        $weight = (float) $decimal;

        if ($weight < 0.09) {
            // get weight in grammes

            $g = round($weight * 1000);

            return $g.' g';

        } else {
            // get weight in Kg

            return round($weight, 2). ' Kg';
        }
    }




    /**
     * get weight with unit for display
     * @return string
     */
    public function getWeight()
    {

        $articlePackaging = $this->getMainArticlePackaging();
        if (!isset($articlePackaging)) {
            return null;
        }

        return crm_Article::weightFormat($articlePackaging->weight);
    }




    /**
     *  copy records with an article field
     *  @param crm_RecordSet $set
     */
    protected function copyLinkedSet(crm_RecordSet $set, crm_Article $target)
    {
        $res = $set->select($set->article->is($this->id));

        foreach($res as $record) {
            $record->id = null;
            $record->article = $target->id;
            $record->save();
        }
    }



    /**
     * Copy references to the article to a new one for a functionnal copy
     * @param crm_Article $target The new article without linked objects
     *
     */
    public function copyLinkedObjects(crm_Article $target)
    {
        $Crm = $this->Crm();


        $this->copyLinkedSet($Crm->ArticlePackagingSet(), $target);
        $this->copyLinkedSet($Crm->CatalogItemSet(), $target);



        // copy upload folder

        $src = $this->uploadPath();
        if ($src->count() > 0) {
            require_once $GLOBALS['babInstallPath'].'utilit/upgradeincl.php';
            $err = bab_recursive_cp($src->tostring(), $target->uploadPath()->tostring());

            if ($err !== true) {
                throw new crm_SaveException($err);
            }
        }
    }




    /**
     * Update properties with CSV row
     * @param	Widget_CsvRow	$row	A CSV row
     * @return 	int						number of updated properties
     */
    public function import(Widget_CsvRow $row)
    {
        $up_prop = 0;



        if (isset($row->supplier)) {
            if (is_numeric($row->supplier)) {

                $up_prop += $this->importProperty('supplier', $row->supplier);

            } else {

                $OSet = $this->Crm()->OrganizationSet();
                $organization = $OSet->get($OSet->name->like($row->supplier));

                if (isset($organization)) {
                    $up_prop += $this->importProperty('supplier', $organization->id);
                }
            }
        }

        if (isset($row->reference)) {
            $up_prop += $this->importProperty('reference', $row->reference);
        }

        if (isset($row->name)) {
            $up_prop += $this->importProperty('name', $row->name);
        }

        if (isset($row->subtitle)) {
            $up_prop += $this->importProperty('subtitle', $row->subtitle);
        }

        if (isset($row->shortdesc)) {
            $up_prop += $this->importProperty('shortdesc', $row->shortdesc);
        }

        if (isset($row->description)) {
            $up_prop += $this->importProperty('description', $row->description);
        }

        if (isset($row->_description_text) && empty($row->description)) {
            $up_prop += $this->importProperty('description', bab_toHtml($row->_description_text));
        }

        if (isset($row->properties)) {
            $up_prop += $this->importProperty('properties', $row->properties);
        }

        if (isset($row->weight)) {
            $up_prop += $this->importProperty('weight', $row->weight);
        }

        if (isset($row->disabled)) {
            $up_prop += $this->importProperty('disabled', $row->disabled);
        }

        if (isset($row->vat)) {
            static $VatSet = null;
            if (null === $VatSet) {
                $VatSet = $this->Crm()->VatSet();
            }

            $vat = $VatSet->get($VatSet->value->like($VatSet->value->input($row->vat)));

            if (isset($vat)) {
                $up_prop += $this->importProperty('vat', $vat->id);
            }

        }


        // create the main articlePackaging from :
        /*
        Prix u. HT :
        Taux de TVA :
        Prix u. TTC non remise : _unit_cost_undiscounted
        Remise :
        Fin remise :
        Poids :
        */



        if ($this->mainpackaging instanceof crm_ArticlePackaging && $this->mainpackaging->id > 0) {
            $articlePackaging = $this->mainpackaging;
        } else {
            $articlePackaging = $this->getParentSet()->mainpackaging->newRecord();
            $this->mainpackaging = $articlePackaging;
        }


        if (!isset($row->mainpackaging)) {
            // create empty mainpackaging with empty values
            $row->mainpackaging = new Widget_CsvRow($row->line(), array());
            $row->mainpackaging->main = 1;
            if ($articlePackaging->packaging instanceof crm_Packaging) {
                $row->mainpackaging->packaging = $articlePackaging->packaging->id;
            } else {
                $row->mainpackaging->packaging = $articlePackaging->packaging;
            }
            $row->mainpackaging->weight = $articlePackaging->weight;
            $row->mainpackaging->unit_cost = $articlePackaging->unit_cost;
            $row->mainpackaging->reduction = $articlePackaging->reduction;
            $row->mainpackaging->reduction_end = $articlePackaging->reduction_end;
        }








        /*@var $articlePackaging crm_ArticlePackaging */

        if (isset($row->_unit_cost_undiscounted) && empty($row->mainpackaging->unit_cost) && isset($vat)) {
            // calcul du HT a partir du TTC non remise

            $vat = (float) $vat->value;
            $coeff = round(100/($vat+100), 3);
            $tax_incl = (float) str_replace(',', '.', $row->_unit_cost_undiscounted);

            $row->mainpackaging->unit_cost = round($coeff * $tax_incl, 3);
        }



        $up_prop += $articlePackaging->import($row->mainpackaging);



        return $up_prop;
    }


    /**
     * Add article to catalogs from an array of category names
     * @param array $categories
     *
     * array(
     * 		0 => array(
     * 			0 => 'categoryname_level1',
     * 			1 => 'categoryname_level2'
     * 		),
     * 		1 => array(
     * 			0 => 'categoryname_level1'
     * 		)
     * )
     */
    public function importCategoriesArray(Array $categories)
    {
        $Crm = $this->Crm();

        $current = array();
        foreach ($this->getCatalogs() as $catalogItem) {
            /*@var $catalogItem crm_CatalogItem */
            $current[$catalogItem->catalog->asText()] = $catalogItem;
        }

        // remove existing categories

        foreach ($categories as $key => $arr) {
            $astext = implode(' > ', $arr);
            if (isset($current[$astext])) {
                unset($categories[$key]);
                unset($current[$astext]);
            }
        }

        // add new categories



        $set = $Crm->CatalogSet();
        $ciSet = $Crm->CatalogItemSet();

        foreach ($categories as $arr) {
            $catalog = null;
            $parentcatalog = 0;
            foreach ($arr as $ancestor) {
                $catalog = $set->get($set->name->like($ancestor)->_AND_($set->parentcatalog->is($parentcatalog)));
                if (!isset($catalog)) {
                    //UNCOMMENT if you want to creat categories
                    // 	$catalog = $set->newRecord();
                    // 	$catalog->name = $ancestor;
                    // 	$catalog->parentcatalog = $parentcatalog;
                    // 	$catalog->save();
                    break; // not found, category ignored
                }

                $parentcatalog = $catalog->id;
            }

            if (!isset($catalog)) {
                continue;
            }

            // create the catalogItem

            $catalogItem = $ciSet->newRecord();
            $catalogItem->article = $this->id;
            $catalogItem->catalog = $catalog->id;
            $catalogItem->save();
        }

        // remove the categories not in import

        foreach ($current as $catalogItem) {
            $catalogItem->delete();
        }

    }




    /**
     * Get the product as an array to generate a RSS 2.0 item
     * @see http://support.google.com/merchants/bin/answer.py?hl=fr&answer=160589
     * @see http://support.google.com/merchants/bin/answer.py?hl=fr&answer=2886435#FR
     *
     * @return array
     */
    public function getRss()
    {
        $firstcatalogitem = null;
        $res = $this->getAccessibleCatalogs();
        $categories = array();

        foreach ($res as $catalogitem) {
            /*@var $catalogitem crm_CatalogItem */

            if (null === $firstcatalogitem) {
                $firstcatalogitem = $catalogitem;
            }
            $catalogs = $catalogitem->catalog->getAncestors();
            $arr = array();
            foreach ($catalogs as $catalog) {
                /*@var $catalog crm_Catalog */
                $arr[] = $catalog->name;
            }
            $categories[] = $arr;
        }


        if (null === $firstcatalogitem) {
            return null;
        }


        $item = array(
            'title' 			=> $this->getName(),
            'description' 		=> $this->getShortDesc(),
            'link'				=> $firstcatalogitem->getRewritenUrl(),
            'g:price'			=> number_format($this->getMainArticlePackaging()->getUnitCostTI(), 2, '.', ''). ' EUR',
            'g:id'				=> $this->getReference(),
            'g:condition'		=> 'neuf'
        );

        foreach ($categories as $path) {
            $item['g:product_type'] = implode(' > ', $path);
        }


        $T = @bab_functionality::get('Thumbnailer');
        /*@var $T Func_Thumbnailer */

        if (false !== $T) {
            if ($photourl = $this->getPhotoUrl()) {
                $item['g:image_link'] = $photourl;
            }


            $others = $this->getOthersPhotosIterator();

            if (($others instanceof Widget_FilePickerIterator)) {
                foreach ($others as $filePickerItem) {
                    /*@var $filePickerItem Widget_FilePickerItem */

                    $T->setSourceFile($filePickerItem->getFilePath());
                    $item['g:additional_image_link'] = $GLOBALS['babUrl'].$T->getThumbnail(400, 400);
                }
            }
        }

        return $item;
    }











    /**
     * @throw crm_DuplicateReferenceException
     * @see programs/crm_TraceableRecord#save($noTrace)
     */
    public function save($noTrace = false)
    {
        if (null !== $this->reference && '' === (string) $this->reference) {
            throw new crm_EmptyReferenceException($this->Crm()->translate('error, empty reference'), $this);
            return false;
        }


        $set = $this->getParentSet();
        $record = $set->get($set->reference->is($this->reference)->_AND_($set->id->is($this->id)->_NOT()));



        if (isset($record)) {
            throw new crm_DuplicateReferenceException(
                sprintf($this->Crm()->translate('duplicate reference %s'), $this->reference),
                $record
            );

            return false;
        }


        return parent::save($noTrace);
    }
}




class crm_ArticleReferenceException extends crm_SaveException
{
    /**
     * Article found duplicate
     * @var crm_Article
     */
    public $article;

    public function __construct($message, crm_Article $article)
    {
        parent::__construct($message);
        $this->article = $article;
    }
}


class crm_DuplicateReferenceException extends crm_ArticleReferenceException
{
}

class crm_EmptyReferenceException extends crm_ArticleReferenceException
{
}
